import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AccessDeniedComponent } from './access.denied.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { TranslateModule } from "@ngx-translate/core";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": AccessDeniedComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        TranslateModule.forChild()
    ], exports: [
        RouterModule
    ], declarations: [
        AccessDeniedComponent
    ]
})
export class AccessDeniedModule {



}