import {
    Component,
    OnInit,
    Inject,
    ViewEncapsulation,
    AfterViewInit,
    ClassProvider,
    Input,
    ChangeDetectorRef
} from '@angular/core';
import { Restangular } from "ngx-restangular";

import { LazyLoadEvent, MenuItem } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';

import { ActivatedRoute } from "@angular/router";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { Pageable } from "../../../../../../model/pageable";

import { TranslateService } from "@ngx-translate/core";
import { RoleService } from "../role.service";
import { RolePermission } from "../../../../../../model/account/role/role-permission.model";
import { AuthorizationType } from "../../../../../../model/account/role/authorization-type.model";
import { Screen } from "../../../../../../model/account/role/screen.model";
import { StoreType } from "../../../../../../model/account/role/store-type.model";
import { WorkingArea } from "../../../../../../model/account/role/working-area.model";
import { Rights } from "../../../../../../model/rights";
import { BrandModel } from "../../../../../../model/brand/brand.model";
import { RoleComboRequest } from "../../../../../../model/account/role-combo-request.model";

@Component({
    selector: "role-permission",
    templateUrl: "./role-permission.component.html",
    providers: [
        MessageService
    ]

})

export class RolePermissionComponent implements OnInit, AfterViewInit {
    public rights = Rights;
    items: MenuItem[];
    @Input() roleId: number;
    cols: any[];
    colsRole: any[];
    roles: any[];
    public brands: BrandModel[] = [];
    public selectedItems: RolePermission[] = [];
    public pageable: Pageable = new Pageable();
    public totalRecords: number = 0;
    public list: RolePermission[] = [];
    //public fullRoleList: Role[] = [];
    public fullRoleTotalRecords: number = 0;
    public fullRolePageable: Pageable = new Pageable();
    //public fullRoleSelectedItems: Role[] = [];
    message: string;
    title: string;
    public rolePermission = new RolePermission();
    public rolePermissionNew = new RolePermission();
    public roleComboRequest: RoleComboRequest = new RoleComboRequest();
    authorizationTypes: AuthorizationType[] = [];
    screens: Screen[] = [];
    storeTypes: StoreType[] = [];
    workingAreas: WorkingArea[] = [];
    getId() {
        return this.roleId;
    }

    constructor(
        private roleService: RoleService,
        private cdRef: ChangeDetectorRef,
        public translateService: TranslateService,
        private _script: ScriptLoaderService,
        private messageService: MessageService,
        private activatedRoute: ActivatedRoute) {

    }
    getRoleCombo(roleId: number) {
        this.roleService.getRoleCombo(roleId).subscribe((response) => {
            console.log('getRoleCombo', response);
            this.roleComboRequest = response.data;
        });
    }
    /*
    getBrands() {
        this.roleService.getBrands().subscribe(data => {
                console.log('getBrands', data);
                this.brands = data.data;
                this.cdRef.detectChanges();
            },
            err => console.error(err)
        );
    }
    getAuthorizationTypesList() {
        this.roleService.getAuthorizationTypesList().subscribe(result => {
            if (result != null) {
                this.authorizationTypes = result.data;
                this.cdRef.detectChanges();
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi' });
        })
    }
    getScreensList() {
        this.roleService.getScreensList().subscribe(result => {
            if (result != null) {
                this.screens = result.data;
                this.cdRef.detectChanges();
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi' });
        })
    }
    getStoreTypesList() {
        this.roleService.getStoreTypesList().subscribe(result => {
            if (result != null) {
                this.storeTypes = result.data;
                this.cdRef.detectChanges();
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi' });
        })
    }
    getWorkingAreasList() {
        this.roleService.getWorkingAreasList().subscribe(result => {
            if (result != null) {
                this.workingAreas = result.data;
                this.cdRef.detectChanges();
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi' });
        })
    }


    findAllRolePermission() {
        this.roleService.getRolePermissionsList(this.getId()).subscribe(
            data => {
                this.list = data.data;
                this.cdRef.detectChanges();
            },
            err => console.error(err)
        );
    }

    */

    ngOnInit() {
        /* this.cols = [
             { field: 'workingArea.name', header: 'Çalışma Alanı' },
             { field: 'screen.name', header: 'Ekran Adı' },
             { field: 'storeType.name', header: 'Mağaza Türü' },
             { field: 'authorizationType.name', header: 'Yetki Türü' },
         ];
         */
        // this.findAllRolePermission();
        // this.getBrands();
        this.getRoleCombo(this.roleId);

    }
    ngAfterViewInit() {
        this.activatedRoute.params.subscribe((params: any) => {
            // alert(JSON.stringify(params));
        });
    }
    roleComboSave() {
        this.roleComboRequest.role = this.getId();

        this.roleService.roleComboSave(this.roleComboRequest).subscribe((parameters) => {
            this.getRoleCombo(this.roleId);
            this.cdRef.detectChanges();
            this.messageService.add({ severity: 'success', summary: 'İşlem başarılı', detail: "Kaydedildi!" });
        }, error => {
            console.log('error', error);
            this.messageService.add({ severity: 'error', summary: 'Hata oluştu', detail: error });
        });
    }
    /*
    delete(item) {
        this.roleService.rolePermissionDelete(item.id).subscribe(() => {
            this.translateService.get('message.delete.title').subscribe(msg => {
                this.title = msg;
            });

            this.translateService.get('message.delete.message').subscribe(msg => {
                this.message = msg;
            });
            this.findAllRolePermission();
            this.cdRef.detectChanges();
            this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });

        }, (response) => {
            this.translateService.get('message.error.title').subscribe(msg => {
                this.title = msg;
            });

            this.messageService.add({ severity: 'error', summary: this.title, detail: response.data.error_description });
        });

    }

    remove(item) {
        this.selectedItems = [];
        this.selectedItems.push(item);
    }

    removes() {

        if (this.selectedItems.length == 0) {
            return;
        }

        this.selectedItems.forEach(item => {
            this.delete(item);
        });
    }


    add() {
        // this.getWorkingAreasList();
        // this.getScreensList();
        // this.getStoreTypesList();
        // this.getAuthorizationTypesList();
        //this.fullRoleList = [];
        //this.findAllRole(null);
    }


    rolePermissionAdd() {
        this.roleComboRequest.role = this.getId();
        this.roleComboRequest.brands = this.brands.filter(x => x.checked);
        this.roleComboRequest.screens = this.screens.filter(x => x.checked);
        this.roleComboRequest.authorizationTypes = this.authorizationTypes.filter(x => x.checked);


        this.roleService.roleComboSave(this.roleComboRequest).subscribe((parameters) => {
            this.findAllRolePermission();
            this.cdRef.detectChanges();
            this.messageService.add({ severity: 'success', summary: 'İşlem başarılı', detail: "Kayıt eklendi!" });
        }, error => {
            console.log('error', error);
            this.messageService.add({ severity: 'error', summary: 'Hata oluştu', detail: error });
        });
    }
*/
}
