import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from './../default.component';
import { FormsModule } from '@angular/forms';
import { DataTableModule, DropdownModule, DialogModule, AutoCompleteModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RealizedMonthlyComponent } from './realized-monthly.component';
import { HttpClientModule } from '@angular/common/http';
import { RealizedMonthlyService } from './realized-monthly.service';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': RealizedMonthlyComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DataTableModule,
        DropdownModule,
        DialogModule,
        AutoCompleteModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild()
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        RealizedMonthlyComponent
    ],
    providers: [RealizedMonthlyService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class RealizedMonthlyModule {


}