import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { PremiumMonthlyService } from "../premium-monthly.service";
import { MessageService } from 'primeng/components/common/messageservice';

import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute } from "@angular/router";
import { Calculation } from "../../../../../model/premium-monthly/calculation.model";
import { Rights } from "../../../../../model/rights";
import { Message } from "primeng/api";
import { environment } from "../../../../../../environments/environment";
import { SelectInput } from "../../settings/data-input/calendar";
import { Calclog } from "../../../../../model/premium-monthly/calclog.model";

@Component({
    selector: 'app-calc-log-list',
    templateUrl: 'calc-log-list.component.html',
    styles: []
})
export class CalcLogListComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public sicil: number;
    public calcid: number;
    public calculation: Calculation[] = [];
    public list: Calclog[] = [];
    selectList: SelectInput[] = [
        { label: 'Seçiniz', value: '0', checked: false }
    ];
    constructor(private premiumMonthlyService: PremiumMonthlyService,
        private messageService: MessageService,
        private activatedRoute: ActivatedRoute,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {
        this.activatedRoute.params.subscribe((params) => {
            //alert(params.sicil);
            this.sicil = params.sicil;
        });

    }

    ngOnInit() {
        debugger;
        this.premiumMonthlyService.calcloglist(this.sicil).subscribe((response) => {
            console.log('calcloglist', response);
            this.calculation = response.data;
            for (const item of response.data) {
                this.selectList.push({
                    label: '[' + item.id + '] ' + (item.time ? item.time : '') + ' Filtre: ' + item.filter,
                    value: item.id,
                    checked: false
                })
            }
        }, (error) => {
            console.log('calcloglist -ERROR', error);
            this.messageService.add({ severity: 'error', summary: 'Uyarı', detail: error.message });
        }, () => {
            this.cdRef.detectChanges();
        });
    }

    getCalclog(id) {
        this.premiumMonthlyService.getCalclog(this.sicil, this.calcid).subscribe((response) => {
            console.log('getCalclog', response);
            this.list = response.data;
        }, (error) => {
            console.log('getCalclog -ERROR', error);
            this.messageService.add({ severity: 'error', summary: 'Uyarı', detail: error.message });
        }, () => {
            this.cdRef.detectChanges();
        });
    }
}
