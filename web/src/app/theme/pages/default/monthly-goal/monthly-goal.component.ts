import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { MonthlyGoalService } from './monthly-goal.service';
import { environment } from '../../../../../environments/environment';

import { SelectInput, Months, Years } from '../settings/data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { PremiumMontly } from "../../../../model/premium-monthly/premium.montly.model";
import { Pageable } from "../../../../model/pageable";
import { Role } from "../../../../model/account/role/role.model";
import { MonthlyGoal } from "../../../../model/monthlygoal/monthly-goal.model";
import { StoreTarget } from "../../../../model/brand/store-target.model";
import { PremiumRateService } from "../settings/premium-rate/premium-rate.service";
import { NewIncentiveProgramme } from "../../../../model/monthlygoal/new-incentive-programme.model";
import { T } from "@angular/core/src/render3";
import { Rights } from "../../../../model/rights";

@Component({
    selector: 'monthly-goal',
    templateUrl: './monthly-goal.component.html',

})
export class MonthlyGoalComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    public totalSaleSplit = 0;
    public storeTargetValue = 0;
    public newIncentiveProgramme = new Array<NewIncentiveProgramme>();
    public storeTarget = StoreTarget;
    spinnerConfig: any = environment.spinner;
    public list: MonthlyGoal[];
    public saveControl = true;
    public draftRevizyon = 0;
    public monthlyGoal = new MonthlyGoal();
    years: SelectInput[];
    months: SelectInput[];
    brands: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];
    stores: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    employees: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];

    constructor(private monthlyGoalService: MonthlyGoalService,
        private premiumRateService: PremiumRateService,
        private messageService: MessageService,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {
        this.years = Years;
        this.months = Months
    }

    ngOnInit() {

        this.brands.push({
            label: 'Victorias Secret (VS)',
            value: '39',
            checked: false
        });

        this.cdRef.detectChanges();
    }
    getStoreTarget() {
        this.monthlyGoalService.getStoreTarget(this.monthlyGoal.year, this.monthlyGoal.month, this.monthlyGoal.store.id)
            .subscribe(result => {
                this.storeTarget = result.data;
                if (result.data != null && result.data.id != null) {
                    this.storeTargetValue = result.data.val;
                } else if (result.data != null && result.data.length > 0) {
                    this.storeTargetValue = result.data[0].val;
                }
                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağaza hedef değeri alınamadı!' + error.message });
            });
    }
    setStores(brandId) {
        this.stores = [{
            label: 'Seçiniz', value: '0', checked: false
        }];

        if (brandId) {
            this.spinner.show();
            this.monthlyGoalService.getStores(brandId).subscribe(result => {

                if (result.total.all) {
                    for (const store of result.data) {
                        this.stores.push({
                            label: store.name,
                            value: store.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağazalar listesi alınamadı!' });
            });
        }
    }
    hedefGuncelle() {
        if (this.monthlyGoal.store.id < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Mağaza seçiniz!' });
            return;
        }
        if (this.monthlyGoal.year < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Yıl seçiniz!' });
            return;
        }
        if (this.monthlyGoal.month < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Periyot seçiniz!' });
            return;
        }
        this.monthlyGoalService.hedefGuncelle(this.monthlyGoal.year, this.monthlyGoal.month, this.monthlyGoal.store.id).subscribe(response => {
            console.log('Hedef güncelle', response);

            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: response.message });

        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: error.message });

        });
    }

    submit() {
        if (this.monthlyGoal.store.id < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Mağaza seçiniz!' });
            return;
        }
        if (this.monthlyGoal.year < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Yıl seçiniz!' });
            return;
        }
        if (this.monthlyGoal.month < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Periyot seçiniz!' });
            return;
        }
        this.spinner.show();
        this.getStoreTarget();
        this.newIncentiveProgramme = new Array<NewIncentiveProgramme>();

        this.monthlyGoalService.getStorePosition(this.monthlyGoal.year, this.monthlyGoal.month, this.monthlyGoal.store.id).subscribe(result => {
            if (result != null) {
                this.list = result.data;

                this.totalSaleSplit = 0;
                this.saveControl = false;
                var i = 0;
                for (let item of this.list) {
                    this.newIncentiveProgramme.push(new NewIncentiveProgramme());
                    this.newIncentiveProgramme[i].store = item.store;
                    this.newIncentiveProgramme[i].position = item.position;
                    this.newIncentiveProgramme[i].positionName = item.position.name;
                    this.newIncentiveProgramme[i].positionId = item.position.id;

                    this.newIncentiveProgramme[i].numStaff = item.numStaff;
                    this.newIncentiveProgramme[i].saleSplit = item.saleSplit;
                    this.newIncentiveProgramme[i].sta100 = item.sta100;
                    this.newIncentiveProgramme[i].sta105 = item.sta105;
                    this.newIncentiveProgramme[i].sta110 = item.sta110;


                    this.getRateByBrandAndPosition(this.newIncentiveProgramme[i])
                    i++;
                    if (item.final) {
                        this.saveControl = true;
                    }
                    this.draftRevizyon = item.revision;
                    this.totalSaleSplit += item.saleSplit;
                }
                this.totalSaleSplit = Math.round(this.totalSaleSplit);
                // console.log(this.newIncentiveProgramme);
                var total100 = new NewIncentiveProgramme();
                total100.position.name = 'TÜM SATIŞ EKİBİ';
                total100.salesTeamAchievement = '100 - 104.9%';
                total100.numStaff = 0;
                total100.saleSplit = this.totalSaleSplit;
                total100[0] = 0;
                total100[1] = 0;
                total100[2] = 0;
                for (let item of this.list) {
                    total100.numStaff += item.numStaff;
                }
                let total105 = JSON.parse(JSON.stringify(total100))
                let total110 = JSON.parse(JSON.stringify(total100))
                total105.salesTeamAchievement = '105 - 109.9%';
                total110.salesTeamAchievement = '110 >%';

                total100.order = "zzzzzzzz_100 - 104.9%"
                total105.order = "zzzzzzzz_105 - 109.9%"
                total110.order = "zzzzzzzz_110 >%"
                debugger;
                setTimeout(() => {

                    for (let item of this.newIncentiveProgramme) {
                        console.log(item.salesTeamAchievement);
                        if (item.salesTeamAchievement == '100 - 104.9%') {
                            console.log('100 - 104.9%');

                            total100.staTotalMonIncTL[0] += item.staTotalMonIncTL[0];
                            total100.staTotalMonIncTL[1] += item.staTotalMonIncTL[1];
                            total100.staTotalMonIncTL[2] += item.staTotalMonIncTL[2];
                        }
                        if (item.salesTeamAchievement == '105 - 109.9%') {
                            total105.staTotalMonIncTL[0] += item.staTotalMonIncTL[0];
                            total105.staTotalMonIncTL[1] += item.staTotalMonIncTL[1];
                            total105.staTotalMonIncTL[2] += item.staTotalMonIncTL[2];
                        }
                        if (item.salesTeamAchievement == '110 >%') {
                            total110.staTotalMonIncTL[0] += item.staTotalMonIncTL[0];
                            total110.staTotalMonIncTL[1] += item.staTotalMonIncTL[1];
                            total110.staTotalMonIncTL[2] += item.staTotalMonIncTL[2];
                        }
                    }
                    this.newIncentiveProgramme.push(total100);
                    this.newIncentiveProgramme.push(total105);
                    this.newIncentiveProgramme.push(total110);
                    console.log(this.newIncentiveProgramme);
                }, 2000);

                //  this.getNewIncentiveProgramme();
                this.cdRef.detectChanges();
            }
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.list = [];
            this.newIncentiveProgramme = [];
            this.saveControl = true;
            this.totalSaleSplit = 0;
            //this.messageService.add({ severity: 'info', summary: 'Hata', detail: 'Sonuç bulunamadı! ' + error.message });
        });
    }
    getNewIncentiveProgramme() {
        for (let item of this.newIncentiveProgramme) {
            this.getRateByBrandAndPosition(item)
        }
        // console.log(this.newIncentiveProgramme);
    }

    getRateByBrandAndPosition(item: NewIncentiveProgramme) {
        this.spinner.show();
        this.premiumRateService.getPremiumRateByBrandIdAndPositionId(this.monthlyGoal.brandId, item.position.id).subscribe(result => {
            item.salesTeamAchievement = '100 - 104.9%';
            item.order = item.position.name + " " + item.salesTeamAchievement;
            item.staPercentage[0] = result.data.p1s1;
            item.staPercentage[1] = result.data.p1s2;
            item.staPercentage[2] = result.data.p1s3;
            item.staIncTLPerPos[0] = item.sta100 * result.data.p1s1;
            item.staIncTLPerPos[1] = item.sta100 * result.data.p1s2;
            item.staIncTLPerPos[2] = item.sta100 * result.data.p1s3;

            item.staTotalMonIncTL[0] = item.staIncTLPerPos[0] * item.numStaff;
            item.staTotalMonIncTL[1] = item.staIncTLPerPos[1] * item.numStaff;
            item.staTotalMonIncTL[2] = item.staIncTLPerPos[2] * item.numStaff;


            //this.newIncentiveProgramme.push(item);
            // var item2 = Object.assign({}, item);
            let item2 = JSON.parse(JSON.stringify(item))
            item2.salesTeamAchievement = '105 - 109.9%';
            item2.order = item2.position.name + " " + item2.salesTeamAchievement;

            item2.staPercentage[0] = result.data.p2s1;
            item2.staPercentage[1] = result.data.p2s2;
            item2.staPercentage[2] = result.data.p2s3;
            item2.staIncTLPerPos[0] = item.sta105 * result.data.p2s1;
            item2.staIncTLPerPos[1] = item.sta105 * result.data.p2s2;
            item2.staIncTLPerPos[2] = item.sta105 * result.data.p2s3;

            item2.staTotalMonIncTL[0] = item2.staIncTLPerPos[0] * item.numStaff;
            item2.staTotalMonIncTL[1] = item2.staIncTLPerPos[1] * item.numStaff;
            item2.staTotalMonIncTL[2] = item2.staIncTLPerPos[2] * item.numStaff;

            this.newIncentiveProgramme.push(item2)

            //            var item3 = Object.assign({}, item2);
            let item3 = JSON.parse(JSON.stringify(item2))
            item3.salesTeamAchievement = '110 >%';
            item3.order = item3.position.name + " " + item3.salesTeamAchievement;

            item3.staPercentage[0] = result.data.p3s1;
            item3.staPercentage[1] = result.data.p3s2;
            item3.staPercentage[2] = result.data.p3s3;

            item3.staIncTLPerPos[0] = item.sta110 * result.data.p3s1;
            item3.staIncTLPerPos[1] = item.sta110 * result.data.p3s2;
            item3.staIncTLPerPos[2] = item.sta110 * result.data.p3s3;

            item3.staTotalMonIncTL[0] = item3.staIncTLPerPos[0] * item.numStaff;
            item3.staTotalMonIncTL[1] = item3.staIncTLPerPos[1] * item.numStaff;
            item3.staTotalMonIncTL[2] = item3.staIncTLPerPos[2] * item.numStaff;

            this.newIncentiveProgramme.push(item3)

            this.cdRef.detectChanges();
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'info', summary: 'Hata', detail: error.message });
        });
    }

    submitSave() {
        if (this.list.length < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Değer bulunamadı!' });
            return;
        }
        if (this.totalSaleSplit != 100) {
            this.messageService.add({
                severity: 'info', summary: 'Uyarı',
                detail: 'Toplam yüzdelik dilim 100 olmak zorundadır! Şuan ki değer: ' + this.totalSaleSplit
            });
            return;
        }
        this.spinner.show();

        for (let item of this.list) {
            this.monthlyGoalService.saveStorePosition(item).subscribe(result => {
                this.messageService.add({
                    severity: 'info', summary: 'İşlem Durumu',
                    detail: item.position.name + ' posizyonu kaydedildi'
                });
                item.id = result.data.id;
            }, error => {
                this.spinner.hide();
                this.messageService.add({
                    severity: 'info', summary: 'Hata',
                    detail: 'Kaydedilirken hata oluştu! Posizyon: ' + item.position.name
                        + ' Hata Mesajı: ' + error.message
                });
            });
        }
        this.spinner.hide();

    }

    changedSaleSplit(item: MonthlyGoal, value) {
        this.totalSaleSplit = 0;
        for (let item of this.list) {
            this.totalSaleSplit += item.saleSplit;
        }
        item.pmst = Math.round(((this.storeTargetValue * item.saleSplit) / 100) / item.numStaff);
        item.sta100 = Math.round(item.pmst);
        item.sta105 = Math.round(item.pmst * 1.07);
        item.sta110 = Math.round(item.pmst * 1.1);
    }
}
