import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { environment } from '../../../../../../environments/environment';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { StorageService } from '../../../../../auth/_services';

@Injectable()
export class DataInputService {
    API_URL = environment.apiURL;
    headers: HttpHeaders = new HttpHeaders({
        'Accept': 'application/json',
        //'Content-Type':  'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*'
    });
    headers2: HttpHeaders = new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*'
    });
    headers3: HttpHeaders = new HttpHeaders({
        //'Accept': 'application/json',
        //'Content-Type' : 'application/json',
        // 'Content-Type': 'application/octet-stream',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*'
    });

    constructor(private http: HttpClient, private storage: StorageService) {
        this.headers = this.headers.append('Authorization', this.storage.getToken());
        this.headers2 = this.headers2.append('Authorization', this.storage.getToken());
        this.headers3 = this.headers3.append('Authorization', this.storage.getToken());
    }

    getFilesXLAlShayaCalendar() {
        return this.http.get(this.API_URL + '/svc/filesXLAlShayaCalendar', {
            headers: this.headers2
        }).pipe(catchError(this.handleError));
    }
    downloadFile(id) {
        // this.headers3.append('Accept', 'text/plain');
        this.headers3.append('Access-Control-Expose-Headers', 'X-Custom-header');
        return this.http.get(this.API_URL + '/svc/filesGet/' + id, {
            headers: this.headers3, observe: 'response'//,responseType: 'text'
            , responseType: 'blob'
        }).pipe(catchError(this.handleError));
    }

    getCalendars(year: number) {
        return this.http.get(this.API_URL + '/svc/calendars/' + year, {
            headers: this.headers2
        }).pipe(catchError(this.handleError));
    }

    getStoretargets(year: number) {
        return this.http.get(this.API_URL + '/svc/storetargets/' + year, {
            headers: this.headers2
        }).pipe(catchError(this.handleError));
    }

    getFilesXLSaleTargets() {
        return this.http.get(this.API_URL + '/svc/filesXLSaleTargets', {
            headers: this.headers2
        }).pipe(catchError(this.handleError));
    }
    upload(target, file, year) {
        const url = this.API_URL + '/svc/' + (target === 'calendar' ? 'xlalshayacalendar' : 'xlsaletargets');

        const formData: FormData = new FormData();
        formData.append('year', year);



        formData.append('upfile', file, file.name);

        return this.http.post(url, formData, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        console.log(error);

        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        if (error.error.code == "UNAUTHORIZED") {
            localStorage.removeItem('currentUser');
            window.location.reload();
        }
        // return an observable with a user-facing error message
        return ErrorObservable.create(error.error);
    };

}