import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';

import { SelectInput } from '../data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { SltPremiumRateService } from "./slt-premium-rate.service";
import { PremiumRate } from "../../../../../model/premium-rate/premium-rate.model";
import { Rights } from "../../../../../model/rights";
import { SltPremiumRate } from "../../../../../model/setting/sltpremiumrate/slt-premium-rate.model";

@Component({
    selector: 'slt-premium-rate',
    templateUrl: './slt-premium-rate.component.html'
})
export class SltPremiumRateComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public sltPremiumRate = new SltPremiumRate();
    list: SltPremiumRate[] = [];
    brands: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];

    constructor(private sltPremiumRateService: SltPremiumRateService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {

        this.sltPremiumRateService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Marka listesi alınamadı!' + error.message });
        });

        /*this.sltPremiumRateService.getPositions().subscribe(result => {
            if (result.total) {
                for (const position of result.data) {
                    if (position.hasBonus == true) {
                        this.positions.push({
                            label: position.name,
                            value: position.id,
                            checked: false
                        })
                    }
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Ünvan listesi alınamadı!'+ error.message });
        });*/
    }

    getSltPremiumRateByBrandId() {
        if (this.sltPremiumRate.brandId == null || this.sltPremiumRate.brandId < 1) {
            // this.messageService.add({severity: 'error', summary: 'Hata', detail: 'Marka bilgisi seçiniz!'});
            return;
        }

        this.spinner.show();
        this.sltPremiumRateService.getPremiumRateByBrandId(this.sltPremiumRate.brandId).subscribe(result => {
            this.spinner.hide();
            this.list = result.data;
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'SLT Prim Oranı bulunamadı! ' + error.message });
        });

    }

    submit() {
        if (this.sltPremiumRate.brandId == null || this.sltPremiumRate.brandId < 1) {
            this.messageService.add({ severity: 'info', summary: 'Hata', detail: 'Marka bilgisi seçiniz!' });
            return;
        }

        this.spinner.show();
        this.sltPremiumRateService.save(this.list).subscribe(result => {
            this.spinner.hide();
            if (result.success) {
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: result.message });
            } else {
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı' + error.message });
        });

    }
}
