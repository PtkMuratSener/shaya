import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { DiscountPeriodService } from './discount-period.service';
import { environment } from '../../../../../../environments/environment';

import { SelectInput, Months, Years } from '../data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { catchError } from "rxjs/operators";
import { SasMonth } from "../../../../../model/setting/discountperiod/sas-month.model";
import { BrandModel } from "../../../../../model/brand/brand.model";
import { Rights } from "../../../../../model/rights";

@Component({
    selector: 'm-bonus',
    templateUrl: './discount-period.component.html'
})
export class DiscountPeriodComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public sasmonth = new SasMonth();
    brands: SelectInput[] = [];


    constructor(private discountPeriodService: DiscountPeriodService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.brands = [{
            label: 'Seçiniz', value: '0', checked: false
        }];
        this.discountPeriodService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Marka listesi alınamadı!' });
        });
    }
    getDiscountPeriodByBrandId(brandId) {
        this.discountPeriodService.getDiscountPeriodByBrandId(brandId).subscribe(result => {
            this.sasmonth = result.data;
            if (this.sasmonth.brand == null) {
                this.sasmonth.brand = new BrandModel();
                this.sasmonth.brand.id = brandId;
            }
        }, error => {
            this.sasmonth = new SasMonth();
            this.sasmonth.brand.id = brandId;
            this.messageService.add({ severity: 'info', summary: 'İndirim Dönemi', detail: 'İndirim Dönemi bulunamadı!' });
        });
    }
    submit() {
        this.spinner.show();

        this.discountPeriodService.save(this.sasmonth).subscribe(result => {
            this.spinner.hide();
            if (result.success) {
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'İşleniyor' });
            } else {
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı' });
        });

    }
}
