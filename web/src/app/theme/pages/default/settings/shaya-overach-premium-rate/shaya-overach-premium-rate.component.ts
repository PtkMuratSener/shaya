import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';

import { SelectInput } from '../data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { ShayaOverachPremiumRateService } from "./shaya-overach-premium-rate.service";
import { PremiumRate } from "../../../../../model/premium-rate/premium-rate.model";
import { Rights } from "../../../../../model/rights";
import { SltPremiumRate } from "../../../../../model/setting/sltpremiumrate/slt-premium-rate.model";
import { ShayaOverachPremiumRate } from "../../../../../model/setting/shayaoverachpremiumrate/shaya-overach-premium-rate.model";

@Component({
    selector: 'shaya-overach-premium-rate',
    templateUrl: './shaya-overach-premium-rate.component.html'
})
export class ShayaOverachPremiumRateComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public shayaOverachPremiumRate = new ShayaOverachPremiumRate();
    list: ShayaOverachPremiumRate[] = [];


    constructor(private shayaOverachPremiumRateService: ShayaOverachPremiumRateService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.shayaOverachPremiumRateService.getBonusRatesOverAchievement().subscribe(result => {
            this.list = result.data;
        }, error => {
            this.list = [];
            this.messageService.add({
                severity: 'info', summary: 'Hata oluştu',
                detail: 'Shaya prim yönetimi için over achievement alınamadı!' + error.message
            });
        });

    }


    submit() {

        this.spinner.show();
        this.shayaOverachPremiumRateService.save(this.list).subscribe(result => {
            this.spinner.hide();
            if (result.success) {
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: result.message });
            } else {
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı' + error.message });
        });

    }
}
