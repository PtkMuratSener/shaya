import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/_guards';
import { Rights } from '../model/rights';

const routes: Routes = [
    {
        'path': '',
        'component': ThemeComponent,
        'canActivate': [AuthGuard],
        'children': [
            {
                'path': '',
                'loadChildren': '.\/pages\/default\/index\/index.module#IndexModule'
            },
            {
                'path': 'parameters',
                'loadChildren': '.\/pages\/default\/settings\/parameters\/parameters.module#ParametersModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'formulas',
                'loadChildren': '.\/pages\/default\/settings\/formulas\/formulas.module#FormulasModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'data-input',
                'loadChildren': '.\/pages\/default\/settings\/data-input\/data-input.module#DataInputModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'bonus-calculate-monthly',
                'loadChildren': '.\/pages\/default\/settings\/bonus-calculate-monthly\/bonus-calculate-monthly.module#BonusCalculateMonthlyModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'discount-period',
                'loadChildren': '.\/pages\/default\/settings\/discount-period\/discount-period.module#DiscountPeriodModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'target-rate',
                'loadChildren': '.\/pages\/default\/settings\/target-rate\/target-rate.module#TargetRateModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'premium-rate',
                'loadChildren': '.\/pages\/default\/settings\/premium-rate\/premium-rate.module#PremiumRateModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'slt-premium-rate',
                'loadChildren': '.\/pages\/default\/settings\/slt-premium-rate\/slt-premium-rate.module#SltPremiumRateModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'shaya-overach-premium-rate',
                'loadChildren': '.\/pages\/default\/settings\/shaya-overach-premium-rate\/shaya-overach-premium-rate.module#ShayaOverachPremiumRateModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'center-annual-line-goal',
                'loadChildren': '.\/pages\/default\/settings\/center-annual-line-goal\/center-annual-line-goal.module#CenterAnnualLineGoalModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'brands-compensation-premium-rate',
                'loadChildren': '.\/pages\/default\/settings\/brands-compensation-premium-rate\/brands-compensation-premium-rate.module#BrandsCompensationPremiumRateModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'shaya-annual-premium-rate',
                'loadChildren': '.\/pages\/default\/settings\/shaya-annual-premium-rate\/shaya-annual-premium-rate.module#ShayaAnnualPremiumRateModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'title-not-annual-bonus',
                'loadChildren': '.\/pages\/default\/settings\/title-not-annual-bonus\/title-not-annual-bonus.module#TitleNotAnnualBonusModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'monthly-goal',
                'loadChildren': '.\/pages\/default\/monthly-goal\/monthly-goal.module#MonthlyGoalModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'annual-draft-premium-calculator',
                'loadChildren': '.\/pages\/default\/annual-draft-premium-calculator\/annual-draft-premium-calculator.module#AnnualDraftPremiumCalculatorModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'central-draft-annual-premium-calculation',
                'loadChildren': '.\/pages\/default\/central-draft-annual-premium-calculation\/central-draft-annual-premium-calculation.module#CentralDraftAnnualPremiumCalculationModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'premium-monthly',
                'loadChildren': '.\/pages\/default\/premium-monthly\/premium-monthly.module#PremiumMonthlyModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'annual-premium',
                'loadChildren': '.\/pages\/default\/annual-premium\/annual-premium.module#AnnualPremiumModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'realized-monthly',
                'loadChildren': '.\/pages\/default\/realized-monthly\/realized-monthly.module#RealizedMonthlyModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'role',
                'loadChildren': '.\/pages\/default\/account\/role\/role.module#RoleModule',
                'canActivate': [AuthGuard]
            },
            {
                'path': 'access-denied',
                'loadChildren': '.\/pages\/default\/access-denied\/access.denied.module#AccessDeniedModule'
            },
            {
                'path': '404',
                'loadChildren': '.\/pages\/default\/not-found\/not-found.module#NotFoundModule'
            },
            {
                'path': '',
                'redirectTo': 'index',
                'pathMatch': 'full'
            }
        ]
    },
    {
        'path': '**',
        'redirectTo': '404',
        'pathMatch': 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ThemeRoutingModule {
}