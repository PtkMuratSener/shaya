import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services';
import { Helpers } from '../../helpers';

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    encapsulation: ViewEncapsulation.None
})

export class LogoutComponent implements OnInit {

    constructor(private _router: Router,
        private _authService: AuthService) {
    }

    ngOnInit(): void {
        Helpers.setLoading(true);
        // reset login status
        this._authService.logOut();
        this._router.navigate(['/login?returnUrl=%2F']);
    }
}