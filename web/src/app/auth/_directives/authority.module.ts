import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { HasRight, HasAnyRight } from "./authority.directive";

@NgModule({
    declarations: [

    ],
    imports: [
    ],
    providers: [

    ],
    exports: [

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: []
})

export class AuthorityModule {
}