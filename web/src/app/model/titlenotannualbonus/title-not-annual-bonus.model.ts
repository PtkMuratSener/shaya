import { ApiModel } from "../api.model";

export class TitleNotAnnualBonus extends ApiModel {
    public brandId: number;
    public positionName: string;
    public hasYearlyBonus: boolean;
}
