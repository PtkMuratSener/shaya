import { ApiModel } from "../api.model";

export class FormSelectOption extends ApiModel {
    label: string;
    value: string;
    items: FormSelectOption[] = [];

    constructor(value: string, text: string, children: FormSelectOption[]) {
        super();
        this.value = value;
        this.label = text;
        this.items = children;
    }
}