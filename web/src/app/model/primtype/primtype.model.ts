import { ApiModel } from "../api.model";


export class PrimTypeModel extends ApiModel {
    public name: string;
    public checked: boolean;
    constructor() {
        super();
    }
}