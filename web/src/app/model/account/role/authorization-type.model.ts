import { ApiModel } from "../../api.model";

export class AuthorizationType extends ApiModel {
    public name: string;
    public createdOn: any;
    public checked: boolean;
    constructor() {
        super();
    }
}