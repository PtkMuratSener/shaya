import { ApiModel } from "../api.model";
import { BrandModel } from "../brand/brand.model";
import { PrimTypeModel } from "../primtype/primtype.model"; //Murat Şener
import { Screen } from "./role/screen.model";
import { AuthorizationType } from "./role/authorization-type.model";

export class RoleComboRequest extends ApiModel {
    public role: number;
    public brands: BrandModel[] = [];//Markalar ,
    public primTypes: PrimTypeModel[] = [];//Prim tipleri , Murat Şener
    public screens: Screen[] = []; // Ekranlar ,
}