import { ApiModel } from "../api.model";

export class StoreActual extends ApiModel {
    public periyotBaslangic: string; // Periyot başı ,
    public periyotSonu: number; // Periyot sonu ,
    public iadeBaslangic: string; // İade başı ,
    public iadeSonu: string; // İade sonu ,
    public iade: number; // İade Miktarı ,
    public actual: number; // Mağaza Net satış miktarı ,
    public brut: number; //Mağaza brüt satış miktarı
}
