import { ApiModel } from "../api.model";


export class BrandModel extends ApiModel {
    public name: string;
    public shortName: string;
    public checked: boolean;
    public birim: string;
    constructor() {
        super();
    }
}