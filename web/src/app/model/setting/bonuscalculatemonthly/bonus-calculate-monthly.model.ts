import { ApiModel } from "../../api.model";

export class BonusCalculateMonthly extends ApiModel {
    public filter: string;
    public fbrand: number;
    public fstore: number;
    public fposition: number;
    public femployee: number;
    public fyear: number;
    public fmonth: number;
    public title: string;
    public total: number;
    public processed: number;
    public active: boolean;
    public lastRun: number;
    constructor() {
        super();
    }
}
