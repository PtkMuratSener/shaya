import { ApiModel } from "../../api.model";
import { BrandModel } from "../../brand/brand.model";


export class SasMonth extends ApiModel {
    public m01: boolean;
    public m02: boolean;
    public m03: boolean;
    public m04: boolean;
    public m05: boolean;
    public m06: boolean;
    public m07: boolean;
    public m08: boolean;
    public m09: boolean;
    public m10: boolean;
    public m11: boolean;
    public m12: boolean;
    public brand: BrandModel;

    constructor() {
        super();
        this.brand = new BrandModel();
    }
}