import { DataStatus } from "./types.model";
export class ApiModel {
    public id: number;
    public version: number;
    public url: string;
    public createdDate: any;
    public dataStatus: DataStatus;

}