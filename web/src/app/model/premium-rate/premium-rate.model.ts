import { ApiModel } from "../api.model";
import { BrandModel } from "../brand/brand.model";
import { Position } from "../position/position.model";


export class PremiumRate extends ApiModel {
    public brand: BrandModel;
    public position: Position;
    public p1s1: number;
    public p1s2: number;
    public p1s3: number;
    public p2s1: number;
    public p2s2: number;
    public p2s3: number;
    public p3s1: number;
    public p3s2: number;
    public p3s3: number;
    constructor() {
        super();
        this.brand = new BrandModel();
        this.position = new Position();
    }
}