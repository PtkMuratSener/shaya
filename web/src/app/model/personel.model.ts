import { ApiModel } from "./api.model";

export class Personel extends ApiModel {
    public firstname: string;
    public lastname: string;
    public organizationName: string = null;
    public url = 'personels';
}