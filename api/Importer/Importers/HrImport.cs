﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using Pusula.Api.Models;

namespace Importer
{
    public class HrImport
    {

        private static string _connectionString;
        private static OracleConnection _connection;

        public HrImport(string connectionString)
        {
            _connectionString = connectionString;
        }

        private static bool Open()
        {
            _connection = new OracleConnection(_connectionString);
            try
            {
                _connection.Open();
                return true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        private static void Close()
        {
            if (_connection == null) return;
            _connection.Close();
            _connection.Dispose();
            _connection = null;
        }

        public void Dispose()
        {
            Close();
        }
        public static int CreateUpdateDelete(string sql)
        {
            Open();
            OracleCommand cmd = new OracleCommand(sql, _connection);
            int result = cmd.ExecuteNonQuery();
            Close();
            return result;
        }

        /// <summary>
        /// BonusRatesSlt tablosunu günceller
        /// Mağazaya yeni eklenen pozisyonları ekler.
        /// </summary>
        /// <param name="localDbCtx">LocalDBCtx</param>
        /// <returns></returns>
        public bool UpdateBonusRatesSlt(LocalDbCtx localDbCtx)
        {
            Console.WriteLine("- Updating BonusRatesSlt from HR");
            Open();
            var newlyAdded = 0;
            var sql = @"SELECT DISTINCT TRUNC(ISYERI_MASRAFMR / 1000) AS BRANDID, GOREV 
                        FROM T_PERSONEL 
                        WHERE ISYERI_MASRAFMR IS NOT NULL
                        ORDER BY 1,2
            ";
            var cmd = _connection.CreateCommand();
            cmd.CommandText = sql;
            var rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                var brandId = rdr.GetInt32(0);
                brandId = brandId == 38 ? 39 : brandId; //VSBA ve VSFA birleşik Victorias olarak görülecek.
                var positionName = rdr.GetString(1);
                Console.WriteLine($"{brandId}.{positionName}");
                var position = localDbCtx.Positions.AsNoTracking()
                    .FirstOrDefault(x => x.Name == positionName);
                if (position == null)
                {
                    //Yoksa ekliyoruz
                    position = new Position
                    {
                        Name = positionName
                    };
                    localDbCtx.Positions.Add(position);
                    localDbCtx.SaveChanges();
                }

                Console.WriteLine($"PID: {position.Id}");
                var bonusRatesSlt = localDbCtx.BonusRatesSlt.AsNoTracking()
                    .Where(b => b.Brand.Id == brandId)
                    .FirstOrDefault(b => b.Position.Id == position.Id);

                if (bonusRatesSlt != null)
                {
                    //Varsa geç.
                    Console.Write(".");
                }
                else
                {
                    var bonusRateSlt = new BonusRateSlt
                    {
                        Brand = localDbCtx.Brands.Find(brandId),
                        Position = localDbCtx.Positions.First(x => x.Name == positionName),
                        HasYearlyBonus = true
                    };
                    localDbCtx.Entry(bonusRateSlt.Brand).State = EntityState.Unchanged;                    
                    localDbCtx.Entry(bonusRateSlt.Position).State = EntityState.Unchanged;                    
                    localDbCtx.BonusRatesSlt.Add(bonusRateSlt);
                    localDbCtx.SaveChanges();
                    newlyAdded++;
                    Console.Write("+");
                    Console.WriteLine(bonusRateSlt.Id);
                }
            }
            Console.WriteLine($"Yeni eklenen: {newlyAdded}");
            Close();
            return true;
        }
        
        public bool Import(LocalDbCtx localDbCtx)
        {
            Open();
            Console.WriteLine("- Importing IKYS_SHY.V_CEZA - CEZAKOD");
            var cezaSql = "SELECT DISTINCT CEZAKOD, CEZA_TUR FROM V_CEZA ORDER BY 1";
            OracleCommand cezaCmd = _connection.CreateCommand();
            cezaCmd.CommandText = cezaSql;
            OracleDataReader cezaRdr = cezaCmd.ExecuteReader();

            while (cezaRdr.Read())
            {
                var ceza = localDbCtx.Cezalar.FirstOrDefault(c => c.Code == cezaRdr.GetString(0));
                if (ceza == null)
                {
                    var c = new Ceza
                    {
                        Code = cezaRdr.GetString(0),
                        Title = cezaRdr.GetString(1),
                        Effective = 0
                    };
                    localDbCtx.Cezalar.Add(c);
                }
                else
                {
                    ceza.Title = cezaRdr.GetString(1);
                }
            }
            localDbCtx.SaveChanges();
            cezaCmd.Dispose();

            Console.WriteLine("- Importing IKYS_SHY.T_PERSONEL");
            var markalar = localDbCtx.Brands.AsNoTracking()
                .Where(x => x.Id > 0)
                .Select(x => x.Id);
            
            // Sicili eşleştirilemeyen personel güncellenmeden kalmasın diye tüm personeli deaktive ediyoruz.
            localDbCtx.Database
                .ExecuteSqlCommand("UPDATE Employees SET Active=0 WHERE Active=1");
 

            foreach (var marka in markalar)
            {
                var recordsNew = 0;
                var recordsUpdated = 0;
                var recordsTotal = 0;
                var sql = $@"
                       SELECT DISTINCT NVL(SICIL_NO, 0) AS Sicil, ADI  AS Name, SOYADI AS Surname,    
                           NVL(GOREV, ' ') AS Position, NVL(GOREV_GRUP_KODU, '00') AS GorevGrup,    
                           NVL(GOREV_GRUBU, ' ') AS GorevGrupText, ISE_GIRIS_TARIHI AS StartDate,    
                           NVL(LDAP_USERNAME, 'nologin') AS LdapUsername, ISYERI_MASRAFMR AS Store,    
                           ROUND(MONTHS_BETWEEN(CURRENT_DATE, TO_DATE(ISE_GIRIS_TARIHI, 'dd.mm.yyyy'))) AS Kidem,    
                           SUBSTR(PERSONEL_TURU, 1, 1) AS Area, P.CALISMA_DURUMU_KOD, UNVAN,  KIRILIM_TANIM3   
                       FROM IKYS_SHY.T_PERSONEL  P    
                       WHERE 1=1   
                           -- AND CALISMA_DURUMU_KOD=1   
                           -- AND TO_DATE(AYRILIS_TARIHI, 'DD.MM.YYYY') > TO_DATE('20171231', 'YYYYMMDD')    
                           AND ISYERI_MASRAFMR LIKE '{marka}___'    
                           -- AND SICIL_NO = 00000    
                       ORDER BY 1";
                var cmd = _connection.CreateCommand();
                cmd.CommandText = sql;
                var rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    recordsTotal++;
                    //Store needed, get store
                    Store store;
                    try
                    {
                        var storeId = Convert.ToInt32(rdr.GetString(8));
                        store = localDbCtx.Stores.Find(storeId);
                        if (store is null)
                        {
                            var brandId = storeId / 1000;
                            brandId = brandId == 38 ? 39 : brandId; //VSBA ve VSFA birleşik Victorias olarak görülecek.
                            var brand = localDbCtx.Brands.FirstOrDefault(x => x.Id == brandId);
                            if (brand == null)
                            {
                                brand = new Brand
                                {
                                    Id = brandId,
                                    Name = rdr.GetString(13),
                                    ShortName = ".",
                                    Birim = "."
                                };
                                localDbCtx.Brands.Add(brand);
                                localDbCtx.SaveChanges();
                            }
                            store = new Store()
                            {
                                Id = Convert.ToInt32(rdr.GetString(8)),
                                Name = "",
                                Aktif = true,
                                Brand = brand
                            };
                            localDbCtx.Entry(store.Brand).State = EntityState.Unchanged;
                            localDbCtx.Stores.Add(store);
                        }

                        localDbCtx.SaveChanges();
                    }
                    catch (InvalidCastException e)
                    {
                        Console.WriteLine($"   X Employee->Store is NULL, SICIL={rdr[0]}." + e.Message);
                        continue;
                    }

                    //Check if Employee exists
                    var sicil = Convert.ToInt32(rdr.GetString(0));
                    var emp = localDbCtx.Employees.Find(sicil);

                    //var startDateTxt = rdr.GetString(6);
                    //var startDate = DateTime.ParseExact(startDateTxt, "dd.MM.yyyy", CultureInfo.InvariantCulture);

                    var position = localDbCtx.Positions.FirstOrDefault(p => p.Name == rdr.GetString(3).Trim());
                    if (position == null)
                    {
                        // Pozisyon tablolarda yoksa ekleyelim
                        position = new Position
                        {
                            Name = rdr.GetString(3)
                        };
                        localDbCtx.Positions.Add(position);
                        localDbCtx.SaveChanges();
                    }

                    var pActive = rdr.GetString(11) == "1";
                    var pGorev = Convert.ToInt32(rdr.GetString(4));
                    var pGorevTxt = rdr.GetString(5);
                    var pKidem = Math.Round(rdr.GetDouble(9));
                    var pLdapUser = rdr.GetString(7);
                    var pName = rdr.GetString(1);
                    var pSurname = rdr.GetString(2);
                    var pStart = rdr.GetString(6);
                    var pArea = rdr.GetString(10);
                    var pUnvan = rdr.GetString(12);

                    if (emp is null)
                    {
                        recordsNew++;
                        emp = new Employee()
                        {
                            Sicil = sicil,
                            Name = pName,
                            Surname = pSurname,
                            Position = position,
                            Gorev = pGorev,
                            GorevText = pGorevTxt,
                            StartDate = pStart,
                            LdapUsername = pLdapUser,
                            Store = store,
                            Kidem = pKidem,
                            Area = pArea,
                            Ceza = 0,
                            Active = pActive,
                            Unvan = pUnvan
                        };
                        localDbCtx.Employees.Add(emp);
                    }
                    else
                    {
                        recordsUpdated++;
                        emp.Name = pName;
                        emp.Surname = pSurname;
                        emp.Position = position;
                        emp.Gorev = pGorev;
                        emp.GorevText = pGorevTxt;
                        emp.StartDate = pStart;
                        emp.LdapUsername = pLdapUser;
                        emp.Store = store;
                        emp.Kidem = pKidem;
                        emp.Area = pArea;
                        emp.Ceza = 0;
                        emp.Active = pActive;
                    }

                    localDbCtx.Entry(emp.Store).State = EntityState.Unchanged;
                    localDbCtx.Entry(emp.Position).State = EntityState.Unchanged;
                    localDbCtx.SaveChanges();
                }

                Console.WriteLine($"   * Import ok {marka}, {recordsNew}N {recordsUpdated}U {recordsTotal}T");
            }

            Console.WriteLine("- Importing IKYS_SHY.V_CEZA");
            var etkiliCezalar = localDbCtx.Cezalar.Where(c => c.Effective > 0);
            var etkinCezaList = new List<string>();
            if (etkiliCezalar.Any())
            {
                foreach (var ceza in etkiliCezalar)
                {
                    etkinCezaList.Add(ceza.Code);
                }

                var cezaSqlTempl = "SELECT CEZAKOD, CEZA_TARIHI, SICIL_NO " +
                                   "FROM V_CEZA " +
                                   " WHERE CEZAKOD IN('" +
                                   String.Join("', '", etkinCezaList.ToArray())
                                   + "')";
                Console.WriteLine(cezaSqlTempl);
                OracleCommand czCmd = _connection.CreateCommand();
                czCmd.CommandText = cezaSqlTempl;
                OracleDataReader cRdr = czCmd.ExecuteReader();
                if (cRdr.HasRows)
                {
                    // Eskileri sil
                    var allCezas = localDbCtx.EmployeeCezas;
                    localDbCtx.EmployeeCezas.RemoveRange(allCezas);
                    
                    while (cRdr.Read())
                    {
                        localDbCtx.EmployeeCezas.Add(new EmployeeCeza
                        {
                            Code = cRdr.GetString(0),
                            Date = cRdr.GetDateTime(1),
                            Sicil = Convert.ToInt32(cRdr.GetString(2))
                        });
                    }

                    localDbCtx.SaveChanges();
                }
                
                cRdr.Dispose();
                czCmd.Dispose();
            }

            var stores = localDbCtx.Stores;
            foreach (var s in stores)
            {
                var empCnt = localDbCtx.Employees
                    .AsNoTracking()
                    .Count(e => e.Store == s);
                s.Aktif = empCnt > 0;
            }

            localDbCtx.SaveChanges();

            return true;
        }
    }
}