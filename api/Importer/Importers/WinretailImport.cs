﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Pusula.Api.Models;

namespace Importer
{
    public class WinretailImport
    {
        private static SqlConnection _connection;

        public WinretailImport(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
        }

        public bool Import(LocalDbCtx localDbCtx)
        {
            // Mağaza ve personel bazlı günlük satış tutarları
            Console.WriteLine("- Importing [SP_Entegrasyon2_PersonelBazliSatis]");
            var today = DateTime.Now.ToString("yyyy.MM.dd");
            var startDate = today;

            var startDateEnv = Environment.GetEnvironmentVariable("RETAIL_SATIS_STARTDATE");
            if (startDateEnv != null)
            {
                startDate = startDateEnv;
            }
            var endDateEnv = Environment.GetEnvironmentVariable("RETAIL_SATIS_ENDDATE");
            if (endDateEnv != null)
            {
                today = endDateEnv;
            }

            Console.WriteLine($"  * Import executing for {startDate}-{today}, to change set RETAIL_SATIS_STARTDATE-RETAIL_SATIS_ENDDATE environment var");
            //FAZ2: markalar vt den çekilecek
            List<string> markalar = new List<string>();
            markalar.Add("39");
            foreach (string marka in markalar)
            {


                var satisSql = $@"DECLARE @temp TABLE
(
    C1 VARCHAR(13),
    MAGAZA VARCHAR(20),
    MAGAZAADI VARCHAR(50),
    TARIH DATETIME,
    SICIL INT,
    PERSADI VARCHAR(50),
    POZISYON VARCHAR(50),
    ADET INT,
    TUTAR FLOAT(15),
    IADET INT,
    ITUTAR FLOAT(15)
);
INSERT @temp  EXEC [ent].[SP_Entegrasyon2_PersonelBazliSatis]
    @MARKA = {marka}, 
    @TARIH1 = '{startDate}',
    @TARIH2 = '{today}', 
    @MAGAZAKODU = NULL, 
    @GUN_DETAYLI = 1;
SELECT
       MAGAZA, MAGAZAADI, TARIH,
       IIF(ISNULL(SICIL, 0) <10, 0, SICIL) AS SICIL,
       ISNULL(PERSADI, 'HAVUZ') AS PERSADI,
       ISNULL(POZISYON, 'O') AS POZISYON,
       IIF(ADET>0,ADET, IADET) AS ADET,
       IIF(TUTAR>0,TUTAR,ITUTAR) AS TUTAR
FROM @temp;
";
                var satisCmd = new SqlCommand(satisSql, _connection);
                using (var satisRdr = satisCmd.ExecuteReader())
                {
                    while (satisRdr.Read())
                    {
                        var sicil = satisRdr.GetInt32(3);
                        var personelTanim = satisRdr.GetString(4).Trim().ToUpper();
						
                        var store = localDbCtx.Stores.Find(Convert.ToInt32(satisRdr[0]));
                        var employee = localDbCtx.Employees
                            .FirstOrDefault(m => m.Sicil == sicil);
                        if (employee == null)
                        {
                            // Bulunamayan sicilleri havuza at.
                            employee = localDbCtx.Employees
                                .FirstOrDefault(m => m.Sicil == 0);
                        }
                        var sale = new Sale()
                        {
                            Store = store,
                            Amount = satisRdr.GetFloat(7),
                            Employee = employee,
                            EmployeePosition = satisRdr.GetString(5),
                            Quantity = satisRdr.GetInt32(6),
                            SaleDate = satisRdr.GetDateTime(2)
                        };
                        try
                        {
                            localDbCtx.Sales.Add(sale);
                            localDbCtx.SaveChanges();
                            var tur = sicil > 10 ? "P" : "H";
                            if (tur == "H")
                                tur = satisRdr.GetFloat(7) < 0 ? "I" : "H";
                            Console.Write(tur);
                        }
                        catch (Exception e)
                        {
                            Console.Write("\n");
                            Console.WriteLine($"E:{sicil}, {e.Message}c");
                        }
                    }

                    Console.WriteLine($"  * Import ok for <Brand>{marka}");
                }
            }

            return true;
        }
    }
}
