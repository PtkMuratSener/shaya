﻿using Microsoft.EntityFrameworkCore;
using Pusula.Api.Models;

namespace Importer
{
    public class LocalDbCtx : DbContext
    {
        public LocalDbCtx(DbContextOptions<LocalDbCtx> options) : base ( options )
        {
        }

        /// <summary>
        /// Çalışanlar
        /// </summary>
        public DbSet<Employee> Employees { get; set; }
        
        /// <summary>
        /// Mağazalar
        /// </summary>
        public DbSet<Store> Stores { get; set; }
        
        /// <summary>
        /// Markalar
        /// </summary>
        public DbSet<Brand> Brands { get; set; }
        
        /// <summary>
        /// Mağaza bazlı personel satış
        /// </summary>
        public DbSet<Sale> Sales { get; set; }
        
        /// <summary>
        /// Pozisyonlar
        /// </summary>
        public DbSet<Position> Positions { get; set; }
        
        /// <summary>
        /// Ceza türleri ve etki süreleri
        /// </summary>
        public DbSet<Ceza> Cezalar { get; set; }
        
        /// <summary>
        /// Personel ceza bilgileri
        /// </summary>
        public DbSet<EmployeeCeza> EmployeeCezas { get; set; }
        
        /// <summary>
        /// AlShaya Takvimi
        /// </summary>
        public DbSet<Calendar> Calendars { get; set; }
        
        /// <summary>
        /// Saha pozisyon prim oranları
        /// </summary>
        public DbSet<BonusRateSlt> BonusRatesSlt { get; set; }
    }
}