using Microsoft.EntityFrameworkCore;
using SalesReload.Database;

namespace SalesReload.Database
{
    public class LocalDbCtx : DbContext
    {
        public LocalDbCtx(DbContextOptions<LocalDbCtx> options) : base ( options )
        {
        }

        /// <summary>
        /// Mağaza bazlı personel satış
        /// </summary>
        public DbSet<Sale> Sales { get; set; }
    }
}