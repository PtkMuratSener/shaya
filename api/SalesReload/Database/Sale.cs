﻿/*
 * Bu class Pusula.Api içinde değiştirildiyse güncellenmeli
 */

using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace SalesReload.Database
{
    /// <summary>
    /// Satış bilgisi
    /// </summary>
    [DataContract]
    public class Sale
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Satış yapılan mağaza
        /// </summary>
        [DataMember(Name = "StoreId")]
        public int StoreId { get; set; }

        /// <summary>
        /// Satış tarihi
        /// </summary>
        [DataMember(Name = "SaleDate")]
        public DateTime SaleDate { get; set; }
        
        /// <summary>
        /// Satış yapan personel
        /// </summary>
        [DataMember(Name = "EmployeeSicil")]
        public int EmployeeSicil { get; set; }
        
        /// <summary>
        /// Satış yapan personel pozisyonu
        /// </summary>
        [DataMember(Name = "EmployeePosition")]
        public string EmployeePosition { get; set; }
        
        /// <summary>
        /// Satış adedi
        /// </summary>
        [DataMember(Name = "Quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Satış tutarı
        /// </summary>
        [DataMember(Name = "Amount")]
        public double Amount { get; set; }
    }
}