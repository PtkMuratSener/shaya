﻿using System;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using SalesReload.Database;

namespace SalesReload
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var configuration = builder.Build();

            var app = new CommandLineApplication
            {
                Name = "SalesReload", 
                Description = "Satış bilgilerini RETAIL den Prim Sistemi'ne tekrar yükler."
            };

            app.HelpOption("-?|-h|--help");
            
            app.ExtendedHelpText = "Bağlantı bilgileri appsettings.json içindedir. \n" +
                                   "explain ile ne yapılacağını görebilirsiniz.";

            app.OnExecute(() =>
            {
                app.ShowHelp();
                return 0;
            });

            app.Command("reload", (command) =>
            {
                command.Description = "Satış verisini yeniden yükler.";
                command.ExtendedHelpText = "Parametrelere uyan LocalDB içindeki eski kayıtlar silinip \n" +
                                           "REATIL'den yeniden yüklenir.\n" +
                                           "Önemli: Tüm opsiyonlar doldurulmalıdır.";
                command.HelpOption("-?|-h|--help");

                var optStartDate = command.Option("--start <YYYYMMDD>",
                    "Başlangıç tarihi (dahil)",
                    CommandOptionType.SingleValue);
                var optEndDate = command.Option("--end <YYYYMMDD>",
                    "Bitiş tarihi (dahil)",
                    CommandOptionType.SingleValue);
                var optStoreId = command.Option("--store <#####>",
                    "Mağaza Id, tüm mağazalar için 0",
                    CommandOptionType.SingleValue);
                var optBrandId = command.Option("--brand <##>",
                    "Marka Id",
                    CommandOptionType.SingleValue);
                var optKeep = command.Option("--keep",
                    "Eski veriyi tut.",
                    CommandOptionType.NoValue);
                var optBatch = command.Option("--batch|--yes|--i-know-what-prameters-causes",
                    "Sorgusuz sualsiz çalış, biliyorum ne yaptığımı.",
                    CommandOptionType.NoValue);

                command.OnExecute(() =>
                {
                    CheckParameters(optBatch, optKeep, optBrandId, optStoreId, optStartDate, optEndDate);
                    if (!optBatch.HasValue())
                    {
                        Console.WriteLine("Geri dönülemeyecek noktadayız,");
                        Console.WriteLine("Parametrelerden emin misiniz?");
                        Console.Write("Evet yazın, onaylayın: ");
                        var key =  Console.ReadLine();
                        if (key != "Evet")
                        {
                            Console.WriteLine("Vazgeçildi, çıkıyoruz.");
                            return 0;
                        }
                    }
                    if (optKeep.HasValue())
                    {
                        Console.WriteLine("Eski veriler tutulacak.");
                    }
                    else
                    {
                        Console.WriteLine("Eski veriler silinecek.");
                        //var silinecekVeri = 0;
                    }

                    configuration.GetConnectionString("retail");
                    configuration.GetConnectionString("localdb");
                    Console.WriteLine(configuration.GetSection("Yazar")["Adı"]);
                    Console.WriteLine(command.Name);
                    return 0;
                });
            });
            
            app.Command("explain", (command) =>
            {
                command.Description = "İşlemi açıklar";
                command.ExtendedHelpText = "Yapılacak işlemi detaylı bilgilerle açıklar.";
                command.HelpOption("-?|-h|--help");

                var optStartDate = command.Option("--start <YYYYMMDD>",
                    "Başlangıç tarihi (dahil)",
                    CommandOptionType.SingleValue);
                var optEndDate = command.Option("--end <YYYYMMDD>",
                    "Bitiş tarihi (dahil)",
                    CommandOptionType.SingleValue);
                var optStoreId = command.Option("--store <#####>",
                    "Mağaza Id, tüm mağazalar için 0",
                    CommandOptionType.SingleValue);
                var optBrandId = command.Option("--brand <##>",
                    "Marka Id",
                    CommandOptionType.SingleValue);
                var optKeep = command.Option("--keep",
                    "Eski veriyi tut.",
                    CommandOptionType.NoValue);
                var optBatch = command.Option("--batch|--yes|--i-know-what-prameters-causes",
                    "Sorgusuz sualsiz çalış, biliyorum ne yaptığımı.",
                    CommandOptionType.NoValue);

                command.OnExecute(() =>
                {
                    if (optBatch.HasValue())
                    {
                        Console.WriteLine("0- Programı kullanmayı biliyorum, parametrelerden eminim.");
                    }
                    else
                    {
                        Console.WriteLine("0- Adım adım onaylıyayım.");
                    }

                    if (optKeep.HasValue())
                    {
                        Console.WriteLine("1- Eski veriler tutulacak.");
                        Console.WriteLine("1a- Hesaplama hatasına sebep olabilir.");
                    }
                    else
                    {
                        Console.WriteLine("1- Eski veriler silinecek.");
                    }
                    Console.WriteLine($"2- İşlem tarih aralığı {optStartDate.Value()}-{optEndDate.Value()}");
                    try
                    {
                        DateTime.ParseExact(optStartDate.Value(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("2a- Başlangıç tarihi anlaşılmadı.");
                    }
                    try
                    {
                        DateTime.ParseExact(optEndDate.Value(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("2a- Bitiş tarihi anlaşılmadı.");
                    }

                    if (int.TryParse(optBrandId.Value(), out var i))
                    {
                        Console.WriteLine($"3- MarkaId: {i}");
                        Console.WriteLine("3a- Marka adı: ");
                        //Marka adı da şu
                    }
                    else
                    {
                        Console.WriteLine("3a- MarkaId anlaşılmadı.");
                    }
                    if (int.TryParse(optStoreId.Value(), out i))
                    {
                        Console.WriteLine($"4- Mağaza Id: {i}");
                        if (i == 0)
                        {
                            Console.WriteLine("4a- Tüm mağazalar");
                        }
                        else
                        {
                            Console.WriteLine("4a- Mağaza Adı:");
                        }
                    }
                    else
                    {
                        Console.WriteLine("4a- Mağaza Id anlaşılmadı.");
                    }
                    
                    Console.WriteLine("5- İşlem.");
                    if (!optKeep.HasValue())
                    {
                        Console.WriteLine("5a- etkilenen kayıt sayısı: 0");
                    }
                    return 0;
                });
            });

            try
            {
                app.Execute(args);
            }
            catch (CommandParsingException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static bool CheckParameters(CommandOption optBatch, CommandOption optKeep, CommandOption optBrandId,
            CommandOption optStoreId, CommandOption optStartDate, CommandOption optEndDate)
        {
            
            return true;
        }
    }
}