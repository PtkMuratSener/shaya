CREATE view VEmployeeBonuses as
  select B.Id                  AS Id,
         B2.Name               AS Brand,
         B2.Id                 AS BrandId,
         S.Name                AS Store,
         B.StoreId             AS StoreId,
         P.Name                AS Position,
         B.PositionId          AS PositionId,
         E.Name                AS Name,
         E.Surname             AS Surname,
         B.EmployeeSicil       AS Sicil,
         B.Amount              AS Bonus,
         B.Notes               AS Notes,
         B.Final               AS Final,
         B.Year                AS Year,
         B.Month               AS Month,
         B.Actual              AS Actual,
         isnull(M.Achieved, 0) AS ActualBrut,
         isnull(S2.Pmst, 0)    AS Target,
         IIF(B.Amount>0, 'Alır', 'Almaz')  AS BonusState,
         CAST(1.0 AS FLOAT) AS BonusRate,
         CAST(B.Actual AS FLOAT) AS MonthlyTotal,
         30 AS BonusTotalDays,
         0 AS BonusFreeDays,
         30 AS BonusPayDays,
         ' ' AS Discipline
  from (((((EmployeeBonuses B
    left join Stores S on ((S.Id =B.StoreId)))
    left join Brands B2 on ( B.BrandId = B2.Id)))
    left join Positions P on ((B.PositionId =P.Id)))
    left join Employees E on ((B.EmployeeSicil = E.Sicil)))
         left join MonthlyAchieveds M on ((
      (M.Sicil = B.EmployeeSicil) and (M.Year = B.Year) and (M.Month = B.Month)))
         LEFT JOIN StorePositions S2 ON
    (P.Id = S2.PositionId AND S2.Year=M.Year AND S2.Month=M.Month AND S2.StoreId=S.Id)
  UNION ALL
  select B.Id                  AS Id,
         B2.Name               AS Brand,
         B2.Id                 AS BrandId,
         S.Name                AS Store,
         B.StoreId             AS StoreId,
         P.Name                AS Position,
         B.PositionId          AS PositionId,
         E.Name                AS Name,
         E.Surname             AS Surname,
         B.Sicil               AS Sicil,
         B.Amount              AS Bonus,
         B.Notes               AS Notes,
         B.Final               AS Final,
         B.Year                AS Year,
         B.Month               AS Month,
         0 AS Actual,
         0 AS ActualBrut,
         0  AS Target,
         IIF(B.Amount>0, 'Alır', 'Almaz')  AS BonusState,
         r.Rate AS BonusRate,
         CAST(B.Salary AS FLOAT) AS MonthlyTotal,
         CASE WHEN B.AmountT = 0 THEN 30 ELSE
           DAY(EOMONTH(DATEFROMPARTS(B.Year, B.Month, 1) ))
           END AS BonusTotalDays,
         CAST(B.AmountO AS INT) AS BonusFreeDays,
         CAST(ISNULL((DATEDIFF(day, CONVERT(DATE, B.WorkStart), CONVERT(DATE, B.WorkEnd))+1), 0)-B.AmountO AS INT) AS BonusPayDays,
         B.CezaNotes AS Discipline
  from (((((BonusSltMonthly B
    left join Stores S on ((S.Id =B.StoreId)))
    left join Brands B2 on ( B.BrandId = B2.Id)))
    left join Positions P on ((B.PositionId =P.Id)))
    left join Employees E on ((B.Sicil = E.Sicil)))
         left join BonusRatesSlt r on (r.BrandId=B.BrandId AND r.PositionId=B.PositionId)
  GO

create procedure Fill_Next_Month_Calculations( 
    @year INT,
    @month INT)
  AS
    DELETE FROM [EmployeeBonuses] WHERE [Year] = @year AND [Month] = @month;
    INSERT INTO EmployeeBonuses
        (BrandId, StoreId, PositionId, CalculationId, EmployeeSicil,  Year, Month, Calculated, Final, Pmst, Amount,
         Notes,StartDay,EndDay,Rate)
    SELECT
           B.Id, S.Id, E.PositionId, 0, E.Sicil, @year, @month,
           0, 0 , 0, 0, 'Hesaplanmadı.',1,30, 1
    FROM Employees E
           LEFT JOIN Stores S ON E.StoreId = S.Id
           LEFT JOIN Brands B ON S.BrandId = B.Id
           LEFT JOIN Positions P ON P.Id = E.PositionId
    WHERE P.HasBonus=1 AND E.Active=1 ;

    DELETE FROM MonthlyAchieveds WHERE Year=@year AND Month=@month;
    INSERT INTO MonthlyAchieveds (Sicil, Year, Month, Target, Achieved)
    SELECT E.EmployeeSicil, E.Year, E.Month, S.Pmst, 0
    FROM
         EmployeeBonuses E
           LEFT JOIN StorePositions S
             ON( E.PositionId=S.PositionId
                   AND S.Year=E.Year
                   AND S.Month=E.Month
                   AND S.StoreId=E.StoreId
                 )
    WHERE E.Year = @year AND E.Month = @month;
go



