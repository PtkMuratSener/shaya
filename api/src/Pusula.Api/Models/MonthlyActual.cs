using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Aylık ulaşılan hedef
    /// </summary>
    [DataContract]
    public class MonthlyActual
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name = "Brand")]
        public string Brand { get; set; }
        
        /// <summary>
        /// Marka Id
        /// </summary>
        [DataMember(Name = "BrandId")]
        public int BrandId { get;  set; }
        
        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name = "Store")]
        public string Store { get;  set; }
        
        /// <summary>
        /// MAğaza Id
        /// </summary>
        [DataMember(Name = "StoreId")]
        public int StoreId { get;  set; }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        [DataMember(Name = "Position")]
        public string Position { get;  set; }
        
        /// <summary>
        /// Pozisyon Id
        /// </summary>
        [DataMember(Name = "PositionId")]
        public int PositionId { get;  set; }
        
        /// <summary>
        /// Ad soyad
        /// </summary>
        [DataMember(Name = "Namesurname")]
        public string Namesurname { get;  set; }
        
        /// <summary>
        /// Sicil
        /// </summary>
        [DataMember(Name = "Sicil")]
        public int Sicil { get;  set; }
        
        /// <summary>
        /// Yıl
        /// </summary>
        [DataMember(Name = "Year")]
        public int Year { get;  set; }
        
        /// <summary>
        /// Ay
        /// </summary>
        [DataMember(Name = "Month")]
        public int Month { get;  set; }
        
        /// <summary>
        /// Hedef
        /// </summary>
        [DataMember(Name = "Target")]
        public decimal Target { get;  set; }
        
        /// <summary>
        /// Gerçekleşen
        /// </summary>
        [DataMember(Name = "Actual")]
        public decimal Actual { get;  set; }
        
        /// <summary>
        /// Oran
        /// </summary>
        [DataMember(Name = "Rate")]
        public decimal Rate { get;  set; }
    }
}