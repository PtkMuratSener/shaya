using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{

    /// <summary>
    /// Rolün yetkili olduğu markalar bağı 
    /// </summary>
    [DataContract]
    public class RoleBrand
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Rol
        /// </summary>
        [DataMember(Name = "Role")]
        public Role Role { get; set; }

        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name = "Brand")]
        public Brand Brand { get; set; }
    }
}