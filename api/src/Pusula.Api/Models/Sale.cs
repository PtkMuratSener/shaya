﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Sale
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int? Id { get; set; }

        /// <summary>
        /// Satış yapılan mağaza
        /// </summary>
        [DataMember(Name = "Store")]
        public Store Store { get; set; }

        /// <summary>
        /// Satış tarihi
        /// </summary>
        [DataMember(Name = "SaleDate")]
        public DateTime SaleDate { get; set; }
        
        /// <summary>
        /// Satış yapan personel
        /// </summary>
        [DataMember(Name = "Employee")]
        public Employee Employee { get; set; }
        
        /// <summary>
        /// Satış yapan personel pozisyonu
        /// </summary>
        [DataMember(Name = "EmployeePosition")]
        public string EmployeePosition { get; set; }
        
        /// <summary>
        /// Satış adedi
        /// </summary>
        [DataMember(Name = "Quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Satış tutarı
        /// </summary>
        [DataMember(Name = "Amount")]
        public double Amount { get; set; }
    }
}