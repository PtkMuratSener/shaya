﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Formula
    {
        /// <summary>
        /// Otomatik anahtar
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int? Id { get; set; }

        /// <summary>
        /// Formül adı
        /// </summary>
        [Required(ErrorMessage = "Name is required")]
        [StringLength(60, ErrorMessage = "Name can't be longer than 60 characters")]
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Formül tipi
        /// </summary>
        [Required(ErrorMessage = "FormulaType is required")]
        [StringLength(60, ErrorMessage = "FormulaType can't be longer than 60 characters")]
        [DataMember(Name = "FormulaType")]
        public string FormulaType { get; set; }

        /// <summary>
        /// Formül içeriği
        /// </summary>
        [Required(ErrorMessage = "Content is required")]
        [DataMember(Name = "Content")]
        public string Content { get; set; }
    }
}