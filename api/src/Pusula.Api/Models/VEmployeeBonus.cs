using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Personel bonus hesapları
    /// </summary>
    [DataContract]
    public class VEmployeeBonus
    {
     
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [DataMember(Name="Id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Marka ID
        /// </summary>
        [DataMember(Name="BrandId")]
        public int BrandId { get; set; }

        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="Brand")]
        public string Brand { get; set; }

        
        /// <summary>
        /// Mağaza Id
        /// </summary>
        [DataMember(Name="StoreId")]
        public int StoreId { get; set; }
        
        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name="Store")]
        public string Store { get; set; }
        
        /// <summary>
        /// Pozisyon Id
        /// </summary>
        [DataMember(Name="PositionId")]
        public int PositionId { get; set; }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        [DataMember(Name="Position")]
        public string Position { get; set; }
        
        /// <summary>
        /// Personel Adı
        /// </summary>
        [DataMember(Name="Name")]
        public string Name { get; set; }
        
        /// <summary>
        /// Personel Soyadı
        /// </summary>
        [DataMember(Name="Surname")]
        public string Surname { get; set; }
        
        /// <summary>
        /// Personel Sicil
        /// </summary>
        [DataMember(Name="Sicil")]
        public int Sicil { get; set; }
        
        /// <summary>
        /// Hesaplanmış bonus miktarı
        /// </summary>
        [DataMember(Name="Bonus")]
        public double Bonus { get; set; }
        
        /// <summary>
        /// İşlem notları
        /// </summary>
        [DataMember(Name="Notes")]
        public string Notes { get; set; }
      
        /// <summary>
        /// Draft mı Final mi?
        /// </summary>
        [DataMember(Name="Final")]
        public bool Final { get; set; }
        
        /// <summary>
        /// Hesaplamanın yılı
        /// </summary>
        [DataMember(Name="Year")]
        public int Year { get; set; }
        
        /// <summary>
        /// Hesaplama Ayı
        /// </summary>
        [DataMember(Name="Month")]
        public int Month { get; set; }
        
        /// <summary>
        /// Personel satışı
        /// </summary>
        [DataMember(Name="Actual")]
        public int Actual { get; set; }

        /// <summary>
        /// Personel satışı - brüt
        /// </summary>
        [DataMember(Name="ActualBrut")]
        public decimal ActualBrut { get; set; }
        
        /// <summary>
        /// Personel hedefi
        /// </summary>
        [DataMember(Name="Target")]
        public int Target { get; set; }
        
        /// <summary>
        /// Prim alma durumu
        /// </summary>
        [DataMember(Name="BonusState")]
        public string BonusState { get; set; }
        
        /// <summary>
        /// Prim oranı
        /// </summary>
        [DataMember(Name="BonusRate")]
        public double BonusRate { get; set; }
        
        /// <summary>
        /// Aylık Satış Tutarı
        /// </summary>
        [DataMember(Name="MonthlyTotal")]
        public double MonthlyTotal { get; set; }
        
        /// <summary>
        /// Prim Gün Sayısı
        /// </summary>
        [DataMember(Name="BonusTotalDays")]
        public int BonusTotalDays { get; set; }
        
        /// <summary>
        /// Kesilecek Rapor/Ücretsiz İzin Gün Sayısı
        /// </summary>
        [DataMember(Name="BonusFreeDays")]
        public int BonusFreeDays { get; set; }
        
        /// <summary>
        /// Prim Ödenecek Gün Sayısı
        /// </summary>
        [DataMember(Name="BonusPayDays")]
        public int BonusPayDays { get; set; }
        
        /// <summary>
        /// Disiplin Ceza Bilgisi
        /// </summary>
        [DataMember(Name="Discipline")]
        public string Discipline { get; set; }
        
        /// <summary>
        /// Prim Tipi (1-Victorias, 2-Shaya)
        /// </summary>
        [DataMember(Name="PrimType")]
        public string PrimType { get; set; }
    }
}