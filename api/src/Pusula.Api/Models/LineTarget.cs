using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Line Hedefi
    /// </summary>
    [DataContract]
    public class LineTarget
    {
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name = "Store", IsRequired = true)]
        public Store Store { get; set; }

        /// <summary>
        /// Yıl
        /// </summary>
        [DataMember(Name="Year", IsRequired = true)]
        public int Year { get; set; }
        
        /// <summary>
        /// Line Adı
        /// </summary>
        [DataMember(Name="LineName", IsRequired = true)]
        [StringLength(64)]
        public string LineName { get; set; }
        
        /// <summary>
        /// Actual Ocak
        /// </summary>
        [DataMember(Name = "A01")]
        public int A01 { get; set; }
        
        /// <summary>
        /// Actual Şubat
        /// </summary>
        [DataMember(Name = "A02")]
        public int A02 { get; set; }
        
        /// <summary>
        /// Actual 
        /// </summary>
        [DataMember(Name = "A03")]
        public int A03 { get; set; }
        
        /// <summary>
        /// Actual Nisan
        /// </summary>
        [DataMember(Name = "A04")]
        public int A04 { get; set; }
        
        /// <summary>
        /// Actual Mayıs
        /// </summary>
        [DataMember(Name = "A05")]
        public int A05 { get; set; }
        
        /// <summary>
        /// Actual Haziran
        /// </summary>
        [DataMember(Name = "A06")]
        public int A06 { get; set; }
        
        /// <summary>
        /// Actual Temmuz
        /// </summary>
        [DataMember(Name = "A07")]
        public int A07 { get; set; }
        
        /// <summary>
        /// Actual Ağustos
        /// </summary>
        [DataMember(Name = "A08")]
        public int A08 { get; set; }
        
        /// <summary>
        /// Actual Eylül
        /// </summary>
        [DataMember(Name = "A09")]
        public int A09 { get; set; }
        
        /// <summary>
        /// Actual Ekim
        /// </summary>
        [DataMember(Name = "A10")]
        public int A10 { get; set; }
        
        /// <summary>
        /// Actual Kasım
        /// </summary>
        [DataMember(Name = "A11")]
        public int A11 { get; set; }
        
        /// <summary>
        /// Actual Aralık
        /// </summary>
        [DataMember(Name = "A12")]
        public int A12 { get; set; }

        /// <summary>
        /// Target Ocak
        /// </summary>
        [DataMember(Name = "T01")]
        public int T01 { get; set; }
        
        /// <summary>
        /// Target Şubat
        /// </summary>
        [DataMember(Name = "T02")]
        public int T02 { get; set; }
        
        /// <summary>
        /// Target 
        /// </summary>
        [DataMember(Name = "T03")]
        public int T03 { get; set; }
        
        /// <summary>
        /// Target Nisan
        /// </summary>
        [DataMember(Name = "T04")]
        public int T04 { get; set; }
        
        /// <summary>
        /// Target Mayıs
        /// </summary>
        [DataMember(Name = "T05")]
        public int T05 { get; set; }
        
        /// <summary>
        /// Target Haziran
        /// </summary>
        [DataMember(Name = "T06")]
        public int T06 { get; set; }
        
        /// <summary>
        /// Target Temmuz
        /// </summary>
        [DataMember(Name = "T07")]
        public int T07 { get; set; }
        
        /// <summary>
        /// Target Ağustos
        /// </summary>
        [DataMember(Name = "T08")]
        public int T08 { get; set; }
        
        /// <summary>
        /// Target Eylül
        /// </summary>
        [DataMember(Name = "T09")]
        public int T09 { get; set; }
        
        /// <summary>
        /// Target Ekim
        /// </summary>
        [DataMember(Name = "T10")]
        public int T10 { get; set; }
        
        /// <summary>
        /// Target Kasım
        /// </summary>
        [DataMember(Name = "T11")]
        public int T11 { get; set; }
        
        /// <summary>
        /// Target Aralık
        /// </summary>
        [DataMember(Name = "T12")]
        public int T12 { get; set; }

    }
}
