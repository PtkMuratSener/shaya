/*
 * Shaya Services
 *
 */

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Employee
    {
        /// <summary>
        /// Personel Sicili
        /// </summary>
        /// <value>Sicil</value>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataMember(Name="Sicil")]
        public int Sicil { get; set; }

        /// <summary>
        /// Personel Adı
        /// </summary>
        [DataMember(Name="Name")]
        public string Name { get; set; }

        /// <summary>
        /// Personel soyadı
        /// </summary>
        [DataMember(Name="Surname")]
        public string Surname { get; set; }
        
        /// <summary>
        /// Pesonel ünvanı
        /// </summary>
        [DataMember(Name="Postion")]
        public Position Position { get; set; }

        /// <summary>
        /// Personel Görev Kodu
        /// </summary>
        [DataMember(Name="Gorev")]
        public int Gorev { get; set; }
        
        /// <summary>
        /// Personel Görev Metni
        /// </summary>
        [DataMember(Name="GorevText")]
        public string GorevText { get; set; }
        
        /// <summary>
        /// İşe başlama tarihi
        /// </summary>
        [DataMember(Name="StartDate")]
        public string StartDate { get; set; }
        
        /// <summary>
        /// Ldap login adı
        /// </summary>
        [DataMember(Name="LdapUsername")]
        public string LdapUsername { get; set; }
        
        /// <summary>
        /// ISYERI_MASRAFMR
        /// </summary>
        [DataMember(Name="Store")]
        public Store Store { get; set; }

        /// <summary>
        /// Kıdem ayı
        /// </summary>
        [DataMember(Name="Kidem")]
        public double Kidem { get; set; }

        /// <summary>
        /// Son 6 aydaki kınama cezası adedi
        /// </summary>
        [DataMember(Name="Ceza")]
        public int Ceza { get; set; }
        
        /// <summary>
        /// Çalışıyor mu? 
        /// CALISMA_DURUMU_KOD == '1'
        /// </summary>
        [DataMember(Name="Active")]
        public bool Active { get; set; }

        /// <summary>
        /// Personel Alanı (M)erkez/(S)aha
        /// </summary>
        [DataMember(Name="Area")]
        [StringLength(1)]
        public string Area { get; set; }

        /// <summary>
        /// Ünvanı
        /// </summary>
        [DataMember(Name="Unvan")]
        [MaxLength(255)]
        public string Unvan { get; set; }
        
        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{this.Name} {this.Surname} [{this.Sicil}]";
        }
    }
}
