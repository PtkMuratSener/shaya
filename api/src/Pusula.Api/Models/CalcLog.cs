using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Hesaplama logu
    /// </summary>
    [DataContract]
    [Table("CalcLog")]
    public class CalcLog
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "id")]
        public int? Id { get; set; }

        /// <summary>
        /// Sicil
        /// </summary>
        [DataMember(Name = "sicil")]
        public int Sicil { get; set; }

        /// <summary>
        /// İşlem detayı
        /// </summary>
        [DataMember(Name = "action")]
        public string Action { get; set; }
        
        /// <summary>
        /// Hesaplama id
        /// </summary>
        [DataMember(Name = "calcid")]
        public int CalcId { get; set; }
        
        /// <summary>
        /// İşlem tipi
        /// </summary>
        [DataMember(Name = "tip")]
        [StringLength(50)]
        public string Tip { get; set; }

        /// <summary>
        /// İşlem sıra no
        /// </summary>
        [DataMember(Name = "sira")]
        public int Sira { get; set; }
    }
}