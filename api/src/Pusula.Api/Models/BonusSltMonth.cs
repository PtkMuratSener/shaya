﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// SLT Aylık prim
    /// </summary>
    [DataContract]
    public class BonusSltMonth
    {
     
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [DataMember(Name="Id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Kaynak hesaplama
        /// </summary>
        [DataMember(Name="Calculation")]
        public Calculation Calculation { get; set; }
        
        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="Brand")]
        public Brand Brand { get; set; }
        
        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name="Store")]
        public Store Store { get; set; }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        [DataMember(Name="Position")]
        public Position Position { get; set; }
        
        /// <summary>
        /// Personel
        /// </summary>
        [DataMember(Name="Employee")]
        [ForeignKey("Sicil")]
        public Employee Employee { get; set; }

        /// <summary>
        /// Personel aktif çalışıyor mu?
        /// Bu alan Ay sonunda çalışacak job ile güncellenecek,
        /// ilk eklendiğinde True
        /// </summary>
        [DataMember(Name="Active")]
        public bool Active { get; set; }
        
        /// <summary>
        /// Draft mı Final mi?
        /// </summary>
        [DataMember(Name="Final")]
        public bool Final { get; set; }

        /// <summary>
        /// Mevsimlik
        /// </summary>
        [DataMember(Name="Seasonal")]
        public bool Seasonal { get; set; }
        
        /// <summary>
        /// Hesaplandı mı?
        /// </summary>
        [DataMember(Name="Calculated")]
        public bool Calculated { get; set; }
        
        /// <summary>
        /// Hesaplanmış bonus miktarı
        /// </summary>
        [DataMember(Name="Amount")]
        public double Amount { get; set; }
        
        /// <summary>
        /// Hesaplamanın yılı
        /// </summary>
        [DataMember(Name="Year")]
        public int Year { get; set; }
        
        /// <summary>
        /// Hesaplama Ayı
        /// </summary>
        [DataMember(Name="Month")]
        public int Month { get; set; }
        
        /// <summary>
        /// Maaş
        /// </summary>
        [DataMember(Name="Salary")]
        public double Salary { get; set; }
        
        /// <summary>
        /// İşlem notları
        /// </summary>
        [DataMember(Name="Notes")]
        public string Notes { get; set; } 
        
        /// <summary>
        /// Ceza notları
        /// </summary>
        [DataMember(Name="CezaNotes")]
        public string CezaNotes { get; set; }

        /// <summary>
        /// Mağazada başlangıç günü (1)
        /// </summary>
        [DataMember(Name="WorkStart")]
        [StringLength(10)]
        public string WorkStart { get; set; }
        
        /// <summary>
        /// Mağazada bitiş günü (30)
        /// </summary>
        [DataMember(Name="WorkEnd")]
        [StringLength(10)]
        public string WorkEnd { get; set; }

        /// <summary>
        /// İzin gün sayısı
        /// </summary>
        [DataMember(Name="AmountO")]
        public double AmountO { get; set; }

        /// <summary>
        /// Transfer durumu (0 Normal, 1 Transfered)
        /// </summary>
        [DataMember(Name="AmountT")]
        public double AmountT { get; set; }
        
        /// <summary>
        /// Mağazafdaki çalışma günü
        /// </summary>
        [DataMember(Name = "WorkDays")]
        public int WorkDays { get; set; }
        
        /// <summary>
        /// Prim ödenecek gün sayısı
        /// </summary>
        [DataMember(Name = "PayDays")]
        public int PayDays { get; set; }
        
        /// <summary>
        /// İzinli gün sayısı
        /// </summary>
        [DataMember(Name = "FreeDays")]
        public int FreeDays { get; set; }
    }
}