using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Merkez çalışan unvanı için prim kontrol koşulları
    /// </summary>
    [DataContract]
    public class HqBonusLine
    {
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Ünite
        /// </summary>
        [DataMember(Name = "Unit")]
        public int Unit { get; set; }

        /// <summary>
        /// Ünite adı
        /// </summary>
        [DataMember(Name = "UnitStr")]
        [StringLength(255)]
        public string UnitStr { get; set; }

        /// <summary>
        /// Unvan
        /// </summary>
        [DataMember(Name = "Title")]
        public int Title { get; set; }

        /// <summary>
        /// Unvan adı
        /// </summary>
        [DataMember(Name = "TitleStr")]
        [StringLength(255)]
        public string TitleStr { get; set; }

        /// <summary>
        /// Line
        /// </summary>
        [DataMember(Name = "Ln1")]
        [StringLength(255)]
        public string Ln1 { get; set; }

        /// <summary>
        /// Line
        /// </summary>
        [DataMember(Name = "Ln2")]
        [StringLength(255)]
        public string Ln2 { get; set; }
        
        /// <summary>
        /// Line
        /// </summary>
        [DataMember(Name = "Ln3")]
        [StringLength(255)]
        public string Ln3 { get; set; }
        
        /// <summary>
        /// LineRate1
        /// </summary>
        [DataMember(Name = "Ln1Rate")]
        public double Ln1Rate { get; set; }
        
        /// <summary>
        /// LineRate2
        /// </summary>
        [DataMember(Name = "Ln2Rate")]
        public double Ln2Rate { get; set; }
        
        /// <summary>
        /// LineRate3
        /// </summary>
        [DataMember(Name = "Ln3Rate")]
        public double Ln3Rate { get; set; }
    }
}
