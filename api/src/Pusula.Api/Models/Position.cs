using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Position
    { 
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [DataMember(Name="Id")]
        public int Id { get; set; }

        /// <summary>
        /// Pozisyon adı
        /// </summary>
        [DataMember(Name="Name")]
        public string Name { get; set; }
        
        /// <summary>
        /// Pozisyon için prim hesaplanacak mı? 
        /// </summary>
        [DataMember(Name="HasBonus")]
        public bool HasBonus { get; set; }

        /// <summary>
        /// Pozisyon için aylık prim hesaplanacak mı? 
        /// </summary>
        [DataMember(Name="HasMonthlyBonus")]
        public bool HasMonthlyBonus { get; set; }
    }
}
