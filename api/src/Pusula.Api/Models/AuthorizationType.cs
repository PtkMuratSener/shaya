using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Yetkilendirmeye esas yetki tipi
    /// </summary>
    public class AuthorizationType
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "Name")]
        [StringLength(128)]
        public string Name { get; set; }

        /// <summary>
        /// Eklenme Tarihi
        /// </summary>
        [DataMember(Name = "CreatedOn")]
        public DateTime CreatedOn { get; set; }
    }
}