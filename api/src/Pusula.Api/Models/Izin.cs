using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// İzin tarihleri
    /// </summary>
    [DataContract]
    public class Izin
    { 
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [DataMember(Name="Id")]
        public int Id { get; set; }

        
        /// <summary>
        /// Periyot
        /// </summary>
        [DataMember(Name="Periyot")]
        public int Periyot { get; set; }
        
        /// <summary>
        /// Personel sicili
        /// </summary>
        [DataMember(Name="Sicil")]
        public int Sicil { get; set; }
        
        /// <summary>
        /// İzin tipi
        /// </summary>
        [DataMember(Name="Tip")]
        public int Tip { get; set; }
        
        /// <summary>
        /// İzin başlangıç tarihi
        /// </summary>
        [DataMember(Name="Bastar")]
        public int Bastar { get; set; }
        
        /// <summary>
        /// İzin bitiş tarihi
        /// </summary>
        [DataMember(Name="Bittar")]
        public int Bittar { get; set; }
    }
}
