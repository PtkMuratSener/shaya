using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Sisteme yüklenen dosya
    /// </summary>
    [DataContract]
    public class UploadedFile
    {
        /// <summary>
        /// Otomatik ID
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int? Id { get; set; }
        
        /// <summary>
        /// Sahip Sicil
        /// </summary>
        [DataMember(Name = "Owner")]
        public int Owner { get; set; }
        
        /// <summary>
        /// Dosya adı
        /// </summary>
        [MaxLength(255)]
        [DataMember(Name = "FileName")]
        public string FileName { get; set; }
        
        /// <summary>
        /// Dosya yüklenme zamanı
        /// </summary>
        [DataMember(Name = "Date")]
        public DateTime Date { get; set; }
        
        /// <summary>
        /// Dosya Tipi
        /// </summary>
        [MaxLength(64)]
        [DataMember(Name = "Type")]
        public string Type { get; set; }
        
        /// <summary>
        /// İçerik (binary)
        /// </summary>
        [DataMember(Name = "Content")]
        public byte[] Content { get; set; }
        
        /// <summary>
        /// Info
        /// </summary>
        [DataMember(Name = "Info")]
        public string Info { get; set; }
        
    }
}