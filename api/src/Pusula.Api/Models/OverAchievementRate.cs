using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
        /// <summary>
        /// Bütçe üstü karlılık prim oranları
        /// </summary>
        [DataContract]
        public class OverAchievementRate
        {
                /// <summary>
                /// Otomatik Id
                /// </summary>
                [DataMember(Name = "Id")]
                public int Id { get; set; }

                /// <summary>
                /// Etiket
                /// </summary>
                [DataMember(Name = "Label")]
                [StringLength(10)]
                public string Label { get; set; }

                /// <summary>
                /// Prim oranı 12 aydan çok
                /// </summary>
                [DataMember(Name = "Rate12More")]
                public double Rate12More { get; set; }

                /// <summary>
                /// Prim oranı 3-12 ay arası
                /// </summary>
                [DataMember(Name = "Rate12Less")]
                public double Rate12Less { get; set; }
        }
}