using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Olay
    /// </summary>
    [DataContract]
    public class EventLog
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Olay Tipi
        /// </summary>
        [DataMember(Name = "Type")]
        public string Type { get; set; }

        /// <summary>
        /// Veri
        /// </summary>
        [DataMember(Name = "Payload")]
        public string Payload { get; set; }

        /// <summary>
        /// İşlemi yapan kullanıcı
        /// </summary>
        [DataMember(Name = "User")] 
        public User User { get; set; }

        /// <summary>
        /// Olay Zamanı
        /// </summary>
        [DataMember(Name = "EventOn")] 
        public DateTime EventOn { get; set; }


    }
}