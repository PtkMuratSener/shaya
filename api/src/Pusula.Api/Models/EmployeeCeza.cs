using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class EmployeeCeza
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int? Id { get; set; }

        /// <summary>
        /// Ceza kodu
        /// </summary>
        [DataMember(Name = "Code")]
        [StringLength(2)]
        public string Code { get; set; }

        /// <summary>
        /// Cezanın ait olduğu personel
        /// </summary>
        [DataMember(Name = "Sicil")]
        public int Sicil { get; set; }

        /// <summary>
        /// Ceza tarihi
        /// </summary>
        [DataMember(Name = "Date")]
        [StringLength(10)]
        public DateTime Date { get; set; }
    }
}
