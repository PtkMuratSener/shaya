﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Proje kullanıcısı
    /// </summary>
    [DataContract]
    public class User
    {
        /// <summary>
        /// Id, otomatik.
        /// </summary>
        [Key]
        public int? Id { get; set; }

        /// <summary>
        /// Kullanıcı sicili, Personel tablosundan eşleşmeli
        /// </summary>
        public int Sicil { get; set; }

        /// <summary>
        /// Kullanıcının adı soyadı
        /// </summary>
        public string NameSurname { get; set; }

        /// <summary>
        /// Kullanıcının LDAP üzerindeki yetkili adı
        /// </summary>
        public string LdapUsername { get; set; }

        /// <summary>
        /// Kullanıcı oturum anahtarı?
        /// </summary>
        public string SessionKey { get; set; }

        /// <summary>
        /// Oturum geçerlilik sonu
        /// </summary>
        public long? SessionValidTil { get; set; }

        /// <summary>
        /// Fotoğraf (varsa)
        /// </summary>
        public string Photo { get; set; }
        
        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{this.NameSurname} [{this.Sicil}]";
        }
    }
}