using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Rolün yetkili olduğu prim tipleri
    /// </summary>
    [DataContract]
    public class RolePrimType
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Rol
        /// </summary>
        [DataMember(Name = "Role")]
        public Role Role { get; set; }

        /// <summary>
        /// Prim Tipi
        /// </summary>
        [DataMember(Name = "PrimType")]
        public PrimType PrimType { get; set; }
    }
}