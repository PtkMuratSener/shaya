﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Parametre
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int? Id { get; set; }


        /// <summary>
        /// Parametre etiketi
        /// </summary>
        [DataMember(Name = "Title")]
        [StringLength(255)]
        public string Title { get; set; }
        
        /// <summary>
        /// Parametre değeri
        /// </summary>
        [DataMember(Name = "Value")]
        [StringLength(255)]
        public string Value { get; set; }
        
        /// <summary>
        /// Açıklama
        /// </summary>
        [DataMember(Name = "Description")]
        public string Description { get; set; }
        
        /// <summary>
        /// Görünür alanlar seçilecek.
        /// </summary>
        [DataMember(Name="Visible")]
        [DefaultValue(false)]
        public bool Visible { get; set; }
    }
}