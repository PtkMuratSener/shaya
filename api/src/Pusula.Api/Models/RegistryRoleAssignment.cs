using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class RegistryRoleAssignment
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Gets or Sets RoleId
        /// </summary>
        [DataMember(Name = "Role")]
        public Role Role { get; set; }
        
        /// <summary>
        /// Personel Sicili
        /// </summary>
        /// <value>Sicil</value>
        [DataMember(Name="Sicil")]
        public int Sicil { get; set; }
        
    }
}