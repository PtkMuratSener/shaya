using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class StoreTarget
    { 
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [Required]
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// Gets or Sets Store
        /// </summary>
        [DataMember(Name="store")]
        public Store Store { get; set; }

        /// <summary>
        /// Gets or Sets Year
        /// </summary>
        [Required]
        [DataMember(Name="year")]
        public int Year { get; set; }

        /// <summary>
        /// Gets or Sets Ap01
        /// </summary>
        [DataMember(Name="month")]
        public int Month { get; set; }

        /// <summary>
        /// Tip Actual mı Hedef mi
        /// </summary>
        [DataMember(Name="isactual")]
        public bool IsActual { get; set; }

        /// <summary>
        /// Değeri
        /// </summary>
        [DataMember(Name="val")]
        public int Val { get; set; }
    }
}
