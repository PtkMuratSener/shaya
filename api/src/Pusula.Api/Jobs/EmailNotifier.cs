using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web.Mvc;
using Postal;

namespace Pusula.Api.Jobs
{
    /// <summary>
    /// Email gönderme sınıfı
    /// </summary>
    public class EmailNotifier
    {
        /// <summary>
        /// Mail sender
        /// </summary>
        public void SendEmail()
        {
            var viewsPath = Path.GetFullPath(@"../Views");

            var engines = new ViewEngineCollection {new FileSystemRazorViewEngine(viewsPath)};

            var service = new EmailService(engines);

            dynamic email = new Email("Approval");
            // Will look for Test.cshtml or Test.vbhtml in Views directory.
            email.Message = "Hello, non-asp.net world!";
            
            new EmailViewResult(email);
            
            /*
            service.Send(email);
            
            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com", // set your SMTP server name here
                Port = 587, // Port 
                EnableSsl = true,
                Credentials = new NetworkCredential("from@gmail.com", "password")
            };
            */
        }
    }
}