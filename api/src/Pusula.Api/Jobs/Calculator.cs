﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Jint;
using Jint.Parser;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Infrastructures;
using Pusula.Api.Infrastructures.Core;
using Pusula.Api.Models;


namespace Pusula.Api.Jobs
{
    /// <inheritdoc />
    public class Calculator : IDisposable
    {

        private readonly PatikaDbContext _localDbContext;
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        /// <summary>
        /// 
        /// </summary>
        /// <param name="localDbContext"></param>
        /// <param name="configuration"></param>
        public Calculator(PatikaDbContext localDbContext, IConfiguration configuration)
        {
            _localDbContext = localDbContext;
            _configuration = configuration;
        }
        
        /// <summary>
        /// Obsolete Method Schedule
        /// </summary>
        /// <returns></returns>
        [Obsolete("Schedule handled by Hangfire itself, no need to respawn")]
        public void Schedule()
        {
            //Hangfire yarıda kalmış işleri hallediyor
            //Tekrar iş başlatmaya gerek yok
            //Cronjobs listesinde olduğundan kaldırılmadı.
        }

        /// <summary>
        /// Hesaplama öncesi transfer kayıtlarını toplar.
        /// </summary>
        /// <returns></returns>
        [AutomaticRetry(Attempts = 3, OnAttemptsExceeded = AttemptsExceededAction.Fail)]
        public async Task PrecalcAsync()
        {
            // Connect to HR-IKYS_IT for Salary info
            using (var connection = new OracleConnection(_configuration.GetConnectionString("IkysIt")))
            {
                connection.Open();

                const string tempSql = @"SELECT SICIL_NO, ISYERI_MASRAFMR,
                    CASE WHEN P.BSLTRH < TO_DATE('$STARTDATE', 'YYYYMMDD') THEN
                      1
                    ELSE
                      TO_NUMBER(TO_CHAR(BSLTRH, 'DD'))
                    END AS BASGUN,
                    CASE WHEN P.BTSTRH > LAST_DAY(TO_DATE('$STARTDATE', 'YYYYMMDD')) THEN
                      TO_NUMBER(TO_CHAR(LAST_DAY(TO_DATE('$STARTDATE', 'YYYYMMDD')), 'DD'))
                    ELSE
                      TO_NUMBER(TO_CHAR(BTSTRH, 'DD'))
                    END AS BITGUN
                      FROM T_PERSONEL_HISTORY P
                    WHERE 
                        (BSLTRH BETWEEN TO_DATE('$STARTDATE', 'YYYYMMDD') AND LAST_DAY(TO_DATE('$STARTDATE', 'YYYYMMDD'))
                    OR  BTSTRH BETWEEN TO_DATE('$STARTDATE', 'YYYYMMDD') AND LAST_DAY(TO_DATE('$STARTDATE', 'YYYYMMDD')))
                    AND HAREKET_TURU='İşyeri Değişikliği'
                    AND SICIL_NO = '$SICIL'
                    ORDER BY SICIL_NO, BSLTRH";

                var calculation = _localDbContext.Calculations.FirstOrDefault(m => m.Active);
                if (calculation == null)
                {
                    return;
                }

                CalcLogFn(0, calculation.Id ?? 0, 0, "Transfer bilgileri hesaplanıyor.", CalcLogInfoType.info);
                var employees = _localDbContext.EmployeeBonuses
                    .Include(s => s.Calculation)
                    .Include(s => s.Employee)
                    .Include(s => s.Store)
                    .Include(s => s.Brand)
                    .Include(s => s.Position)
                    .Where(x => x.Calculation == calculation);
                if (employees.Any())
                {
                    foreach (var e in employees)
                    {
                        var transferSql = tempSql.Replace("$SICIL", $"{e.Employee.Sicil}");
                        transferSql = transferSql.Replace("$STARTDATE",
                            new DateTime(e.Year, e.Month, 1).ToString("yyyyMMdd"));

                        OracleCommand transferCmd = connection.CreateCommand();
                        transferCmd.CommandText = transferSql;
                        OracleDataReader transferRdr = transferCmd.ExecuteReader();


                        while (transferRdr.Read())
                        {
                            var store = Convert.ToInt32(transferRdr.GetString(1)); // ISYERI_MASRAFMR
                            var basgun = transferRdr.GetInt32(2); // BASGUN
                            var bitgun = transferRdr.GetInt32(3); // BITGUN
                            if (e.Store.Id == store)
                            {
                                e.StartDay = basgun;
                                e.EndDay = bitgun;
                            }
                            else
                            {
                                _localDbContext.EmployeeBonuses.Add(new EmployeeBonus
                                {
                                    Amount = 0,
                                    Brand = e.Brand,
                                    Calculated = false,
                                    Calculation = e.Calculation,
                                    Employee = e.Employee,
                                    Final = false,
                                    Month = e.Month,
                                    Pmst = e.Pmst,
                                    Position = e.Position,
                                    Store = _localDbContext.Stores.Find(store),
                                    Year = e.Year,
                                    Notes = "Hesaplanmadı",
                                    StartDay = basgun,
                                    EndDay = bitgun
                                });
                            }
                        }

                        transferRdr.Dispose();
                        transferCmd.Dispose();
                    }
                }

                await _localDbContext.SaveChangesAsync();
            }
        }

        /// <summary>
        /// NON-SLT Aylık Prim Hesapla
        /// </summary>
        /// <returns></returns>
        public async Task CalculateAsync()
        {
            var calcid = 0;
            try
            {
                // Connect to HR-IKYS_IT
                var oracleConnection = new OracleConnection(_configuration.GetConnectionString("IkysIt"));
                oracleConnection.Open();

                var calculation = _localDbContext.Calculations
                    .Where(m => m.Title == "NON-SLT Monthly")
                    .FirstOrDefault(m => m.Active);
                if (calculation == null)
                {
                    return;
                }

                calcid = calculation.Id??0;
                var sn = 0;
                CalcLogFn(sn, calculation.Id ?? 0, 0, "Hesaplama işlemi başladı.", CalcLogInfoType.info);
                await _localDbContext.SaveChangesAsync();
                // Henüz hesaplanmamış personelleri al
                var employees = _localDbContext.EmployeeBonuses
                    .Include(e => e.Employee).ThenInclude(e => e.Store)
                    .Include(e => e.Employee).ThenInclude(e => e.Position)
                    .Include(s => s.Store).ThenInclude(s => s.Brand)
                    .Include(s => s.Position)
                    .Where(c => c.Calculation.Id == calculation.Id)
                    .Where(a => a.Calculated == false);
                if (employees.Any())
                {
                    foreach (var emp in employees)
                    {
                        sn = 0;
                        CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, "Non-SLT hesaplama işlemi.",
                            CalcLogInfoType.info);
                        //Hesaplama Parametreleri 
                        var parametresDictionary = new Dictionary<string, double>();
                        //var ayBas = new DateTime(emp.Year, emp.Month, emp.StartDay).ToString("yyyyMMdd");
                        //var aySon = new DateTime(emp.Year, emp.Month, emp.EndDay).ToString("yyyyMMdd");
                        var ayBas = _localDbContext.Calendars.AsNoTracking()
                            .Where(y => y.Year == emp.Year)
                            .Where(m => m.Month == emp.Month)
                            .Min(b => b.Date)
                            .ToString();
                        var aySon = _localDbContext.Calendars.AsNoTracking()
                            .Where(y => y.Year == emp.Year)
                            .Where(m => m.Month == emp.Month)
                            .Max(b => b.Date)
                            .ToString();


                        CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                            $"{emp.Store} mağazasında hesaplama başlangıç tarihi {ayBas}", CalcLogInfoType.info);
                        CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                            $"{emp.Store} mağazasında hesaplama bitiş tarihi {aySon}", CalcLogInfoType.info);

                        double bonus = 0;
                        var calculateNotes = "Tamam.";
                        var calculateBonus = true;
                        var storeMonthlySaleTarget = 0;
                        var storeMonthlySaleActual = 0.0;

                        #region Mağaza aylık hedef tuttu mu

                        if (calculateBonus)
                        {
                            var str = _localDbContext.StoreTargetsRetail.AsNoTracking()
                                .Include(s => s.Store)
                                .Where(s => s.Store.Id == emp.Store.Id)
                                .Where(y => y.Year == emp.Year)
                                .OrderBy(o => o.Id)
                                .LastOrDefault();
                            if (str != null)
                            {
                                storeMonthlySaleTarget = str.GetTarget(emp.Month);
                                storeMonthlySaleActual = str.GetActual(emp.Month);
                            }

                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                $"Mağaza gerçekleşen satış tutarı: {storeMonthlySaleActual}", CalcLogInfoType.info);
                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                $"Mağaza hedeflenen satış tutarı: {storeMonthlySaleTarget}", CalcLogInfoType.info);
                            if (storeMonthlySaleActual.CompareTo(0) == 0 && storeMonthlySaleTarget == 0)
                            {
                                bonus = 0;
                                calculateBonus = false;
                                calculateNotes = "Mağaza hedef kaydı yok.";
                            }

                            if (storeMonthlySaleActual < storeMonthlySaleTarget)
                            {
                                bonus = 0;
                                calculateNotes = "Mağaza hedef kontrolü: Hedeflenen satışı yapmamış.";
                                calculateBonus = false;
                            }


                            if (calculateBonus)
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    "Mağaza hedef kontrolü: Hedeflenen satışı yapmış.",
                                    CalcLogInfoType.check_pass);
                            }
                            else
                            {
                                bonus = 0;
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, calculateNotes,
                                    CalcLogInfoType.check_fail);
                            }
                        }

                        #endregion

                        #region İndirim dönemi kontrol

                        if (calculateBonus)
                        {
                            var brandId = emp.Store.Brand.Id;
                            if (IsSasMonth(brandId, emp.Month))
                            {
                                bonus = 0;
                                calculateNotes = $"{emp.Month} periyotu indirim dönemi. Prim hesaplanmaz.";
                                calculateBonus = false;
                            }

                            if (calculateBonus)
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    $"{emp.Month} periyotu indirim dönemi değil. Prim hesaplanır.",
                                    CalcLogInfoType.check_pass);
                            }
                            else
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, calculateNotes,
                                    CalcLogInfoType.check_fail);
                            }
                        }

                        #endregion

                        #region Pozisyon için bonus var mı?


                        if (calculateBonus)
                        {
                            if (!emp.Position.HasBonus)
                            {
                                bonus = 0;
                                calculateNotes = $"{emp.Position.Name} pozisyonu için prim hesaplanmaz.";
                                calculateBonus = false;
                            }

                            if (calculateBonus)
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    $"{emp.Position.Name} pozisyonu için prim hesaplanır.",
                                    CalcLogInfoType.check_pass);
                            }
                            else
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, calculateNotes,
                                    CalcLogInfoType.check_fail);
                            }
                        }


                        #endregion

                        #region Kıdem kontrol

                        if (calculateBonus)
                        {
                            //CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                            //    $"İşbaşı tarihi prim hesaplanan dönemden 2 ay önce mi?: {emp.Employee.StartDate}",
                            //    "Info");

                            var tn = new DateTime(emp.Year, emp.Month, 1).AddMonths(-2);
                            var tb = DateTime.ParseExact(emp.Employee.StartDate, "dd.MM.yyyy",
                                CultureInfo.InvariantCulture);

                            if (tb >= tn)
                            {

                                bonus = 0;
                                calculateNotes = "Kıdemsiz.";
                                calculateBonus = false;
                            }

                            if (calculateBonus)
                            {
                                //CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, "İşbaşı kıdemi yeterli.", "Success");
                            }
                            else
                            {
                                //CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, calculateNotes, "Fail");
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    "Deneme süresindeki personele prim hesaplanıyor.", CalcLogInfoType.info); //CR:PY-92
                            }

                            calculateBonus = true;
                        }

                        #endregion

                        #region ceza var mı

                        if (calculateBonus)
                        {
                            var employeeType = "NONSLT";
                            var cezas = _localDbContext.EmployeeCezas.Where(c => c.Sicil == emp.Employee.Sicil);
                            if (cezas.Any())
                            {
                                var cezaCount = 0;
                                var currentYearPeriod = (emp.Year * 12) + emp.Month;

                                var cezaLookup = new Dictionary<string, int>();
                                var czls = _localDbContext.Cezalar.Where(c => c.Effective > 0);

                                if (employeeType == "NONSTL")
                                {
                                    //Kınama + Ünvan İndirimi
                                    czls = czls.Where(n => n.Code == "01" || n.Code == "06");
                                }

                                if (employeeType == "SLT")
                                {
                                    //Kınama + Ünvan İndirimi + Yazılı Uyarı
                                    czls = czls.Where(n => n.Code == "01" || n.Code == "06" || n.Code == "04");
                                }

                                if (czls.Any())
                                {
                                    foreach (var czl in czls)
                                    {
                                        cezaLookup.Add(czl.Code, czl.Effective);
                                    }
                                }


                                foreach (var ceza in cezas)
                                {
                                    var cza = currentYearPeriod - ((ceza.Date.Year * 12) + ceza.Date.Month);
                                    if (cza < cezaLookup[ceza.Code]) //ceza alınan ay süreye dahil 
                                    {
                                        cezaCount++;
                                        var cezaNfo = _localDbContext.Cezalar.AsNoTracking()
                                            .Where(c => c.Code == ceza.Code)
                                            .Select(z => z.Title)
                                            .FirstOrDefault();
                                        cezaNfo += ": " + ceza.Date.ToString("yyyy-mm-dd");
                                        CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                            $"Etkin ceza kaydı. {cezaNfo}",
                                            CalcLogInfoType.info);
                                    }
                                }

                                if (cezaCount > 0)
                                {
                                    bonus = 0;
                                    calculateNotes = "Disiplin cezası etkisi altında. Prim hesaplanmayacak.";
                                    calculateBonus = false;
                                }
                            }

                            if (calculateBonus)
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    "Ceza kaydı yok. Prim hesaplanacak.", CalcLogInfoType.check_pass);
                            }
                            else
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, calculateNotes,
                                    CalcLogInfoType.check_fail);
                            }

                        }

                        #endregion

                        #region Formula Type

                        //FAZ2: veritabanından alınacak.
                        var formulaType = "LBRANDS";
                        CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                            "Prim hesaplamada VS formülü kullanılacak", CalcLogInfoType.info);

                        #endregion

                        #region Pozisyon hedefi tutuyor mu

                        var positionMonthlySaleTarget = 0;
                        var employeeMonthlySaleTotal = 0;
                        var winretailStoreMonthlyPoolSale = 0.0;
                        if (calculateBonus)
                        {
                            //Winretail değerleri
                            var dateBegin =
                                DateTime.ParseExact(ayBas, "yyyyMMdd",
                                    CultureInfo.InvariantCulture);
                            var dateFin =
                                DateTime.ParseExact(aySon, "yyyyMMdd",
                                    CultureInfo.InvariantCulture); 


                            var winretailStoreMonthlySaleBrute = _localDbContext.Sales
                                .AsNoTracking()
                                .Where(e => e.Store.Id == emp.Store.Id)
                                .Where(e => e.SaleDate >= dateBegin && e.SaleDate <= dateFin)
                                .Where(e => e.Amount > 0)
                                .Sum(e => e.Amount);

                            var iadePeriyotGunu = 15;
                            var paramIadePeriyotGunu = _localDbContext.Parametres.AsNoTracking()
                                .FirstOrDefault(x => x.Title == "RefundPeriodStartDay");
                            if (paramIadePeriyotGunu != null)
                            {
                                iadePeriyotGunu = int.Parse(paramIadePeriyotGunu.Value);
                            }
                            var iadePeriyotBas = _localDbContext.Calendars.AsNoTracking()
                                .Where(y => y.Year == emp.Year)
                                .Where(m => m.Month == emp.Month)
                                .OrderBy(b => b.Date)
                                .Skip(iadePeriyotGunu-1)
                                .First();
                            var iadePeriyotSon = _localDbContext.Calendars.AsNoTracking()
                                .Where(y => y.Date > int.Parse(aySon))
                                .OrderBy(b => b.Date)
                                .Skip(iadePeriyotGunu-1)
                                .First();
                            var iadePeriyotBasDate = DateTime.ParseExact(iadePeriyotBas.Date.ToString(), "yyyyMMdd",
                                    CultureInfo.InvariantCulture);                            
                            var iadePeriyotSonDate = DateTime.ParseExact(iadePeriyotSon.Date.ToString(), "yyyyMMdd",
                                    CultureInfo.InvariantCulture);                            

                            var wrStoreMonthlyIade = _localDbContext.Sales.AsNoTracking()
                                .Where(e => e.Store.Id == emp.Store.Id)
                                .Where(e => e.SaleDate >= iadePeriyotBasDate && e.SaleDate < iadePeriyotSonDate)
                                .Where(e => e.Amount < 0)
                                .Sum(e => e.Amount);
                            storeMonthlySaleActual = winretailStoreMonthlySaleBrute + wrStoreMonthlyIade; //iade eksi olacağından topla

                            
                            
                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                $"İade periyodu: {iadePeriyotBas.Date}-{iadePeriyotSon.Date}",CalcLogInfoType.info);                              
                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                $" Mağaza brüt satış miktarı: {winretailStoreMonthlySaleBrute}",
                                CalcLogInfoType.info);
                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                $" Mağaza net satış miktarı: {storeMonthlySaleActual}", CalcLogInfoType.info);
                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                $"Mağaza iade miktarı: {wrStoreMonthlyIade}",CalcLogInfoType.info);                            

                            var employeeSaleTotal = _localDbContext.Sales
                                .AsNoTracking()
                                .Where(e => e.Employee.Sicil == emp.Employee.Sicil)
                                .Where(e => e.SaleDate >= dateBegin && e.SaleDate <= dateFin)
                                .Sum(e => e.Amount);

                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                $" Personel aylık satış miktarı: {employeeSaleTotal}", CalcLogInfoType.info);

                            //Normalize
                            employeeMonthlySaleTotal =
                                (int) Math.Round(employeeSaleTotal *
                                                 (storeMonthlySaleActual / winretailStoreMonthlySaleBrute));
                            emp.Actual = employeeMonthlySaleTotal;
                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                $" Personel normalize edilmiş aylık satış miktarı: {employeeMonthlySaleTotal}",
                                CalcLogInfoType.info);


                            var ps = _localDbContext.StorePositions.AsNoTracking()
                                //.Include(s => s.Store)
                                //.Include(s => s.Position)
                                .Where(s => s.Store.Id == emp.Store.Id)
                                .Where(s => s.Position.Id == emp.Position.Id)
                                .Where(y => y.Year == emp.Year)
                                .Where(m => m.Month == emp.Month)
                                .FirstOrDefault(a => a.Position.HasBonus); //Position Sale Target
                            if (ps != null)
                            {
                                //Transfer durumunda gittiği mağazada pozisyon yoksa kendi mağazasındaki alınacak.
                                ps = _localDbContext.StorePositions.AsNoTracking()
                                    .Where(s => s.Store.Id == emp.Employee.Store.Id)
                                    .Where(s => s.Position.Id == emp.Employee.Position.Id)
                                    .Where(y => y.Year == emp.Year)
                                    .Where(m => m.Month == emp.Month)
                                    .FirstOrDefault(a => a.Position.HasBonus);
                            }

                            if (ps != null)
                            {
                                var storeDays = (emp.EndDay - emp.StartDay) + 1;
                                var transferRate = 1.0;
                                if (storeDays < 30)
                                {
                                    transferRate = storeDays / 30.0;
                                    CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                        $" Dönem içi transfer olmuş, yeni hesaplama oranı: {transferRate}",
                                        CalcLogInfoType.info);
                                }

                                positionMonthlySaleTarget = Convert.ToInt32(ps.Pmst * transferRate);
                                formulaType = ps.FormulType;
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    $" Pozisyon satış hedefi: {positionMonthlySaleTarget}", CalcLogInfoType.info);

                                if (employeeMonthlySaleTotal < positionMonthlySaleTarget)
                                {
                                    bonus = 0;
                                    calculateNotes = "Personel satışı hedef altında. Prim hesaplanmayacak.";
                                    calculateBonus = false;
                                }

                                if (calculateBonus)
                                {
                                    CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                        "Personel satışı pozisyon hedefini geçmiş. Prim hesaplanacak",
                                        CalcLogInfoType.check_pass);
                                }
                                else
                                {
                                    CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                        calculateNotes, CalcLogInfoType.check_fail);
                                }
                            }
                        }

                        #endregion

                        #region EmployeeBonusRate

                        var empBonusRate = 0.0;
                        if (calculateBonus)
                        {
                            var bonusRates = _localDbContext.BonusRates
                                .AsNoTracking()
                                .Where(b => b.Brand.Id == emp.Brand.Id)
                                .FirstOrDefault(b => b.Position.Id == emp.Position.Id);

                            if (bonusRates == null)
                            {
                                calculateBonus = false;
                                calculateNotes = "Prim oranı girilmemiş.";
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    " Pozisyon için prim oranları girilmemiş. Prim hesaplanamaz.",
                                    CalcLogInfoType.check_fail);
                            }
                            else
                            {
                                var storeActualRate = storeMonthlySaleActual / storeMonthlySaleTarget;
                                var positionActualRate = employeeMonthlySaleTotal / (double) positionMonthlySaleTarget;
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    $" Mağaza aktif satış oranı: {storeActualRate}", "Info");
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    $" Pozisyon aktif satış oranı: {positionActualRate}", "Info");

                                if (storeActualRate >= 1.10)
                                {
                                    if (positionActualRate >= 1.10)
                                    {
                                        empBonusRate = bonusRates.P3s3;
                                    }
                                    else if (positionActualRate >= 1.05)
                                    {
                                        empBonusRate = bonusRates.P2s3;
                                    }
                                    else if (positionActualRate >= 1.0)
                                    {
                                        empBonusRate = bonusRates.P1s3;
                                    }
                                }
                                else if (storeActualRate >= 1.05)
                                {
                                    if (positionActualRate >= 1.10)
                                    {
                                        empBonusRate = bonusRates.P3s2;
                                    }
                                    else if (positionActualRate >= 1.05)
                                    {
                                        empBonusRate = bonusRates.P2s2;
                                    }
                                    else if (positionActualRate >= 1.0)
                                    {
                                        empBonusRate = bonusRates.P1s2;
                                    }
                                }
                                else if (storeActualRate >= 1.0)
                                {
                                    if (positionActualRate >= 1.10)
                                    {
                                        empBonusRate = bonusRates.P3s1;
                                    }
                                    else if (positionActualRate >= 1.05)
                                    {
                                        empBonusRate = bonusRates.P2s1;
                                    }
                                    else if (positionActualRate >= 1.0)
                                    {
                                        empBonusRate = bonusRates.P1s1;
                                    }
                                }
                            }

                            if (empBonusRate <= 0)
                            {
                                calculateBonus = false;
                                calculateNotes = "Prim alamaz.";
                            }

                            emp.Rate = empBonusRate;
                            _localDbContext.SaveChanges();
                            if (calculateBonus)
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, $"Prim oranı: {empBonusRate}",
                                    "Info");
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    "Satış miktarı içn prim oranı bulundu.", "Success");
                            }
                            else
                            {
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, calculateNotes, "Fail");
                            }
                        }

                        #endregion

                        #region İzin günleri

                        var bonusFreeDays = 0;
                        if (calculateBonus)
                        {
                            //Ücretsiz İzin, Hastalık Raporu, Doğum Raporu
                            //32	Doğum(Raporlu) İzni         Hepsi 
                            //19	Ücretsiz İzin(Merkez Ofis)  Hepsi
                            //34	Ücretsiz İzin(Saha)         Hepsi
                            //31	Hastalik(Raporlu) İzni      2 den sonrası

                            try
                            {
                                var izinSql = "SELECT IZIN_TIPI, TO_CHAR(BASLANGIC_TARIHI, 'yyyyMMdd'), " +
                                              "TO_CHAR(BITIS_TARIHI, 'yyyyMMdd') " +
                                              "FROM V_IZIN " +
                                              "WHERE IZIN_TIPI IN('19', '31', '32', '34') " +
                                              $"AND SICIL_NO='{emp.Employee.Sicil}' " +
                                              $"AND BITIS_TARIHI >= TO_DATE('{ayBas}', 'yyyyMMdd') " +
                                              $"AND BASLANGIC_TARIHI <= TO_DATE('{aySon}', 'yyyyMMdd') " +
                                              "ORDER BY 2";


                                var izinCmd = oracleConnection.CreateCommand();
                                izinCmd.CommandText = izinSql;
                                var izinRdr = izinCmd.ExecuteReader();


                                //var ayBas = Convert.ToInt32(new DateTime(employee.Year, employee.Month, 1).ToString("yyyyMMdd"));
                                //var aySon = Convert.ToInt32(new DateTime(employee.Year, employee.Month, 1).AddMonths(1).AddDays(-1).ToString("yyyyMMdd"));
                                var gunler = new Dictionary<int, bool>();
                                for (var i = 1; i < 31; i++)
                                {
                                    gunler.Add(i, false);
                                }

                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, " İzin işlemleri", "Info");
                                while (izinRdr.Read())
                                {
                                    var izinTipi = izinRdr.GetString(0);
                                    var basTar = Convert.ToInt32(izinRdr.GetString(1));
                                    var bitTar = Convert.ToInt32(izinRdr.GetString(2));

                                    CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                        $" İzin tipi: {izinTipi}, {basTar}-{bitTar}", "Info");

                                    //Ay içinde kalsın
                                    basTar = basTar < Convert.ToInt32(ayBas) ? Convert.ToInt32(ayBas) : basTar;
                                    bitTar = bitTar > Convert.ToInt32(aySon) ? Convert.ToInt32(aySon) : bitTar;

                                    CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                        $" Periyot içinde izin: {basTar}-{bitTar}", "Info");
                                    if (izinTipi == "31")
                                    {
                                        for (var i = (basTar - (Convert.ToInt32(ayBas) - 1));
                                            i <= (bitTar - (Convert.ToInt32(ayBas) - 1));
                                            i++)
                                        {
                                            gunler[i] = true;
                                        }
                                    }
                                    else
                                    {
                                        if (bitTar - basTar > 30)
                                        {
                                            //Bir ay 30 günden fazla hesaplanmıyor.
                                            bonusFreeDays += 30;
                                        }
                                        else
                                        {
                                            bonusFreeDays += bitTar - basTar;
                                        }
                                    }
                                }

                                var raporGunSablon = "";
                                var raporGun = 0;
                                foreach (var gun in gunler)
                                {
                                    if (gun.Value)
                                    {
                                        raporGunSablon += "X";
                                        raporGun += 1;
                                    }
                                    else
                                    {
                                        raporGunSablon += "-";
                                        if (raporGun > 2)
                                        {
                                            bonusFreeDays += raporGun - 2;
                                        }

                                        raporGun = 0;
                                    }
                                }

                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    $" İzin tablosu: {raporGunSablon}",
                                    "Info");

                                bonusFreeDays = (bonusFreeDays > 30) ? 30 : bonusFreeDays;
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    $"Prim hesabından düşülecek izin günü: {bonusFreeDays}", "Info");
                            }
                            catch (Exception e)
                            {
                                _localDbContext.EventLogs.Add(new EventLog
                                {
                                    EventOn = DateTime.Now,
                                    Payload = e.Message,
                                    Type = "NONSLT:IZIN"
                                });
                                _localDbContext.SaveChanges();
                            }
                        }


                        #endregion

                        if (calculateBonus)
                        {

                            #region VTden alınan sabitler

                            var employeeShrinkageRate = 0.0;
                            var bonusDays = 30;

                            var param = _localDbContext.Parametres
                                .FirstOrDefault(n => n.Title == "employee_shrinkage_rate");
                            if (param != null)
                            {
                                employeeShrinkageRate = Convert.ToDouble(param.Value);
                            }

                            param = _localDbContext.Parametres
                                .FirstOrDefault(n => n.Title == "bonus_days");
                            if (param != null)
                            {
                                bonusDays = Convert.ToInt32(param.Value);
                            }

                            #endregion

                            parametresDictionary.Add("position_monthly_sale_target", positionMonthlySaleTarget);
                            parametresDictionary.Add("employee_monthly_sale_total", employeeMonthlySaleTotal);
                            parametresDictionary.Add("store_monthly_sale_target", storeMonthlySaleTarget);
                            parametresDictionary.Add("employee_bonus_rate", empBonusRate);
                            parametresDictionary.Add("employee_shrinkage_rate", employeeShrinkageRate);
                            parametresDictionary.Add("bonus_day", bonusDays);
                            parametresDictionary.Add("bonus_free_day", bonusFreeDays);
                            parametresDictionary.Add("employee_salary", 0);
                            parametresDictionary.Add("pool_sale_amount", winretailStoreMonthlyPoolSale);

                            //CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, "Hesaplama Betiği:", "Info");
                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, "Hesaplama verileri",
                                CalcLogInfoType.info);

                            var engine = new Engine();
                            var formulaTemp = new FormulaTemplate();
                            foreach (var parametre in formulaTemp.Parametres)
                            {
                                // template içindeki bütün parametreler içi değerler dict içine atılması lazım.
                                engine.SetValue(parametre.Key, parametresDictionary[parametre.Key]);
                                CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil,
                                    $" {parametre.Key} = {parametresDictionary[parametre.Key]}", CalcLogInfoType.info);
                            }

                            var formulas = _localDbContext.Formulas.AsNoTracking()
                                .FirstOrDefault(f => f.FormulaType == formulaType);
                            var bonusFormula = "0";
                            if (formulas != null)
                            {
                                bonusFormula = formulas.Content;
                            }

                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, $"Prim formülü: {bonusFormula}",
                                CalcLogInfoType.info);

                            JavaScriptParser parser = new JavaScriptParser();
                            parser.Parse(bonusFormula);
                            bonus = engine.Execute(bonusFormula)
                                .GetCompletionValue()
                                .AsNumber();

                            CalcLogFn(sn++, (int) calculation.Id, emp.Employee.Sicil, $"Sonuç = {bonus}",
                                CalcLogInfoType.info);
                        }

                        CalcLogFn(sn, (int) calculation.Id, emp.Employee.Sicil, $"Prim miktar: {bonus}",
                            CalcLogInfoType.info);
                        CalcLogFn(sn, (int) calculation.Id, emp.Employee.Sicil, $"Notlar:{calculateNotes}",
                            CalcLogInfoType.info);
                        CalcLogFn(sn, (int) calculation.Id, emp.Employee.Sicil, "Hesaplama işlemi tamalandı.",
                            CalcLogInfoType.finished);
                        emp.Amount = bonus;
                        emp.Calculated = true;
                        emp.Notes = calculateNotes;
                        calculation.Processed = calculation.Processed + 1;
                        calculation.LastRun = DateTimeOffset.Now.ToUnixTimeSeconds();
                        await _localDbContext.SaveChangesAsync();
                    }
                }

                calculation.Active = false;
                await _localDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                //Hata vereni kapat.
                var calculation = _localDbContext.Calculations.Find(calcid);
                calculation.Active = false;
                _localDbContext.EventLogs.Add(new EventLog
                {
                    EventOn = DateTime.Now,
                    Payload = e.Message,
                    Type = "ERROR:SYS:CALCULATEASYNC"
                });
                await _localDbContext.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Prim hesaplama öncesi işlemler.
        /// </summary>
        public void SltAylikPrimPrepareDaily()
        {
            var lastMonth = DateTime.Now.AddMonths(-1);
            SltAylikPrimPrepareDailyParameterized(lastMonth.Year, lastMonth.Month);
        }

        /// <summary>
        /// Aylık prim prepare 
        /// </summary>
        /// <param name="fYear"></param>
        /// <param name="fMonth"></param>
        /// <param name="fSicil">0</param>
        public void SltAylikPrimPrepareDailyParameterized(int fYear, int fMonth, int fSicil=0)
        {
            var ayBas = new DateTime(fYear, fMonth, 1).ToString("yyyyMMdd");
            var aySon = new DateTime(fYear, fMonth,
                DateTime.DaysInMonth(fYear, fMonth)).ToString("yyyyMMdd");
                        
            #region İzin Kaydını çek

            // Önceden alınmışsa temizle
            _localDbContext.Izins.RemoveRange(_localDbContext.Izins
                .Where(x => x.Periyot == (fYear * 100 + fMonth))
            );
            _localDbContext.SaveChanges();

                var izinSql = "SELECT " +
                              "SICIL_NO, IZIN_TIPI, TO_CHAR(BASLANGIC_TARIHI, 'yyyyMMdd'), " +
                              "TO_CHAR(BITIS_TARIHI, 'yyyyMMdd') " +
                              "FROM V_IZIN " +
                              "WHERE IZIN_TIPI IN('19', '31', '32', '34') " +
                              $"AND BASLANGIC_TARIHI <= TO_DATE('{aySon}', 'yyyyMMdd') " +
                              $"AND BITIS_TARIHI >= TO_DATE('{ayBas}', 'yyyyMMdd') ";

                using (var oracleConnection = new OracleConnection(_configuration.GetConnectionString("IkysIt")))
                {
                    try
                    {
                        oracleConnection.Open();
                        var izinCmd = oracleConnection.CreateCommand();
                        izinCmd.CommandText = izinSql;
                        var izinRdr = izinCmd.ExecuteReader();
                        while (izinRdr.Read())
                        {
                            var iSicil = int.Parse(izinRdr.GetString(0));
                            var iTipi = int.Parse(izinRdr.GetString(1));
                            var iBasTar = int.Parse(izinRdr.GetString(2));
                            // DB kayıtlı olan bitiş tarihi işe başladığı günü gösterir. O gün hariç izinlidir.
                            var iBitTarDate =
                                DateTime.ParseExact(izinRdr.GetString(3), "yyyyMMdd", CultureInfo.InvariantCulture);
                            iBitTarDate = iBitTarDate.AddDays(-1);
                            var iBitTar = int.Parse(iBitTarDate.ToString("yyyyMMdd"));
                            _localDbContext.Izins.Add(new Izin
                            {
                                Periyot = ((fYear * 100) + fMonth),
                                Sicil = iSicil,
                                Bastar = iBasTar,
                                Bittar = iBitTar,
                                Tip = iTipi
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        _localDbContext.EventLogs.Add(new EventLog
                        {
                            EventOn = DateTime.Now,
                            Payload = e.Message,
                            Type = "ERROR:SYS:PREPARE:IZIN"
                        });
                    }

                    if(oracleConnection.State == ConnectionState.Open) oracleConnection.Close();
                }

                _localDbContext.SaveChanges();
                
            #endregion

            #region Transfer bilgisini çek

            var bonusEmployeesTransfer = _localDbContext.BonusSltMonthly
                .Include(e => e.Employee)
                .Include(e => e.Store)
                .Include(e => e.Brand)
                .Include(e => e.Position)
                .Where(x => x.Year == fYear)
                .Where(x => x.Month == fMonth)
                .Where(x => x.Final == false);
            if (fSicil != 0)
            {
                bonusEmployeesTransfer = bonusEmployeesTransfer.Where(x => x.Employee.Sicil == fSicil);
            }

            if (!bonusEmployeesTransfer.Any())
            {
                _localDbContext.EventLogs.Add(new EventLog
                {
                    EventOn = DateTime.Now,
                    Payload = $"{fYear}-{fMonth} periyotuna ait Transferi hesaplanacak taslak BonusSltMontly kaydı yok.",
                    Type = "ERROR:SYS:PREPARE:DAILY"
                });
                _localDbContext.SaveChanges();
                return;
            }
            
            Debug.WriteLine("PREPARE: TRANSFER");
            using (var connectionTr = new OracleConnection(_configuration.GetConnectionString("IkysIt")))
            {

                foreach (var emp in bonusEmployeesTransfer)
                {
                    try
                    {
                        var sicil = emp.Employee.Sicil;
                        emp.AmountT = 0;
                        emp.WorkStart = ayBas;
                        emp.WorkEnd = ayBas;
                        emp.Active = false;
                        Debug.WriteLine($"TRANSFER: {sicil}");

                        var transfersSql = $@"SELECT DISTINCT
                            TO_CHAR(NVL(FIILI_GECIS_TRH, BSLTRH), 'YYYYMMDD') AS F,
                            TO_CHAR(BSLTRH, 'YYYYMMDD') AS S,
                            TO_CHAR(BTSTRH, 'YYYYMMDD') AS E,
                            ISYERI_MASRAFMR, CALISMA_DURUMU_KOD, GOREV
                        FROM T_PERSONEL_HISTORY
                        WHERE SICIL_NO = '{sicil}'
                        ORDER BY 1 DESC, 2 DESC";
                        if (connectionTr.State == ConnectionState.Closed)
                        {
                            connectionTr.Open();
                        }

                        var transfersCmd = connectionTr.CreateCommand();
                        transfersCmd.CommandText = transfersSql;
                        var transfersRdr = transfersCmd.ExecuteReader();
                        var transferList = new List<Transfer>();

                        var oncekiBitis = "21991231";
                        while (transfersRdr.Read())
                        {
                            var store = Convert.ToInt32(transfersRdr.GetString(3));
                            var figTrh = transfersRdr.GetString(0);
                            var aktif = transfersRdr.GetString(4).Trim() == "1";
                            var positionStr = transfersRdr.GetString(5).Trim();
                            var position = _localDbContext.Positions
                                               .AsNoTracking()
                                               .FirstOrDefault(x => x.Name == positionStr) ?? _localDbContext.Positions.Find(0);
                            transferList.Add(new Transfer(store, figTrh, oncekiBitis, aktif, position.Id));
                            oncekiBitis = figTrh;
                        }

                        transfersRdr.Close();
                        if(connectionTr.State == ConnectionState.Open) connectionTr.Close();

                        transferList = transferList.OrderBy(x => x.Baslangic).ToList();
                        var ayBasInt = Convert.ToInt32(ayBas);
                        var aySonInt = Convert.ToInt32(aySon);
                        var transferred = false;
                        foreach (var transfer in transferList)
                        {
                            var seasonal = false;
                            var basguni = Math.Max(transfer.GetBaslangicInt(), ayBasInt);
                            var bitguni = Math.Min(transfer.GetBitisInt(), aySonInt);
                            if (bitguni < basguni) continue; //Başlamadan bittiyse geç
                            seasonal = transfer.Pozisyon == 327;  //327=Mevsimlik Barista.
                            

                            var basgun = basguni.ToString();
                            var bitgun = bitguni.ToString();
                            var theStore = _localDbContext.Stores
                                .Include(s => s.Brand)
                                .First(s => s.Id == transfer.StoreId);
                            //Kendi kaydıysa
                            if (emp.Store.Id == transfer.StoreId && emp.Position.Id == transfer.Pozisyon)
                            {
                                emp.WorkStart = basgun;
                                emp.WorkEnd = bitgun;
                                emp.Active = transfer.Aktif && !seasonal;
                                emp.Seasonal = seasonal;
                                emp.Position = _localDbContext.Positions.Find(transfer.Pozisyon);
                                _localDbContext.Entry(emp.Position).State = EntityState.Unchanged;
                                if (transfer.Aktif && (!basgun.Equals(ayBas) || !bitgun.Equals(aySon)))
                                {
                                    emp.AmountT = 1;
                                    transferred = true;
                                }

                                _localDbContext.SaveChanges();
                                continue;
                            }

                            //Önceden bulunmuşsa
                            var existent = _localDbContext.BonusSltMonthly
                                .Where(s => s.Employee.Sicil == sicil)
                                .Where(s => s.Position.Id == transfer.Pozisyon)
                                .Where(s => s.Store.Id == transfer.StoreId)
                                .Where(s => s.Year == emp.Year)
                                .FirstOrDefault(s => s.Month == emp.Month);
                            if (existent != null)
                            {
                                continue;
                            }

                            //Hiç yoksa
                            var transferredEmp = new BonusSltMonth();
                            var values = _localDbContext.Entry(emp).CurrentValues.Clone();
                            values["Id"] = 0;
                            values["StoreId"] = transfer.StoreId;
                            values["BrandId"] = theStore.Brand.Id;
                            values["PositionId"] = transfer.Pozisyon;
                            _localDbContext.Entry(transferredEmp).CurrentValues.SetValues(values);
                            transferredEmp.WorkStart = basgun;
                            transferredEmp.WorkEnd = bitgun;
                            transferredEmp.Active = transfer.Aktif && !seasonal;
                            transferredEmp.Seasonal = seasonal;
                            _localDbContext.BonusSltMonthly.Add(transferredEmp);
                            if (transfer.Aktif && (!basgun.Equals(ayBas) || !bitgun.Equals(aySon)))
                                transferredEmp.AmountT = 1;
                            transferredEmp.Notes = "Transfer";
                            transferred = true;
                        }

                        emp.AmountT = transferred ? 2 : 0;
                        _localDbContext.SaveChanges();

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.StackTrace);
                        //_localDbContext.EventLogs.Add(new EventLog
                        //{
                        //    EventOn = DateTime.Now,
                        //    Payload = e.Message,
                        //    Type = "ERROR:SHAYA:PREPARE:TRANSFER"
                        //});
                        //_localDbContext.SaveChanges();
                    }
                }
            }

            #endregion

            #region İşbaşı tarihi

            var bonusEmployeesIsbasi = _localDbContext.BonusSltMonthly
                .Include(e => e.Employee)
                .Include(e => e.Store)
                .Include(e => e.Brand)
                .Include(e => e.Position)
                .Where(x => x.Year == fYear)
                .Where(x => x.Month == fMonth)
                .Where(x => x.AmountT < 1)      //Transfer olmuşların başlama tarihleri başka.
                .Where(x => x.Final == false);
            if (fSicil != 0)
            {
                bonusEmployeesIsbasi = bonusEmployeesIsbasi.Where(x => x.Employee.Sicil == fSicil);
            }

            if (!bonusEmployeesIsbasi.Any())
            {
                _localDbContext.EventLogs.Add(new EventLog
                {
                    EventOn = DateTime.Now,
                    Payload = $"{fYear}-{fMonth} periyotuna ait İşbaşı tarihi hesaplanacak taslak BonusSltMontly kaydı yok.",
                    Type = "ERROR:SYS:PREPARE:DAILY"
                });
                _localDbContext.SaveChanges();
            }


            Debug.WriteLine("PREPARE: WORKSTART DAYS");
            using (var connectionIt = new OracleConnection(_configuration.GetConnectionString("IkysIt")))
            {
                foreach (var emp in bonusEmployeesIsbasi)
                {
                    try
                    {
                        var sicil = emp.Employee.Sicil;
                        Debug.WriteLine($"ISBASI: {sicil}");
                        if (connectionIt.State == ConnectionState.Closed)
                        {
                            connectionIt.Open();
                        }

                        //V_SORUMLULUK kaydı yoksa atla
                        var isbasiGerekSql = $"SELECT COUNT(SICIL) FROM V_SORUMLULUK WHERE SICIL='{sicil}'";
                        using (var isbasiGerekCmd = connectionIt.CreateCommand())
                        {
                            isbasiGerekCmd.CommandText = isbasiGerekSql;
                            var isbasiGerekRdr = isbasiGerekCmd.ExecuteReader();
                            if (isbasiGerekRdr.Read())
                            {
                                var cnt = isbasiGerekRdr.GetInt32(0);
                                if(connectionIt.State == ConnectionState.Open) connectionIt.Close();
                                if (cnt <= 0)
                                {
                                    if (aySon.Equals(emp.WorkEnd) && !emp.Seasonal)
                                        emp.Active = true;
                                    continue;
                                }
                            }
                            else
                            {
                                if(connectionIt.State == ConnectionState.Open) connectionIt.Close();
                                continue;
                            }
                        }

                        if (connectionIt.State != ConnectionState.Open)
                        {
                            connectionIt.Open();
                        }
                        
                        var isbasiTarihiSql = $@"SELECT 
                               TO_CHAR(GREATEST(TO_DATE(P.KURUMA_ILK_GIRIS_TARIHI, 'DD.MM.YYYY'), TO_DATE('{ayBas}', 'YYYYMMDD')), 'YYYYMMDD') AS BSL,
                               TO_CHAR(LEAST(TO_DATE(NVL(P.AYRILIS_TARIHI, '31.12.2199'), 'DD.MM.YYYY'), TO_DATE('{aySon}', 'YYYYMMDD')), 'YYYYMMDD') AS BTS,
                               KADRO_DURUMU_KOD,
                               TO_CHAR(GREATEST(V.SORUMLULUK_BASLANGIC_TRH, TO_DATE('{ayBas}', 'YYYYMMDD')), 'YYYYMMDD') AS BSL2,
                               TO_CHAR(LEAST(NVL(V.SORUMLULUK_BITIS_TRH, TO_DATE('31.12.2199', 'DD.MM.YYYY')), TO_DATE('{aySon}', 'YYYYMMDD')), 'YYYYMMDD') AS BTS2
                        FROM T_PERSONEL P
                        LEFT JOIN V_SORUMLULUK V ON (
                            V.SICIL=P.SICIL_NO
                            AND
                            TO_DATE('{aySon}', 'YYYYMMDD') BETWEEN V.SORUMLULUK_BASLANGIC_TRH AND V.SORUMLULUK_BITIS_TRH
                        )
                        WHERE P.SICIL_NO='{sicil}' AND V.MASRAF_MERKEZI_KOD='{emp.Store.Id}'
                        ";
                        var isbasiCmd = connectionIt.CreateCommand();
                        isbasiCmd.CommandText = isbasiTarihiSql;
                        var isbasiRdr = isbasiCmd.ExecuteReader();
                        if (isbasiRdr.Read())
                        {
                            var kadroDurumuKod = isbasiRdr.GetString(2);
                            if (kadroDurumuKod == "07") //Mevsimlik
                            {
                                if (isbasiRdr.IsDBNull(3))
                                {
                                    emp.Active = false;
                                    emp.Calculated = true;
                                    emp.Amount = 0;
                                    emp.Notes = "Mevsimlik Çalışan";
                                    emp.WorkStart = ayBas;
                                    emp.WorkEnd = ayBas;
                                }
                                else
                                {
                                    emp.WorkStart = isbasiRdr.GetString(3);
                                    emp.WorkEnd = isbasiRdr.GetString(4);
                                }

                                emp.Seasonal = true;
                            }
                            else
                            {
                                if (!isbasiRdr.IsDBNull(3))
                                {
                                    emp.WorkStart = isbasiRdr.GetString(0);
                                    emp.WorkEnd = isbasiRdr.GetString(1);
                                    emp.Seasonal = false;
                                }
                            }
                        }
                        if(connectionIt.State == ConnectionState.Open) connectionIt.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        //_localDbContext.EventLogs.Add(new EventLog
                        //{
                        //    EventOn = DateTime.Now,
                        //    Payload = e.Message,
                        //    Type = "ERROR:SYS:PREPARE:WORKSTART"
                        //});
                        //_localDbContext.SaveChanges();
                    }
                }
                _localDbContext.SaveChanges();
            }

            //Ay sonunda çalışıyor mu
            var activeWorkers = _localDbContext.BonusSltMonthly
                .Include(e => e.Employee)
                .Include(e => e.Store)
                .Include(e => e.Brand)
                .Include(e => e.Position)
                .Where(x => x.Year == fYear)
                .Where(x => x.Month == fMonth)
                .Where(x => x.Final == false);
            if (fSicil != 0)
            {
                activeWorkers = activeWorkers.Where(x => x.Employee.Sicil == fSicil);
            }

            foreach (var emp in activeWorkers)
            {
                if (aySon.Equals(emp.WorkEnd))
                    emp.Active = true;
            }
            
            #endregion

            #region Ücret Alanlarını Doldur

            var bonusEmployeesSalary = _localDbContext.BonusSltMonthly
                .Include(e => e.Employee)
                .Include(e => e.Store)
                .Include(e => e.Brand)
                .Include(e => e.Position)
                .Where(x => x.Year == fYear)
                .Where(x => x.Month == fMonth)
                .Where(x => x.Final == false);
            if (fSicil != 0)
            {
                bonusEmployeesSalary = bonusEmployeesSalary.Where(x => x.Employee.Sicil == fSicil);
            }

            if (!bonusEmployeesSalary.Any())
            {
                _localDbContext.EventLogs.Add(new EventLog
                {
                    EventOn = DateTime.Now,
                    Payload = $"{fYear}-{fMonth} periyotuna ait Maaş alınacak taslak BonusSltMontly kaydı yok.",
                    Type = "ERROR:SYS:PREPARE:DAILY"
                });
                return;
            }
            Debug.WriteLine("PREPARE: SALARY");
            using (var connectionPrf = new OracleConnection(_configuration.GetConnectionString("IkysPrf")))
            {
                foreach (var emp in bonusEmployeesSalary)
                {
                    try
                    {
                        var sicil = emp.Employee.Sicil;

                        Debug.WriteLine($"UCRET: {sicil}");
                        connectionPrf.Open();
                        var ucretSql = $@"SELECT BIRIM_UCRET FROM V_UCRET 
                                 WHERE SICIL='{sicil}' AND TO_DATE('{emp.WorkStart}', 'yyyyMMdd') 
                                 BETWEEN BASLAMA_TARIHI AND BITIS_TARIHI";

                        var ucretCmd = connectionPrf.CreateCommand();
                        ucretCmd.CommandText = ucretSql;
                        var ucretRdr = ucretCmd.ExecuteReader();
                        if (ucretRdr.Read())
                        {
                            emp.Salary = ucretRdr.GetDouble(0);
                        }
                        else
                        {
                            Debug.WriteLine($"ERROR:SYS:PREPARE:SALARY: No Salary info for {sicil}");
                        }
                        if(connectionPrf.State == ConnectionState.Open) connectionPrf.Close();
                    }
                    catch (Exception e)
                    {
                        if(connectionPrf.State == ConnectionState.Open) connectionPrf.Close();
                        //_localDbContext.EventLogs.Add(new EventLog
                        //{
                        //    EventOn = DateTime.Now,
                        //    Payload = e.Message,
                        //    Type = "ERROR:SYS:PREPARE:SALARY"
                        //});
                        Console.WriteLine($"ERROR:SYS:PREPARE:SALARY: {e.Message}");
                    }

                    if(connectionPrf.State == ConnectionState.Open) connectionPrf.Close();
                }
                _localDbContext.SaveChanges();
            }

            #endregion

            _localDbContext.SaveChanges();
        }
    

        /// <summary>
        /// SLT Aylık Prim Hesapla
        /// </summary>
        /// <returns></returns>
        public void SltAylikPrim()
        {
                var calculation = _localDbContext.Calculations
                    .Where(m => m.Title == "SLT Monthly")
                    //.Where(m => m.LastRun >= 10)
                    .FirstOrDefault(m => m.Active);
                if (calculation == null)
                {
                    Debug.WriteLine("ERROR: SLT Monthly no calculation but job triggered.");
                    return;
                }

                var calcId = calculation.Id ?? 0;
                var bonusEmployees = _localDbContext.BonusSltMonthly
                    .Include(e => e.Employee)
                    .Include(e => e.Store)
                    .Include(e => e.Brand)
                    .Include(e => e.Position)
                    .Where(x => x.Calculated == false)
                    .Where(x => x.Calculation.Id == calcId)
                    .Where(x => x.Active)
                    .OrderBy(x => x.Brand.Id)
                        .ThenBy(x => x.Store.Id)
                        .ThenBy(x => x.Employee.Sicil);
                if (!bonusEmployees.Any())
                {
                    Debug.WriteLine("ERROR: SLT Monthly no Employee to calculate.");
                    //Hesaplanacak personel yoksa işi tamamlanmış olarak işaretle.
                    calculation.Active = false;
                    calculation.Total = calculation.Processed;
                    _localDbContext.SaveChanges();
                    return;
                }

                foreach (var emp in bonusEmployees)
                {
      
                    int sicil;
                    int year;
                    int month;
                    int brandId;
                    int positionId;
                    int storeId;
                    try
                    {
                        sicil = emp.Employee.Sicil;
                        year = emp.Year;
                        month = emp.Month;
                        brandId = emp.Brand.Id;
                        positionId = emp.Position.Id;
                        storeId = emp.Store.Id;
                    }
                    catch (Exception e)
                    {
                        _localDbContext.EventLogs.Add(new EventLog
                        {
                            EventOn = DateTime.Now,
                            Payload = e.ToString(),
                            Type = "ERROR:SLTAYLIK_BEGINING_SOMETHING_NULL"
                        });
                        continue;
                        
                    }

                    //Onaylanmış hesaplamalar tekrar hesaplanmayacak
                    if (emp.Final)
                    {
                        calculation.Processed = calculation.Processed + 1;
                        calculation.LastRun = DateTimeOffset.Now.ToUnixTimeSeconds();
                        _localDbContext.SaveChanges();
                        continue;
                    }

                    //Hesaplananları tekrar hesaplama
                    if (emp.Calculated)
                    {
                        var updateCntSql = $@"UPDATE Calculations  
                            SET Processed = ( 
                                SELECT COUNT(Id) FROM BonusSltMonthly 
                                WHERE CalculationId={calculation.Id} 
                                  AND Calculated=1
                            ) 
                            WHERE Id={calculation.Id}
                        ";
                        _localDbContext.Database.ExecuteSqlCommand(updateCntSql);
                        calculation.Processed = calculation.Processed + 1;
                        calculation.LastRun = DateTimeOffset.Now.ToUnixTimeSeconds();
                        _localDbContext.SaveChanges();
                        continue;
                    }

                    #region ResetLocalVars

                    var calculateBonus = true;
                    string calculationMessage;
                    var bonusRate = 0.0;
                    var storeWorkDays = 0;
                    var bonus = 0.0;
                    var periodDays = 30;

                    var logNum = _localDbContext.CalcLogs.AsNoTracking()
                        .Where(c => c.Sicil == sicil)
                        .Count(c => c.CalcId == calcId);

                    CalcLogFn(logNum++, calcId, sicil, $"Shaya tipi prim hesaplama işlemi {year}-{month}.", CalcLogInfoType.info);
                    CalcLogFn(logNum++, calcId, sicil, $"Mağaza {emp.Store.Name} {emp.Store.Id} ", CalcLogInfoType.info);
                    var ayBas = emp.WorkStart; //Bu alanlar liste oluşturulurken dolduruluyor.
                    var aySon = emp.WorkEnd;
                    if (ayBas == null)
                    {
                        ayBas = new DateTime(year, month, 1).ToString("yyyyMMdd");
                    }

                    if (aySon == null)
                    {
                        aySon = new DateTime(year, month, DateTime.DaysInMonth(year, month)).ToString("yyyyMMdd");
                    }
                    CalcLogFn(logNum++, calcId, sicil, $"Çalışma tarihleri: {ayBas}-{aySon}", CalcLogInfoType.info);

                    #endregion
                    try
                    {

                        #region Mağaza hedefini tutmuş mu?

                        if (emp.Position.Name.StartsWith("MAĞAZA MÜDÜR"))
                        {
                            var targetok = MultiTargetsOk(sicil, year, month, storeId);
                            switch (targetok)
                            {
                                case 3:
                                    calculationMessage = "Sorumlu mağazalar toplam hedefi tutmuş. Prim hesaplanacak.";
                                    CalcLogFn(logNum++, calcId, sicil, calculationMessage, CalcLogInfoType.check_pass);
                                    break;
                                case 2:
                                    calculateBonus = false;
                                    calculationMessage = "Sorumlu mağazalar toplam hedefi tutmamış. Prim hesaplanmayacak.";
                                    CalcLogFn(logNum++, calcId, sicil, calculationMessage, CalcLogInfoType.check_fail);
                                    break;
                                case 1:
                                    calculationMessage = "Mağaza hedefi tutmuş. Prim hesaplanacak.";
                                    CalcLogFn(logNum++, calcId, sicil, calculationMessage, CalcLogInfoType.check_pass);
                                    break;
                                default: //also case 0
                                    calculateBonus = false;
                                    calculationMessage = "Mağaza hedefi tutmamış. Prim hesaplanmayacak.";
                                    CalcLogFn(logNum++, calcId, sicil, calculationMessage, CalcLogInfoType.check_fail);
                                    emp.Notes = calculationMessage;
                                    break;
                            }
                        }
                        else
                        {
                            if (TargetOk(storeId, year, month))
                            {
                                calculationMessage = "Mağaza hedefi tutmuş. Prim hesaplanacak.";
                                CalcLogFn(logNum++, calcId, sicil, calculationMessage, CalcLogInfoType.info);
                            }
                            else
                            {
                                calculateBonus = false;
                                calculationMessage = "Mağaza hedefi tutmamış. Prim hesaplanmayacak.";
                                CalcLogFn(logNum++, calcId, sicil, calculationMessage, CalcLogInfoType.check_fail);
                            }
                        }

                        #endregion

                        #region Aktif çalışan kontrol

                        // Sadece aktif çalışanlar listelendiğinden  burada kontrol yapılmıyor.

                        #endregion

                        #region Ceza etki süresi kontrol

                        if (calculateBonus)
                        {
                            if (InPunishmentPeriod(sicil, year, month, "SLT", out var cezaNotes))
                            {
                                calculateBonus = false;
                                calculationMessage = "Personel ceza etki süresinde. Prim hesaplanmayacak.";
                                CalcLogFn(logNum++, calcId, sicil, cezaNotes, CalcLogInfoType.info);
                                CalcLogFn(logNum++, calcId, sicil, calculationMessage, CalcLogInfoType.check_fail);
                                emp.CezaNotes = cezaNotes;
                            }
                            else
                            {
                                emp.CezaNotes = "";
                                calculationMessage = "Personel ceza etki süresinde değil. Prim hesaplanacak.";
                                CalcLogFn(logNum++, calcId, sicil, calculationMessage, CalcLogInfoType.info);
                            }
                        }

                        #endregion

                        #region Unvan prim alıyor mu?

                        if (calculateBonus)
                        {
                            CalcLogFn(logNum++, calcId, sicil, $"Personel unvanı(pozisyonu): {emp.Position.Name}", CalcLogInfoType.info);
                            if (UnvanOk(emp.Position.Id))
                            {
                                    CalcLogFn(logNum++, calcId, sicil, "Pozisyon prim alıyor.", CalcLogInfoType.check_pass);
                            }
                            else
                            {
                                calculateBonus = false;
                                calculationMessage = "Pozisyon prim almaz.";
                                CalcLogFn(logNum++, calcId, sicil, "Pozisyon prim almıyor.", CalcLogInfoType.check_fail);
                            }
                        }

                        #endregion

                        #region İzin günleri

                        if (calculateBonus)
                        {
                            CalcLogFn(logNum++, calcId, sicil, "İzin günleri konrol ediliyor.", CalcLogInfoType.info);
                            ayBas = emp.WorkStart;
                            aySon = emp.WorkEnd;
                            if (ayBas == null || aySon == null)
                            {
                                //Workstart ve workend boş olursa logla
                                throw new Exception($"{sicil} için workstart, workend null");
                            }
                            var ayBasDate = DateTime.ParseExact(ayBas, "yyyyMMdd", CultureInfo.InvariantCulture);
                            var aySonDate = DateTime.ParseExact(aySon, "yyyyMMdd", CultureInfo.InvariantCulture);
                            var storePeriodDays = (aySonDate.Date - ayBasDate.Date).Days + 1;

                            storePeriodDays = storePeriodDays > 30 ? 30 : storePeriodDays;
                            
                            if (emp.AmountT.Equals(0))
                            {
                                //Transfer olmadıysa ay 30 alınacak
                                periodDays = 30;
                            }
                            else
                            {
                                periodDays = 30; // DateTime.DaysInMonth(year, month);
                                CalcLogFn(logNum++, calcId, sicil, "Transfer olmuş. Transfer istisnaları geçerli.", CalcLogInfoType.info);
                            }
                            var bonusFreeDays = IzinGunSayisi(sicil, year, month, ayBas, aySon, calcId, ref logNum);

                            bonusFreeDays = bonusFreeDays > periodDays ? periodDays : bonusFreeDays; //30 günlerde 31 izinli görülmesin.
                            storeWorkDays = storePeriodDays - bonusFreeDays;

                            emp.AmountO = bonusFreeDays;
                            emp.FreeDays = bonusFreeDays;
                            emp.WorkDays = periodDays;
                            emp.PayDays = storeWorkDays;
                            _localDbContext.SaveChanges();
                            CalcLogFn(logNum++, calcId, sicil, $"Periyot gün sayısı: {periodDays}", CalcLogInfoType.info);
                            CalcLogFn(logNum++, calcId, sicil, $"Mağazada çalışılan gün sayısı: {storePeriodDays}", CalcLogInfoType.info);
                            CalcLogFn(logNum++, calcId, sicil, $"İzinli hesaplanan gün sayıs: {bonusFreeDays}", CalcLogInfoType.info);
                            CalcLogFn(logNum++, calcId, sicil, $"Prim ödenecek gün sayıs: {storeWorkDays}", CalcLogInfoType.info);
                        }

                        #endregion

                        #region Prim oranını bul

                        if (calculateBonus)
                        {
                            bonusRate = FindBonusRate(brandId, positionId);
                            CalcLogFn(logNum++, calcId, sicil, $"{emp.Position.Name} pozisyonu için prim oranı: {bonusRate}",
                                CalcLogInfoType.info);
                        }

                        #endregion

                        #region Hesaplama

                        if (calculateBonus)
                        {
                            var bonusFormula = @" employee_monthly_bonus =
                                employee_monthly_salary * slt_position_bonus_rate *
                                employee_store_work_days /  store_period_days
                             ";
                            /*
                            _localDbContext.Formulas.Add(new Formula
                            {
                                Content = "employee_monthly_bonus = employee_monthly_salary * slt_position_bonus_rate * employee_store_work_days /  30",
                                FormulaType = "SLTMothly",
                                Name = "SLTMonthly"
                            });
                            */

                            CalcLogFn(logNum++, calcId, sicil, "Hesaplama verileri:", CalcLogInfoType.info);
                            var engine = new Engine();

                            // Ücret
                            engine.SetValue("employee_monthly_salary", emp.Salary);
                            CalcLogFn(logNum++, calcId, sicil, $"employee_monthly_salary={emp.Salary}", CalcLogInfoType.info);

                            // Pozisyon prim oranı
                            engine.SetValue("slt_position_bonus_rate", bonusRate);
                            CalcLogFn(logNum++, calcId, sicil, $"slt_position_bonus_rate={bonusRate}", CalcLogInfoType.info);

                            //Çalıştığı gün
                            engine.SetValue("employee_store_work_days", storeWorkDays);
                            CalcLogFn(logNum++, calcId, sicil, $"employee_store_work_days={storeWorkDays}",
                                CalcLogInfoType.info);

                            //Mağazadaki gün
                            engine.SetValue("store_period_days", periodDays);
                            CalcLogFn(logNum++, calcId, sicil, $"store_period_days={periodDays}", CalcLogInfoType.info);

                            var formulas = _localDbContext.Formulas.AsNoTracking()
                                .FirstOrDefault(f => f.FormulaType == "SLTMonthly");

                            if (formulas != null)
                            {
                                bonusFormula = formulas.Content;
                            }

                            #region Parttime Barista Hesaplaması

                            if (emp.Position.Name == "PART TIME BARİSTA")
                            {
                                CalcLogFn(logNum++, calcId, sicil, "Part-time Barista için istisna hesaplama yapılacak.", CalcLogInfoType.info);
                                var partTimeBaristaPrim = "77";
                                var param = _localDbContext.Parametres.AsNoTracking()
                                    .First(n => n.Title == "PartTimeBaristaPrim");
                                if (param != null)
                                {
                                    partTimeBaristaPrim = param.Value;
                                }

                                bonusFormula =
                                    $"employee_monthly_bonus = employee_store_work_days / {periodDays} * {partTimeBaristaPrim}";
                            }

                            #endregion

                            CalcLogFn(logNum++, calcId, sicil, $"Formül: {bonusFormula}", CalcLogInfoType.info);

                            JavaScriptParser parser = new JavaScriptParser();
                            parser.Parse(bonusFormula);
                            var scriptRes = engine.Execute(bonusFormula)
                                .GetCompletionValue()
                                .AsNumber();
                            bonus = double.IsNaN(scriptRes) || double.IsInfinity(scriptRes) ? 0.0 : scriptRes;

                            CalcLogFn(logNum++, calcId, sicil, $"Sonuç = {bonus}", CalcLogInfoType.info);
                            calculationMessage = "Hesaplandı.";
                           
                            _localDbContext.SaveChanges();

                        }
                            
                        #endregion
                    }
                    catch (Exception e)
                    {
                        _localDbContext.EventLogs.Add(new EventLog
                        {
                            EventOn = DateTime.Now,
                            Payload = e.ToString(),
                            Type = "ERROR:SYS:CALCULATE:SLTAYLIK"
                        });       
                        calculationMessage = "Hata oluştu";
                    }

                    CalcLogFn(logNum++, calcId, sicil, $"Kayda geçen prim miktarı: {bonus}", CalcLogInfoType.info);
                    CalcLogFn(logNum, calcId, sicil, "Hesaplama işlemi tamamlandı.", CalcLogInfoType.finished);
                    Debug.WriteLine(bonus);
                    emp.Amount = bonus;
                    emp.Calculated = true;
                    emp.Notes = calculationMessage;
                    calculation.Processed = calculation.Processed + 1;
                    calculation.LastRun = DateTimeOffset.Now.ToUnixTimeSeconds();
                    _localDbContext.SaveChanges();
                }
                calculation.Total = calculation.Processed;
                calculation.LastRun = DateTimeOffset.Now.ToUnixTimeSeconds();
                calculation.Active = false;
                _localDbContext.SaveChanges();
        }

        
        #region Helper Funtions

        /// <summary>
        /// Personelin mağazası hedefi tutmuş mu?
        /// </summary>
        /// <param name="storeId">Mağaza Id</param>
        /// <param name="year">İşlem Yılı</param>
        /// <param name="month">İşlem Ayı</param>
        /// <returns>Boolean: True; Mağaza satışı hedefinden büyükse.</returns>
        /// <exception cref="Exception"></exception>
        private bool TargetOk(int storeId, int year, int month)
        {
            var storeTargets = _localDbContext.StoreTargetsRetail.AsNoTracking()
                .Where(x => x.Year == year)
                .FirstOrDefault(x => x.Store.Id == storeId);

            if (storeTargets == null)
            {
                Debug.WriteLine($"Mağaza hedefleri bulunamadı. {year}-{month}:{storeId}");
                return false;
            }

            var actual = storeTargets.GetActual(month);
            var target = storeTargets.GetTarget(month);
            return actual >= target;
        }

        private int MultiTargetsOk(int sicil, int year, int month, int storeId)
        {
            
            var ayBas = new DateTime(year, month, 1).ToString("yyyyMMdd");
            var aySon = new DateTime(year, month, DateTime.DaysInMonth(year, month)).ToString("yyyyMMdd");
            var sql = $@"SELECT DISTINCT ISYERI, ISYERI_MASRAFMR, ISYERI_NO
                            FROM T_PERSONEL P
                            WHERE ISYERI_NO IN (
                              SELECT SORUMLULUK_KOD FROM V_SORUMLULUK
                              WHERE
                                    SORUMLULUK_BASLANGIC_TRH <= TO_DATE('{aySon}', 'yyyyMMdd')
                                    AND SORUMLULUK_BITIS_TRH >= TO_DATE('{ayBas}', 'yyyyMMdd')
                                    AND SICIL='{sicil}'
                              UNION ALL
                              SELECT DISTINCT ISYERI_NO FROM V_SORUMLULUK
                              WHERE
                                    SORUMLULUK_BASLANGIC_TRH <= TO_DATE('{aySon}', 'yyyyMMdd')
                                    AND SORUMLULUK_BITIS_TRH >= TO_DATE('{ayBas}', 'yyyyMMdd')
                                    AND SICIL='{sicil}'
                            )";
            
            var actualTotal = 0;
            var targetTotal = 0;
            var sorumlulukCnt = 0;

            using (var connectionIt = new OracleConnection(_configuration.GetConnectionString("IkysIt")))
            {
                connectionIt.Open();
                var cmd = connectionIt.CreateCommand();
                cmd.CommandText = sql;
                var rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var storeIdX = int.Parse(rdr.GetString(1));
                    sorumlulukCnt++;
                    var storeTargets = _localDbContext.StoreTargetsRetail.AsNoTracking()
                        .Where(x => x.Year == year)
                        .FirstOrDefault(x => x.Store.Id == storeIdX);

                    if (storeTargets != null)
                    {
                        actualTotal += storeTargets.GetActual(month);
                        targetTotal += storeTargets.GetTarget(month);
                    }
                    else
                    {
                        Debug.WriteLine($"Sorumluluk mağaza hedefleri bulunamadı.({sicil}) {year}-{month}:{storeIdX}");
                    }
                }

                if(connectionIt.State == ConnectionState.Open) connectionIt.Close();
            }

            if ((actualTotal == 0 && targetTotal == 0) || sorumlulukCnt < 2)
            {
                //tek mağaza sorumlu personel
                return TargetOk(storeId, year, month) ? 1 : 0;
            }
            else
            {
                //çoklu mağaza sorumlu personel
                return actualTotal >= targetTotal ? 3 : 2;
            }
        }

        private bool InPunishmentPeriod(int sicil, int year, int month, string employeeType, out string cezaNotes)
        {
            cezaNotes = "";
            var cezaListe = new List<string>();
            var cezas = _localDbContext.EmployeeCezas.Where(c => c.Sicil == sicil);
            if (!cezas.Any()) return false;

            var cezaCount = 0;
            var cezaLookup = new Dictionary<string, int>();
            var cezaNames = new Dictionary<string, string>();
            var czls = _localDbContext.Cezalar.Where(c => c.Effective > 0);

            switch (employeeType)
            {
                case "NONSTL":
                    //Kınama + Ünvan İndirimi
                    czls = czls.Where(n => n.Code == "01" || n.Code == "06");
                    break;
                case "SLT":
                    //Kınama + Ünvan İndirimi + Yazılı Uyarı
                    czls = czls.Where(n => n.Code == "01" || n.Code == "06" || n.Code == "04");
                    break;
            }

            if (czls.Any())
            {
                foreach (var czl in czls)
                {
                    cezaLookup.Add(czl.Code, czl.Effective);
                    cezaNames.Add(czl.Code, czl.Title);
                }
            }


            foreach (var ceza in cezas)
            {
                var cza = (year * 12 + month) - (ceza.Date.Year * 12 + ceza.Date.Month);
                if (cza < 0) continue; // Gelecek zamandaki cezaları atla
                if (cza >= cezaLookup[ceza.Code]) continue; //Ceza etki süresi dışında ise atla
                var cezaDate = ceza.Date.ToString(format: "yyyy-MM-dd");
                cezaListe.Add($"{cezaDate}-{cezaNames[ceza.Code]} ({cezaLookup[ceza.Code]} Ay)");
                cezaCount++;
            }

            if (cezaCount <= 0) return false;
            cezaNotes += string.Join(",\n ", cezaListe);
            return true;
        }

        /// <summary>
        /// Listedeki unvanlara prim verilmez.
        /// </summary>
        /// <param name="posId"></param>
        /// <returns></returns>
        private bool UnvanOk(int posId)
        {
            var pos = _localDbContext.Positions.Find(posId);
            return pos != null && pos.HasMonthlyBonus;
        }


        /// <summary>
        /// İzin gün sayısını bul
        /// </summary>
        /// <param name="sicil" type="int">Sicil numarası</param>
        /// <param name="year">İşlem yılı</param>
        /// <param name="month">Periyot</param>
        /// <param name="workBas">Çalışma baş</param>
        /// <param name="workSon">Çalışma son</param>
        /// <param name="calcId"></param>
        /// <param name="logNum"></param>
        /// <returns>int Çalışılmayan gün sayısı</returns>
        private int IzinGunSayisi(int sicil, int year, int month, string workBas, string workSon, int calcId, ref int logNum)
        {
            //Ücretsiz İzin, Hastalık Raporu, Doğum Raporu
            //32	Doğum(Raporlu) İzni         Hepsi 
            //19	Ücretsiz İzin(Merkez Ofis)  Hepsi
            //34	Ücretsiz İzin(Saha)         Hepsi
            //31	Hastalik(Raporlu) İzni      2 den sonrası, birleşirse de 
            var izinTipleri = new Dictionary<int, string>
            {
                {19, "Ücretsiz İzin(Merkez Ofis)"},
                {31, "Hastalik(Raporlu)"},
                {32, "Doğum(Raporlu) İzni"},
                {34, "Ücretsiz İzin(Saha)"}
            };
            var raporGunSablon = "";
//            CalcLogFn(logNum++, calcId, sicil, "İzin Rapor Hesaplama",CalcLogInfoType.info);
            var gunler = new Dictionary<string, int>();

            var bonusFreeDays = 0;
            var periyot = year * 100 + month;
            var izins = _localDbContext.Izins.AsNoTracking()
                .Where(x => x.Periyot == periyot)
                .Where(x => x.Sicil == sicil);
            if (izins.Any())
            {
                var ayBasDate = DateTime.ParseExact(workBas, "yyyyMMdd", CultureInfo.InvariantCulture);
                var aySonDate = DateTime.ParseExact(workSon, "yyyyMMdd", CultureInfo.InvariantCulture);


                for (var day = ayBasDate.Date; day.Date <= aySonDate.Date; day = day.AddDays(1))
                {
                    gunler.Add(day.ToString("yyyyMMdd"), 0);
                }

                foreach (var izin in izins)
                {
                    var izinTipi = izin.Tip;
                    var basTar = DateTime.ParseExact(izin.Bastar.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    var bitTar = DateTime.ParseExact(izin.Bittar.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    //CalcLogFn(logNum++, calcId, sicil, $" İzin~<{izinTipi}>: {basTar}-{bitTar}","INFO");
                    for (var day = basTar.Date; day.Date <= bitTar.Date; day = day.AddDays(1))
                    {
                        gunler[day.ToString("yyyyMMdd")] = izinTipi;
                    }
                    var basTarStr = basTar.ToString(format: "yyyy.MM.dd");
                    var bitTarStr = bitTar.ToString(format: "yyyy.MM.dd");
                    CalcLogFn(logNum++, calcId, sicil, $"İzin: {basTarStr}-{bitTarStr}  {izinTipleri[izinTipi]}", CalcLogInfoType.info);
                }

                //Tarih sırasına al
                var gunList = gunler.Keys.ToList();
                gunList.Sort();

                var raporGun = 0;
                //Sağlık raporlarının 2 gününü düş
                foreach (var gunKey in gunList)
                {
                    var izinTipi = gunler[gunKey];
                    if (izinTipi == 31)
                    {
                        raporGun++;
                        if (raporGun <= 2)
                        {
                            gunler[gunKey] = 0;
                        }
                    }
                    else
                    {
                        raporGun = 0;
                    }
                }
                
                
                for (var day = ayBasDate.Date; day.Date <= aySonDate.Date; day = day.AddDays(1))
                {
                    var rgs = "+";
                    if (gunler[day.ToString("yyyyMMdd")] > 0)
                    {
                        bonusFreeDays++;
                        rgs = "x";
                    }

                    raporGunSablon += rgs;
                }
            }
            if(bonusFreeDays>0)
                CalcLogFn(logNum++, calcId, sicil, $"Hastalık raporu çizelgesi: {raporGunSablon}",CalcLogInfoType.info);

            bonusFreeDays = (bonusFreeDays > 30) ? 30 : bonusFreeDays;
            return bonusFreeDays;
        }


        /// <summary>
        /// Prim oranını bulur
        /// </summary>
        /// <param name="brandId"></param>
        /// <param name="positionId"></param>
        /// <returns></returns>
        private double FindBonusRate(int brandId, int positionId)
        {
            var bonusRatesSlt = _localDbContext.BonusRatesSlt
                .AsNoTracking()
                .Where(m => m.Brand.Id == brandId)
                .FirstOrDefault(m => m.Position.Id == positionId);
            return bonusRatesSlt?.Rate ?? 0;
        }
        
        /// <summary>
        /// Returns if Month is SAS month, if false then calculation will continue
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        private bool IsSasMonth(int brand, int month)
        {
            var sasMonthses = _localDbContext.SasMonthses.AsNoTracking()
                .FirstOrDefault(m => m.Brand.Id == brand);
            return sasMonthses != null && sasMonthses.IsSasMonth(month);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AutomaticRetry(Attempts = 1, OnAttemptsExceeded = AttemptsExceededAction.Delete)]
        public void CleanUpAsync()
        {
            //await _localDbContext.Database.ExecuteSqlCommandAsync("UPDATE Calculations SET Active = false WHERE Active = True");
        }

        private void CalcLogFn(int sn, int calcid, int sicil, string action, string tip)
        {
            Debug.WriteLine($"{sn}, {calcid}, {sicil}, '{tip}', '{action}'");
            using (var dbContext = new PatikaDbContext(_configuration.GetConnectionString("LocalMssql")))
            {

                dbContext.CalcLogs.Add(new CalcLog
                {
                    Sira = sn,
                    Action = action,
                    CalcId = calcid,
                    Sicil = sicil,
                    Tip = tip
                });
                dbContext.SaveChanges();
            }
        }
        
        #endregion
        
        /// <inheritdoc />
        public void Dispose()
        {
            //throw new NotImplementedException();
        }
    }
}
