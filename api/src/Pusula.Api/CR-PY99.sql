SET FOREIGN_KEY_CHECKS=0;

alter table Brands modify column Id int(11) not null;

alter table Stores modify column Id int(11) not null;

SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE `StoreTargetsRetail` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Year` int DEFAULT 0 NOT NULL,
    `StoreId` int NULL,
    `Iadeler` text,
    `A01` int DEFAULT 0 NOT NULL,
    `A02` int DEFAULT 0 NOT NULL,
    `A03` int DEFAULT 0 NOT NULL,
    `A04` int DEFAULT 0 NOT NULL,
    `A05` int DEFAULT 0 NOT NULL,
    `A06` int DEFAULT 0 NOT NULL,
    `A07` int DEFAULT 0 NOT NULL,
    `A08` int DEFAULT 0 NOT NULL,
    `A09` int DEFAULT 0 NOT NULL,
    `A10` int DEFAULT 0 NOT NULL,
    `A11` int DEFAULT 0 NOT NULL,
    `A12` int DEFAULT 0 NOT NULL,
    `A13` int DEFAULT 0 NOT NULL,
    `T01` int DEFAULT 0 NOT NULL,
    `T02` int DEFAULT 0 NOT NULL,
    `T03` int DEFAULT 0 NOT NULL,
    `T04` int DEFAULT 0 NOT NULL,
    `T05` int DEFAULT 0 NOT NULL,
    `T06` int DEFAULT 0 NOT NULL,
    `T07` int DEFAULT 0 NOT NULL,
    `T08` int DEFAULT 0 NOT NULL,
    `T09` int DEFAULT 0 NOT NULL,
    `T10` int DEFAULT 0 NOT NULL,
    `T11` int DEFAULT 0 NOT NULL,
    `T12` int DEFAULT 0 NOT NULL,
    `T13` int DEFAULT 0 NOT NULL,
    PRIMARY KEY (`Id`),
    CONSTRAINT `FK_StoreTargetsRetail_Stores_StoreId` FOREIGN KEY (`StoreId`) REFERENCES `Stores` (`Id`) ON DELETE RESTRICT
);

CREATE INDEX `IX_StoreTargetsRetail_StoreId` ON StoreTargetsRetail (`StoreId`);

ALTER TABLE EmployeeBonuses ADD Actual int DEFAULT 0 NOT NULL;

-- VEmployeeBonuses değişecek, 
-- Actual kolonu ActualBrut olacak  
-- Actual kolonu EmployeeBonuses.Actual a yönlenecek
