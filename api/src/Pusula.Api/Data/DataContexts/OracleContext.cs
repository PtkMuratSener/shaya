﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Oracle.ManagedDataAccess.Client;

namespace Pusula.Api.Data.DataContexts
{
    /// <summary>
    /// 
    /// </summary>
    public class OracleContext
    {
        /// <summary>
        /// tnsnames.ora
        /// </summary>
        public static string ConnectionString { set; get; }
        private static OracleConnection _connection = null;

        private static bool Open()
        {
            _connection = new OracleConnection(ConnectionString);
            try
            {
                _connection.Open();
                return true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        private static void Close()
        {
            if (_connection == null) return;
            _connection.Close();
            //_connection.Dispose();
            _connection = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static int CreateUpdateDelete(string sql)
        {
            Open();
            OracleCommand cmd = new OracleCommand(sql, _connection);
            int result = cmd.ExecuteNonQuery();
            Close();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IList<T> QueryToList<T>(string sql)
        {
            Open();
            OracleDataReader dtr = QueryForReader(sql);
            var list = DataReaderToList<T>(dtr);
            Close();
            return list;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static object QueryForObject<T>(string sql)
        {
            Open();
            OracleDataReader dtr = QueryForReader(sql);
            var obj = DataReaderToObject<T>(dtr);
            Close();
            return obj;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private static OracleDataReader QueryForReader(string sql)
        {
            try
            {
                OracleCommand cmd = _connection.CreateCommand();
                cmd.CommandText = sql;
                OracleDataReader dtr = cmd.ExecuteReader();
                return dtr;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rdr"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static IList<T> DataReaderToList<T>(OracleDataReader rdr)
        {
            
            IList<T> list = new List<T>();

            while (rdr.Read())
            {
                T t = System.Activator.CreateInstance<T>();
                Type obj = t.GetType();
               
                for (int i = 0; i < rdr.FieldCount; i++)
                //for (int i = 0; i < obj.GetProperties().Length; i++)
                {
                    object tempValue = null;
                    tempValue = rdr.GetValue(i);
                    if (rdr.IsDBNull(i))
                    {
                        string typeFullName = obj.GetProperty(rdr.GetName(i)).PropertyType.FullName;
                        tempValue = GetDbNullValue(typeFullName);
                    }
                    else
                    {
                        tempValue = rdr.GetValue(i);
                    }
                    var prop = obj.GetProperty(rdr.GetName(i).ToLowerInvariant());

                    tempValue = Convert.ChangeType(tempValue, prop.PropertyType );

                    
                    prop.SetValue(t, tempValue);
                }
                list.Add(t);
            }
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private static bool IsNumeric(object expression)
        {
            if (expression == null)
                return false;

            double number;
            return Double.TryParse( Convert.ToString( expression
                    , CultureInfo.InvariantCulture)
                , System.Globalization.NumberStyles.Any
                , NumberFormatInfo.InvariantInfo
                , out number);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rdr"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static object DataReaderToObject<T>(OracleDataReader rdr)
        {
            T t = System.Activator.CreateInstance<T>();
            Type obj = t.GetType();
            if (rdr.Read())
            {
                for (int i = 0; i < rdr.FieldCount; i++)
                {
                    object tempValue = null;
                    if (rdr.IsDBNull(i))
                    {
                        string typeFullName = obj.GetProperty(rdr.GetName(i)).PropertyType.FullName;
                        tempValue = GetDbNullValue(typeFullName);
                    }
                    else
                    {
                        tempValue = rdr.GetValue(i);
                    }
                    obj.GetProperty(rdr.GetName(i)).SetValue(t, tempValue, null);
                }
                return t;
            }
            else
                return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        private static object GetDbNullValue(string typeFullName)
        {
            typeFullName = typeFullName.ToLower();

            if (typeFullName == OracleDbType.Varchar2.ToString().ToLower())
            {
                return String.Empty;
            }
            if (typeFullName == OracleDbType.Int32.ToString().ToLower())
            {
                return 0;
            }
            if (typeFullName == OracleDbType.Date.ToString().ToLower())
            {
                return Convert.ToDateTime("");
            }
            if (typeFullName == OracleDbType.Boolean.ToString().ToLower())
            {
                return false;
            }
            if (typeFullName == OracleDbType.Int16.ToString().ToLower())
            {
                return 0;
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <returns></returns>
        public static int ExecuteProcedure(string procName)
        {
            return ExecuteSql(procName, null, CommandType.StoredProcedure);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
        public static int ExecuteProcedure(string procName, OracleParameter[] pars)
        {
            return ExecuteSql(procName, pars, CommandType.StoredProcedure);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strSql"></param>
        /// <returns></returns>
        public static int ExecuteSql(string strSql)
        {
            return ExecuteSql(strSql, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strSql"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public static int ExecuteSql(string strSql, OracleParameter[] paras)
        {
            return ExecuteSql(strSql, paras, CommandType.Text);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strSQL"></param>
        /// <param name="paras"></param>
        /// <param name="cmdType"></param>
        /// <returns></returns>
        public static int ExecuteSql(string strSQL, OracleParameter[] paras, CommandType cmdType)
        {
            int i = 0;
            Open();
            OracleCommand cmd = new OracleCommand(strSQL, _connection) { CommandType = cmdType };
            if (paras != null)
            {
                cmd.Parameters.AddRange(paras);
            }
            i = cmd.ExecuteNonQuery();
            Close();


            return i;
        }
    }
}
