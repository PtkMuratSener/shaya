using Microsoft.EntityFrameworkCore;
using Pusula.Api.Models;

namespace Pusula.Api.Data.DataContexts
{
    /// <summary>
    /// Yerel veritabanı
    /// </summary>
    public class PatikaDbContext : DbContext
    {

        /// <inheritdoc />
        /// <summary>
        /// </summary>
        /// <param name="options"></param>
        public PatikaDbContext(DbContextOptions<PatikaDbContext> options) :base(options)
        {
        }
        
        /// <inheritdoc />
        public PatikaDbContext(string connectionString): base(GetOptions(connectionString))
        {
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            return new DbContextOptionsBuilder().UseSqlServer(connectionString).Options;
        }

        /// <summary>
        /// Markalar
        /// </summary>
        public DbSet<Brand> Brands { get; set; }
        
        /// <summary>
        /// Mağazalar
        /// </summary>
        public DbSet<Store> Stores { get; set; }
        /// <summary>
        /// Al-Shaya Calendar Data
        /// </summary>
        public DbSet<Calendar> Calendars { get; set; }

        /// <summary>
        /// Formüller
        /// </summary>
        public DbSet<Formula> Formulas { get; set; }
            
        /// <summary>
        /// For CalendarOperations response
        /// </summary>
        public DbSet<ResponseBeginsEnds> ResponsiBeginsEnds {get; set; }

        /// <summary>
        /// Çalışanlar
        /// </summary>
        public DbSet<Employee> Employees { get; set; }
        
        /// <summary>
        /// SAS Ayları
        /// </summary>
        public DbSet<SasMonths> SasMonthses { get; set; }
        
        /// <summary>
        /// Ceza türleri ve etki süreleri
        /// </summary>
        public DbSet<Ceza> Cezalar { get; set; }
        
        /// <summary>
        /// Sistem parametreleri
        /// </summary>
        public DbSet<Parametre> Parametres { get; set; }
        
        /// <summary>
        /// Mağaza bazlı Pozisyon bilgileri
        /// </summary>
        public DbSet<StorePosition> StorePositions { get; set; }
        
        /// <summary>
        /// Personel bazlı satış verisi
        /// </summary>
        public DbSet<Sale> Sales { get; set; }
        
        /// <summary>
        /// Proje kullanıcıları
        /// </summary>
        public DbSet<User> Users { get; set; }
        
        /// <summary>
        /// Hesaplamalar
        /// </summary>
        public DbSet<Calculation> Calculations { get; set; }
        
        /// <summary>
        /// Roller
        /// </summary>
        public DbSet<Role> Roles { get; set; }
        
        /// <summary>
        /// Yetkiler
        /// </summary>
        public DbSet<AuthorizationType> AuthorizationTypes { get; set; }
        
        /// <summary>
        /// Ekranlar
        /// </summary>
        public DbSet<Screen> Screens { get; set; }
        
        /// <summary>
        /// Mağazalar
        /// </summary>
        public DbSet<StoreType> StoreTypes { get; set; }
        
        /// <summary>
        /// Çalışma Alanı
        /// </summary>
        public DbSet<WorkingArea> WorkingAreas { get; set; }
        
        /// <summary>
        /// Roller İzinleri
        /// </summary>
        public DbSet<RolePermission> RolePermissions { get; set; }
        
        /// <summary>
        /// Sicil Rol Atama
        /// </summary>
        public DbSet<RegistryRoleAssignment> RegistryRoleAssignments { get; set; }
        
        /// <summary>
        /// Unvan Rol Atama
        /// </summary>
        public DbSet<TitleRoleAssignment> TitleRoleAssignments { get; set; }

        /// <summary>
        /// Ünvanlar
        /// </summary>
        public DbSet<Position> Positions { get; set; }
        
        /// <summary>
        /// Personel primleri
        /// </summary>
        public DbSet<EmployeeBonus> EmployeeBonuses { get; set; }

        /// <summary>
        /// Personel primleri View
        /// </summary>
        public DbSet<VEmployeeBonus> VEmployeeBonuses { get; set; }
        
        /// <summary>
        /// Prim Oranları
        /// </summary>
        public DbSet<BonusRate> BonusRates { get; set; }
        
        /// <summary>
        /// Personel ceza bilgileri
        /// </summary>
        public DbSet<EmployeeCeza> EmployeeCezas { get; set; }
        
        /// <summary>
        /// Marka bazında satış hedef oranları
        /// </summary>
        public DbSet<BrandSaleSplit> BrandSaleSplits { get; set; }
        
        /// <summary>
        /// Sisteme yüklene dosyalar
        /// </summary>
        public DbSet<UploadedFile> UploadedFiles { get; set; }
        
        /// <summary>
        /// Revizyon kaydı.
        /// </summary>
        public DbSet<Revision> Revisions { get; set; }

        /// <summary>
        /// Aylık gerçekleşen hedef
        /// </summary>
        public DbSet<MonthlyActual> MonthlyActuals { get; set; }
            
        /// <summary>
        /// Hesaplama log
        /// </summary>
        public DbSet<CalcLog> CalcLogs { get; set; }

        /// <summary>
        /// Retailden hesaplanan mağaza hedefleri
        /// </summary>
        public DbSet<StoreTargetRetail> StoreTargetsRetail { get; set; }
        
        //FAZ_2 --------------------------------------------
        
        /// <summary>
        /// SLT Prim Oranları
        /// </summary>
        public DbSet<BonusRateSlt> BonusRatesSlt { get; set; }
        
        /// <summary>
        /// Bütçe üstü karlılık prim oranları
        /// </summary>
        public DbSet<OverAchievementRate> OverAchievementRates { get; set; }
        
        /// <summary>
        /// Merkez çalışanları için prim kontrol koşuları
        /// </summary>
        public DbSet<HqBonusLine> HqBonusLines { get; set; }
        
        /// <summary>
        /// Personel izin bilgileri
        /// </summary>
        public DbSet<Izin> Izins { get; set; }
        
        /// <summary>
        /// Aylık SLT primleri
        /// </summary>
        public DbSet<BonusSltMonth> BonusSltMonthly { get; set; }
        
        /// <summary>
        /// Rolün yetki alanındaki markalar.
        /// </summary>
        public DbSet<RoleBrand> RoleBrands { get; set; }
        
        /// <summary>
        /// Prim tipleri
        /// </summary>
        public DbSet<PrimType> PrimTypes { get; set; }
        
        /// <summary>
        /// Rollerin yetkili prim tipleri
        /// </summary>
        public DbSet<RolePrimType> RolePrimTypes { get; set; }
        
        /// <summary>
        /// Line Hedefleri
        /// </summary>
        public DbSet<LineTarget> LineTargets { get; set; }
        
        /// <summary>
        /// Olay kütüğü
        /// </summary>
        public DbSet<EventLog> EventLogs { get; set; }
    }
}