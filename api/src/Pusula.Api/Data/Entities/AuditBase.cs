﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pusula.Api.Models.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class AuditBase
    {
        
        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Create Date is required")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "CreatedBy is required")]
        public string createBy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string updateBy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataStatus { get; set; } 
    }
}