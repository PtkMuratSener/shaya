﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;

namespace Pusula.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("/svc")]
    public class StoreController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <summary>
        /// Mağaza İşlemleri
        /// </summary>
        /// <param name="context"></param>
        public StoreController(PatikaDbContext context)
        {
            _context = context;
        }
/*
        /// <summary>
        /// Yeni mağaza ekler
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("store")]
        [ValidateModelState]
        [SwaggerOperation("StoreAdd")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(ActionResponseContract),
            description: "Success")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract),
            description: "Failed")]
        public IActionResult StoreAdd([FromBody] Store request)
        {
            try
            {
                using (_context)
                {
                    _context.Stores.Add(request);
                    _context.SaveChanges();
                    var insertedStore = _context.Stores.Last();
                    ActionResponseContract responseContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Store eklendi.",
                        Code = "PARAMETER_ADD_SUCCESS",
                        Data = insertedStore
                    };
                    return Ok(responseContract);
                }

                //return StatusCode(200);
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "PARAMETER_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);

            }
        }
*/
        /// <summary>
        /// Store detay bilgileri
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("store/{id}")]
        [ValidateModelState]
        [SwaggerOperation("StoreGetById")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(ListResponseContract),
            description: "Success")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract),
            description: "Not found")]
        public IActionResult StoreGetById([FromRoute] int id)
        {
            var store = _context.Stores.Find(id);
            if (store != null)
            {
                ListResponseContract responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = 1},
                    Data = store
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Store bulunamadı.",
                Code = "PARAMETER_EMPTY_SET"
            };
            return NotFound(failedResponseContract);

        }
/*
        /// <summary>
        /// Store sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("store/{id}")]
        [ValidateModelState]
        [SwaggerOperation("StoreDeleteById")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(ActionResponseContract),
            description: "Success")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract),
            description: "Failed")]
        public IActionResult StoreDeleteById([FromRoute] int id)
        {
            var store = _context.Stores.Find(id);
            if (store == null)
            {
                //Kayıt yoksa 
                ActionResponseContract failedResponseContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Store bulunamadı.",
                    Code = "PARAMETER_EMPTY_SET"
                };
                return NotFound(failedResponseContract);
            }

            _context.Stores.Remove(store);
            _context.SaveChanges();
            return StatusCode(200, "Success");

        }

*/

        /// <summary>
        /// Mağazanın personelini listeler.
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("storeEmployees/{storeId}")]
        [ValidateModelState]
        [SwaggerOperation("StoreGetEmployeesById")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(ListResponseContract),
            description: "Success")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract),
            description: "Not found")]
        public IActionResult StoreGetEmployeesById([FromRoute] int storeId)
        {
            var userArea = HttpContext.Items["UserArea"] as string;
            if (userArea == "S")
            {
                var store = (int) HttpContext.Items["UserStore"];
                storeId = store == storeId ? store : 0;
            }

            var storeEmployees = _context.Employees.AsNoTracking()
                .Include(s => s.Store)
                .Include(s => s.Position)
                .Where(e => e.Store.Id == storeId)
                .Where(a => a.Active);
                //.Select(x => new {x.Sicil, x.Name, x.Surname});
            if (storeEmployees.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = storeEmployees.Count()},
                    Data = storeEmployees.ToList()
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Personel bulunamadı.",
                Code = "STOREEMPLOYEES_EMPTY_SET"
            };
            return NotFound(failedResponseContract);

        }
    }
}
