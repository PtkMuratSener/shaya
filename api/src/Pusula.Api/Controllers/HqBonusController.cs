using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <summary>
    /// Merkez prim işlemleri
    /// </summary>
    [Route("/svc")]
    public class HqBonusController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <inheritdoc />
        public HqBonusController(PatikaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Merkez çalışan unvanı için prim kontrol koşulları listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("hqBonusLines")]
        [ValidateModelState]
        [SwaggerOperation("HqBonusLines")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult HqBonusLines()
        {
            var hqBonusLines = _context.HqBonusLines.AsNoTracking();
            if (hqBonusLines.Any())
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Merkez çalışan unvanı için prim kontrol koşulları",
                    Code = "HQBONUSLINES_GET_SUCCESS",
                    Data = hqBonusLines
                };
                return Ok(responseSuccessContract);
            }
            
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Merkez çalışan unvanı için prim kontrol koşulları bulunamadı.",
                Code = "HQBONUSLINES_EMPTY_SET"
            };
            return NotFound(responseContract);
        }

        /// <summary>
        /// Merkez çalışan unvanı için prim kontrol koşulları listesi kayıt.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("hqBonusLines")]
        [ValidateModelState]
        [SwaggerOperation("HqBonusLinesSave")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult HqBonusLinesSave([FromBody] List<HqBonusLine> request)
        {
            try
            {
                using (_context)
                {
                    foreach (var req in request)
                    {
                        var pId = req.Id;
                        var hqBonusLine = _context.HqBonusLines
                            .FirstOrDefault(b => b.Id == pId);
                        if (hqBonusLine != null)
                        {
                            hqBonusLine.Ln1 = req.Ln1;
                            hqBonusLine.Ln2 = req.Ln2;
                            hqBonusLine.Ln3 = req.Ln3;
                            hqBonusLine.Ln1Rate = req.Ln1Rate;
                            hqBonusLine.Ln2Rate = req.Ln2Rate;
                            hqBonusLine.Ln3Rate = req.Ln3Rate;
                            _context.SaveChanges();
                        }
                    }

                    ActionResponseContract responseSuccessContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Merkez çalışan unvanı için prim kontrol koşulları güncellendi.",
                        Code = "HQBONUSLINES_UPDATE_SUCCESS",
                        Data = request
                    };
                    return Ok(responseSuccessContract);

                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "HQBONUSLINES_UPDATE_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }
     }
}