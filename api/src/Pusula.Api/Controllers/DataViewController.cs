using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    
    /// <inheritdoc />
    /// <summary>
    /// Yüklenen verileri gösteren servisler
    /// </summary>
    [Route("/svc")]
    public class DataViewController : Controller
    {
        /// <summary>
        /// Database bağı
        /// </summary>
        private readonly PatikaDbContext _localDbContext;

        /// <inheritdoc />
        public DataViewController(PatikaDbContext localDbContext)
        {
            _localDbContext = localDbContext;
        }

        /// <summary>
        /// Mağaza hedefleri listesi
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("storetargets/{year}")]
        [SwaggerOperation("GetStoreTargets")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ActionResponseContract), "Success")]
        public IActionResult GetStoreTargets([FromRoute] int year)
        {
            var storeTargetRetails = _localDbContext.StoreTargetsRetail.AsNoTracking()
                .Include(x => x.Store)
                .Where(x => x.Year == year)
                .Select(x => new
                {
                    StoreId = x.Store.Id,
                    Store = x.Store.Name,
                    x.Year,
                    x.A01,
                    x.T01,
                    x.A02,
                    x.T02,
                    x.A03,
                    x.T03,
                    x.A04,
                    x.T04,
                    x.A05,
                    x.T05,
                    x.A06,
                    x.T06,
                    x.A07,
                    x.T07,
                    x.A08,
                    x.T08,
                    x.A09,
                    x.T09,
                    x.A10,
                    x.T10,
                    x.A11,
                    x.T11,
                    x.A12,
                    x.T12,
                    TotalActual=x.A13,
                    TotalTarget=x.T13
                });
            if (storeTargetRetails.Any())
            {
                var storeTargetRetailssResponse = new ActionResponseContract
                {
                    Success = true,
                    Message = "Yüklenmiş ve hesaplanmış mağaza hedefleri",
                    Code = "UPLOADED_TARGETS_INFO",
                    Data = storeTargetRetails
                };
                return Ok(storeTargetRetailssResponse);
            }

            var storeTargetRetailResponse = new ActionResponseContract
            {
                Success = false,
                Message = $"Yüklenmiş mağaza hedefi bulunamadı. Yıl: {year}",
                Code = "UPLOADED_TARGETS_EMPTY_SET",
            };
            return NotFound(storeTargetRetailResponse);
        }
    
        /// <summary>
        /// Mağaza hedefleri listesi
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("calendars/{year}")]
        [SwaggerOperation("GetCalendars")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ActionResponseContract), "Success")]
        public IActionResult GetCalendars([FromRoute] int year)
        {
            var calendars = _localDbContext.Calendars.AsNoTracking()
                .Where(x => x.Year == year)
                .Select(x => new
                {
                    x.Id,
                    x.Year,
                    x.Date,
                    x.Month,
                    x.Quarter,
                    x.Week,
                    x.Season
                });
            if (calendars.Any())
            {
                var storeTargetRetailssResponse = new ActionResponseContract
                {
                    Success = true,
                    Message = "Al-Shaya takvim",
                    Code = "CALENDARS_INFO",
                    Data = calendars
                };
                return Ok(storeTargetRetailssResponse);
            }

            var storeTargetRetailResponse = new ActionResponseContract
            {
                Success = false,
                Message = $"Takvim bulunamadı. Yıl: {year}",
                Code = "CALENDARS_EMPTY_SET",
            };
            return NotFound(storeTargetRetailResponse);
        }
    }
}
