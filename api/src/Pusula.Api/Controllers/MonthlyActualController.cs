using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Requests;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    [Route("/svc")]
    public class MonthlyActualController : Controller
    {
        private readonly PatikaDbContext _localDbContext;

        /// <inheritdoc />
        public MonthlyActualController(PatikaDbContext localDbContext)
        {
            _localDbContext = localDbContext;
        }

        /// <summary>
        /// Aylık gerçekleşen satış tutarları
        /// </summary>
        /// <param name="request">Sorgu Parametreleri</param>
        /// <returns>PagingResponse</returns>
        [HttpPost]
        [Route("monthlyActual")]
        [ValidateModelState]
        [SwaggerOperation("MonthlyActual")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(PagingResponse), description: "Success")]
        public IActionResult MonthlyActual([FromBody]MonthlyActualRequest request)
        {

            var monthlyActuals = _localDbContext.MonthlyActuals.AsNoTracking();
            
            if(request.BrandId != 0)
                monthlyActuals =monthlyActuals.Where(c => c.BrandId == request.BrandId);
            var userArea = HttpContext.Items["UserArea"] as string;
            if (userArea == "S")
            {
                var store = (int) HttpContext.Items["UserStore"];
                monthlyActuals = monthlyActuals.Where(c => c.StoreId == store);
            }
            else
            {

                if (request.StoreId != 0)
                    monthlyActuals = monthlyActuals.Where(c => c.StoreId == request.StoreId);
            }

            if (request.PositionId != 0)
                monthlyActuals = monthlyActuals.Where(c => c.PositionId == request.PositionId);
            if (request.EmployeeId != 0)
                monthlyActuals = monthlyActuals.Where(c => c.Sicil == request.EmployeeId);
            if (request.Month != 0)
                monthlyActuals = monthlyActuals.Where(c => c.Month == request.Month);
            if (request.Year != 0)
                monthlyActuals = monthlyActuals.Where(c => c.Year == request.Year);

                var sort = request.Sort ?? "id,asc";
                var sortParam = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[0];
                var sortDir = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[1];
                switch (sortParam)
                {
                    case "brand":
                        monthlyActuals = sortDir == "asc" ? monthlyActuals.OrderBy(o => o.Brand) : monthlyActuals.OrderByDescending(o => o.Brand);
                        break;
                    case "store":
                        monthlyActuals = sortDir == "asc" ? monthlyActuals.OrderBy(o => o.Store) : monthlyActuals.OrderByDescending(o => o.Store);
                        break;
                    case "position":
                        monthlyActuals = sortDir == "asc" ? monthlyActuals.OrderBy(o => o.Position) : monthlyActuals.OrderByDescending(o => o.Position);
                        break;
                    case "namesurname":
                        monthlyActuals = sortDir == "asc" ? monthlyActuals.OrderBy(o => o.Namesurname) : monthlyActuals.OrderByDescending(o => o.Namesurname);
                        break;
                    case "sicil":
                        monthlyActuals = sortDir == "asc" ? monthlyActuals.OrderBy(o => o.Sicil) : monthlyActuals.OrderByDescending(o => o.Sicil);
                        break;
                    case "actual":
                        monthlyActuals = sortDir == "asc" ? monthlyActuals.OrderBy(o => o.Actual) : monthlyActuals.OrderByDescending(o => o.Actual);
                        break;
                    case "target":
                        monthlyActuals = sortDir == "asc" ? monthlyActuals.OrderBy(o => o.Target) : monthlyActuals.OrderByDescending(o => o.Target);
                        break;
                    case "rate":
                        monthlyActuals = sortDir == "asc" ? monthlyActuals.OrderBy(o => o.Rate) : monthlyActuals.OrderByDescending(o => o.Rate);
                        break;
                    default:
                        monthlyActuals=monthlyActuals.OrderBy(o => o.Id);
                        break;
                }

            if (monthlyActuals.Any())
            {
                var page = request.Page ?? 0;
                var itemsPerPage = request.Size ?? 50;

                var tmpContent = monthlyActuals
                    .Skip(page * itemsPerPage)
                    .Take(itemsPerPage)
                    .Cast<dynamic>()
                    .ToList();

                var response = new PagingResponse(monthlyActuals, tmpContent, request);
                return Ok(response);
            }

            ActionResponseContract calcResponse = new ActionResponseContract
            {
                Success = false,
                Message = "Aylık gerçekleşen satış sonucu yok.",
                Code = "MONTHLYACTUAL_EMPTY_SET",
            };
            return NotFound(calcResponse);
        }
    }
}