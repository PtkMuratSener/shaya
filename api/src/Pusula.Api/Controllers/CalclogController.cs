using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Hesaplama detay servisleri
    /// </summary>
    [Route("/svc")]
    public class CalclogController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <inheritdoc />
        public CalclogController(PatikaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Hesaplamalar listesi
        /// </summary>
        /// <param name="sicil"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("calcloglist/{sicil}")]
        [SwaggerOperation("ListCalculations")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(Calculation), "Success")]        
        public IActionResult ListCalculations([FromRoute]int sicil)
        {
            if (sicil > 0)
            {
                var calculations = _context.CalcLogs.AsNoTracking()
                    .Where(s => s.Sicil == sicil)
                    .Select(s => s.CalcId)
                    .Distinct();
                var calculationList = _context.Calculations.AsNoTracking()
                    .Where(x => calculations.Contains(x.Id ?? 0))
                    .OrderByDescending(x => x.LastRun)
                    .Select(x => new
                    {
                        x.Id,
                        x.Filter,
                        Time = DateTimeOffset.FromUnixTimeSeconds(x.LastRun).ToString("yyyy.MM.dd hh:mm")
                    });
                
                if (calculationList.Any())
                {
                    var responseContract = new ListResponseContract
                    {
                        Limit = 0,
                        Offset = 0,
                        Total = new TotalProperty
                        {
                            All = calculationList.Count(),
                            Filtered = 0
                        },
                        Data = calculationList
                    };
                    return StatusCode(200, responseContract);
                }
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "hesaplama bulunamadı.",
                Code = "CALCULATIONS_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }
        
        /// <summary>
        /// Hesaplama logu
        /// </summary>
        /// <param name="sicil"></param>
        /// <param name="calcid"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("calclog/{sicil}/{calcid}")]
        [SwaggerOperation("ListCalculationLog")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(CalcLog), "Success")]        
        public IActionResult ListCalculationLog([FromRoute]int sicil, [FromRoute]int calcid)
        {
            if (sicil > 0)
            {
                var calculationLogs = _context.CalcLogs.AsNoTracking()
                    .Where(s => s.Sicil == sicil)
                    .Where(s => s.CalcId == calcid)
                    .OrderBy(s => s.Sira);
                if (calculationLogs.Any())
                {
                    var responseContract = new ListResponseContract
                    {
                        Limit = 0,
                        Offset = 0,
                        Total = new TotalProperty
                        {
                            All = calculationLogs.Count(),
                            Filtered = 0
                        },
                        Data = calculationLogs
                    };
                    return StatusCode(200, responseContract);
                }
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Hesaplama logu bulunamadı.",
                Code = "CALCLOG_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }
    }
}
