
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Pusula.Api.Attributes;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Jobs;
using Pusula.Api.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Arkaplan işleri
    /// </summary>
    [Route("/svc")]
    public class EngineController : Controller
    {

        private readonly PatikaDbContext _localDbContext;
        private readonly Dictionary<int, string> _responseMessages;
        private readonly IConfiguration _configuration;

        /// <inheritdoc />
        public EngineController(PatikaDbContext localDbContext, IConfiguration configuration)
        {
            _localDbContext = localDbContext;
            _configuration = configuration;

            _responseMessages = new Dictionary<int, string> {
                {0, "Tamam"}, 
                {1, "Hata: Marka oranları girilmemiş."},
                {2, "Bilgi: Finalize edilmiş."}
            };

        }

        /// <summary>
        ///  Mağaza ağırlıklı satış hedef oranları hesapla
        /// </summary>
        /// <param name="brandId">Marka Id</param>
        /// <param name="year">Yıl</param>
        /// <param name="month">Ay</param>
        /// <returns>string</returns>
        [HttpGet]
        [Route("engineer/csp")]
        [ValidateModelState]
        [SwaggerOperation("Main")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(string), "Success")]
        public IActionResult Main([FromQuery] int brandId, [FromQuery] int year, [FromQuery] int month)
        {
            Engineer eng = new Engineer(_localDbContext);
            try
            {
                var oppa = eng.CalculateStorePositions(brandId, year, month);
                return Ok(_responseMessages[oppa]);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>
        /// Sistem durumu kontrol
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/check")]
        [ValidateModelState]
        [SwaggerOperation("Check")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(string), "Success")]
        public IActionResult Check()
        {
            RecurringJob.AddOrUpdate<Engineer>("cron-daily", e => e.CronDaily(), Cron.Daily(3));
            RecurringJob.AddOrUpdate<Calculator>("daily-prim-repare", c => c.SltAylikPrimPrepareDaily(), Cron.Daily(4));
            /*
            var str = _localDbContext.StoreTargetsRetail.AsNoTracking()
                .Include(s => s.Store)
                .Where(s => s.Store.Id == 39013)
                .Where(y => y.Year == 2018)
                .OrderBy(o => o.Id)
                .LastOrDefault();
            var stb = str.GetTarget(10);
            var sta = str.GetActual(10);
            
            var res = $"sta={sta}, stb:{stb}";
            */
            const string res = "CronDaily, SltAylikPrimPrepareDaily re-scheduled";
            return Ok(res);
        }

        /// <summary>
        /// SLT Aylık Prim hesaplaması öncesi transfer işlemleri.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="background"></param>
        /// <param name="sicil">0</param>
        /// <returns></returns>
        [HttpGet]
        [Route("engineer/sltAylikPrimPrepare")]
        [ValidateModelState]
        [SwaggerOperation("Sapp")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(string), "Success")]
        public IActionResult Sapp([FromQuery] int year, [FromQuery] int month, [FromQuery] bool background, [FromQuery] int sicil=0)
        {
            try
            {
                using (var calc = new Calculator(_localDbContext, _configuration))
                {
                    if (background)
                    {
                        var bjc = new BackgroundJobClient();
                        bjc.Enqueue<Calculator>(x => x.SltAylikPrimPrepareDailyParameterized(year, month, sicil));
                        return Ok($"SltAylikPrimPrepare started for {year}, {month} as background job");
                    }
                    else
                    {
                        calc.SltAylikPrimPrepareDailyParameterized(year, month, sicil);
                        return Ok($"SltAylikPrimPrepare executed for {year}, {month}");
                    }
                }
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        /// <summary>
        /// SLT Aylık prim hesaplama işlemi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/sltAylikPrimHesapla")]
        [ValidateModelState]
        [SwaggerOperation("SltAylikPrimHesapla")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(string), "Success")]
        public IActionResult SltAylikPrimHesapla()
        {
            var request = new
            {
                Year = 2018,
                Month = 10,
                BrandId = 39,
                StoreId = 0
            };
            var newCalculation = new Calculation
            {
                Active = true,
                Filter = $"{request.Year}/{request.Month}/{request.BrandId}/{request.StoreId}",
                Processed = 0,
                Title = "SLT Monthly",
                Total = 0,
                LastRun = 0,
                Fyear = request.Year,
                Fmonth = request.Month,
                Fbrand = request.BrandId,
                Fstore = request.StoreId
            };
            _localDbContext.Calculations.Add(newCalculation);
            _localDbContext.SaveChanges();

            _localDbContext.Database
                .ExecuteSqlCommand($"CALL FillBonusSltMonthly({request.Year}, {request.Month}, {newCalculation.Id})");

            var bjc = new BackgroundJobClient();
            bjc.Enqueue<Calculator>( x => x.SltAylikPrim());

            var sltBonus = _localDbContext.BonusSltMonthly
                .Include(x => x.Employee)
                .Include(x => x.Brand)
                .Include(x => x.Store)
                .Include(x => x.Position)
                
                .Take(10);
            return Ok(new
            {
                msg = "SLT Aylık prim hesaplama işi başladı.",
                data = sltBonus
            });

        }
        
        /// <summary>
        /// Development purpose function
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/doNothingService")]
        [ValidateModelState]
        [SwaggerOperation("DoNothing")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(string), "Success")]
        public IActionResult DoNothing()
        {
            return Ok("OK");
        }
        
        /// <summary>
        /// Sistem durum bilgisi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getStatus")]
        [ValidateModelState]
        [SwaggerOperation("GetStatus")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(string), "Success")]
        public IActionResult GetStatus()
        {
            var ret = new Dictionary<string, string>();
            var todayS = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
            var shDatesS = _localDbContext.Calendars.FirstOrDefault(c => c.Date == todayS);
            if (shDatesS == null)
            {
                return Ok("Calendar not found");
            }
            ret.Add("Tarih", DateTime.Now.ToString("yyyyMMdd"));

            var dom = _localDbContext.Calendars
                .AsNoTracking()
                .Where(x => x.Year == shDatesS.Year)
                .Where(x => x.Month == shDatesS.Month)
                .Count(x => x.Date <= shDatesS.Date);
            ret.Add("AlShaya", $"{shDatesS.Year:D4}{shDatesS.Month:D2}{dom:D2}");
            
            var iadePeriyotGunu = 0;
            var paramIadePeriyotGunu = _localDbContext.Parametres.AsNoTracking()
                .FirstOrDefault(x => x.Title == "RefundPeriodStartDay");
            if (paramIadePeriyotGunu != null)
            {
                iadePeriyotGunu = int.Parse(paramIadePeriyotGunu.Value);
                ret.Add("IadePeriyotGunu", iadePeriyotGunu.ToString());
            }
            var iadePeriyotBas = _localDbContext.Calendars.AsNoTracking()
                .Where(y => y.Year == shDatesS.Year)
                .Where(m => m.Month == shDatesS.Month)
                .OrderBy(b => b.Date)
                .Skip(iadePeriyotGunu-1)
                .First();
            ret.Add("IadePeriyotBas", iadePeriyotBas.Date.ToString());
            
            var periyotBas = _localDbContext.Calendars.AsNoTracking()
                .Where(y => y.Year == shDatesS.Year)
                .Where(m => m.Month == shDatesS.Month)
                .OrderBy(b => b.Date)
                .First();
            ret.Add("AlShaya Baş", periyotBas.Date.ToString());

            var periyotSon = _localDbContext.Calendars.AsNoTracking()
                .Where(y => y.Year == shDatesS.Year)
                .Where(m => m.Month == shDatesS.Month)
                .OrderByDescending(b => b.Date)
                .First();
            ret.Add("AlShaya Son", periyotSon.Date.ToString());

            var iadePeriyotSon = _localDbContext.Calendars.AsNoTracking()
                .Where(y => y.Date > periyotSon.Date)
                .OrderBy(b => b.Date)
                .Skip(iadePeriyotGunu-1)
                .First();
            ret.Add("IadePeriyotSon", iadePeriyotSon.Date.ToString());
            return Ok(ret);
        }
    }
}
