﻿using Microsoft.AspNetCore.Mvc;
using System;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Net;
using Pusula.Api.Attributes;
using Pusula.Api.Models;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Formül işlemleri
    /// </summary>
    [Route("/svc")]
    public class FormulaController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <summary>
        /// Formül İşlemleri
        /// </summary>
        /// <param name="context"></param>
        public FormulaController(PatikaDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Yeni formül ekler
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("formula")]
        [ValidateModelState]
        [SwaggerOperation("FormulaAdd")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Created")]
        public IActionResult FormulaAdd([FromBody]Formula request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Veri modeli hatası");
                }
                using (_context)
                {
                    _context.Formulas.Add(request);
                    _context.SaveChanges();
                    var insertedRecord = _context.Formulas.Last();
                    ActionResponseContract responseContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Formül eklendi.",
                        Code = "FORMULA_ADD_SUCCESS",
                        Data = insertedRecord
                    };
                    return Ok(responseContract);
                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "FORMULA_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
                
            }
        }
        
        /// <summary>
        /// Formül günceller (.Content)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("formula")]
        [ValidateModelState]
        [SwaggerOperation("FormulaUpdate")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult FormulaUpdate([FromBody]Formula request)
        {
            var formula = _context.Formulas.Find(request.Id);
            if (formula != null)
            {
                formula.Content = request.Content;
                _context.SaveChanges();
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Formül güncellendi.",
                    Code = "FORMULA_UPDATE_SUCCESS",
                    Data = formula
                };
                return Ok(responseSuccessContract);  
            }
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Formül güncellenemedi.",
                Code = "FORMULA_UPDATE_FAILED",
            };
            return NotFound(responseContract);
        }
        
        /// <summary>
        /// Parametre listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("formula")]
        [ValidateModelState]
        [SwaggerOperation("FormulaGetAll")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ListResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Not found")]
        public IActionResult FormulaGetAll()
        {
            var parametres = _context.Formulas.ToList();
            if (parametres.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = parametres.Count},
                    Data = parametres
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Formül bulunamadı.",
                Code = "FORMULA_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }
        
        /// <summary>
        /// Parametre detay bilgileri
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("formula/{id}")]
        [ValidateModelState]
        [SwaggerOperation("FormulaGetById")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ListResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Not found")]
        public IActionResult FormulaGetById([FromRoute] int id)
        {
            var parametre = _context.Formulas.Find(id);
            if (parametre != null)
            {
                ListResponseContract responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = 1},
                    Data = parametre
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Formül bulunamadı.",
                Code = "FORMULA_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
            
        }
        
        /// <summary>
        /// Parametre sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("formula/{id}")]
        [ValidateModelState]
        [SwaggerOperation("FormulaDeleteById")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult FormulaDeleteById([FromRoute] int id)
        {
            var record = _context.Formulas.Find(id);
            if (record == null)
            {
                //Kayıt yoksa 
                ActionResponseContract failedResponseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = "Formül bulunamadı.",
                    Code = "FORMULA_EMPTY_SET"
                };
                return NotFound(failedResponseContract);
            }

            _context.Formulas.Remove(record);
            _context.SaveChanges();
            ActionResponseContract successResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Formül silindi.",
                Code = "FORMULA_DELETED"
            };
            return Ok(successResponseContract);
        }
        
        /// <summary>
        /// Formül editörü için şablon
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("formulaTemplate")]
        [SwaggerOperation("FormulaGetTemplate")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        public IActionResult FormulaGetTemplate()
        {
            var template = new FormulaTemplate();
            var responseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Formül şablonu.",
                Code = "FORMULA_TEMPLATE",
                Data = template
            };
            return StatusCode(200, responseContract);
        }
    }
}