using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NPOI.XSSF.UserModel;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Requests;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Infrastructures.Core.Extensions;
using Pusula.Api.Jobs;
using Pusula.Api.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Prim hesaplama işlemi
    /// </summary>
    [Route("/svc")]
    public class PrimHesaplamaController : Controller
    {

        private readonly PatikaDbContext _localDbContext;

        /// <summary>
        /// Prim hesaplama 
        /// </summary>
        /// <param name="localDbContext"></param>
        public PrimHesaplamaController(PatikaDbContext localDbContext)
        {
            _localDbContext = localDbContext;
            _localDbContext = localDbContext;
        }

        /// <summary>
        /// Hesaplama sonuçlarını getir.
        /// </summary>
        /// <param name="request">Sorgu Parametreleri</param>
        /// <returns></returns>
        [HttpPost]
        [Route("hesaplamaSonuc")]
        [ValidateModelState]
        [SwaggerOperation("PrimHesapmalaReport")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        public IActionResult PrimHesapmalaReport([FromBody] CalculationRequest request)
        {
            try
            {
                var calculatedBonuses = _localDbContext.VEmployeeBonuses.AsNoTracking();
                if (request.BrandId != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.BrandId == request.BrandId);
                var userArea = HttpContext.Items["UserArea"] as string;
                if (userArea == "S")
                {
                    var store = (int) HttpContext.Items["UserStore"];
                    calculatedBonuses = calculatedBonuses.Where(c => c.StoreId == store);
                }
                else
                {
                    if (request.StoreId != 0)
                        calculatedBonuses = calculatedBonuses.Where(c => c.StoreId == request.StoreId);
                }

                if (request.PositionId != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.PositionId == request.PositionId);
                if (request.EmployeeId != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.Sicil == request.EmployeeId);
                if (request.Month != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.Month == request.Month);
                if (request.Final != null && request.Final != "0")
                {
                    calculatedBonuses = calculatedBonuses.Where(c => c.Final == (request.Final == "2"));
                }

                // Prim tipine göre yetkilendirme fitresi 
                var authPrimTypes = (HttpContext.Items["PrimTypes"] as string) ?? "0";
                if (request.BrandId == 39 && authPrimTypes.Contains(request.PrimType))
                {
                    if (request.PrimType != "0") // (0=Hepsi)
                    {
                        //var primTypeList = authPrimTypes.Split(",").ToList();
                        calculatedBonuses = calculatedBonuses.Where(c => c.PrimType == request.PrimType);
                    }
                }

                calculatedBonuses = calculatedBonuses.Where(c => c.Year == request.Year);

                var sort = request.Sort ?? "surname,asc";
                var sortParam = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[0];
                var sortDir = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[1];
                switch (sortParam)
                {
                    case "brand":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Brand)
                            : calculatedBonuses.OrderByDescending(o => o.Brand);
                        break;
                    case "store":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Store)
                            : calculatedBonuses.OrderByDescending(o => o.Store);
                        break;
                    case "position":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Position)
                            : calculatedBonuses.OrderByDescending(o => o.Position);
                        break;
                    case "name":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Name)
                            : calculatedBonuses.OrderByDescending(o => o.Name);
                        break;
                    case "surname":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Surname)
                            : calculatedBonuses.OrderByDescending(o => o.Surname);
                        break;
                    case "sicil":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Sicil)
                            : calculatedBonuses.OrderByDescending(o => o.Sicil);
                        break;
                    case "bonus":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Bonus)
                            : calculatedBonuses.OrderByDescending(o => o.Bonus);
                        break;

                    default:
                        calculatedBonuses = calculatedBonuses.OrderBy(o => o.Id);
                        break;
                }

                var calculatedBonusesList = calculatedBonuses
                    .Select(x => new
                    {
                        x.Id,
                        x.Brand,
                        x.Store,
                        x.Position,
                        x.Name,
                        x.Surname,
                        x.Sicil,
                        x.Actual,
                        x.ActualBrut,
                        x.Month,
                        x.Bonus,
                        x.Notes,
                        x.Target,
                        x.Final,
                        x.BonusState,
                        x.BonusRate,
                        x.MonthlyTotal,
                        x.BonusTotalDays,
                        x.BonusFreeDays,
                        x.BonusPayDays,
                        x.Discipline
                    });

                if (calculatedBonusesList.Any())
                {
                    var page = request.Page ?? 0;
                    var itemsPerPage = request.Size ?? 50;

                    var tmpContent = calculatedBonusesList
                        .Skip(page * itemsPerPage)
                        .Take(itemsPerPage)
                        .ToList();

                    //var response = new PagingResponse(calculatedBonusesList, tmpContent, request);
                    var sortingen = new[]
                    {
                        new
                        {
                            property = sortParam,
                            direction = sortDir,
                            ascending = sortDir == "asc",
                            descending = sortDir == "desc",
                            ignoreCase = false,
                            nullHandling = "NATIVE"
                        }
                    };

                    var resp = new
                    {
                        first = page == 0,
                        last = page == calculatedBonusesList.Count() / itemsPerPage + 1,
                        number = page,
                        numberOfElements = tmpContent.Count,
                        size = itemsPerPage,
                        sort = sortingen,
                        totalElements = calculatedBonusesList.Count(),
                        totalPages = (calculatedBonusesList.Count() / itemsPerPage) + 1,
                        content = tmpContent
                    };

                    return Ok(resp);
                }

                var calcResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = "Hesaplama sonucu yok.",
                    Code = "CALCULATION_EMPTY_SET",
                };
                return NotFound(calcResponse);
            }
            catch (Exception ex)
            {
                var calculationResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "CALCULATION_RESULTS_ERROR",
                    Data = ex
                };
                return NotFound(calculationResponse);
            }
        }

        /// <summary>
        /// Prim hesaplama sonuçları Excel exportu
        /// </summary>
        /// <param name="request">CalculationRequest</param>
        /// <returns>Binary xlsx file</returns>
        [HttpPost]
        [Route("hesaplamaSonucFile")]
        [ValidateModelState]
        [SwaggerOperation("PrimHesapmalaReportFile")]
        [Consumes("application/json")]
        public IActionResult PrimHesapmalaReportFile([FromBody] CalculationRequest request)
        {
            try
            {
                var calculatedBonuses = _localDbContext.VEmployeeBonuses.AsNoTracking();
                if (request.BrandId != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.BrandId == request.BrandId);
                var userArea = HttpContext.Items["UserArea"] as string;
                if (userArea == "S")
                {
                    var store = (int) HttpContext.Items["UserStore"];
                    calculatedBonuses = calculatedBonuses.Where(c => c.StoreId == store);
                }
                else
                {
                    if (request.StoreId != 0)
                        calculatedBonuses = calculatedBonuses.Where(c => c.StoreId == request.StoreId);
                }

                if (request.PositionId != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.PositionId == request.PositionId);
                if (request.EmployeeId != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.Sicil == request.EmployeeId);
                if (request.Month != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.Month == request.Month);
                if (request.Final != null && request.Final != "0")
                {
                    calculatedBonuses = calculatedBonuses.Where(c => c.Final == (request.Final == "2"));
                }

                calculatedBonuses = calculatedBonuses.Where(c => c.Year == request.Year);

                calculatedBonuses = calculatedBonuses
                    .OrderBy(x => x.Brand)
                        .ThenBy(x => x.Store)
                            .ThenBy(x => x.Position)
                                .ThenBy(x => x.Sicil);
                var calculatedBonusesList = calculatedBonuses
                    .Select(x => new
                    {
                        x.Id,
                        x.Brand,
                        x.Store,
                        x.Position,
                        x.Name,
                        x.Surname,
                        x.Sicil,
                        x.Actual,
                        x.ActualBrut,
                        x.Month,
                        x.Bonus,
                        x.Notes,
                        x.Target,
                        x.Final,
                        x.BonusState,
                        x.BonusRate,
                        x.MonthlyTotal,
                        x.BonusTotalDays,
                        x.BonusFreeDays,
                        x.BonusPayDays,
                        x.Discipline
                    });

                if (calculatedBonusesList.Any())
                {

                    var workbook = new XSSFWorkbook();
                    var sheet = workbook.CreateSheet($"Prim {request.Year}-{request.Month}");
                    
                    var tmpContent = calculatedBonusesList.ToList();

                    var columns = new[] { "Id", "Brand", "Store", "Sicil",  "Name", "Surname", "Position",
                        "Target", "Actual", "ActualBrut", "Month", "Bonus", "Final", "BonusState",
                        "BonusRate",  "BonusTotalDays", "BonusFreeDays", "BonusPayDays",
                        "Discipline","Notes" };  
  
                    var headers = new[] { "Id", "Marka", "Mağaza", "Sicil", "Ad", "Soyad", "Pozisyon", 
                        "Hedef", "Gerçekleşen", "Gerçekleşen Brüt", "Periyot", "Prim Tutarı", "Final", "Prim Alma Durumu",
                        "Prim Oranı", "Prim Gün Sayısı", "Kesilecek Rapor/Ücretsiz İzin Gün Sayısı", "Prim Ödenecek Gün Sayısı",
                        "Disiplin Ceza Bilgisi", "Notlar"
                        
                    };  
                    
                    var headerRow = sheet.CreateRow(0);  
  
                    for (var i = 0; i < columns.Length; i++)  
                    {  
                        var cell = headerRow.CreateCell(i);  
                        cell.SetCellValue(headers[i]);  
                    } 

                    for (var i = 0; i < tmpContent.Count; i++)  
                    {  
                        var rowIndex = i + 1;  
                        var row = sheet.CreateRow(rowIndex);  
  
                        for (var j = 0; j < columns.Length; j++)  
                        {  
                            var cell = row.CreateCell(j);  
                            var o = tmpContent[i];
                            var col = o.GetType().GetProperty(columns[j]).GetValue(o, null);
                            var val = col?.ToString() ?? "";
                            if (columns[j] == "Final")
                            {
                                val =col != null && (bool) col ? "Final" : "Taslak";
                            }

                            cell.SetCellValue(val);  
                        }  
                    }

                    var exportData = new NPOIMemoryStream {AllowClose = false};

                    workbook.Write(exportData);
                    
                    exportData.Seek(0, SeekOrigin.Begin);
                    exportData.AllowClose = true;
                    return File(exportData, "application/vnd.ms-excel", $"prim-{request.Year}-{request.Month}.xlsx");
                }

                var calcResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = "Hesaplama sonucu yok.",
                    Code = "CALCULATION_EMPTY_SET",
                };
                return NotFound(calcResponse);
            }
            catch (Exception ex)
            {
                var calculationResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "CALCULATION_RESULTS_ERROR",
                    Data = ex
                };
                return NotFound(calculationResponse);
            }
        }


        /// <summary>
        /// Hesaplama bilgisi ver
        /// </summary>
        /// <param name="hesaplamaId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("hesaplamaDurum/{hesaplamaId}")]
        [SwaggerOperation("HesaplamaDurum")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ActionResponseContract), "Success")]
        public IActionResult HesaplamaDurum([FromRoute] int hesaplamaId)
        {
            using (_localDbContext)
            {
                var calculation = _localDbContext.Calculations.Find(hesaplamaId);
                if (calculation != null)
                {
                    if (calculation.Related != 0)
                    {
                        var related = _localDbContext.Calculations.Find(calculation.Related);
                        if (related != null)
                        {
                            calculation.Total += related.Total;
                            calculation.Processed += related.Processed;
                        }
                    }
                    var calculationContinuesResponse = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Hesaplama bilgileri.",
                        Code = "CALCULATION_INFO",
                        Data = calculation
                    };
                    return Ok(calculationContinuesResponse);
                }
            }

            var calculationResponse = new ActionResponseContract
            {
                Success = false,
                Message = "Kritere uygun bir hesaplama yok.",
                Code = "CALCULATION_EMPTY_SET",
            };
            return NotFound(calculationResponse);
        }


        /// <summary>
        /// Tüm hesaplamalar listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("hesaplamalar")]
        [SwaggerOperation("PrimHesaplamalarListele")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        public IActionResult PrimHesaplamalarListele()
        {
            var calcsList = _localDbContext.Calculations.ToList();
            if (calcsList.Count > 0)
            {
                var calculationContinuesResponse = new ListResponseContract
                {
                    Total = new TotalProperty
                    {
                        All = calcsList.Count,
                        Filtered = calcsList.Count
                    },
                    Offset = 0,
                    Limit = calcsList.Count,
                    Data = calcsList
                };
                return Ok(calculationContinuesResponse);

            }
            else
            {
                var calculationResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = "Hesaplama listesi boş.",
                    Code = "CALCULATION_EMPTY_SET"
                };
                return NotFound(calculationResponse);
            }
        }


        /// <summary>
        /// Yeni hesaplama işi
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpPost]
        [Route("hesapla")]
        [ValidateModelState]
        [SwaggerOperation("PrimHesapmalaAdd")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ActionResponseContract), "Created")]
        public IActionResult PrimHesapmalaAdd([FromBody] CalculationRequest request)
        {
            var calculation = _localDbContext.Calculations.FirstOrDefault(m => m.Active);
            if (calculation != null)
            {
                //Hesaplama tek iş çalışacak. Aktik iş varsa devam etmiyoruz. 
                var calculationContinuesResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = "Yürütülen bir hesaplama var.",
                    Code = "CALCULATION_CONTINUIES",
                    Data = calculation
                };
                return Ok(calculationContinuesResponse);
            }
            
            string bonusType; // SLT / NSL
            var theResponse = "Detay: ";
            var relatedCalcId = 0;

            #region Prim tipini belirle (NONSLT/SLT)

            try
            {
                var brand = _localDbContext.Brands.Find(request.BrandId);
                if (brand == null)
                {
                    bonusType = "HATA";
                }else if (brand.ShortName == "VS")
                {
                    var sas = _localDbContext.SasMonthses.AsNoTracking()
                        .FirstOrDefault(x => x.Brand == brand);
                    if (sas != null && sas.IsSasMonth(request.Month))
                    {
                        bonusType = "SLT";
                    }
                    else
                    {
                        bonusType = "NSL,SLT";
                    }
                }
                else
                {
                    bonusType = "SLT";
                }

                if (bonusType == "HATA")
                {
                    throw new Exception("Marka beklenmeyen değerde.");
                }
            }
            catch (Exception ex)
            {
                var failedResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "CALCULATION_ADD_FAILED",
                    Data = ex
                };
                return NotFound(failedResponse);
            }
            
            #endregion
            
            #region NonSLT hesaplama işlemi
            
            if (bonusType.Contains("NSL"))
            { 
                
                var bradList = new List<int> {39};
                try
                {
                    var thisDay = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                    var shayaDate = _localDbContext.Calendars.AsNoTracking()
                        .Where(c => c.Date == thisDay)
                        .OrderBy(c => c.Id)
                        .LastOrDefault();
                    if (shayaDate == null)
                    {
                        // Hesaplamalarda kullanıldığından, gerekli.
                        throw new Exception("Al-Shaya takvimi yüklenmemiş.");
                    }

                    if (request.Year >= shayaDate.Year && request.Month >= shayaDate.Month)
                    {
                        //İçinde bulunduğumuz ay ve sonrası hesaplanma yapılamaz.
                        throw new Exception("İstenen hesaplama gelecek verisi içeriyor.");
                    }

                    if (request.Month < 1 || request.Month > 12)
                    {
                        throw new Exception("İstenen hesaplama bilinmeyen bir aya ait.");
                    }

                    var calcFilter = "";

                    // Hesaplama listesi ay başında oluşturuldu,
                    // Final statüde olmayanlarla başla
                    var calcEmployees = _localDbContext.EmployeeBonuses
                        .Where(x => x.Final == false)
                        .Where(x => x.Year == request.Year)
                        .Where(x => x.Month == request.Month);

                    if (request.EmployeeId == 0)
                    {
                        if (request.StoreId == 0)
                        {
                            // Mağaza hepsi dediyse markalara bakıp ekleyelim
                            if (request.BrandId == 0)
                            {
                                calcEmployees = calcEmployees
                                    .Where(b => bradList.Contains(b.Brand.Id));
                                calcFilter = "Tüm Markalar/";
                            }
                            else
                            {
                                var brand = _localDbContext.Brands.AsNoTracking()
                                    .First(p => p.Id == request.BrandId);
                                calcEmployees = calcEmployees
                                    .Where(b => b.Brand.Id == request.BrandId);
                                calcFilter = $"{brand.Name}/";
                            }
                        }
                        else
                        {
                            // Mağaza tek ise Marka eklemeye gerek yok
                            var store = _localDbContext.Stores.AsNoTracking()
                                .First(s => s.Id == request.StoreId);
                            calcEmployees = calcEmployees
                                .Where(s => s.Store.Id == request.StoreId);
                            calcFilter = $"VS/{store.Name}/";
                        }

                        if (request.PositionId == 0)
                        {
                            calcFilter += "Tüm Pozisyonlar/";
                        }
                        else
                        {
                            var pozisyon = _localDbContext.Positions.AsNoTracking()
                                .First(p => p.Id == request.PositionId);
                            calcEmployees = calcEmployees
                                .Where(p => p.Position.Id == request.PositionId);
                            calcFilter = $"{pozisyon.Name}/";
                        }

                        calcFilter += "Tüm Personel/";
                    }
                    else
                    {
                        var emp = _localDbContext.Employees.AsNoTracking()
                            .First(e => e.Sicil == request.EmployeeId);
                        calcEmployees = calcEmployees
                            .Where(e => e.Employee.Sicil == request.EmployeeId);

                        calcFilter += $"{emp.Name} {emp.Surname}/";
                    }

                    calcFilter += $"{request.Year}/{request.Month}";

                    
                    if (calcEmployees.Any())
                    {
                        var newCalculation = new Calculation
                        {
                            Active = true,
                            Filter = calcFilter, // "*/*/*/*/2018/9",
                            Processed = 0,
                            Title = "NON-SLT Monthly",
                            Total = 0,
                            LastRun = 0,
                            Fyear = request.Year,
                            Fmonth = request.Month,
                            Fbrand = request.BrandId,
                            Fstore = request.StoreId,
                            Fposition = request.PositionId,
                            Femployee = request.EmployeeId
                        };
                        _localDbContext.Calculations.Add(newCalculation);
                        _localDbContext.SaveChanges();
                        foreach (var e in calcEmployees)
                        {
                            e.Amount = 0;
                            e.Calculation = newCalculation;
                            e.Calculated = false;
                            e.Notes = ".";

                        }

                        _localDbContext.SaveChanges();


                        var numRecords = _localDbContext.EmployeeBonuses
                            .Count(e => e.Calculation.Id == newCalculation.Id);
                        if (numRecords <= 0)
                        {
                            _localDbContext.Calculations.Remove(newCalculation);
                            _localDbContext.SaveChanges();
                            throw new Exception("Filtreye uyan personel yok.");
                        }

                        newCalculation.Total = numRecords;
                        if (bonusType == "NSL,SLT")
                        {
                            relatedCalcId = relatedCalcId == 0 ? newCalculation.Id ?? 0 : relatedCalcId;
                        }
                        newCalculation.Related = relatedCalcId;
                        _localDbContext.SaveChanges();

                        BackgroundJobClient jobs = new BackgroundJobClient();
                        var j1 = jobs.Enqueue(() => Console.WriteLine(" *** Calculation Non-SLT Started  ***"));
                        var j2 = jobs.ContinueWith<Calculator>(j1, x => x.PrecalcAsync());
                        var j3 = jobs.ContinueWith<Calculator>(j2, x => x.CalculateAsync());
                        var j4 = jobs.ContinueWith<Calculator>(j3, x => x.CleanUpAsync());
                        jobs.ContinueWith(j4, () => Console.WriteLine(" *** Calculation Non-SLT Finished ***"));
                        calculation = newCalculation;
                        theResponse += "Non-SLT Aylık hesaplama eklendi. ";
                    }
                    else
                    {
                        throw new Exception("Filtreye uyan personel yok.");
                    }
                }
                catch (Exception ex)
                {
                    var failedResponse = new ActionResponseContract
                    {
                        Success = false,
                        Message = ex.Message,
                        Code = "CALCULATION_ADD_FAILED",
                        Data = ex
                    };
                    return NotFound(failedResponse);
                }
            }
            
            #endregion

            #region SLT Prim hesaplama işlemi

            if (bonusType.Contains("SLT"))
            {
                // SLT Aylık Prim hesabı
                try
                {
                    var today = DateTime.Today;
                    if (request.Year >= today.Year && request.Month >= today.Month)
                    {
                        //İçinde bulunduğumuz ay ve sonrası hesaplanma yapılamaz.
                        throw new Exception("İstenen hesaplama gelecek verisi içeriyor.");
                    }

                    var filter = $"{request.Year}/{request.Month}";
                    if (request.BrandId == 0)
                    {
                        filter += "/Tüm Markalar";
                    }
                    else
                    {
                        var b = _localDbContext.Brands.Find(request.BrandId);
                        filter += "/" + b.Name;
                    }

                    if (request.StoreId == 0)
                    {
                        filter += "/Tüm Mağazalar";
                    }
                    else
                    {
                        var s = _localDbContext.Stores.Find(request.StoreId);
                        filter += "/" + s.Name;
                    }

                    var newCalculation = new Calculation
                    {
                        Active = true,
                        Filter = filter,
                        Processed = 0,
                        Title = "SLT Monthly",
                        Total = 0,
                        LastRun = 0,
                        Fyear = request.Year,
                        Fmonth = request.Month
                    };
                    _localDbContext.Calculations.Add(newCalculation);
                    _localDbContext.SaveChanges();

                    var brsEmployees = _localDbContext.BonusSltMonthly
                        .Where(x => x.Final == false)
                        .Where(x => x.Year == request.Year)
                        .Where(x => x.Month == request.Month);

                    if (request.EmployeeId == 0)
                    {
                        if (request.StoreId == 0)
                        {
                            // Marka hepsi değilse
                            if (request.BrandId != 0)
                            {
                                brsEmployees = brsEmployees
                                    .Where(b => b.Brand.Id == request.BrandId);
                            }
                        }
                        else
                        {
                            // Mağaza tek ise Marka eklemeye gerek yok
                            brsEmployees = brsEmployees
                                .Where(s => s.Store.Id == request.StoreId);
                        }

                        if (request.PositionId != 0)
                        {
                            brsEmployees = brsEmployees
                                .Where(p => p.Position.Id == request.PositionId);
                        }
                    }
                    else
                    {
                        brsEmployees = brsEmployees
                            .Where(e => e.Employee.Sicil == request.EmployeeId);
                    }

                    var sas = _localDbContext.SasMonthses.AsNoTracking()
                        .FirstOrDefault(x => x.Brand.Id == request.BrandId);
                    if (sas != null && !sas.IsSasMonth(request.Month))
                    {
                        //SAS ayları dışında Non-SLT için hesaplama yapma.
                        brsEmployees = brsEmployees
                            .Where(p => p.Position.HasBonus == false);
                    }

                    foreach (var e in brsEmployees)
                    {
                        e.Amount = 0;
                        e.Calculation = newCalculation;
                        e.Calculated = false;
                        e.Notes = ".";

                    }


                    var sql = "UPDATE BonusSltMonthly " +
                              $"SET CalculationId={newCalculation.Id} " +
                              $"WHERE Year={request.Year} AND Month={request.Month} ";
                    if (request.BrandId != 0)
                        sql += $"AND BrandId={request.BrandId} ";
                    if (request.StoreId != 0)
                        sql += $"AND StoreId={request.StoreId} ";
                    if (request.PositionId != 0)
                        sql += $"AND PositionId={request.PositionId} ";
                    if (request.EmployeeId != 0)
                        sql += $"AND Sicil={request.EmployeeId} ";

                    _localDbContext.Database.ExecuteSqlCommand(sql);


                    var numRecords = _localDbContext.BonusSltMonthly
                        .Count(e => e.Calculation.Id == newCalculation.Id);
                    if (numRecords <= 0)
                    {
                        _localDbContext.Calculations.Remove(newCalculation);
                        _localDbContext.SaveChanges();
                        //throw new Exception("Filtreye uyan personel yok.");
                    }
                    else
                    {
                        newCalculation.Total = numRecords;
                        newCalculation.Fbrand = request.BrandId;
                        newCalculation.Fstore = request.StoreId;
                        newCalculation.Fposition = request.PositionId;
                        newCalculation.Femployee = request.EmployeeId;

                        if (bonusType == "NSL,SLT")
                        {
                            relatedCalcId = relatedCalcId == 0 ? newCalculation.Id ?? 0 : relatedCalcId;
                        }

                        newCalculation.Related = relatedCalcId;
                        _localDbContext.SaveChanges();

                        BackgroundJobClient jobs = new BackgroundJobClient();
                        var j1 = jobs.Enqueue(() => Console.WriteLine(" *** CalculationSLT Started  ***"));
                        var j3 = jobs.ContinueWith<Calculator>(j1, x => x.SltAylikPrim());
                        jobs.ContinueWith(j3, () => Console.WriteLine(" *** CalculationSLT Finished ***"));

                        calculation = newCalculation;
                        theResponse += "SLT Aylık hesaplama eklendi. ";
                    }
                }
                catch (Exception ex)
                {
                    var failedResponse = new ActionResponseContract
                    {
                        Success = false,
                        Message = ex.Message,
                        Code = "CALCULATIONSLT_ADD_FAILED",
                        Data = ex
                    };
                    return NotFound(failedResponse);
                }
            }

            #endregion

            var responseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Hesaplama işi eklendi." + bonusType + theResponse,
                Code = "CALCULATION_ADD_SUCCESS",
                Data = calculation
            };
            return Ok(responseContract);
            
        }


        /// <summary>
        /// Taslak hesaplamaları kalıcı yapar
        /// </summary>
        /// <param name="draftIds"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("draftToFinal")]
        [ValidateModelState]
        [SwaggerOperation("PrimHesaplamaToFinal")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ActionResponseContract), "Success")]
        public IActionResult PrimHesaplamaToFinal([FromBody] int[] draftIds)
        {
            try
            {
                var draftIdsEnumarable = new List<int>();
                foreach (var draftId in draftIds)
                {
                    draftIdsEnumarable.Add(draftId);
                }

                var ueb = _localDbContext.EmployeeBonuses
                    .Where(i => draftIdsEnumarable.Contains(i.Id ?? 0));
                if (ueb.Any())
                {
                    ueb.ToList().ForEach(a => { a.Final = true; });
                }

                var ubm =_localDbContext.BonusSltMonthly
                    .Where(i => draftIdsEnumarable.Contains(i.Id));
                if (ubm.Any())
                {
                    ubm.ToList()
                        .ForEach(a => { a.Final = true; });
                }

                _localDbContext.SaveChanges();

                var responseContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Hesaplama kalıcı hale getirildi.",
                    Code = "CALCULATION_FINALIZE_SUCCESS"
                };
                return Ok(responseContract);
            }
            catch (Exception e)
            {
                var failedResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = e.Message,
                    Code = "CALCULATION_FINALIZE_FAILED",
                    Data = e
                };
                return NotFound(failedResponse);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns>Mağaza satış ve periyot bilgileri</returns>
        [HttpGet]
        [Route("storeActualsInfo")]
        [SwaggerOperation("StoreActualsInfo")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(StoreActualsResponse), "Success")]
        public IActionResult StoreActualsInfo([FromQuery] int storeId, [FromQuery] int year, [FromQuery] int month)
        {
            try
            {
                var iadePeriyotGunu = 13;
                var paramIadePeriyotGunu = _localDbContext.Parametres.AsNoTracking()
                    .FirstOrDefault(x => x.Title == "RefundPeriodStartDay");
                if (paramIadePeriyotGunu != null)
                {
                    iadePeriyotGunu = int.Parse(paramIadePeriyotGunu.Value);
                }
                var periyotBasS = _localDbContext.Calendars.AsNoTracking()
                    .Where(y => y.Year == year)
                    .Where(m => m.Month == month)
                    .Min(b => b.Date)
                    .ToString();
                var periyotSonS = _localDbContext.Calendars.AsNoTracking()
                    .Where(y => y.Year == year)
                    .Where(m => m.Month == month)
                    .Max(b => b.Date)
                    .ToString();
                var iadePeriyotBas = _localDbContext.Calendars.AsNoTracking()
                    .Where(y => y.Year == year)
                    .Where(m => m.Month == month)
                    .OrderBy(b => b.Date)
                    .Skip(iadePeriyotGunu-1)
                    .First();
                var iadePeriyotSon = _localDbContext.Calendars.AsNoTracking()
                    .Where(y => y.Year == year)
                    .Where(y => y.Date > int.Parse(periyotSonS))
                    .OrderBy(b => b.Date)
                    .Skip(iadePeriyotGunu-1)
                    .First();
                var iadePeriyotBasS = iadePeriyotBas.Date.ToString();
                var iadePeriyotSonS = iadePeriyotSon.Date.ToString();

                var storeIade = 0;
                var storeActual = 0;
                var storeBrut = 0;
                var storeTargetRetail = _localDbContext.StoreTargetsRetail.AsNoTracking()
                    .Where(y => y.Year == year)
                    .FirstOrDefault(s => s.Store.Id == storeId);
                if (storeTargetRetail != null)
                {
                    storeActual = storeTargetRetail.GetActual(month);
                    storeIade = storeTargetRetail.GetIade(month);
                    storeBrut = storeActual + storeIade;
                }
                
                var saResponse = new StoreActualsResponse
                {
                    PeriyotBaslangic = periyotBasS,
                    PeriyotSonu = periyotSonS,
                    IadeBaslangic = iadePeriyotBasS,
                    IadeSonu = iadePeriyotSonS,
                    Iade = storeIade,
                    Actual = storeActual,
                    Brut = storeBrut
                };
                

                var responseContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Mağaza aktif satış bilgileri.",
                    Code = "ACTUALINFO_SUCCESS",
                    Data = saResponse
                };
                return Ok(responseContract);
            }
            catch (Exception e)
            {
                var calculationResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = $"Hesaplama listesi boş. {e.Message}",
                    Code = "ACTUALINFO_EMPTY_SET"
                };
                return NotFound(calculationResponse);
            }
        }

        /// <summary>
        /// Yıllık Prim hesaplama ekle
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("yhesapla")]
        [ValidateModelState]
        [SwaggerOperation("SPrimHesapmalaAdd")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ActionResponseContract), "Created")]
        public IActionResult SPrimHesapmalaAdd([FromBody] YearlyCalculationAddRequest req)
        {
            try
            {
                using (_localDbContext)
                {
                    var ycalc = new Calculation
                    {
                        Active = true,
                        Fbrand = req.BrandId,
                        Femployee = 0,
                        Filter = $"{req.Type}/{req.Year}/{req.BrandId}/{req.StoreId}/{req.PositionId}",
                        Fmonth = 13,
                        Fposition = req.PositionId,
                        Fstore = req.StoreId,
                        Fyear = req.Year,
                        Processed = 0,
                        Total = 10,
                        LastRun = 0,
                        Title = "AnnualDraft"
                    };
                    _localDbContext.Calculations.Add(ycalc);
                    _localDbContext.SaveChanges();

                    var responseContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Yıllık Hesaplama işi eklendi.",
                        Code = "ANNUALCALCULATION_ADD_SUCCESS",
                        Data = new
                        {
                            request = req,
                            calc = ycalc
                        }
                    };
                    return Ok(responseContract);
                }
            }
            catch (Exception ex)
            {
                var failedResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "ANNUALCALCULATION_ADD_FAILED",
                    Data = ex
                };
                return NotFound(failedResponse);
            }
        }
    }
}
