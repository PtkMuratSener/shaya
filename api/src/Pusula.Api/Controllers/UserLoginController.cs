﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Novell.Directory.Ldap;
using Oracle.ManagedDataAccess.Client;
using Pusula.Api.Attributes;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Models;


namespace Pusula.Api.Controllers
{
    /// <summary>
    /// Kullanıcı LDAP login işleri
    /// </summary>
    public class UserLoginController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly PatikaDbContext _localDbContext;
        private HttpContext _httpContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="localDbContext"></param>
        public UserLoginController(IConfiguration configuration, PatikaDbContext localDbContext)
        {
            _httpContext = HttpContext;
            _configuration = configuration;
            _localDbContext = localDbContext;
        }

        /// <summary>
        /// Kullanıcı LDAP Yetki
        /// </summary>
        /// <param name="userName">Kullanıcı adı</param>
        /// <param name="password">Kullanıcı parolası</param>
        /// <response code="200">success</response>
        /// <response code="404">failed</response>
        [HttpPost]
        [Route("/login")]
        [ValidateModelState]
        [SwaggerOperation("LoginPost")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Success")]
        [SwaggerResponse(statusCode: 404, type: typeof(string), description: "Failed")]
        public IActionResult LoginPost([FromForm] [Required()] string userName,
            [FromForm] [Required()] string password)
        {
            try
            {
                var adSoyad = "";
                var sicil = 0; 
                
                var da = Environment.GetEnvironmentVariable("DISABLE_AUTH");
                if (da == null)
                {
                    var conn = new LdapConnection();
                    conn.Connect(_configuration["LdapHost"], Convert.ToInt32(_configuration["LdapPort"]));
                    conn.Bind(LdapConnection.Ldap_V3, $@"mha\{userName}", password);

                    var searchBase = "DC=mha,DC=local";
                    var searchFilter = $"(sAMAccountName={userName})";


                    var attributes = new string[]
                        {"samaccountname", "displayname", "thumbnailphoto", "employeeID", "extensionAttribute12"};


                    var queue = conn.Search(searchBase, LdapConnection.SCOPE_SUB, searchFilter, attributes,
                        false, null, null);

                    LdapMessage message;

                    while ((message = queue.getResponse()) != null)
                    {
                        if (!(message is LdapSearchResult)) continue;

                        var entry = ((LdapSearchResult) message).Entry;

                        var attributeSet = entry.getAttributeSet();
                        try
                        {

                            var sicilStr = "0";
                            if (sicilStr == "0" && attributeSet.getAttribute("extensionAttribute12") != null)
                            {
                                sicilStr = attributeSet.getAttribute("extensionAttribute12").StringValue;
                            }

                            sicil = Convert.ToInt32(sicilStr);

                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.Message);
                        }

                        adSoyad = attributeSet.getAttribute("displayName").StringValue;
                        Debug.WriteLine(attributeSet.getAttribute("displayName").StringValue);
                    }
                }
                
                // Users tablosunda yoksa ekleyelim.
                var user = _localDbContext.Users
                    .FirstOrDefault(m => m.LdapUsername == userName);
                
                if (user == null)
                {
                    var hrEmpSql = "SELECT DISTINCT (SICIL_NO) AS Sicil, ADI, SOYADI, LDAP_USERNAME AS LdapUsername, " +
                                   "ISYERI_MASRAFMR AS Store, SUBSTR(PERSONEL_TURU, 1, 1), SUBSTR(CINSIYET, 1, 1) " +
                                   "FROM IKYS_SHY.T_PERSONEL  P WHERE " +
                                   $"P.LDAP_USERNAME = '{userName}'";
                    var oraConnection =
                        new OracleConnection(_configuration.GetConnectionString("ShayaOracleConnectionString"));
                    oraConnection.Open();
                    OracleCommand hrEmpCmd = oraConnection.CreateCommand();
                    hrEmpCmd.CommandText = hrEmpSql;
                    OracleDataReader hrEmpRdr = hrEmpCmd.ExecuteReader();

                    if (hrEmpRdr.Read())
                    {
                        adSoyad = hrEmpRdr.GetString(1) + " " + hrEmpRdr.GetString(2);
                        sicil = int.Parse(hrEmpRdr.GetString(0));
                    }

                    user = new User
                    {
                        Sicil = sicil,
                        LdapUsername = userName,
                        NameSurname = adSoyad,
                        Photo = ".",
                        SessionKey = "."
                    };
                    _localDbContext.Users.Add(user);
                }
                

                var plainText = Guid.NewGuid().ToString() + $":{userName}:{user.Sicil}:" +
                                new Random().Next(1000, 9999).ToString();
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
                var sessionKey = Convert.ToBase64String(plainTextBytes);
                user.SessionKey = sessionKey;
                user.SessionValidTil = DateTimeOffset.Now.AddMinutes(30).ToUnixTimeSeconds();
                _localDbContext.SaveChanges();

                List<int> roleIds = new List<int>();

                var registryRoleAssignments = _localDbContext.RegistryRoleAssignments.AsNoTracking()
                    .Include(t => t.Role)
                    .Where(r => r.Sicil == user.Sicil);
                /* hocam sen kullanıcı giriş yaptığından
                 * önce elinde kullanıcının sicil no su ile  RegistryRoleAssignments tablosuna gidiceksin
                 * buradan o sicile ait tüm rolleri çekeceksin...
                 */
                if (registryRoleAssignments.Any())
                {
                    foreach (var registryRoleAssignment in registryRoleAssignments)
                    {
                        roleIds.Add(registryRoleAssignment.Role.Id);
                    }
                }

                /*
                 * daha sonra giriş yapan
                 * kullanıcının pozisyonId değperi ile
                 * titleRoleAssignments tablosuna gideceksin..
                 * burdan da tüm rolIdleri çekeceksin
                 */
                var employee = _localDbContext.Employees
                    .Include(p => p.Position)
                    .FirstOrDefault(p => p.Sicil == user.Sicil);
                if (employee != null)
                {
                    var titleRoleAssignments = _localDbContext.TitleRoleAssignments
                        .AsNoTracking()
                        .Include(a => a.Role)
                        .Where(t => t.Position.Id == employee.Position.Id);

                    if (titleRoleAssignments.Any())
                    {
                        foreach (var titleRoleAssignment in titleRoleAssignments)
                        {
                            roleIds.Add(titleRoleAssignment.Role.Id);
                        }
                    }
                }

                var roleIdsArray = roleIds.ToArray();
                /*
                foreach (var ri in roleIdsArray)
                {
                    Debug.WriteLine($"RoleIds: {ri}");
                }
                */
                
                var rps = _localDbContext.RolePermissions.AsNoTracking()
                    .Include(a => a.Screen)
                    .Include(b => b.Role)
                    .Include(c => c.AuthorizationType)
                    .Include(d => d.StoreType)
                    .Include(e => e.WorkingArea)
                    .Where(y => roleIdsArray.Contains(y.Role.Id))
                    .ToList();

                return Ok(new
                {
                    success = true,
                    message = "Login ok.",
                    code = "LOGIN_OK",
                    data = new
                    {
                        token = sessionKey,
                        nameSurname = user.NameSurname,
                        sicil = user.Sicil,
                        photo = user.Photo,
                        role = rps
                    }
                });
            }
            catch (Exception ex)
            {
                return NotFound(new
                {
                    success = false,
                    message = ex.Message,
                    code = "LOGIN_FAILED",
                    data = ex
                });
            }
        }
    }
}