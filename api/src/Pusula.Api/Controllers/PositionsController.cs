using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <summary>
    /// Pozisyon işlemleri
    /// </summary>
    [Route("/svc")]
    public class PositionsController : Controller
    {

        private readonly PatikaDbContext _context;


        /// <inheritdoc />
        public PositionsController(PatikaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Yılllık prim oranı hesaplanacak pozisyonlar
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("posHasNoYearlyBonus/{brandId}")]
        [ValidateModelState]
        [SwaggerOperation("PosHasNoYearlyBonusList")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult PosHasNoYearlyBonusList([FromRoute] int brandId)
        {
            var positionsHasYearlyBonus = _context.BonusRatesSlt
                .Where(m => m.Brand.Id == brandId)
                .Select(x => new PositionsHasYearlyBonusTemplate
                {
                    Id = x.Id,
                    BrandId = x.Brand.Id,
                    PositionId = x.Position.Id,
                    PositionName = x.Position.Name,
                    HasYearlyBonus = x.HasYearlyBonus
                });
            if (positionsHasYearlyBonus.Any())
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Yıllık prim oranı hesaplanacak pozisyonlar listesi başarılı.",
                    Code = "POSHASYEARLYBONUS_GET_SUCCESS",
                    Data = positionsHasYearlyBonus
                };
                return Ok(responseSuccessContract);
            }

            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Bulunamadı.",
                Code = "POSHASYEARLYBONUS_EMPTY_SET"
            };
            return NotFound(responseContract);
        }
        
        /// <summary>
        /// Yıllık prim hesaplanacak pozisyonları kaydet.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("posHasNoYearlyBonus")]
        [ValidateModelState]
        [SwaggerOperation("PosHasNoYearlyBonusSave")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult PosHasNoYearlyBonusSave([FromBody]List<PositionsHasYearlyBonusTemplate> request)
        {
            try
            {
                using (_context)
                {
                    foreach (var req in request)
                    {
                        var bonusRate = _context.BonusRatesSlt
                            .FirstOrDefault(b => b.Id == req.Id);
                        if (bonusRate != null)
                        {
                            bonusRate.HasYearlyBonus = req.HasYearlyBonus;
                            _context.SaveChanges();

                        }
                    }
                    //throw new Exception("Güncellenecek pozisyon bulunamadı.");

                    ActionResponseContract responseSuccessContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Pozisyonlar güncellendi.",
                        Code = "POSHASYEARLYBONUS_UPDATE_SUCCESS",
                        Data = request
                    };
                    return Ok(responseSuccessContract);

                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "POSHASYEARLYBONUS_UPDATE_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }

    }
}
