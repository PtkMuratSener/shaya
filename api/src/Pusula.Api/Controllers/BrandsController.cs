﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Markalar
    /// </summary>
    [Route("/svc")]
    public class BrandsController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <summary>
        /// DBContext al
        /// </summary>
        /// <param name="context"></param>
        public BrandsController(PatikaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Markalar Listesi
        /// </summary>
        /// <response code="200">success</response>
        /// <response code="404">Brand not found</response>
        [HttpGet]
        [Route("brands")]
        [ValidateModelState]
        [SwaggerOperation("BrandsGet")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Not found")]
        public IActionResult BrandsGet()
        {
            var userArea = HttpContext.Items["UserArea"] as string;
            var allBrands = _context.Brands.AsNoTracking();
            if (userArea == "S")
            {
                // Saha personeli sadece kendi markasını görür
                var storeId = (int) HttpContext.Items["UserStore"];
                var brandId =_context.Stores.AsNoTracking()
                    .Where(s => s.Id ==storeId)
                    .Select(s => s.Brand.Id)
                    .First();
                allBrands = allBrands.Where(s => s.Id == brandId);    
            }
            else
            {
                var roleIds = new List<int>();
                var brandIds = new List<int>();
                var ur = HttpContext.Items["UserRoles"] as string;
                if (ur != null)
                {
                    var ris = ur.Split(",");
                    foreach (var ri in ris)
                    {
                        roleIds.Add(Int32.Parse(ri));    
                    }
                }

                var roleBrandIds = _context.RoleBrands.AsNoTracking()
                    .Include(x => x.Brand)
                    .Where(x => roleIds.Contains(x.Role.Id))
                    .Select(x => new
                    {
                        x.Brand.Id
                    });
                foreach (var roleBrandId in roleBrandIds)
                {
                    brandIds.Add(roleBrandId.Id);
                }

                allBrands = allBrands.Where(x => brandIds.Contains(x.Id));
            }

            if (allBrands.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = allBrands.Count()},
                    Data = allBrands
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Marka bulunamadı.",
                Code = "BRAND_EMPTY_SET"
            };
                return NotFound(failedResponseContract);
        }
        /// <summary>
        /// Store listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("brandStores/{brandId}")]
        [ValidateModelState]
        [SwaggerOperation("StoreGetAll")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK,  typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int) HttpStatusCode.NotFound,  typeof(ActionResponseContract), "Not found")]
        public IActionResult StoreGetAll([FromRoute] int brandId)
        {
            var stores = _context.Stores.AsNoTracking()
                .Include(b => b.Brand)
                .Where(s => s.Aktif);
            
            if(brandId > 0)
                stores =  stores.Where(b => b.Brand.Id == brandId);
            var userArea = HttpContext.Items["UserArea"] as string;
            if (userArea == "S")
            {
                var store = (int) HttpContext.Items["UserStore"];

                stores = stores.Where(s => s.Id == store);
            }
            
            if (stores.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = stores.Count()},
                    Data = stores
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Mağaza bulunamadı.",
                Code = "STORES_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }

    }
}