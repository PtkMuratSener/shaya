﻿using Microsoft.AspNetCore.Mvc;
using System;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Requests;
using Pusula.Api.Models;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Jobs;
using Remotion.Linq.Clauses;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [Route("/svc")]
    public class StorePositionsController : Controller
    {
        private readonly PatikaDbContext _context;
        
        /// <summary>
        /// LocalhDbContext 
        /// </summary>
        /// <param name="context"></param>
        public StorePositionsController(PatikaDbContext context)
        {
            _context = context;
        }

        #region deprecated
        
        /*
        /// <summary>
        /// Yeni storeposition ekler
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("storePosition")]
        [ValidateModelState]
        [SwaggerOperation("StorePositionAdd")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ActionResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        public IActionResult StorePositionAdd([FromBody]StorePosition request)
        {
            return StatusCode((int)HttpStatusCode.NotImplemented, ".");
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Veri modeli hatası");
                }
                
                using (_context)
                {
                    var storeTarget = _context.StoreTargets.AsNoTracking()
                        .Where(y => y.Year == request.Year && y.Month == request.Month && y.IsActual == false)
                        .FirstOrDefault(s => s.Store.Id == request.Store.Id);
                    if (storeTarget == null)
                    {
                        throw new Exception("Mağaza hedefi bulunamadı.");
                    }

                    var pmst = ((storeTarget.Val / 100) * request.SaleSplit) / request.NumStaff;
                    var sta105 = Convert.ToInt32(pmst * 1.05);
                    var sta110 = Convert.ToInt32(pmst * 1.10);

                    request.Pmst = Convert.ToInt32(pmst);
                    request.Sta100 = Convert.ToInt32(pmst);
                    request.Sta105 = Convert.ToInt32(sta105);
                    request.Sta110 = Convert.ToInt32(sta110);

                    _context.StorePositions.Add(request);
                    _context.Entry(request.Store).State = EntityState.Unchanged;
                    _context.Entry(request.Position).State = EntityState.Unchanged;
                    _context.SaveChanges();
                    var insertedStoreposition = _context.StorePositions
                        .Include(s => s.Store)
                        .Include(s => s.Position)
                        .Last();
                    ActionResponseContract responseContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Mağaza pozisyonu eklendi.",
                        Code = "STOREPOSITION_ADD_SUCCESS",
                        Data = insertedStoreposition
                    };
                    return Ok(responseContract);
                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "STOREPOSITION_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }

            ActionResponseContract responseUnexpected = new ActionResponseContract
            {
                Success = false,
                Message = "BEklenmeyen hata.",
                Code = "STOREPOSITION_ADD_FAILED",
            };
            return NotFound(responseUnexpected);

            
        }
        */
        
        #endregion
        
        /// <summary>
        /// storeposition günceller
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("storePosition")]
        [ValidateModelState]
        [SwaggerOperation("StorePositionUpdate")]
        [Consumes("application/json")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ActionResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        public IActionResult StorePositionUpdate([FromBody]StorePosition request)
        {
            try
            {
                using (_context)
                {
                    var sp = _context.StorePositions
                        .Include(s => s.Store)
                        .Include(s => s.Position)
                        .FirstOrDefault(s=> s.Id == request.Id);
                    if (sp != null)
                    {
                        if (sp.Final)
                        {
                            throw new Exception("Veri değiştirilemez.");
                        }

                        var storeTarget = _context.StoreTargetsRetail.AsNoTracking()
                            .Where(y => y.Year == request.Year)
                            .FirstOrDefault(s => s.Store.Id == request.Store.Id);
                        if (storeTarget == null)
                        {
                            throw new Exception("Mağaza hedefi bulunamadı.");
                        }

                        var pmst = ((storeTarget.GetTarget(request.Month) / (decimal)100) * request.SaleSplit) / request.NumStaff;
                        var sta105 = Convert.ToInt32(pmst * (decimal)1.05);
                        var sta110 = Convert.ToInt32(pmst * (decimal)1.10);

                        sp.SaleSplit = request.SaleSplit;
                        request.Pmst = Convert.ToInt32(pmst);
                        request.Sta100 = Convert.ToInt32(pmst);
                        request.Sta105 = Convert.ToInt32(sta105);
                        request.Sta110 = Convert.ToInt32(sta110);
                        _context.Entry(request.Store).State = EntityState.Unchanged;
                        _context.Entry(request.Position).State = EntityState.Unchanged;
                        _context.SaveChanges();
                        
                        ActionResponseContract responseContract = new ActionResponseContract
                        {
                            Success = true,
                            Message = "Mağaza pozisyonu güncellendi.",
                            Code = "STOREPOSITION_UPDATE_SUCCESS",
                            Data = sp
                        };
                        return Ok(responseContract);
                    }
                }
                ActionResponseContract responseNot = new ActionResponseContract
                {
                    Success = false,
                    Message = "Güncellenecek kayıt yok.",
                    Code = "STOREPOSITION_ADD_FAILED"
                };
                return NotFound(responseNot );
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "STOREPOSITION_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
                
            }
        }

        
        /// <summary>
        /// Mağaza bazlı Storeposition listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("storePosition/{year}/{month}/{storeId}")]
        [ValidateModelState]
        [SwaggerOperation("StorePositionGetAll")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Not found")]
        public IActionResult StorepositionGetAll([FromRoute]int year, [FromRoute]int month, [FromRoute]int storeId)
        {
            var storepositions = _context.StorePositions.AsNoTracking()
                .Include(s => s.Store)
                .ThenInclude(t => t.Brand)
                .Include(s => s.Position)
                .Where(y => y.Year == year)
                .Where(y => y.Month == month)
                .Where(s => s.Store.Id == storeId);
            if (storepositions.Any())
            {
                ListResponseContract responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = storepositions.Count()},
                    Data = storepositions
                };
                return StatusCode(200, responseContract);
            }
            //Veri Engine içinde hesaplanacağındna burdan çık.
            
            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Mağaza pozisyonu bulunamadı.",
                Code = "STOREPOSITION_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }

        /// <summary>
        /// Storeposition detay bilgileri
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("storePosition/{id}")]
        [ValidateModelState]
        [SwaggerOperation("StorepositionGetById")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Not found")]
        public IActionResult StorepositionGetById([FromRoute] int id)
        {

            var storeposition = _context.StorePositions.Find(id);
            if (storeposition != null)
            {
                ListResponseContract responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = 1},
                    Data = storeposition
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Mağaza pozisyonu bulunamadı.",
                Code = "STOREPOSITION_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
            
        }

        /// <summary>
        /// Pozisyonlar Listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("positionList")]
        [ValidateModelState]
        [SwaggerOperation("PositionList")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Not found")]
        public IActionResult PositionList()
        {
            var positions = _context.Positions.AsNoTracking()
                .Where(p => p.HasBonus)
                .Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.HasBonus
                });
            if (positions.Any())
            {
                ListResponseContract responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = positions.Count()},
                    Data = positions
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Pozisyon bulunamadı.",
                Code = "POSITIONS_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }

        /// <summary>
        /// Position List of store
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("positionList/{storeId}")]
        [ValidateModelState]
        [SwaggerOperation("PositionListByStore")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Not found")]
        public IActionResult PositionListByStore([FromRoute] int storeId)
        {
            var positions = _context.Employees.AsNoTracking()
                .Where(x => x.Store.Id == storeId)
                .Select(m => new
                {
                    m.Position.Id,
                    m.Position.Name,
                    m.Position.HasBonus
                }).Distinct();
        

            if (positions.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = positions.Count()},
                    Data = positions
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Pozisyon bulunamadı.",
                Code = "POSITIONS_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }

        
        /// <summary>
        /// Pozisyonlar listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("positionListAll")]
        [ValidateModelState]
        [SwaggerOperation("PositionListAll")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Not found")]
        public IActionResult PositionListAll()
        {
            var positions = _context.Positions.AsNoTracking()
                .OrderBy(p => p.Name)
                .ToList();
            if (positions.Any())
            {
                ListResponseContract responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = positions.Count},
                    Data = positions
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Pozisyon bulunamadı.",
                Code = "POSITIONS_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }

        /// <summary>
        /// Mağaza Hedefleri
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("storeTarget/{year}/{month}/{storeId}")]
        [ValidateModelState]
        [SwaggerOperation("StoreTarget")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Not found")]
        public IActionResult StoreTarget([FromRoute]int year, [FromRoute]int month, [FromRoute]int storeId)
        {
            var targets = _context.StoreTargetsRetail
                .Include(s => s.Store)
                .ThenInclude(b => b.Brand)
                .Where(y => y.Year == year)
                .LastOrDefault(s => s.Store.Id == storeId);
            if (targets != null)
            {
                //TODO: StoreTarget e benzet data yı
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {
                        All = 1,
                        Filtered=1
                    },
                    Offset = 0,
                    Limit = 1,
                    Data = targets
                };
                return StatusCode(200, responseContract);
            }

            
            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Pozisyon bulunamadı.",
                Code = "STORETARGET_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }
        
        /// <summary>
        /// Mağaza pozisyon hedeflerini günceller
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("storePositionsUpdate")]
        [ValidateModelState]
        [SwaggerOperation("StorePositionsUpdate")]
        [Produces("application/json")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(ActionResponseContract), "Not found")]
        public IActionResult StorePositionsUpdate([FromBody]StorePositionsUpdateRequest request)
        {
            var currentStorePositions = _context.StorePositions.AsNoTracking()
                .Include(s => s.Store)
                .ThenInclude(s => s.Brand)
                .Include(s => s.Position)
                .Where(s => s.Year == request.Year)
                .Where(s => s.Month == request.Period)
                .Where(s => s.Store.Id == request.StoreId)
                .ToList();
            foreach (var csp in currentStorePositions)
            {
                csp.Final = false;
            }
            _context.SaveChanges();

            Engineer eng = new Engineer(_context);
            try
            {
                var oppa = eng.CalculateStorePosition(request.StoreId, request.Year, request.Period);
                if (oppa == 0)
                {
                    var sp = _context.StorePositions.AsNoTracking()
                        .Include(s => s.Store)
                        .ThenInclude(s => s.Brand)
                        .Include(s => s.Position)
                        .Where(s => s.Year == request.Year)
                        .Where(s => s.Month == request.Period)
                        .Where(s => s.Store.Id == request.StoreId);
                    var responseContract = new ListResponseContract
                    {
                        Total = new TotalProperty
                        {
                            All = sp.Count(),
                            Filtered = sp.Count()
                        },
                        Offset = 0,
                        Limit = sp.Count(),
                        Data = sp
                    };
                    string authHeader = Request.Headers["Authorization"];
                    var hdr = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(authHeader));
                    var owner = Convert.ToInt32(hdr.Split(":")[2]);
                    var user = _context.Users.AsNoTracking()
                        .First(u => u.Sicil == owner);

                    var payload = new
                    {
                        Request = request,
                        Old = currentStorePositions,
                        New = sp
                    };
                    var eventLog = new EventLog
                    {
                        Type = "STORE_TARGET_UPDATE",
                        EventOn = DateTime.Now,
                        User = user,
                        Payload = JsonConvert.SerializeObject(payload, Formatting.Indented)

                    };
                    _context.EventLogs.Add(eventLog);
                    _context.Entry(eventLog.User).State = EntityState.Unchanged;
                    _context.SaveChanges();
                    return StatusCode(200, responseContract);
                }

                var failedResponseContract = new ActionResponseContract
                {
                    Success = true,
                    Message = $"Mağaza aylık hedefleri güncellenemedi. {request.Year}{request.Period}/{request.StoreId}",
                    Code = "POSITIONS_UPDATE_FAIL",
                    Data = oppa
                };
                return NotFound(failedResponseContract);
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}
