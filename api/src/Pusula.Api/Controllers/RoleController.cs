using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Requests;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <summary>
    /// Rol yetki işlemleri
    /// </summary>
    [Route("/svc")]
    public class RoleController : Controller
    {
       private readonly PatikaDbContext _context;

        /// <summary>
        /// SAS Ayları İşlemleri
        /// </summary>
        /// <param name="context"></param>
        public RoleController(PatikaDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Role ekle ve güncelle
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("role")]
        [ValidateModelState]
        [SwaggerOperation("RoleAdd")]
        [SwaggerResponse(200, typeof(string),"Success")]
        [SwaggerResponse(400, typeof(string),"Failed")]
        public IActionResult RoleAddOrUpdate([FromBody]Role request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Veri modeli hatası");
                }

                using (_context)
                { 
                    if (request.Id < 1)
                    {
                        request.CreatedOn = DateTime.Now;
                        _context.Roles.Add(request);
                        _context.SaveChanges();

                        var responseContract = new ActionResponseContract
                        {
                            Success = true,
                            Message = "Rol eklendi.",
                            Code = "ROLE_ADD_SUCCESS",
                            Data = request
                        };
                        return Ok(responseContract);
                    }
                    else
                    {
                        var role = _context.Roles.Find(request.Id);
                        role.Id = request.Id;
                        role.Name = request.Name;
                        _context.SaveChanges();
                        var responseContract = new ActionResponseContract
                        {
                            Success = true,
                            Message = "Rol güncellendi.",
                            Code = "ROLE_UPDATE_SUCCESS",
                            Data = request
                        };
                        return Ok(responseContract);
                    }
                }
 
            }
            catch (Exception ex)
            {
                var responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "ROLE_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);

            }
        }
        
        /// <summary>
        /// Role listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("role")]
        [ValidateModelState]
        [SwaggerOperation("RoleGetAll")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(List<Role>),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult RoleGetAll()
        {
            var role = _context.Roles.ToList();
            if (role.Any())
            {
                var responseContract = new 
                {
                    first = true,
                    last = true,
                    number = 1,
                    numberOfElements = role.Count,
                    size = role.Count,
                    totalElements = role.Count,
                    totalPages = 1,
                    content = role,
                    Total = new TotalProperty {All = role.Count},
                    Data = role
                };

                return StatusCode(200, responseContract); 
            }
            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Role bulunamadı.",
                Code = "ROLE_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }
        
        /// <summary>
        /// Rol detay bilgileri
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("role/{id}")]
        [ValidateModelState]
        [SwaggerOperation("RoleGetById")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(Ceza),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult RoleGetById([FromRoute] int id)
        {
            var role = _context.Roles.Find(id);
            if (role != null)
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = 1},
                    Data = role
                };
                return StatusCode(200, responseContract);                
            }
            
            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Rol bulunamadı.",
                Code = "ROLE_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }

        /// <summary>
        /// Role sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("role/{id}")]
        [ValidateModelState]
        [SwaggerOperation("RoleDeleteById")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(string),"Success")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult RoleDeleteById([FromRoute] int id)
        {
            var role = _context.Roles.Find(id);
            if (role == null)
            {
                //Kayıt yoksa 
                var failedResponseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = "Rol bulunamadı.",
                    Code = "ROLE_EMPTY_SET"
                };
                return NotFound(failedResponseContract);
            }

            var registryRoleAssignments = _context.RegistryRoleAssignments.Where(w => w.Role.Id == id);
            var titleRoleAssignments = _context.TitleRoleAssignments.Where(w => w.Role.Id == id);
            if (!registryRoleAssignments.Any() && !titleRoleAssignments.Any())
            {
                var rolePermissions = _context.RolePermissions.Where(w => w.Role.Id == id);
                _context.RolePermissions.RemoveRange(rolePermissions); 
            }
            else
            {
                //Kayıt yoksa 
                var failedResponseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = "Sicil rol ve ünvan rol ataması yapıldığından rol silinemedi!",
                    Code = "ROLE_NOT_DELETED"
                };
                return NotFound(failedResponseContract);
            }
            _context.Roles.Remove(role);
            _context.SaveChanges();
            var successResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Rol silindi.",
                Code = "ROLE_DELETED"
            };
            return Ok(successResponseContract);
        }



        /// <summary>
        /// Yetki Türleri
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("role/authorizationTypes")]
        [ValidateModelState]
        [SwaggerOperation("AuthorizationTypesGetAll")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(List<Role>),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult AuthorizationTypesGetAll()
        {
            var authorizationTypes = _context.AuthorizationTypes.ToList();
            if (authorizationTypes.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = authorizationTypes.Count},
                    Data = authorizationTypes
                };
                return StatusCode(200, responseContract);             
            }    
            
            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Yetkiler bulunamadı.",
                Code = "AUTHORIZATION_TYPE_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
            
        }
       
        /// <summary>
        /// Ekranlar
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("role/screens")]
        [ValidateModelState]
        [SwaggerOperation("screens")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(List<Role>),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult Screens()
        {
            var screens = _context.Screens.ToList();
            if (screens.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = screens.Count},
                    Data = screens
                };
                return StatusCode(200, responseContract);   
            }    
            
            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Ekran sabitleri bulunamadı.",
                Code = "SCREEN_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
            
        }
        
        
        /// <summary>
        /// Ekranlar
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("role/storeTypes")]
        [ValidateModelState]
        [SwaggerOperation("StoreTypes")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(List<Role>),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult StoreTypes()
        {
            var storeTypes = _context.StoreTypes.ToList();
            if (storeTypes.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = storeTypes.Count},
                    Data = storeTypes
                };
                return StatusCode(200, responseContract);   
            }    
            
            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Mağaza Türleri bulunamadı.",
                Code = "STORE_TYPE_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
            
        }
           
        /// <summary>
        /// Ekranlar
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("role/workingAreas")]
        [ValidateModelState]
        [SwaggerOperation("WorkingAreas")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(List<Role>),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult WorkingAreas()
        {
            var workingAreas = _context.WorkingAreas.ToList();
            if (workingAreas.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = workingAreas.Count},
                    Data = workingAreas
                };
                return StatusCode(200, responseContract);   
            }
            
            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Çalışma alanı bulunamadı.",
                Code = "WORKING_AREAS_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
            
        }

        
        /// <summary>
        /// role göre yetkiler listesi
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("rolePermission/{roleId}")]
        [ValidateModelState]
        [SwaggerOperation("RolePermissionGetByRoleId")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(Ceza),"Success")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult RolePermissionGetByRoleId([FromRoute] int roleId)
        {
            var rolePermissions = _context.RolePermissions.AsNoTracking()
                .Include(x => x.Role)
                .Include(x => x.Screen)
                .Include(x => x.AuthorizationType)
                .Include(x => x.StoreType)
                .Include(x => x.WorkingArea)
                .Where(x => x.Role != null && x.Role.Id == roleId);
                
            if (rolePermissions.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = rolePermissions.Count()},
                    Data = rolePermissions
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Role ait yetkiler bulunamadı.",
                Code = "ROLE_PERMISSION_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }
        
        /// <summary>
        /// Rol İzni ekle
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("rolePermission")]
        [ValidateModelState]
        [SwaggerOperation("RolePermission")]
        [SwaggerResponse(200, typeof(string),"Success")]
        [SwaggerResponse(400, typeof(string),"Failed")]
        public IActionResult RolePermission([FromBody] RolePermission request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Veri modeli hatası");
                }
                
            using (_context)
            {
 
                request.CreatedOn = DateTime.Now;
                _context.RolePermissions.Add(request);
                _context.Entry(request.Role).State = EntityState.Unchanged;
                _context.Entry(request.Screen).State = EntityState.Unchanged;
                _context.Entry(request.AuthorizationType).State = EntityState.Unchanged;
                _context.Entry(request.StoreType).State = EntityState.Unchanged;
                _context.Entry(request.WorkingArea).State = EntityState.Unchanged;
                _context.SaveChanges();
                var insertedRecord = _context.Parametres.Last();
                var responseContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Role İzinleri eklendi.",
                    Code = "ROLE_PERMISSION_ADD_SUCCESS",
                    Data = insertedRecord
                };
                return Ok(responseContract);
            }

            }
            catch (Exception ex)
            {
                var responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "ROLE_PERMISSION_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
                
            }
        }
        
        /// <summary>
        /// Role yetkisi sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("rolePermission/{id}")]
        [ValidateModelState]
        [SwaggerOperation("RolePermissionDeleteById")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(string),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult RolePermissionDeleteById([FromRoute] int id)
        {
            var rolePermissions = _context.RolePermissions.Find(id);
            if (rolePermissions == null)
            {
                //Kayıt yoksa 
                var failedResponseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = "Rol izinleri bulunamadı.",
                    Code = "ROLE_PERMISSION_EMPTY_SET"
                };
                return NotFound(failedResponseContract);                
            }

            _context.RolePermissions.Remove(rolePermissions);
            _context.SaveChanges();
            var successResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Rol izinleri silindi.",
                Code = "ROLE_PERMISSION_DELETED"
            };
            return Ok(successResponseContract);
            
        }

        /// <summary>
        /// Rolün yetkili olduğu markaları listeler
        /// </summary>
        /// <param name="roleId">Rol Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("roleBrands/{roleId}")]
        [ValidateModelState]
        [SwaggerOperation("RoleBrandsGetByRoleId")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(Ceza),"Success")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult RoleBrandsGetByRoleId([FromRoute] int roleId)
        {
            var roleBrands = _context.RoleBrands.AsNoTracking()
                .Include(x => x.Brand)
                .Include(x => x.Role)
                .Where(x => x.Role.Id == roleId);
            if (roleBrands.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = roleBrands.Count()},
                    Data = roleBrands
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Rol yetkili markalar bulunamadı.",
                Code = "ROLE_BRANDS_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }

        /// <summary>
        /// Role marka yetkisi ekler.
        /// </summary>
        /// <param name="request">RoleBrand listesi</param>
        /// <returns>200,400</returns>
        /// <exception cref="Exception"></exception>
        [HttpPost]
        [Route("roleBrands")]
        [ValidateModelState]
        [SwaggerOperation("RoleBrandAssignment")]
        [SwaggerResponse(200, typeof(string),"Success")]
        [SwaggerResponse(400, typeof(string),"Failed")]
        public IActionResult RoleBrandAssignment([FromBody] List<RoleBrandAddRequestItem> request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Veri modeli hatası");
                }

                using (_context)
                {
                    //Eski markaları silelim
                    var oldRoleBrands = _context.RoleBrands
                        .Where(r => r.Role.Id == request[0].BrandId);
                    _context.RoleBrands.RemoveRange(oldRoleBrands);
                    
                    //Yenilerini ekleyelim.
                    foreach (var rb in request)
                    {
                        var role = _context.Roles.Find(rb.RoleId);
                        var brand = _context.Brands.Find(rb.BrandId);
                        _context.RoleBrands.Add(new RoleBrand
                        {
                            Role = role,
                            Brand = brand
                        });
                    }
                    _context.SaveChanges();
                    
                    var responseContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Rol marka ataması yapıldı.",
                        Code = "ROLE_BRAND_ASSIGNMENT_SUCCESS",
                    };
                    return Ok(responseContract);
                }

            }
            catch (Exception ex)
            {
                var responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "ROLE_BRAND_ASSIGNMENT_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);

            }
        }
        
        /// <summary>
        /// Sicil Role atama    a1
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("titleRoleAssignment")]
        [ValidateModelState]
        [SwaggerOperation("TitleRoleAssignment")]
        [SwaggerResponse(200, typeof(string),"Success")]
        [SwaggerResponse(400, typeof(string),"Failed")]
        public IActionResult TitleRoleAssignments([FromBody] TitleRoleAssignment request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Veri modeli hatası");
                }

                using (_context)
                {
                    _context.TitleRoleAssignments.Add(request);
                    _context.Entry(request.Role).State = EntityState.Unchanged;
                    _context.Entry(request.Position).State = EntityState.Unchanged;
                    _context.SaveChanges();
                    var insertedRecord = _context.Parametres.Last();
                    var responseContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Ünvan Rol ataması yapıldı.",
                        Code = "TITLE_ROLE_ASSIGNMENT_ADD_SUCCESS",
                        Data = insertedRecord
                    };
                    return Ok(responseContract);
                }

            }
            catch (Exception ex)
            {
                var responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "TITLE_ROLE_ASSIGNMENT_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);

            }
        }

        /// <summary>
        /// role göre ünvanlar listesi
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("titleRoleAssignment/{roleId}")]
        [ValidateModelState]
        [SwaggerOperation("TitleRoleAssignmentGetByRoleId")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(Ceza),"Success")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult TitleRoleAssignmentGetByRoleId([FromRoute] int roleId)
        {
            var titleRoleAssignments = _context.TitleRoleAssignments.Include(x => x.Role).Include(x => x.Position)
                .Where(x => x.Role != null && x.Role.Id == roleId).ToList();
            if (titleRoleAssignments.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = titleRoleAssignments.Count},
                    Data = titleRoleAssignments
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Ünvan Rol bulunamadı.",
                Code = "TITLE_ROLE_ASSIGNMENT_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }


        /// <summary>
        /// Role atama sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("titleRoleAssignment/{id}")]
        [ValidateModelState]
        [SwaggerOperation("TitleRoleAssignmentDeleteById")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(string),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult TitleRoleAssignmentDeleteById([FromRoute] int id)
        {
            var titleRoleAssignments = _context.TitleRoleAssignments.Find(id);
            if (titleRoleAssignments == null)
            {
                //Kayıt yoksa 
                var failedResponseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = "Ünvan Rolu bulunamadı.",
                    Code = "TITLE_ROLE_ASSIGNMENT_EMPTY_SET"
                };
                return NotFound(failedResponseContract);
            }
            _context.TitleRoleAssignments.Remove(titleRoleAssignments);
            _context.SaveChanges();
            var successResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Ünvan Rolu silindi.",
                Code = "TITLE_ROLE_ASSIGNMENT_DELETED"
            };
            return Ok(successResponseContract);
        }
        
        /// <summary>
        /// Sicil Role atama 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("registryRoleAssignment")]
        [ValidateModelState]
        [SwaggerOperation("RegistryRoleAssignment")]
        [SwaggerResponse(200, typeof(string),"Success")]
        [SwaggerResponse(400, typeof(string),"Failed")]
        public IActionResult RegistryRoleAssignment([FromBody]RegistryRoleAssignment request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new Exception("Veri modeli hatası");
                }

                using (_context)
                {
                    if (request.Id < 1)
                    {
                        _context.RegistryRoleAssignments.Add(request);
                        _context.Entry(request.Role).State = EntityState.Unchanged;
                        _context.SaveChanges();
                        var insertedRecord = _context.RegistryRoleAssignments.Last();
                        var responseContract = new ActionResponseContract
                        {
                            Success = true,
                            Message = "Sicil rol ataması yapıldı.",
                            Code = "REGISTRY_ROLE_ASSIGNMENT_ADD_SUCCESS",
                            Data = insertedRecord
                        };
                        return Ok(responseContract);
                    }
                    else
                    {
                        var registryRoleAssignments = _context.RegistryRoleAssignments.Find(request.Id);
                        registryRoleAssignments.Id = request.Id;
                        registryRoleAssignments.Role = request.Role;
                        registryRoleAssignments.Sicil = request.Sicil;
                        _context.SaveChanges();
                        var responseContract = new ActionResponseContract
                        {
                            Success = true,
                            Message = "Sicil rol ataması güncellendi.",
                            Code = "REGISTRY_ROLE_ASSIGNMENT_UPDATE_SUCCESS",
                            Data = request
                        };
                        return Ok(responseContract);
                    }
                }
            }
            catch (Exception ex)
            {
                var responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "REGISTRY_ROLE_ASSIGNMENT_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);

            }
        }

        /// <summary>
        /// role göre siciller listesi
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("registryRoleAssignment/{roleId}")]
        [ValidateModelState]
        [SwaggerOperation("RegistryRoleAssignmentGetByRoleId")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(Ceza),"Success")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult RegistryRoleAssignmentGetByRoleId([FromRoute] int roleId)
        {
            var registryRoleAssignments = _context.RegistryRoleAssignments.Include(i=>i.Role).Where(x => x.Role != null && x.Role.Id == roleId).ToList();
            if (registryRoleAssignments.Any())
            {
                var responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = registryRoleAssignments.Count},
                    Data = registryRoleAssignments
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            var failedResponseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Sicil Rol bulunamadı.",
                Code = "REGISTRY_ROLE_ASSIGNMENT_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }


        /// <summary>
        /// Role atama sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("registryRoleAssignment/{id}")]
        [ValidateModelState]
        [SwaggerOperation("RegistryRoleAssignmentDeleteById")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(string),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]
        public IActionResult RegistryRoleAssignmentDeleteById([FromRoute] int id)
        {
            var registryRoleAssignments = _context.RegistryRoleAssignments
                .Include(x=>x.Role)
                .FirstOrDefault(w=>w.Id==id);
            if (registryRoleAssignments == null)
            {
                //Kayıt yoksa 
                var failedResponseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = "Sicil Rolu bulunamadı.",
                    Code = "REGISTRY_ROLE_ASSIGNMENT_EMPTY_SET"
                };
                return NotFound(failedResponseContract);
                
            }
            _context.RegistryRoleAssignments.Remove(registryRoleAssignments);
            _context.SaveChanges();
            var successResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Sicil Rolu silindi.",
                Code = "REGISTRY_ROLE_ASSIGNMENT_DELETED"
            };
            return Ok(successResponseContract);
            
        }

        /// <summary>
        /// Role toplu yetki atama
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("roleCombo")]
        [ValidateModelState]
        [SwaggerOperation("RoleComboSave")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(string),"Success")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, typeof(string),"Not found")]        
        public IActionResult RoleComboSave([FromBody] RoleComboRequest request)
        {
            try
            {
                var role = _context.Roles.Find(request.Role);
                
                //Eski markaları silelim
                var oldRoleBrands = _context.RoleBrands
                    .Where(r => r.Role.Id == request.Role);
                _context.RoleBrands.RemoveRange(oldRoleBrands);

                //Yenilerini ekleyelim.
                foreach (var brand in request.Brands)
                {
                    if (brand.Checked)
                    {
                        _context.RoleBrands.Add(new RoleBrand
                        {
                            Role = role,
                            Brand = _context.Brands.Find(brand.Id)
                        });
                    }
                }

                //Ekranları temizleyelim
                var oldScreens = _context.RolePermissions
                    .Where(r => r.Role.Id == request.Role);
                _context.RolePermissions.RemoveRange(oldScreens);
                
                //Yenilerini ekleyelim
                foreach (var screen in request.Screens)
                {
                    if (screen.View)
                    {
                        var authType = _context.AuthorizationTypes.Find(1); // Görme
                        var rolePermission = new RolePermission
                        {
                            Role = role,
                            AuthorizationType = authType,
                            CreatedOn = DateTime.Now,
                            Screen = _context.Screens.Find(screen.Id),
                            StoreType = _context.StoreTypes.Find(1),
                            WorkingArea = _context.WorkingAreas.Find(1)
                        };
                        _context.Entry(rolePermission.Role).State = EntityState.Unchanged;
                        _context.Entry(rolePermission.AuthorizationType).State = EntityState.Unchanged;
                        _context.Entry(rolePermission.Screen).State = EntityState.Unchanged;
                        _context.Entry(rolePermission.StoreType).State = EntityState.Unchanged;
                        _context.Entry(rolePermission.WorkingArea).State = EntityState.Unchanged;
                        _context.RolePermissions.Add(rolePermission);
                    }
                    
                    if (screen.Edit)
                    {
                        var authType = _context.AuthorizationTypes.Find(2);  //Değiştirme
                        var rolePermission = new RolePermission
                        {
                            Role = role,
                            AuthorizationType = authType,
                            CreatedOn = DateTime.Now,
                            Screen = _context.Screens.Find(screen.Id),
                            StoreType = _context.StoreTypes.Find(1),
                            WorkingArea = _context.WorkingAreas.Find(1)
                        };
                        _context.Entry(rolePermission.Role).State = EntityState.Unchanged;
                        _context.Entry(rolePermission.AuthorizationType).State = EntityState.Unchanged;
                        _context.Entry(rolePermission.Screen).State = EntityState.Unchanged;
                        _context.Entry(rolePermission.StoreType).State = EntityState.Unchanged;
                        _context.Entry(rolePermission.WorkingArea).State = EntityState.Unchanged;
                        _context.RolePermissions.Add(rolePermission);
                    }
                }
                
                //Eski rol pprim tiplerini silelim
                var oldRolePrimTypes = _context.RolePrimTypes
                    .Where(r => r.Role.Id == request.Role);
                _context.RolePrimTypes.RemoveRange(oldRolePrimTypes);
                
                //yeni rol prim tiplerini ekleyelim
                foreach (var comboPrimType in request.PrimTypes)
                {
                    if (comboPrimType.Checked)
                    {
                        _context.RolePrimTypes.Add(new RolePrimType
                        {
                            Role = role,
                            PrimType = _context.PrimTypes.Find(comboPrimType.Id)
                        });
                    }
                }
    
                _context.SaveChanges();

                var responseContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Rol marka, ekran, yetki ataması yapıldı.",
                    Code = "ROLE_COMBO_ASSIGNMENT_SUCCESS",
                    Data = ""
                };
                return Ok(responseContract);
            }
            catch (Exception e)
            {
                var failedResponseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = "Rol marka, ekran, yetki ataması yapılamadı.",
                    Code = "ROLE_COMBO_ASSIGNMENT_FAILED",
                    Data = e
                };
                return NotFound(failedResponseContract);
            }
        }

        /// <summary>
        /// Rol yetki görme / şablon
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("roleCombo/{roleId}")]
        [ValidateModelState]
        [SwaggerOperation("RoleComboRead")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(RoleComboRequest), "Success")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(string), "Not found")]
        public IActionResult RoleComboRead([FromRoute] int roleId)
        {
            try
            {
                var brands = _context.Brands.FromSql("SELECT * FROM ActiveBrands").AsNoTracking();
                var screens = _context.Screens.AsNoTracking();

                var roleBrands = new Dictionary<int, bool>();
                var roleScreens = new Dictionary<int, int>(); //0 false, 1 view, 2 edit, 3 both
                var rolePrimTypes = new Dictionary<int, bool>();

                var roleComboResponse = new RoleComboRequest
                {
                    Role = roleId,
                    Brands = new List<RoleComboBrand>(),
                    Screens = new List<RoleComboScreen>(),
                    PrimTypes = new List<RoleComboPrimType>()
                };

                if (roleId > 0)
                {
                    var rolePermissions = _context.RolePermissions.AsNoTracking()
                        .Include(x => x.Screen)
                        .Include(x => x.AuthorizationType)
                        .Where(x => x.Role.Id == roleId)
                        .Select(x => new
                        {
                            S = x.Screen.Id,
                            A = x.AuthorizationType.Id
                        });
                    if (rolePermissions.Any())
                    {
                        foreach (var rolePermission in rolePermissions)
                        {
                            var a = rolePermission.A;
                            if (roleScreens.ContainsKey(rolePermission.S))
                            {
                                a = Math.Max(a, roleScreens[rolePermission.S]);
                                roleScreens[rolePermission.S] = a;
                            }
                            else
                            {
                                roleScreens.Add(rolePermission.S, a);
                            }
                        }
                    }

                    var roleBrandis = _context.RoleBrands.AsNoTracking()
                        .Include(x => x.Brand)
                        .Where(x => x.Role.Id == roleId)
                        .Select(x => new
                        {
                            B = x.Brand.Id
                        })
                        .ToList();
                    if (roleBrandis.Any())
                    {
                        foreach (var rb in roleBrandis)
                        {
                            roleBrands.Add(rb.B, true);
                        }
                    }

                    var rolePrimTypeIds = _context.RolePrimTypes.AsNoTracking()
                        .Where(x => x.Role.Id == roleId)
                        .Select(x => new
                        {
                            T = x.PrimType.Id
                        })
                        .ToList();
                    if (rolePrimTypeIds.Any())
                    {
                        foreach (var rpt in rolePrimTypeIds)
                        {
                            rolePrimTypes.Add(rpt.T, true);
                        }
                    }
                }
                
                foreach (var brand in brands)
                {
                    roleComboResponse.Brands.Add(new RoleComboBrand
                    {
                        Id = brand.Id,
                        Name = brand.Name,
                        Checked = roleBrands.ContainsKey(brand.Id) && roleBrands[brand.Id]
                    });
                }
                
                foreach (var screen in screens)
                {
                    roleComboResponse.Screens.Add(new RoleComboScreen
                    {
                        Id = screen.Id,
                        Name = screen.Name,
                        View = roleScreens.ContainsKey(screen.Id) && roleScreens[screen.Id] == 1,
                        Edit = roleScreens.ContainsKey(screen.Id) && roleScreens[screen.Id] == 2
                    });
                }

                var primTypes = _context.PrimTypes.AsNoTracking();
                foreach (var primType in primTypes)
                {
                    roleComboResponse.PrimTypes.Add(new RoleComboPrimType
                    {
                        Id = primType.Id,
                        Name = primType.Name,
                        Checked = rolePrimTypes.ContainsKey(primType.Id) && rolePrimTypes[primType.Id]
                    });
                }

                var responseContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Rol marka, ekran, yetki " + (roleId == 0 ? "şablonu" : "listesi"),
                    Code = "ROLE_COMBO_DISPLAY_SUCCESS",
                    Data = roleComboResponse
                };
                return Ok(responseContract);
            }
            catch (Exception e)
            {
                var failedResponseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = "Rol marka, ekran, yetki gösterilemedi.",
                    Code = "ROLE_COMBO_DISPLAY_FAILED",
                    Data = e
                };
                return NotFound(failedResponseContract);
            }
        }
    }
}