﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Models;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// </summary>
    [Route("/svc")]
    public sealed class XlUploadsApiController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public XlUploadsApiController(PatikaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Imports Al Shaya Calendar XL file
        /// </summary>
        /// <remarks>Service accepts Excel xlsx files that contains A Shaya calendar of periods</remarks>
        /// <param name="upfile">Calendar file.</param>
        /// <param name="year">Year that file belongs</param>
        [HttpPost]
        [Route("xlalshayacalendar")]
        [ValidateModelState]
        [SwaggerOperation("XlalshayacalendarPost")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ActionResponseContract), "Success")]
        [SwaggerResponse((int) HttpStatusCode.NotAcceptable, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.Conflict, typeof(ActionResponseContract), "Failed")]
        public IActionResult XlalshayacalendarPost([FromForm] [Required()] IFormFile upfile, [FromForm] [Required()] string year)
        {
            int theyear;
            try
            {
                theyear = Convert.ToInt32(year);
            }
            catch (FormatException e)
            {
                var invalidYearResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = "Yıl geçersiz. " + e.Message,
                    Code = "CALENDAR_IMPORT_ERROR"
                };
                return StatusCode((int) HttpStatusCode.NotAcceptable, invalidYearResponse);
            }

            var absYear = Math.Abs(theyear);
            var calendars = _context.Calendars.Where(c => c.Year == absYear);

            if (calendars.Any())
            {
                /*
                 * If calendar data exists for given year and overwrite not chosen
                 */
                if (theyear > 0)
                {
                    var dataExistsForYearResponse = new ActionResponseContract
                    {
                        Success = false,
                        Message = "Bu yıl verisi mevcut.", //+ $"Üzerine yazmak için yılı şöyle gönderin: -{theyear}",
                        Code = "CALENDAR_IMPORT_ERROR"
                    };
                    return StatusCode((int) HttpStatusCode.Conflict, dataExistsForYearResponse);
                }

                /*
                 * If overwrite chosen then remove old records
                 */
                theyear = Math.Abs(theyear);
                _context.Calendars.RemoveRange(_context.Calendars.Where(c => c.Year == theyear));
            }

            var fs = upfile.OpenReadStream();
            var workbook = new XSSFWorkbook(fs);
            var sheet = workbook.GetSheetAt(0);

            using (_context)
            {
                for (var rowIndex = 0; rowIndex <= sheet.LastRowNum; rowIndex++)
                {
                    var row = sheet.GetRow(rowIndex);
                    if (row != null)
                    {
                        if (row.GetCell(0) == null)
                        {
                            continue;
                        }

                        _context.Calendars.Add(new Calendar
                        {
                            Year = theyear,
                            Date = Convert.ToInt32(row.GetCell(0).DateCellValue.ToString("yyyyMMdd")),
                            Month = Convert.ToInt32(row.GetCell(1).NumericCellValue),
                            Week = Convert.ToInt32(row.GetCell(2).NumericCellValue),
                            Quarter = Convert.ToInt32(row.GetCell(3).NumericCellValue),
                            Season = Convert.ToInt32(row.GetCell(4).NumericCellValue)
                        });
                    }
                }
                _context.SaveChanges();
                string authHeader = Request.Headers["Authorization"];
                var hdr = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(authHeader));
                var owner = Convert.ToInt32(hdr.Split(":")[2]);
                var tfs = upfile.OpenReadStream();
                BinaryReader br = new BinaryReader(tfs);
                byte[] bytes = br.ReadBytes((Int32)tfs.Length);
                _context.UploadedFiles.Add(new UploadedFile
                {
                    FileName = upfile.FileName,
                    Owner = owner,
                    Date = DateTime.Now,
                    Content = bytes,
                    Type = "Calendar",
                    Info = year
                });
                _context.SaveChanges();
            }

            
            var responseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Takvim alındı.",
                Code = "CALENDAR_IMPORT_SUCCESS",
            };
            return Ok(responseContract);
        }


        /// <summary>
        /// Imports Store Targets XL file
        /// </summary>
        /// <remarks>Service accepts Excel xlsx files that contains store sale targets and actual values</remarks>
        /// <param name="upfile">Store targets file.</param>
        /// <param name="year">Year that file belongs</param>
        [HttpPost]
        [Route("xlsaletargets")]
        [ValidateModelState]
        [SwaggerOperation("XlsaletargetsPost")]
        [Produces("application/json")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ActionResponseContract), "Success")]
        [SwaggerResponse((int) HttpStatusCode.NotAcceptable, typeof(ActionResponseContract), "Failed")]
        public IActionResult XlsaletargetsPost([FromForm] [Required] IFormFile upfile, [FromForm] [Required] string year)
        {
            Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var fs = upfile.OpenReadStream();
            var workbook = new XSSFWorkbook(fs);
            var sheet = workbook.GetSheetAt(0);

            int theyear;
            try
            {
                theyear = Convert.ToInt32(year);
            }
            catch (FormatException e)
            {
                var invalidYearResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = "Yıl geçersiz. " + e.Message,
                    Code = "SALETARGETS_IMPORT_ERROR"
                };
                return StatusCode((int) HttpStatusCode.NotAcceptable, invalidYearResponse);
            }

            using (_context)
            {
                try
                {
                    var oldStores = _context.Stores;
                    foreach (var os in oldStores)
                    {
                        os.Aktif = false;
                    }

                    _context.SaveChanges();
                    
                    //Yeni mağaza var mı
                    for (var rowIndex = 0; rowIndex <= sheet.LastRowNum; rowIndex++)
                    {
                        var row = sheet.GetRow(rowIndex);
                        if (row?.GetCell(0) == null)
                        {
                            continue;
                        }
                        
                        if( Convert.ToInt32(row.GetCell(0).NumericCellValue) == 0) continue; //Kapalı mağaza
                        
                        var storeId = Convert.ToInt32(row.GetCell(0).NumericCellValue);
                        var storeName = row.GetCell(1).StringCellValue.Remove(0, 6).Trim();
                        
                        var brandId = storeId / 1000;
                        brandId = brandId == 38 ? 39 : brandId; //VSBA ve VSFA birleşik Victorias olarak görülecek.
                        var brand = _context.Brands.Find(brandId) ?? _context.Brands.Find(0);
                            
                        var theStore = _context.Stores.Find(storeId);
                        if (theStore == null)
                        {
                            //Mağaza yoksa ekle
                            theStore = new Store
                            {
                                Id = Convert.ToInt32(row.GetCell(0).NumericCellValue),
                                Aktif = true,
                                Brand = brand,
                                Name = storeName
                            };
                  
                            _context.Stores.Add(theStore);
                            _context.SaveChanges();
                        }
                        else
                        {
                            theStore.Aktif = true;
                            theStore.Name = storeName;
                        }
                    }

                    _context.SaveChanges();
                    


                    for (var rowIndex = 0; rowIndex <= sheet.LastRowNum; rowIndex++)
                    {
                        var row = sheet.GetRow(rowIndex);
                        if (row?.GetCell(0) == null)
                        {
                            continue;
                        }
                        
                        if( Convert.ToInt32(row.GetCell(0).NumericCellValue) == 0) continue; //Kapalı mağaza

                        var storeId = Convert.ToInt32(row.GetCell(0).NumericCellValue);
                        var theStore = _context.Stores.AsNoTracking()
                            .Include(s => s.Brand)
                            .First(s=> s.Id == storeId);

                        var storeTargetRetail = _context.StoreTargetsRetail
                            .Include(s => s.Store)
                            .ThenInclude(s => s.Brand)
                            .Where(y => y.Year == theyear)
                            .FirstOrDefault(s => s.Store.Id == theStore.Id);
                        if (storeTargetRetail != null)
                        {
                            for (var p = 1; p <= 13; p++)
                            {
                                storeTargetRetail.SetActual(p, Convert.ToInt32(row.GetCell(1+p).NumericCellValue));
                                storeTargetRetail.SetTarget(p, Convert.ToInt32(row.GetCell(14+p).NumericCellValue));
                            }
                        }
                        else
                        {
                            storeTargetRetail = new StoreTargetRetail
                            {
                                Store = theStore,
                                Year = theyear
                            };
                            for (var p = 1; p <= 13; p++)
                            {
                                storeTargetRetail.SetActual(p, Convert.ToInt32(row.GetCell(1+p).NumericCellValue));
                                storeTargetRetail.SetTarget(p, Convert.ToInt32(row.GetCell(14+p).NumericCellValue));
                            }
                            _context.StoreTargetsRetail.Add(storeTargetRetail);
                        }
                    }
                    _context.SaveChanges();

                    var responseContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Mağaza hedefleri alındı.",
                        Code = "SALETARGETS_IMPORT_SUCCESS",
                    };

                    _context.SaveChanges();
                    
                    string authHeader = Request.Headers["Authorization"];
                    var hdr = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(authHeader));
                    var owner = Convert.ToInt32(hdr.Split(":")[2]);
                    var tfs = upfile.OpenReadStream();
                    var br = new BinaryReader(tfs);
                    var bytes = br.ReadBytes((int)tfs.Length);
                    _context.UploadedFiles.Add(new UploadedFile
                    {
                        FileName = upfile.FileName,
                        Owner = owner,
                        Date = DateTime.Now,
                        Content = bytes,
                        Type = "SaleTargets",
                        Info = year
                    });
                    _context.SaveChanges();

                    return Ok(responseContract);

                }
                catch (Exception e)
                {
                    var failedResponse = new ActionResponseContract
                    {
                        Success = false,
                        Message = e.Message,
                        Code = "SALETARGETS_IMPORT_ERROR",
                        Data = e
                    };
                    return NotFound(failedResponse);
                }
            }
        }
        
        /// <summary>
        /// Takvim dosyalarını sunar
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("filesXLAlShayaCalendar")]
        [ValidateModelState]
        [SwaggerOperation("FilesXLAlShayaCalendarGet")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        public IActionResult FilesXlAlShayaCalendarGet()
        {
            var files = _context.UploadedFiles
                .Where(f => f.Type == "Calendar")
                .OrderByDescending(x => x.Date)
                .Select(x => new
                {
                    x.Id,
                    x.FileName,
                    x.Date,
                    x.Owner,
                    Year=x.Info
                });
            return Ok(files);
        }

        /// <summary>
        /// Hedef dosyalarını sunar
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("filesXLSaleTargets")]
        [ValidateModelState]
        [SwaggerOperation("FilesXLSaleTargetsGet")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        public IActionResult FilesXlSaleTargetsGet()
        {
            var files = _context.UploadedFiles
                .Where(f => f.Type == "SaleTargets")
                .OrderByDescending(x => x.Date)
                .Select(x => new
                {
                    x.Id,
                    x.FileName,
                    x.Date,
                    x.Owner,
                    Year=x.Info
                });
            return Ok(files);
        }

        /// <summary>
        /// Binary dosya
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("filesGet/{fileId}")]
        [SwaggerOperation("FilesGet")]
        public IActionResult FilesGet(int fileId)
        {
            var file = _context.UploadedFiles
                .Find(fileId);
            if (file != null)
            {
                return File(file.Content, "application/octet-stream", file.FileName);
            }

            return NotFound("Dosya yok.");
        }
    }
}
