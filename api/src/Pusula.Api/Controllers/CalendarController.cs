﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Models;

namespace Pusula.Api.Controllers
{ 
    /// <inheritdoc />
    /// <summary>
    /// AlShaya takvimi işlemleri
    /// </summary>
    [Route("/svc")]
    public class CalendarController : Controller
    { 
        
        private readonly PatikaDbContext _context;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public CalendarController(PatikaDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Returns date info of Al-Shaya period
        /// </summary>
        
        /// <param name="year">Queried year</param>
        /// <param name="month">period number, format ##</param>
        /// <response code="200">Date converted to Al-Shaya Calendar</response>
        [HttpGet]
        [Route("dateFrom/{year}/{month}")]
        [ValidateModelState]
        [SwaggerOperation("DateFrom")]
        [SwaggerResponse(statusCode: 200, type: typeof(ResponseBeginsEnds), description: "Returns month begin-end")]
        public IActionResult DateFrom([FromRoute][Required]int year, [FromRoute][Required]int month)
        {
            try
            {
            var dateFromResponse = _context.Set<ResponseBeginsEnds>()
                .FromSql("SELECT MIN(`Date`) AS begins," +
                         "MAX(`Date`) AS ends " +
                         "FROM `calendar` " +
                         "WHERE `Year` = {0} AND `Month` = {1}", year, month).ToList();
            if (dateFromResponse.Any())
            {
                return StatusCode(200, dateFromResponse[0]);
            }
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }

            return StatusCode(404, "Not Found");
        }

        /// <summary>
        /// Returns Al Shaya info of date
        /// </summary>

        /// <param name="year">Queried year</param>
        /// <param name="date">Date formatted YYYYMMDD</param>
        /// <response code="200">Date converted to Al-Shaya Calendar</response>
        [HttpGet]
        [Route("dateTo/{year}/{date}")]
        [ValidateModelState]
        [SwaggerOperation("DateTo")]
        [SwaggerResponse(200, typeof(Calendar), "Date converted to Al-Shaya Calendar")]
        public IActionResult DateTo([FromRoute] [Required] int year, [FromRoute] [Required] int date)
        {
            var dates = _context.Calendars.FirstOrDefault(c => c.Year == year && c.Date == date);
            return dates != null ? StatusCode(200, dates) : StatusCode(404, "Not found");
        }
    }
}
