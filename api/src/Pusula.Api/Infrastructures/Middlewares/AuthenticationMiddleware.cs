﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Pusula.Api.Data.DataContexts;

namespace Pusula.Api.Infrastructures.Middlewares
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="next"></param>
        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="localDbContext"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context, PatikaDbContext localDbContext)
        {
            context.Items["UserArea"] = "M";
            context.Items["UserStore"] = 0;
            context.Items["PrimTypes"] = "0";
            
            var da = Environment.GetEnvironmentVariable("DISABLE_AUTH");
            if (da != null)
            {
                await _next.Invoke(context);
                return;
            }

            if (context.Request.Path.ToString().StartsWith("/svc/filesGet"))
            {
                await _next.Invoke(context);
                return;
            }

            if (!context.Request.Path.ToString().StartsWith("/svc"))
            {
                await _next.Invoke(context);
                return;
            }

            string authHeader = context.Request.Headers["Authorization"];
            if (authHeader != null)
            {
                var user = localDbContext.Users
                    .Where(m => m.SessionKey == authHeader)
                    .FirstOrDefault(z => z.SessionValidTil > DateTimeOffset.Now.ToUnixTimeSeconds());
                if (user != null)
                {
                    //uzat 30 dk daha
                    user.SessionValidTil = DateTimeOffset.Now.AddMinutes(30).ToUnixTimeSeconds();
                    await localDbContext.SaveChangesAsync();

                    var emp = localDbContext.Employees.AsNoTracking()
                        .Include(s => s.Store)
                        .FirstOrDefault(e => e.Sicil == user.Sicil);
                    if (emp != null)
                    {
                        context.Items["UserArea"] = emp.Area;
                        context.Items["UserStore"] = emp.Store.Id;
                        context.Items["UserSicil"] = emp.Sicil;
                    }

                    //Roller
                    List<int> roleIds = new List<int>();

                    var registryRoleAssignments = localDbContext.RegistryRoleAssignments.AsNoTracking()
                        .Include(t => t.Role)
                        .Where(r => r.Sicil == user.Sicil);
                    if (registryRoleAssignments.Any())
                    {
                        foreach (var registryRoleAssignment in registryRoleAssignments)
                        {
                            roleIds.Add(registryRoleAssignment.Role.Id);
                        }
                    }

                    var employee = localDbContext.Employees
                        .Include(p => p.Position)
                        .FirstOrDefault(p => p.Sicil == user.Sicil);
                    if (employee != null)
                    {
                        var titleRoleAssignments = localDbContext.TitleRoleAssignments
                            .AsNoTracking()
                            .Include(a => a.Role)
                            .Where(t => t.Position.Id == employee.Position.Id);

                        if (titleRoleAssignments.Any())
                        {
                            foreach (var titleRoleAssignment in titleRoleAssignments)
                            {
                                roleIds.Add(titleRoleAssignment.Role.Id);
                            }
                        }
                    }

                    context.Items["UserRoles"] = string.Join(",", roleIds);

                    var primTypes = localDbContext.RolePrimTypes.AsNoTracking()
                        .Include(x => x.PrimType)
                        .Where(x => roleIds.Contains(x.Role.Id))
                        .ToList();
                    foreach (var rolePrimType in primTypes)
                    {
                        context.Items["PrimTypes"] += "," + rolePrimType.PrimType.Id;
                    }

                    await _next.Invoke(context);
                }
                else
                {
                    var resp = JsonConvert.SerializeObject(new
                    {
                        success = false,
                        message = "Authorization geçersiz.",
                        code = "UNAUTHORIZED",
                        data = new { }
                    });
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = 401; //Unauthorized
                    await context.Response.WriteAsync(resp);
                }
            }
            else
            {
                // no authorization header
                var resp = JsonConvert.SerializeObject(new
                {
                    success = false,
                    message = "Authorization gerekli.",
                    code = "UNAUTHORIZED",
                    data = new { }
                });
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = 401; //Unauthorized
                await context.Response.WriteAsync(resp);
            }
        }
    }
}