namespace Pusula.Api.Infrastructures.Core
{
    /// <summary>
    /// Hesaplama Tipi
    /// </summary>
    public enum CalcTypeEnum
    {
        /// <summary>
        /// Bütçe üstü
        /// </summary>
        OverAchievement = 1,
        
        /// <summary>
        /// Telafi
        /// </summary>
        Telafi = 2,
        
        /// <summary>
        /// Karlılık
        /// </summary>
        Karlilik = 3,
        
        /// <summary>
        /// Kesinti
        /// </summary>
        Shrinkage = 4,
        
        /// <summary>
        /// Toplam
        /// </summary>
        Total = 9
    }
}