using System;
using System.Globalization;

namespace Pusula.Api.Infrastructures
{
    /// <summary>
    /// Transfer objesi
    /// </summary>
    public class Transfer
    {
        /// <inheritdoc />
        public Transfer(int storeId, string baslangic, string bitis, bool aktif, int position)
        {
            StoreId = storeId;
            Baslangic = DateTime.ParseExact(baslangic, "yyyyMMdd", CultureInfo.InvariantCulture);
            Bitis = DateTime.ParseExact(bitis, "yyyyMMdd", CultureInfo.InvariantCulture).AddDays(-1);
            Aktif = aktif;
            Pozisyon = position;
        }

        /// <summary>
        /// Çalışma Durumu
        /// </summary>
        public bool Aktif { get; set; }

        /// <summary>
        /// Mağaza Id
        /// </summary>
        public int StoreId { get; set; }
        
        /// <summary>
        /// Başlangıc tarihi
        /// </summary>
        public DateTime Baslangic { get; set; }
        
        /// <summary>
        /// Bitis Tarihi
        /// </summary>
        public DateTime Bitis { get; set; }

        /// <summary>
        /// Bitiş Str
        /// </summary>
        /// <returns></returns>
        public string GetBitisStr()
        {
            return this.Bitis.ToString("yyyyMMdd");
        }
        /// <summary>
        /// Başlangıç Str
        /// </summary>
        /// <returns></returns>
        public string GetBaslangicStr()
        {
            return this.Baslangic.ToString("yyyyMMdd");
        }
        
        /// <summary>
        /// Bitiş Str
        /// </summary>
        /// <returns></returns>
        public int GetBitisInt()
        {
            return Convert.ToInt32(this.Bitis.ToString("yyyyMMdd"));
        }
        /// <summary>
        /// Başlangıç Str
        /// </summary>
        /// <returns></returns>
        public int GetBaslangicInt()
        {
            return Convert.ToInt32(this.Baslangic.ToString("yyyyMMdd"));
        }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        public int Pozisyon { get; set; }
        
    }
}