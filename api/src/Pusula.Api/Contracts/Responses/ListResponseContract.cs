﻿namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// 
    /// </summary>
    public class ListResponseContract
    {
        /// <summary>
        /// Kayıt istatistiği
        /// </summary>
        public TotalProperty Total { get; set; }
        
        /// <summary>
        /// Optional: Limit offseti
        /// </summary>
        public int? Offset { get; set; }
        
        /// <summary>
        /// Optional: set limitlnmiş ise limit adedi
        /// </summary>
        public int? Limit { get; set; }
        
        /// <summary>
        /// Veri, her zaman List
        /// </summary>
        public object Data { get; set; }
    }

    /// <summary>
    /// Kayıt istatistiği
    /// </summary>
    public class TotalProperty
    {
        /// <summary>
        /// Tüm recordset adedi
        /// </summary>
        public int All { get; set; }
        
        /// <summary>
        /// Filtrelenmiş adedi
        /// </summary>
        public int? Filtered { get; set; }
    } 
}