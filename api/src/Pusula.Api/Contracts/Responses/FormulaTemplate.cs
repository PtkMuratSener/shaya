﻿using System.Collections.Generic;

namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// KV çifti
    /// </summary>
    public class Pair
    {
        /// <summary>
        /// Anahtar
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Değer
        /// </summary>
        public string Value { get; set; }
    }
    /// <summary>
    /// Formüller için şablon
    /// </summary>
    public class FormulaTemplate
    {
        /// <inheritdoc />
        public FormulaTemplate()
        {
            Language = "javascript";
            Code = "personal_bonus = (employee_monthly_sale_total * employee_bonus_rate) ";
            //    +"– (employee_shrinkage_rate * employee_monthly_sale_total * employee_bonus_rate);";
            Parametres = new List<Pair>();
            Parametres.Add(new Pair{Key="employee_monthly_sale_total", Value="Personelin aylık normalize edilmiş satış tutarı."});
            Parametres.Add(new Pair {Key="employee_bonus_rate",Value="Personele verilecek prim oranı."});
            Parametres.Add(new Pair {Key="employee_shrinkage_rate",Value="Kısıtlama oranı."});
            Parametres.Add(new Pair {Key="bonus_day",Value="Prime esas çalışma günü."});
            Parametres.Add(new Pair {Key="bonus_free_day",Value="Prim hesaplanmayacak gün sayısı."});
            Parametres.Add(new Pair {Key="employee_salary",Value="Personel maaşı."});
            Parametres.Add(new Pair {Key="pool_sale_amount",Value="Mağaza havuz satış toplamı."});
        }

        /// <summary>
        /// Kodun dili [Javascript]
        /// </summary>
        public string Language { get; set; }
        
        /// <summary>
        /// Kod içeriği
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// Kullanılabilir parametreler listesi
        /// </summary>
        public List<Pair> Parametres { get; set; }
    }
}