namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// Aylık Satış Primi
    /// </summary>
    public class PrimResponseMonthly : PrimResponseBase
    {
        /// <summary>
        /// Aylık Satış Prim Tutarı
        /// </summary>
        public double MonthlyTotal { get; set; }
    }
}