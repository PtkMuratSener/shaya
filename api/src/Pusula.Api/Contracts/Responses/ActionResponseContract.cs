﻿namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// Genel işlemler sonucu 
    /// </summary>
    public class ActionResponseContract
    {
        /// <summary>
        /// işlem başarı durumu
        /// </summary>
               
        public bool Success { get; set; }
        
        /// <summary>
        /// İşlem mesajı
        /// </summary>
        public string Message { get; set; }
        
        /// <summary>
        /// Optional: sonuç kodu örn: ACTION_POST_SUCCESS
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// Optional: sonuç detay veri
        /// </summary>
        public object Data { get; set; }
    }
}