namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// Mağaza aktif satış bilgileri
    /// </summary>
    public class StoreActualsResponse
    {
        /// <summary>
        /// Periyot başı
        /// </summary>
        public string PeriyotBaslangic { get; set; }
        
        /// <summary>
        /// Periyot sonu
        /// </summary>
        public string PeriyotSonu { get; set; }
                
        /// <summary>
        /// İade başı
        /// </summary>
        public string IadeBaslangic { get; set; }
        
        /// <summary>
        /// İade sonu
        /// </summary>
        public string IadeSonu { get; set; }

        /// <summary>
        /// İade Miktarı
        /// </summary>
        public int Iade { get; set; }
        
        /// <summary>
        /// Mağaza Net satış miktarı
        /// </summary>
        public int Actual { get; set; }
        
        /// <summary>
        /// Mağaza brüt satış miktarı
        /// </summary>
        public int Brut { get; set; }
    }
}