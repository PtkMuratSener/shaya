using System;

namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// Prim listesi ortak alanlar
    /// </summary>
    public class PrimResponseBase
    {
        /// <summary>
        /// İşyeri Id
        /// </summary>
        public string StoreId { get; set; }
        
        /// <summary>
        /// İşyeri Adı
        /// </summary>
        public string StoreName { get; set; }
        
        /// <summary>
        /// Sicil No
        /// </summary>
        public int Sicil { get; set; }
        
        /// <summary>
        /// Çalışan Adı Soyadı
        /// </summary>
        public string NameSurname { get; set; }
        
        /// <summary>
        /// Çalışan Ücreti
        /// </summary>
        public int Salary { get; set; }
        
        /// <summary>
        /// Unvanı
        /// </summary>
        public string Unvan { get; set; }
    
        /// <summary>
        /// İşe Giriş Tarihi
        /// </summary>
        public string DateStart { get; set; }
        
        /// <summary>
        /// Transfer Tarihi
        /// </summary>
        public string DateTransfer { get; set; }
        
        /// <summary>
        /// Prim Alma Durumu
        /// </summary>
        public bool HasBonus { get; set; }
        
        /// <summary>
        /// Prim Oranı
        /// </summary>
        public double BonusRate { get; set; }
        
        /// <summary>
        /// Prim Tutarı
        /// </summary>
        public double NetAmount { get; set; }
        
        /// <summary>
        /// Prim Gün Sayısı
        /// </summary>
        public int BonusTotalDays { get; set; }
        
        /// <summary>
        /// Kesilecek Rapor/Ücretsiz İzin Gün Sayısı
        /// </summary>
        public int BonusFreeDays { get; set; }
        
        /// <summary>
        /// Prim Ödenecek Gün Sayısı
        /// </summary>
        public int BonusPayDays { get; set; }
        
        /// <summary>
        /// Disiplin Ceza Bilgisi
        /// </summary>
        public string Discipline { get; set; }
        
        /// <summary>
        /// Notlar (Transfer Bilgisi)
        /// </summary>
        public string Notes { get; set; }
    }
}