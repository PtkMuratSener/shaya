using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Pusula.Api.Models.Entities;

namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// Sayfalamalı Sonuçlar
    /// </summary>
    [DataContract]
    public class PagingResponse
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullContent">Tüm sorgu</param>
        /// <param name="content">Sayfa</param>
        /// <param name="request">İstek</param>
        public PagingResponse(IQueryable<dynamic>fullContent, List<dynamic> content, PaginationBase request)
        {
            var sort = request.Sort ?? "id,asc";
            var sortParam = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[0];
            var sortDir = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[1];
            Sort = new Sort
            {
                Property = sortParam,
                Ascending = sortDir == "asc",
                Descending = sortDir == "desc",
                Direction = sortDir,
                IgnoreCase = false,
                NullHandling = "NATIVE"

            };
            var page = request.Page ?? 0;
            var itemsPerPage = request.Size ?? 50; 
            First = page == 0;
            Last = page == fullContent.Count() / itemsPerPage + 1;
            Number = page;
            NumberOfElements = content.Count;
            Size = itemsPerPage;
            TotalElements = fullContent.Count();
            TotalPages = (fullContent.Count() / itemsPerPage) + 1;
            Content = content;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content">Sayfa</param>
        /// <param name="request">İstek</param>
        public PagingResponse(List<dynamic> content, PaginationBase request)
        {
            var sort = request.Sort ?? "id,asc";
            sort = request.Sort == "" ? "id,asc" : request.Sort;
            var sortParam = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[0];
            var sortDir = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[1];
            Sort = new Sort
            {
                Property = sortParam,
                Ascending = sortDir == "asc",
                Descending = sortDir == "desc",
                Direction = sortDir,
                IgnoreCase = false,
                NullHandling = "NATIVE"

            };
            var page = request.Page ?? 0;
            var itemsPerPage = request.Size ?? 50;
            itemsPerPage = itemsPerPage == 0 ? 50 : itemsPerPage; 
            First = page == 0;
            Last = page == content.Count / itemsPerPage + 1;
            Number = page;
            NumberOfElements = content.Count;
            Size = itemsPerPage;
            TotalElements = content.Count;
            TotalPages = (content.Count / itemsPerPage) + 1;
            Content = content;
        }

        /// <summary>
        /// İlk sayfa mı?
        /// </summary>
        [DataMember(Name="First")]
        public bool First { get; set; }
        
        /// <summary>
        /// Son sayfa mı?
        /// </summary>
        [DataMember(Name="Last")]
        public bool Last  { get; set; }
        
        /// <summary>
        /// Saayfa numarası
        /// </summary>
        [DataMember(Name="Number")]
        public int Number  { get; set; }
        
        /// <summary>
        /// Sayfadaki kayıt sayısı
        /// </summary>
        [DataMember(Name="NumberOfElements")]
        public int NumberOfElements  { get; set; }
        
        /// <summary>
        /// Sayfalama boyutu
        /// </summary>
        [DataMember(Name="Size")]
        public int Size  { get; set; }
        
        /// <summary>
        /// Sıralama
        /// </summary>
        [DataMember(Name="Sort")]
        public Sort Sort  { get; set; }
        
        /// <summary>
        /// Toplam kayıt sayısı
        /// </summary>
        [DataMember(Name="TotalElements")]
        public int TotalElements  { get; set; }
        
        /// <summary>
        /// Toplam sayfa sayısı
        /// </summary>
        [DataMember(Name="TotalPages")]
        public int TotalPages  { get; set; }
        
        /// <summary>
        /// İçerik
        /// </summary>
        [DataMember(Name="Content")]
        public List<object> Content  { get; set; }
    }


    /// <summary>
    /// Sıralama ölçütleri
    /// </summary>
    public class Sort
    {
        /// <summary>
        /// Kriter
        /// </summary>
        [DataMember(Name="Property")]
        public string Property  { get; set; }
        
        /// <summary>
        /// Yön
        /// </summary>
        [DataMember(Name="Direction")]
        public string Direction  { get; set; }
        
        /// <summary>
        /// Artan sıralama
        /// </summary>
        [DataMember(Name="Ascending")]
        public bool Ascending  { get; set; }
        
        /// <summary>
        /// Azalan sıralama
        /// </summary>
        [DataMember(Name="Descending")]
        public bool Descending  { get; set; }
        
        /// <summary>
        /// Büyük küçük harf duyarlı
        /// </summary>
        [DataMember(Name="IgnoreCase")]
        public bool IgnoreCase  { get; set; }
        
        /// <summary>
        /// Null ele alımı
        /// </summary>
        [DataMember(Name="NullHandling")]
        public string NullHandling  { get; set; }        
    }
}