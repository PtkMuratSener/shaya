﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Pusula.Api.Infrastructures.Core;
using Pusula.Api.Models.Entities;

namespace Pusula.Api.Contracts.Requests
{
    /// <inheritdoc />
    /// <summary>
    /// Hesaplama parametreleri
    /// (Tümü için 0 girin)
    /// </summary>
    [DataContract]
    public class YCalculationRequest: PaginationBase
    {
        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="BrandId")]
        public int BrandId { get; set; }

        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name="StoreId")]
        public int StoreId { get; set; }
        
        /// <summary>
        /// Yıl
        /// </summary>
        [DataMember(Name="Year")]
        [Required]
        [Range(2018, 2038)]
        public int Year { get; set; }

        /// <summary>
        /// Durum
        /// </summary>
        [DataMember(Name="Final")]
        public bool Final { get; set; }
    }
}