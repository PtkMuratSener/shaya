﻿using System.Runtime.Serialization;

namespace Pusula.Api.Contracts.Requests
{
    /// <summary>
    /// Login bilgileri
    /// </summary>
    [DataContract]
    public class LoginRequest
    {
        /// <summary>
        /// Kullanıcı adı (LDAP Username)
        /// </summary>
        [DataMember(IsRequired = true, Name = "Username")]
        public string Username { get; set; }

        /// <summary>
        /// Parolası
        /// </summary>
        [DataMember(IsRequired = true, Name = "Password")]
        public string Password { get; set; }
    }
}