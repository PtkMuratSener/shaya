﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Pusula.Api.Models.Entities;

namespace Pusula.Api.Contracts.Requests
{
    /// <inheritdoc />
    /// <summary>
    /// Hesaplama parametreleri
    /// (Tümü için 0 girin)
    /// </summary>
    [DataContract]
    public class CalculationRequest: PaginationBase
    {
        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="BrandId")]
        public int BrandId { get; set; }

        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name="StoreId")]
        public int StoreId { get; set; }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        [DataMember(Name="PositionId")]
        public int PositionId { get; set; }

        /// <summary>
        /// Personel
        /// </summary>
        [DataMember(Name="EmployeeId")]
        public int EmployeeId { get; set; }

        /// <summary>
        /// Yıl
        /// </summary>
        [DataMember(Name="Year")]
        [Required]
        [Range(2018, 2038)]
        public int Year { get; set; }

        /// <summary>
        /// Ay
        /// </summary>
        [DataMember(Name="Month")]
        [Range(0,12)]
        public int Month { get; set; }

        /// <summary>
        /// Kalıcı veri 
        /// </summary>
        [DataMember(Name="Final")]
        public string Final { get; set; }
        
        /// <summary>
        /// Prim tipi (1 Victorias, 2-Shaya)
        /// </summary>
        [DataMember(Name = "PrimType")]
        public string PrimType { get; set; }
    }
}
