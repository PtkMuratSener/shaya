using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Pusula.Api.Models.Entities;

namespace Pusula.Api.Contracts.Requests
{
    /// <summary>
    /// Aylık gerçekleşen hedefler isteği
    /// </summary>
    [DataContract]
    public class MonthlyActualRequest: PaginationBase
    {
        /// <summary>
        /// Marka Id (Tümü=0)
        /// </summary>
        [DataMember(Name="BrandId")]
        [Required]
        public int BrandId { get; set; }
        
        /// <summary>
        /// Mağaza Id (Tümü=0)
        /// </summary>
        [DataMember(Name="StoreId")]
        [Required]
        public int StoreId { get; set; }
        
        /// <summary>
        /// Pozisyon Id (Tümü=0)
        /// </summary>
        [DataMember(Name="PositionId")]
        [Required]
        public int PositionId { get; set; }
        
        /// <summary>
        /// Employee Id (Tümü=0)
        /// </summary>
        [DataMember(Name="EmployeeId")]
        [Required]
        public int EmployeeId { get; set; }

        /// <summary>
        /// Yıl
        /// </summary>
        [DataMember(Name="Year")]
        public int Year { get; set; }

        /// <summary>
        /// Ay
        /// </summary>
        [DataMember(Name="Month")]
        public int Month { get; set; }
    }
}