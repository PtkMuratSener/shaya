CREATE view VEmployeeBonuses as
select DISTINCT
       B.Id                               AS Id,
       B2.Name                            AS Brand,
       B2.Id                              AS BrandId,
       S.Name                             AS Store,
       B.StoreId                          AS StoreId,
       P.Name                             AS Position,
       B.PositionId                       AS PositionId,
       E.Name                             AS Name,
       E.Surname                          AS Surname,
       B.EmployeeSicil                    AS Sicil,
       B.Amount                           AS Bonus,
       B.Notes                            AS Notes,
       B.Final                            AS Final,
       B.Year                             AS Year,
       B.Month                            AS Month,
       B.Actual                           AS Actual,
       isnull(M.Achieved, 0)              AS ActualBrut,
       isnull(S2.Pmst, 0)                 AS Target,
       IIF(B.Amount > 0, 'Alır', 'Almaz') AS BonusState,
       ISNULL(B.Rate, 0)                  AS BonusRate,
       CAST(B.Actual AS FLOAT)            AS MonthlyTotal,
       30                                 AS BonusTotalDays,
       0                                  AS BonusFreeDays,
       30                                 AS BonusPayDays,
       ' '                                AS Discipline,
       '1'                                AS PrimType
from (((((EmployeeBonuses B
  left join Stores S on ((S.Id = B.StoreId)))
  left join Brands B2 on (B.BrandId = B2.Id)))
  left join Positions P on ((B.PositionId = P.Id)))
  left join Employees E on ((B.EmployeeSicil = E.Sicil)))
  LEFT JOIN VSasMonths SM ON (SM.BrandId=B.BrandId AND SM.MNTH=B.Month)
  left join MonthlyAchieveds M on ((
    (M.Sicil = B.EmployeeSicil) and (M.Year = B.Year) and (M.Month = B.Month)))
  LEFT JOIN StorePositions S2 ON
  (P.Id = S2.PositionId AND S2.Year = M.Year AND S2.Month = M.Month AND S2.StoreId = S.Id)
  WHERE SM.SAS = 0
UNION ALL
select B.Id                               AS Id,
       B2.Name                            AS Brand,
       B2.Id                              AS BrandId,
       S.Name                             AS Store,
       B.StoreId                          AS StoreId,
       P.Name                             AS Position,
       B.PositionId                       AS PositionId,
       E.Name                             AS Name,
       E.Surname                          AS Surname,
       B.Sicil                            AS Sicil,
       B.Amount                           AS Bonus,
       IIF(B.Notes='.', 'Mevsimlik çalışan', B.Notes) AS Notes,
       B.Final                            AS Final,
       B.Year                             AS Year,
       B.Month                            AS Month,
       0                                  AS Actual,
       0                                  AS ActualBrut,
       0                                  AS Target,
       IIF(B.Amount > 0, 'Alır', 'Almaz') AS BonusState,
       ISNULL(r.Rate, 0.0)                AS BonusRate,
       CAST(B.Salary AS FLOAT)            AS MonthlyTotal,
       B.WorkDays                         AS BonusTotalDays,
       B.FreeDays                         AS BonusFreeDays,
       B.PayDays                          AS BonusPayDays,
       B.CezaNotes                        AS Discipline,
       '2'                                AS PrimType
from (((((BonusSltMonthly B
  left join Stores S on ((S.Id = B.StoreId)))
  left join Brands B2 on (B.BrandId = B2.Id)))
  left join Positions P on ((B.PositionId = P.Id)))
  left join Employees E on ((B.Sicil = E.Sicil)))
       left join BonusRatesSlt r on (r.BrandId = B.BrandId AND r.PositionId = B.PositionId)
  WHERE (B.Active=1 OR (B.Active=0 AND B.Seasonal=1 AND E.Active=1 AND E.StoreId=B.StoreId))
  and ((B.Month IN (SELECT MNTH FROM VSasMonths WHERE SAS = 0) AND P.HasBonus = 0)
      OR (B.Month IN (SELECT MNTH FROM VSasMonths WHERE SAS = 1))
      )
go


