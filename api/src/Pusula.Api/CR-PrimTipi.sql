CREATE TABLE [PrimTypes] (
  [Id] int NOT NULL IDENTITY,
  [Name] nvarchar(128) NULL,
  CONSTRAINT [PK_PrimTypes] PRIMARY KEY ([Id])
  );

GO

CREATE TABLE [RolePrimTypes] (
  [Id] int NOT NULL IDENTITY,
  [PrimTypeId] int NULL,
  [RoleId] int NULL,
   CONSTRAINT [PK_RolePrimTypes] PRIMARY KEY ([Id]),
  CONSTRAINT [FK_RolePrimTypes_PrimTypes_PrimTypeId] FOREIGN KEY ([PrimTypeId]) REFERENCES [PrimTypes] ([Id]) ON DELETE NO ACTION,
  CONSTRAINT [FK_RolePrimTypes_Roles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [Roles] ([Id]) ON DELETE NO ACTION
  );

GO

CREATE INDEX [IX_RolePrimTypes_PrimTypeId] ON [RolePrimTypes] ([PrimTypeId]);

GO

CREATE INDEX [IX_RolePrimTypes_RoleId] ON [RolePrimTypes] ([RoleId]);

GO

INSERT INTO [PrimTypes] ([Name]) VALUES ('Victorias Secret');
INSERT INTO [PrimTypes] ([Name]) VALUES ('Shaya');
GO
