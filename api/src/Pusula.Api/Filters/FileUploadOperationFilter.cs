using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Filters
{
    /// <summary>
    /// Adds File Upload form element
    /// </summary>
    public class FileUploadOperation : IOperationFilter
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="operation">Operation</param>
        /// <param name="context">OperationFilterContext</param>
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.OperationId.ToLower().Substring(0, 2) == "xl") 
            {
                operation.Parameters.Clear();
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "year",
                    In = "formData",
                    Description = "Year",
                    Required = true,
                    Type = "int"
                });
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "upfile",
                    In = "formData",
                    Description = "Upload File",
                    Required = true,
                    Type = "file"
                });
                operation.Consumes.Add("multipart/form-data");
            }
        }
    }
}