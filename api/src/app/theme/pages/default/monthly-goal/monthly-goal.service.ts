import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../../../environments/environment';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { StorageService } from './../../../../auth/_services';
import { MonthlyGoal } from "../../../../model/monthlygoal/monthly-goal.model";

@Injectable()
export class MonthlyGoalService {
    API_URL = environment.apiURL;
    headers: HttpHeaders = new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*'
    });

    constructor(private http: HttpClient, private storage: StorageService) {
        this.headers = this.headers.append('Authorization', this.storage.getToken());
    }

    getBrands() {
        return this.http.get(this.API_URL + '/svc/brands', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getStores(brandId) {
        return this.http.get(this.API_URL + '/svc/brandStores/' + brandId, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    hedefGuncelle(year: number, month: number, storeId: number) {
        return this.http.post(this.API_URL + '/svc/storePositionsUpdate', { year: year, period: month, storeId: storeId }, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getStorePosition(year: number, month: number, storeId: number) {
        return this.http.get(this.API_URL + '/svc/storePosition/' + year + "/" + month + "/" + storeId, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    saveStorePosition(monthlyGoal: MonthlyGoal) {
        if (monthlyGoal.id < 1) {
            return this.http.post(this.API_URL + '/svc/storePosition', monthlyGoal, {
                headers: this.headers
            }).pipe(catchError(this.handleError));
        }
        else {
            return this.http.put(this.API_URL + '/svc/storePosition', monthlyGoal, {
                headers: this.headers
            }).pipe(catchError(this.handleError));
        }
    }
    getStoreTarget(year: number, month: number, storeId: number) {
        return this.http.get(this.API_URL + '/svc/storeTarget/' + year + "/" + month + "/" + storeId, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        if (error.error.code == "UNAUTHORIZED") {
            localStorage.removeItem('currentUser');
            window.location.reload();
        }
        // return an observable with a user-facing error message
        return ErrorObservable.create(error.error);
    };

}