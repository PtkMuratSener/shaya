import { Component, OnInit, ChangeDetectionStrategy, NgZone, ChangeDetectorRef } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { RoleService } from './role.service';
import { environment } from '../../../../../../environments/environment';
import { Role } from "../../../../../model/account/role/role.model";
import { forEach } from "../../../../../../../node_modules/@angular/router/src/utils/collection";
import { AuthorizationType } from "../../../../../model/account/role/authorization-type.model";
import { Screen } from "../../../../../model/account/role/screen.model";
import { StoreType } from "../../../../../model/account/role/store-type.model";
import { WorkingArea } from "../../../../../model/account/role/working-area.model";
import { RolePermission } from "../../../../../model/account/role/role-permission.model";
import { Pageable } from "../../../../../model/pageable";
import { Router } from "@angular/router";
import { Rights } from "../../../../../model/rights";

@Component({
    selector: 'm-data',
    templateUrl: './role.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoleComponent implements OnInit {
    public rights = Rights;
    roles: Role[] = [];
    rolePermissions: RolePermission[] = [];

    public role: Role = new Role();
    public roleNew: Role = new Role();
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;

    public list: Role[];
    cols: any[];
    public pageable: Pageable = new Pageable();
    public totalRecords: number = 0;
    public selectedItems: Role[] = [];

    constructor(private zone: NgZone,
        private router: Router,
        private cdRef: ChangeDetectorRef,
        private roleService: RoleService,
        private messageService: MessageService) {
        this.roles = new Array<Role>();
    }

    ngOnInit() {
        this.cols = [
            { field: 'name', header: 'name' },
            { field: 'createdOn', header: 'createdOn' },
        ];
        this.getRoleList();
    }

    getRoleList() {
        this.roleService.getRoleList().subscribe(result => {
            if (result != null) {
                this.list = result.data;
                this.cdRef.detectChanges();
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi' });
        })
    }
    getRolePermissionsList(id) {
        //console.log(id);
        this.rolePermissions = [];
        this.roleService.getRolePermissionsList(id).subscribe(result => {
            if (result != null) {
                this.rolePermissions = result.data;
                this.totalRecords = result.data.totalRecords;
                this.cdRef.detectChanges();
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi' });
        })
    }

    /* submit() {
         this.roleService.save(this.roleNew).subscribe((parameters) => {
             this.getRoleList();
             this.messageService.add({severity: 'success', summary: 'İşlem başarılı', detail: "Kayıt eklendi!"});
         }, error => {
             console.log('error', error);
             this.messageService.add({severity: 'error', summary: 'Hata oluştu', detail: error});
         });
     }
     */
    delete(item) {
        //console.log(item);
        this.roleService.delete(item.id).subscribe((parameters) => {
            this.getRoleList();
            this.messageService.add({ severity: 'success', summary: 'İşlem başarılı', detail: "Kayıt silindi!" });
            this.cdRef.detectChanges();

        }, error => {
            this.messageService.add({ severity: 'error', summary: 'Hata oluştu', detail: error.message });
            this.cdRef.detectChanges();
        });
    }
    remove(item) {
        this.selectedItems = [];
        this.selectedItems.push(item);
    }

    removes() {
        if (this.selectedItems.length == 0) {
            return;
        }
        this.selectedItems.forEach(item => {
            this.delete(item);
        });
    }

    add() {
        this.router.navigate(['role/add']);
    }

    edit(item) {
        if (item) {
            this.router.navigate(['role/' + item.id]);
        }
    }


}
