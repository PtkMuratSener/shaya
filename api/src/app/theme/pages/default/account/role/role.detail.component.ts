import { AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from 'primeng/components/common/messageservice';

import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { TranslateService } from "@ngx-translate/core";
import { Role } from "../../../../../model/account/role/role.model";
import { RoleService } from "./role.service";
import { Rights } from "../../../../../model/rights";

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./role.detail.component.html",
    providers: [
        MessageService
    ]
})

export class RoleDetailComponent implements OnInit, AfterViewInit {
    public rights = Rights;
    public role: Role = new Role();
    public currentRole: Role = new Role();

    title = '';
    message = '';
    public isNew: boolean = false;
    public userTypes: any = [];


    constructor(
        private _script: ScriptLoaderService,
        private messageService: MessageService,
        private roleService: RoleService,
        private cdRef: ChangeDetectorRef,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private translateService: TranslateService) {

        this.activatedRoute.params.subscribe((params: any) => {
            if (params['id']) {
                this.getRoleById(params['id']);
            }
            if (params['id'] == 'add') {
                this.isNew = true;
            }
        });

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

    }

    getRoleById(id) {
        if (id != null && parseInt(id) > 0) {
            this.roleService.getRoleById(id).subscribe((data) => {
                this.role = data.data;
            }, (response) => {
                this.translateService.get('message.error.title').subscribe(msg => {
                    this.title = msg;
                });

                this.messageService.add({ severity: 'error', summary: this.title, detail: response.data.error_description });
            });
        }
    }

    saveOrCreate(valid) {
        if (valid) {
            this.roleService.saveOrUpdate(this.role).subscribe((data) => {

                if (this.role.id > 0) {
                    this.translateService.get('message.update.title').subscribe(msg => {
                        this.title = msg;
                    });

                    this.translateService.get('message.update.message').subscribe(msg => {
                        this.message = msg;
                    });
                    this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });
                } else {
                    this.role = data.data;
                    this.isNew = false;
                    this.translateService.get('message.save.title').subscribe(msg => {
                        this.title = msg;
                    });

                    this.translateService.get('message.save.message').subscribe(msg => {
                        this.message = msg;
                    });

                    this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });
                }


                this.cdRef.detectChanges();

            }, (response) => {
                this.translateService.get('message.error.title').subscribe(msg => {
                    this.title = msg;
                });

                this.messageService.add({ severity: 'error', summary: this.title, detail: response.data.error_description });
                this.cdRef.detectChanges();
            });
        }
    }

    homePage() {
        this.router.navigate(['role']);
    }

}
