import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';

import { NgxSpinnerService } from 'ngx-spinner';
import { CenterAnnualLineGoalService } from "./center-annual-line-goal.service";
import { Rights } from "../../../../../model/rights";
import { ShayaOverachPremiumRate } from "../../../../../model/setting/shayaoverachpremiumrate/shaya-overach-premium-rate.model";
import { CenterAnnualLineGoalModel } from "../../../../../model/setting/centerannuallinegoal/center-annual-line-goal.model";
import { SelectInput } from "../data-input/calendar";

@Component({
    selector: 'center-annual-line-goal',
    templateUrl: './center-annual-line-goal.component.html'
})
export class CenterAnnualLineGoalComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public centerAnnualLineGoalModel = new CenterAnnualLineGoalModel();
    list: CenterAnnualLineGoalModel[] = [];
    targets: SelectInput[] = [
        { label: 'Seçiniz', value: '', checked: false },
        { label: 'Line 1', value: 'Line 1', checked: false },
        { label: 'Line 7', value: 'Line 7', checked: false },
        { label: 'Line 12', value: 'Line 12', checked: false },
        { label: 'Line 17', value: 'Line 17', checked: false },
        { label: 'Line 24', value: 'Line 24', checked: false },
        { label: 'Shrinkage', value: 'Shrinkage', checked: false },
    ];

    constructor(private centerAnnualLineGoalService: CenterAnnualLineGoalService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.centerAnnualLineGoalService.getHqBonusLines().subscribe(result => {
            this.list = result.data;
        }, error => {
            this.list = [];
            this.messageService.add({
                severity: 'info', summary: 'Hata oluştu',
                detail: ' Merkez yıllık line hedefler alınamadı!' + error.message
            });
        });

    }


    submit() {
        this.spinner.show();
        this.centerAnnualLineGoalService.save(this.list).subscribe(result => {
            this.spinner.hide();
            if (result.success) {
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: result.message });
            } else {
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı' + error.message });
        });

    }
}
