import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../../environments/environment';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { StorageService } from '../../../../../auth/_services';
import { SltPremiumRate } from "../../../../../model/setting/sltpremiumrate/slt-premium-rate.model";
import { ShayaOverachPremiumRate } from "../../../../../model/setting/shayaoverachpremiumrate/shaya-overach-premium-rate.model";
import { CenterAnnualLineGoalModel } from "../../../../../model/setting/centerannuallinegoal/center-annual-line-goal.model";

@Injectable()
export class CenterAnnualLineGoalService {
    API_URL = environment.apiURL;
    headers: HttpHeaders = new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*'
    });

    constructor(private http: HttpClient, private storage: StorageService) {
        this.headers = this.headers.append('Authorization', this.storage.getToken());
    }

    getHqBonusLines() {
        return this.http.get(this.API_URL + '/svc/hqBonusLines', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }


    save(centerAnnualLineGoalModel: CenterAnnualLineGoalModel[]) {
        return this.http.put(this.API_URL + '/svc/hqBonusLines', centerAnnualLineGoalModel, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        if (error.error.code == "UNAUTHORIZED") {
            localStorage.removeItem('currentUser');
            window.location.reload();
        }
        // return an observable with a user-facing error message
        return ErrorObservable.create(error.error);
    };

}