import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';

import { SelectInput } from '../data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { TitleNotAnnualBonusService } from "./title-not-annual-bonus.service";
import { PremiumRate } from "../../../../../model/premium-rate/premium-rate.model";
import { Rights } from "../../../../../model/rights";
import { SltPremiumRate } from "../../../../../model/setting/sltpremiumrate/slt-premium-rate.model";
import { WorkingArea } from "../../../../../model/account/role/working-area.model";
import { RoleService } from "../../account/role/role.service";
import { TitleNotAnnualBonus } from "../../../../../model/titlenotannualbonus/title-not-annual-bonus.model";
import { FormSelectOption } from "../../../../../model/titlenotannualbonus/form-select-option.model";

@Component({
    selector: 'title-not-annual-bonus',
    templateUrl: './title-not-annual-bonus.component.html'
})
export class TitleNotAnnualBonusComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public titleNotAnnualBonus = new TitleNotAnnualBonus();
    brands: FormSelectOption[] = [];
    list: TitleNotAnnualBonus[] = [];
    /* brands: SelectInput[] = [{
         label: 'Seçiniz', value: '0', checked: false
     }];*/
    positions: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];

    constructor(private titleNotAnnualBonusService: TitleNotAnnualBonusService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.titleNotAnnualBonusService.getBrands().subscribe(result => {
            var newFormSelectOption = new FormSelectOption('0', "Merkez", []);
            newFormSelectOption.items.push(new FormSelectOption('0', 'Merkez', null))
            this.brands = [];
            this.brands.push(newFormSelectOption);
            if (result.total) {
                var newFormSelectOption2 = new FormSelectOption('0', "Saha", []);
                for (const brand of result.data) {
                    newFormSelectOption2.items.push(new FormSelectOption(brand.id, brand.name + " (" + brand.shortName + ")", []))
                }
                this.brands.push(newFormSelectOption2);
            }
        }, error => {
            this.messageService.add({
                severity: 'info',
                summary: 'Hata oluştu',
                detail: 'Marka listesi alınamadı!' + error.message
            });
        });
    }

    getTitleNotAnnualBonusByBrandId() {
        if (this.titleNotAnnualBonus.brandId == null || this.titleNotAnnualBonus.brandId < 0) {
            // this.messageService.add({severity: 'error', summary: 'Hata', detail: 'Marka bilgisi seçiniz!'});
            return;
        }
        this.spinner.show();
        this.titleNotAnnualBonusService.getPremiumRateByBrandId(this.titleNotAnnualBonus.brandId).subscribe(result => {
            this.spinner.hide();
            this.list = result.data;
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Pozisyon listesi alınamadı! ' + error.message });
        });

    }

    submit() {
        if (this.titleNotAnnualBonus.brandId == null || this.titleNotAnnualBonus.brandId < 0) {
            this.messageService.add({ severity: 'info', summary: 'Hata', detail: 'Marka bilgisi seçiniz!' });
            return;
        }

        this.spinner.show();
        this.titleNotAnnualBonusService.save(this.list).subscribe(result => {
            this.spinner.hide();
            if (result.success) {
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: result.message });
            } else {
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı' + error.message });
        });

    }
}
