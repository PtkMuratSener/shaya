import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ParametersComponent } from './parameters.component';
import { HttpClientModule } from '@angular/common/http';
import { ParametersService } from './parameters.service';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': ParametersComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        ReactiveFormsModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild()
    ],
    exports: [
        RouterModule,
        ParametersComponent
    ],
    declarations: [
        ParametersComponent
    ],
    providers: [ParametersService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ParametersModule {


}