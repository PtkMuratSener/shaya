export class Parameter {
    constructor(public id: number, public title: string, public value: any, public description: string) { }
}