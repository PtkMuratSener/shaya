import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { FormsModule } from '@angular/forms';
import { DropdownModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';
import { ShayaAnnualPremiumRateComponent } from "./shaya-annual-premium-rate.component";
import { ShayaAnnualPremiumRateService } from "./shaya-annual-premium-rate.service";
import { OnlyNumber } from "../../../../../_directives/only-number.directive";
import { OnlyLetter } from "../../../../../_directives/only-letter.directive";

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': ShayaAnnualPremiumRateComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DropdownModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild()
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        ShayaAnnualPremiumRateComponent
    ],
    providers: [ShayaAnnualPremiumRateService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ShayaAnnualPremiumRateModule {


}