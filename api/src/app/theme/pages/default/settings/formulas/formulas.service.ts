import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../../environments/environment';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { StorageService } from '../../../../../auth/_services';
import { Formula } from '../../../../../model/setting/formula/formula.model';

@Injectable()
export class FormulasService {
    API_URL = environment.apiURL;
    headers: HttpHeaders = new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*'
    });

    constructor(private http: HttpClient, private storage: StorageService) {
        this.headers = this.headers.append('Authorization', this.storage.getToken());
    }

    getList() {
        return this.http.get(this.API_URL + '/svc/formula', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getFormulaTemplate() {
        return this.http.get(this.API_URL + '/svc/formulaTemplate', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    save(formula: Formula) {
        return this.http[formula.id ? 'put' : 'post'](this.API_URL + '/svc/formula', formula, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        if (error.error.code == "UNAUTHORIZED") {
            localStorage.removeItem('currentUser');
            window.location.reload();
        }
        // return an observable with a user-facing error message
        return ErrorObservable.create('Something bad happened; please try again later.');
    };

}