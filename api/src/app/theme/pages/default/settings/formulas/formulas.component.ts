import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';
import { FormulasService } from './formulas.service';
import { Formula } from '../../../../../model/setting/formula/formula.model';
import { FormulaTemplate } from "../../../../../model/setting/formula/formula.template.model";
import { Rights } from "../../../../../model/rights";

@Component({
    selector: 'm-formulas',
    templateUrl: './formulas.component.html'
})
export class FormulasComponent implements OnInit {
    public rights = Rights;
    spinnerConfig: any = environment.spinner;
    msgs: Message[] = [];
    public formulas: Formula[];
    public formulaTemplate = new FormulaTemplate();
    // form: FormGroup;
    // items: FormArray;
    public calculate = 0;
    defaultFormulas: Formula[] = [];

    constructor(private formulasService: FormulasService, private messageService: MessageService) {
        //  this.createForm();
        //console.log(2222)
        this.getFormulaTemplate();
        this.formulasService.getList().subscribe(result => {
            //debugger;
            this.formulas = result.data;
            // console.log(this.formulas);
        }, error => {
            this.messageService.add({ severity: 'error', summary: 'Hata oluştu', detail: 'Liste alınamadı' });
        });
    }

    ngOnInit() {

        //console.log(111)
    }


    getFormulaTemplate() {
        this.formulasService.getFormulaTemplate().subscribe(result => {
            this.formulaTemplate = result.data;
            for (let item of this.formulaTemplate.parametres) {
                item.result = 0;
            }
            // console.log(this.formulaTemplate);
            /*  var i = 0;
             for (const item of result.data.parametres) {
                 console.log(item);
                 var keyVal = new KeyValue();
                 keyVal.key = item;
                 this.formulaTemplate.parametre[i] = new KeyValue();
                 this.formulaTemplate.parametre[i].key=item;
                 i++;
             }*/
            // console.log(this.formulaTemplate);
        }, error => {
            this.messageService.add({ severity: 'error', summary: 'Hata oluştu', detail: 'Formül template listesi alınamadı' });
        });
    }
    /*createForm() {
      this.form = this.fb.group({
        'formulas': this.fb.array([])
      });
    }
  
  
    addFormula(formula: Formula) {
      this.items = this.form.get('formulas') as FormArray;
      this.items.push(this.fb.group({
        id: formula.id,
        name: formula.name,
        formulaType: formula.formulaType,
        content: formula.content
      }));
    }
  
    get formulas() {
     return (this.form.get('formulas') as FormArray).controls;
    }
    */
    test(item) {
        var copyValue = item.content;

        copyValue = copyValue.split(' ').join('')
        var x = copyValue.split('=');
        copyValue = x[x.length - 1];
        for (let keyVal of this.formulaTemplate.parametres) {
            if (keyVal.result == null) {
                keyVal.result = 0;
            }
            copyValue = copyValue.split(keyVal.key.trim()).join(keyVal.result.toString())
        }
        copyValue = copyValue.replace(';', '');
        try {
            this.calculate = eval(copyValue);
            return true;
        }
        catch (e) {
            console.log(e);
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Hatalı formül girdiniz!' });
            return false;
        }


    }
    submit() {
        // const formModel = this.form.value;
        // const formulas = formModel.formulas;

        for (const item of this.formulas) {
            if (this.test(item)) {
                this.formulasService.save(item).subscribe(result => {
                    this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'Kaydedildi' });
                }, error => {
                    this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Kayıt yapılamadı' });
                });
            }
        }
    }
}
