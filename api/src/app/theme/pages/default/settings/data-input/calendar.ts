export interface SelectInput {
    label: string;
    value: string;
    checked: boolean;
}

export var Years: SelectInput[] = [];

let yil = new Date().getFullYear();
if (new Date().getMonth() == 12) {
    yil = yil + 1; // Aralık ayında sonraki yıl işlemleri yapılabilsin.
}
for (var _i = 2018; _i <= yil; _i++)
    Years.push({
        label: _i.toString(), value: _i.toString(), checked: false
    });

export const Months: SelectInput[] = [{
    label: 'Ocak', value: '1', checked: false
}, {
    label: 'Şubat', value: '2', checked: false
}, {
    label: 'Mart', value: '3', checked: false
}, {
    label: 'Nisan', value: '4', checked: false
}, {
    label: 'Mayıs', value: '5', checked: false
}, {
    label: 'Haziran', value: '6', checked: false
}, {
    label: 'Temmuz', value: '7', checked: false
}, {
    label: 'Ağustos', value: '8', checked: false
}, {
    label: 'Eylül', value: '9', checked: false
}, {
    label: 'Ekim', value: '10', checked: false
}, {
    label: 'Kasım', value: '11', checked: false
}, {
    label: 'Aralık', value: '12', checked: false
}];
