import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { Message } from "primeng/api";

import { Rights } from "../../../../../../model/rights";
import { environment } from "../../../../../../../environments/environment";
import { DataInputService } from "../data-input.service";
import { MessageService } from "primeng/components/common/messageservice";
import { NgxSpinnerService } from "ngx-spinner";
import { Calendar } from "../../../../../../model/setting/datainput/calendar.model";
import { StoreTarget } from "../../../../../../model/setting/datainput/store-target.model";
import { Angular5Csv } from "angular5-csv/Angular5-csv";

@Component({
    selector: 'm-dataview',
    templateUrl: './data-view.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class DataViewComponent implements OnInit {

    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    year: number;
    storetargets: StoreTarget[] = [];
    public API_URL = environment.apiURL;

    constructor(private activatedRoute: ActivatedRoute,
        private dataInputService: DataInputService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService, ) {
        this.activatedRoute.params.subscribe((params) => {
            this.year = params.year;
            console.log('year', this.year);
        })
    }

    ngOnInit() {
        this.getStoretargets();
    }


    getStoretargets() {
        this.dataInputService.getStoretargets(this.year).subscribe((response) => {
            console.log('getStoretargets', response);
            this.storetargets = response.data;
        })
    }

    exportCSV() {
        let csv = [];
        let self = this;
        var options = {
            fieldSeparator: ';',
            quoteStrings: '"',
            decimalseparator: ',',
            showLabels: true,
            showTitle: false,
            useBom: true,
            noDownload: false,
            headers: [
                "YIL",
                "KODU",
                "MAĞAZA",
                "GERÇEKLEŞEN OCAK",
                "GERÇEKLEŞEN ŞUBAT",
                "GERÇEKLEŞEN MART",
                "GERÇEKLEŞEN NİSAN",
                "GERÇEKLEŞEN MAYIS",
                "GERÇEKLEŞEN HAZİRAN",
                "GERÇEKLEŞEN TEMMUZ",
                "GERÇEKLEŞEN AĞUSTOS",
                "GERÇEKLEŞEN EYLÜL",
                "GERÇEKLEŞEN EKİM",
                "GERÇEKLEŞEN KASIM",
                "GERÇEKLEŞEN ARALIK",
                "GERÇEKLEŞEN TOPLAM",

                "HEDEF OCAK",
                "HEDEF ŞUBAT",
                "HEDEF MART",
                "HEDEF NİSAN",
                "HEDEF MAYIS",
                "HEDEF HAZİRAN",
                "HEDEF TEMMUZ",
                "HEDEF AĞUSTOS",
                "HEDEF EYLÜL",
                "HEDEF EKİM",
                "HEDEF KASIM",
                "HEDEF ARALIK",
                "HEDEF TOPLAM"
            ]
        };

        this.storetargets.forEach(function(item) {
            csv.push({
                "year": item.year,
                "storeId": item.storeId,
                "store": item.store,
                "a01": item.a01,
                "a02": item.a02,
                "a03": item.a03,
                "a04": item.a04,
                "a05": item.a05,
                "a06": item.a06,
                "a07": item.a07,
                "a08": item.a08,
                "a09": item.a09,
                "a10": item.a10,
                "a11": item.a11,
                "a12": item.a12,
                "totalActual": item.totalActual,
                "t01": item.t01,
                "t02": item.t02,
                "t03": item.t03,
                "t04": item.t04,
                "t05": item.t05,
                "t06": item.t06,
                "t07": item.t07,
                "t08": item.t08,
                "t09": item.t09,
                "t10": item.t10,
                "t11": item.t11,
                "t12": item.t12,
                "totalTarget": item.totalTarget
            });
        });


        let fileName = 'cognos_verileri';


        new Angular5Csv(csv, fileName, options);
    }
}