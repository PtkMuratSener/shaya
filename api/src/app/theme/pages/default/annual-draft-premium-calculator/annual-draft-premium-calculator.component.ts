import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { AnnualDraftPremiumCalculatorService } from './annual-draft-premium-calculator.service';
declare var jQuery: any;

import { SelectInput, Years } from '../settings/data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { catchError } from "rxjs/operators";
import { BonusCalculateMonthly } from "./../../../../model/setting/bonuscalculatemonthly/bonus-calculate-monthly.model";
import { Rights } from "./../../../../model/rights";
import { environment } from "../../../../../environments/environment";

@Component({
    selector: 'annual-draft-premium-calculator',
    templateUrl: './annual-draft-premium-calculator.component.html'
})
export class AnnualDraftPremiumCalculatorComponent implements OnInit, OnDestroy {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public progressValue: number = 0;
    intervalId: number;
    process: number;
    total: number;
    bonusCalculateMonthly: BonusCalculateMonthly;
    years: SelectInput[];
    months: SelectInput[];
    brands: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    stores: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    employees: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];

    constructor(private bonusCalculateService: AnnualDraftPremiumCalculatorService,
        private messageService: MessageService,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {
        this.years = Years;
    }

    ngOnInit() {
        this.bonusCalculateService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Marka listesi alınamadı!' });
        });
        this.bonusCalculateService.getPositions().subscribe(result => {
            if (result.total) {
                for (const position of result.data) {
                    this.positions.push({
                        label: position.name,
                        value: position.id,
                        checked: false
                    })
                }
            }
        }, error => {
            //this.messageService.add({severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi'});
        });
    }
    ngOnDestroy() {
        clearInterval(this.intervalId);
    }


    getEmployees(storeId) {
        this.employees = [{
            label: 'Tümü', value: '0', checked: false
        }];

        if (storeId) {
            this.spinner.show();
            this.bonusCalculateService.getStoreEmployees(storeId).subscribe(result => {
                if (result.total.all) {
                    for (const emp of result.data) {
                        this.employees.push({
                            label: emp.name + " " + emp.surname + " (" + emp.sicil + ")",
                            value: emp.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağaza çalışanları listesi alınamadı!' });
            });
        }
    }

    submit(model) {
        //alert(JSON.stringify(model));
        debugger;
        if (model.year == null || model.year < 1) {
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Yıl bilgisi seçiniz!' });
            return;
        }
        this.spinner.show();
        this.bonusCalculateService.save(model).subscribe(result => {
            this.spinner.hide();
            //document.getElementById("process_bar_modal").click();
            if (result.success) {
                this.bonusCalculateMonthly = result.data.calc;
                jQuery("#process_bar_modal").modal("show");
                this.progressValue = 0;
                this.process = 0;
                this.total = 0;
                this.intervalId = setInterval(() => {
                    this.bonusCalculateService.getHesaplamaDurum(this.bonusCalculateMonthly.id).subscribe(response => {
                        if (response.data.total < 1) {
                            console.log("dönen değer:", response);
                            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Toplam hesaplanacak personel sayısı 0 olduğundan işlem durduruldu!' });
                            clearInterval(this.intervalId);
                            this.intervalId = null;
                            this.spinner.hide();
                            jQuery("#process_bar_modal").modal("hide");
                            this.cdRef.detectChanges();
                            return;
                        }
                        this.process = response.data.processed;
                        this.total = response.data.total;
                        this.progressValue = (response.data.processed / response.data.total) * 100;

                        this.progressValue = this.progressValue == null ? 0 : this.progressValue;
                        if (response.data.processed == response.data.total) {
                            clearInterval(this.intervalId);
                            this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'Hesaplama tamamlandı!' });

                        }
                    }, error => {
                        this.spinner.hide();
                        this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Hesaplama durumu servisten alınamadı.' });
                        clearInterval(this.intervalId);
                    });

                }, 20000);
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'İşleniyor' });
            } else {
                if (result.code == "CALCULATION_CONTINUIES") {//CALCULATION_INFO
                    if (result != null && result.data != null) {
                        if (this.bonusCalculateMonthly == null) {
                            this.bonusCalculateMonthly = new BonusCalculateMonthly();
                        }
                        this.bonusCalculateMonthly.id = result.data.calc.id;

                    }
                    if (this.intervalId == null || this.intervalId < 1) {
                        console.log(this.intervalId);
                        this.intervalId = setInterval(() => {
                            this.bonusCalculateService.getHesaplamaDurum(this.bonusCalculateMonthly.id).subscribe(response => {
                                if (response.data.total < 1) {
                                    console.log("dönen değer:", response);
                                    this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Toplam hesaplanacak personel sayısı 0 olduğundan işlem durduruldu!' });
                                    clearInterval(this.intervalId);
                                    this.intervalId = null;
                                    this.spinner.hide();
                                    return;
                                }
                                this.progressValue = (response.data.processed / response.data.total) * 100;

                                this.progressValue = this.progressValue == null ? 0 : this.progressValue;
                                this.process = response.data.processed;
                                this.total = response.data.total;

                                if (response.data.processed == response.data.total) {
                                    clearInterval(this.intervalId);
                                    this.intervalId = null;
                                    this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'Hesaplama tamamlandı!' });
                                }
                            }, error => {
                                this.spinner.hide();
                                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Hesaplama durumu servisten alınamadı.' });
                                clearInterval(this.intervalId);
                            });

                        }, 20000);
                    }
                    this.cdRef.detectChanges();
                    jQuery("#process_bar_modal").modal("show");
                }
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı! ' + error.message });
        });
    }
}
