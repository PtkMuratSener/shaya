import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from './../../../layouts/layout.module';
import { DefaultComponent } from './../default.component';
import { FormsModule } from '@angular/forms';
import { DropdownModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AnnualDraftPremiumCalculatorComponent } from './annual-draft-premium-calculator.component';
import { HttpClientModule } from '@angular/common/http';
import { AnnualDraftPremiumCalculatorService } from './annual-draft-premium-calculator.service';
import { ProgressBarModule } from "angular-progress-bar"
import { PremiumCalculatorComponent } from "../component/premium-calculator/premium-calculator.component";
import { PremiumCalculatorService } from "../component/premium-calculator/premium-calculator.service";
import { BonusCalculateMonthlyService } from "../settings/bonus-calculate-monthly/bonus-calculate-monthly.service";

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': AnnualDraftPremiumCalculatorComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DropdownModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild(),
        ProgressBarModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        AnnualDraftPremiumCalculatorComponent,
        PremiumCalculatorComponent
    ],
    providers: [AnnualDraftPremiumCalculatorService, PremiumCalculatorService, BonusCalculateMonthlyService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AnnualDraftPremiumCalculatorModule {


}