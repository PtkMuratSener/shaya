import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from './../default.component';
import { FormsModule } from '@angular/forms';
import { DataTableModule, DropdownModule, DialogModule, AutoCompleteModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PremiumMonthlyComponent } from './premium-monthly.component';
import { HttpClientModule } from '@angular/common/http';
import { PremiumMonthlyService } from './premium-monthly.service';
import { CalcLogListComponent } from './calc-log-list/calc-log-list.component';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': PremiumMonthlyComponent
            },
            {
                'path': ':sicil',
                'component': CalcLogListComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DataTableModule,
        DropdownModule,
        DialogModule,
        AutoCompleteModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild()
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        PremiumMonthlyComponent,
        CalcLogListComponent
    ],
    providers: [PremiumMonthlyService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class PremiumMonthlyModule {


}