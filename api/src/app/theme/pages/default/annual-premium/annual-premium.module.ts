import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from './../default.component';
import { FormsModule } from '@angular/forms';
import { DataTableModule, DropdownModule, DialogModule, AutoCompleteModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AnnualPremiumComponent } from './annual-premium.component';
import { HttpClientModule } from '@angular/common/http';
import { AnnualPremiumService } from './annual-premium.service';
import { OverAchievementComponent } from './over-achievement/over-achievement.component';
import { TelafiComponent } from "./telafi/telafi.component";
import { KarlilikComponent } from "./karlilik/karlilik.component";
import { ShrinkageComponent } from "./shrinkage/shrinkage.component";

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': AnnualPremiumComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DataTableModule,
        DropdownModule,
        DialogModule,
        AutoCompleteModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild()
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        AnnualPremiumComponent,
        OverAchievementComponent,
        TelafiComponent,
        KarlilikComponent,
        ShrinkageComponent
    ],
    providers: [AnnualPremiumService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AnnualPremiumModule {


}