import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { CentralDraftAnnualPremiumCalculationService } from './central-draft-annual-premium-calculation.service';
import { environment } from './../../../../../environments/environment';
declare var jQuery: any;

import { NgxSpinnerService } from 'ngx-spinner';
import { catchError } from "rxjs/operators";
import { BonusCalculateMonthly } from "./../../../../model/setting/bonuscalculatemonthly/bonus-calculate-monthly.model";
import { Rights } from "./../../../../model/rights";
import { SelectInput, Months, Years } from "../settings/data-input/calendar";

@Component({
    selector: 'm-central-draft-annual-premium-calculation',
    templateUrl: './central-draft-annual-premium-calculation.component.html'
})
export class CentralDraftAnnualPremiumCalculationComponent implements OnInit, OnDestroy {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public progressValue: number = 0;
    intervalId: number;
    process: number;
    total: number;
    bonusCalculateMonthly: BonusCalculateMonthly;
    years: SelectInput[];
    months: SelectInput[];
    brands: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    stores: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    employees: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];

    model: any = {
        brandId: 0,
        year: 0
    };

    constructor(private centralDraftAnnualPremiumCalculationService: CentralDraftAnnualPremiumCalculationService,
        private messageService: MessageService,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {
        this.years = Years;
        this.months = Months
    }

    ngOnInit() {
        this.centralDraftAnnualPremiumCalculationService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Marka listesi alınamadı!' });
        });
    }
    ngOnDestroy() {
        clearInterval(this.intervalId);
    }

    submit() {
        if (this.model.year == null || this.model.year < 1) {
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Yıl bilgisi seçiniz!' });
            return;
        }
        if (this.model.month == null || this.model.month < 1) {
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Periyot bilgisi seçiniz!' });
            return;
        }
        this.spinner.show();
        this.centralDraftAnnualPremiumCalculationService.save(this.model).subscribe(result => {
            this.spinner.hide();
            //document.getElementById("process_bar_modal").click();
            if (result.success) {
                this.bonusCalculateMonthly = result.data;
                jQuery("#process_bar_modal").modal("show");
                this.progressValue = 0;
                this.process = 0;
                this.total = 0;
                this.intervalId = setInterval(() => {
                    this.centralDraftAnnualPremiumCalculationService.getHesaplamaDurum(this.bonusCalculateMonthly.id).subscribe(response => {
                        if (response.data.total < 1) {
                            console.log("dönen değer:", response);
                            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Toplam hesaplanacak personel sayısı 0 olduğundan işlem durduruldu!' });
                            clearInterval(this.intervalId);
                            this.intervalId = null;
                            this.spinner.hide();
                            return;
                        }
                        this.process = response.data.processed;
                        this.total = response.data.total;
                        this.progressValue = (response.data.processed / response.data.total) * 100;

                        this.progressValue = this.progressValue == null ? 0 : this.progressValue;
                        if (response.data.processed == response.data.total) {
                            clearInterval(this.intervalId);
                            this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'Hesaplama tamamlandı!' });

                        }
                    }, error => {
                        this.spinner.hide();
                        this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Hesaplama durumu servisten alınamadı.' });
                        clearInterval(this.intervalId);
                    });

                }, 1000);
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'İşleniyor' });
            } else {
                if (result.code == "CALCULATION_CONTINUIES") {//CALCULATION_INFO
                    if (result != null && result.data != null) {
                        if (this.bonusCalculateMonthly == null) {
                            this.bonusCalculateMonthly = new BonusCalculateMonthly();
                        }
                        this.bonusCalculateMonthly.id = result.data.id;

                    }
                    if (this.intervalId == null || this.intervalId < 1) {
                        console.log(this.intervalId);
                        this.intervalId = setInterval(() => {
                            this.centralDraftAnnualPremiumCalculationService.getHesaplamaDurum(this.bonusCalculateMonthly.id).subscribe(response => {
                                if (response.data.total < 1) {
                                    console.log("dönen değer:", response);
                                    this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Toplam hesaplanacak personel sayısı 0 olduğundan işlem durduruldu!' });
                                    clearInterval(this.intervalId);
                                    this.intervalId = null;
                                    this.spinner.hide();
                                    return;
                                }
                                this.progressValue = (response.data.processed / response.data.total) * 100;

                                this.progressValue = this.progressValue == null ? 0 : this.progressValue;
                                this.process = response.data.processed;
                                this.total = response.data.total;

                                if (response.data.processed == response.data.total) {
                                    clearInterval(this.intervalId);
                                    this.intervalId = null;
                                    this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'Hesaplama tamamlandı!' });
                                }
                            }, error => {
                                this.spinner.hide();
                                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Hesaplama durumu servisten alınamadı.' });
                                clearInterval(this.intervalId);
                            });

                        }, 1000);
                    }
                    this.cdRef.detectChanges();
                    jQuery("#process_bar_modal").modal("show");
                }
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı! ' + error.message });
        });
    }
}
