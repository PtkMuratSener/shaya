import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { UserModel } from '../../../auth/_models';
import { StorageService } from '../../../auth/_services';

declare let mLayout: any;


@Component({
    selector: 'app-header-nav',
    templateUrl: './header-nav.component.html',
    encapsulation: ViewEncapsulation.None
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    public language: string;
    public me: UserModel = new UserModel();

    constructor(private translate: TranslateService,
        private storageService: StorageService) {
    }

    ngOnInit() {
        this.me = this.storageService.getAccount();
    }

    ngAfterViewInit() {
        mLayout.initHeader();
    }

}