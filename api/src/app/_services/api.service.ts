import { ApiModel } from '../model/api.model';
import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { LazyLoadEvent } from 'primeng/primeng';
import { Pageable } from '../model/pageable';

@Injectable()
export class ApiService<T extends ApiModel> {
    public apiUrl: string = 'api/';

    constructor(public service: Restangular, public model: T) {
        this.apiUrl = this.apiUrl + this.model.url;
    }

    findAll(event: LazyLoadEvent, pageable: Pageable, search: any) {
        const sort = this.setPagingParameters(pageable, event);
        const query = { page: pageable.page, size: pageable.size, sort: sort };
        this.setSearchCache(search);
        return this.service.one(this.apiUrl).get(Object.assign(query, search))
    }

    findAllCustom(url: string, event: LazyLoadEvent, pageable: Pageable, search: any) {
        const sort = this.setPagingParameters(pageable, event);
        const query = { page: pageable.page, size: pageable.size, sort: sort };

        return this.service.one(this.apiUrl + '/' + url).get(Object.assign(query, search))
    }

    approve(event: LazyLoadEvent, pageable: Pageable) {
        const sort = this.setPagingParameters(pageable, event);
        return this.service.one(this.apiUrl, 'approve').get({ page: pageable.page, size: pageable.size, sort: sort })
    }

    private setPagingParameters(pageable: Pageable, event: LazyLoadEvent) {
        pageable.page = Math.floor(event == null ? 0 : event.first / event.rows);
        pageable.page = pageable.page > -1 ? pageable.page : 0;

        pageable.size = event == null ? (pageable == null ? 50 : pageable.size) : event.rows;

        const key = null != event ? null == event.sortField ? 'id' : event.sortField : 'id';
        const value = null == event ? 'desc' : 1 == event.sortOrder ? 'asc' : 'desc';
        const sort = key + ',' + value;
        return sort;
    }

    autocomplete(q: any) {
        return this.service.one(this.apiUrl, 'autoComplete').get(q)
    }

    findById(id: number) {
        return this.service.one(this.apiUrl, id).get({ single: true });
    }

    findAllById(id: number) {
        return this.service.one(this.apiUrl + '/detail/' + id).get();
    }

    findBy(value: string) {
        return this.service.one(this.apiUrl + '/search/' + value.trim()).get();
    }

    filterByHql(hql: string) {
        return this.service.one(this.apiUrl + '/search?query=' + hql, '').post
    }

    getParentList() {
        return this.service.one(this.apiUrl, 'getParentList').get();
    }

    list() {
        return this.service.one(this.apiUrl, 'select').get();
    }

    saveOrUpdate(rec: T) {
        return this.service.all(this.apiUrl).post(JSON.stringify(rec), { single: true });
    }

    delete(id: number) {
        return this.service.one(this.apiUrl, id).remove();
    }

    getService() {
        return this.service
    }

    getApiUrl() {
        return this.apiUrl
    }

    getSearchCache() {
        let data = JSON.parse(localStorage.getItem(this.apiUrl))
        return data != null ? data : this.model;
    }

    setSearchCache(search: any) {
        localStorage.setItem(this.apiUrl, JSON.stringify(search));
    }
}
