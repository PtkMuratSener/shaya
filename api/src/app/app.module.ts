import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScriptLoaderService } from "./_services/script-loader.service";
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";

import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { RestangularModule, Restangular } from 'ngx-restangular';
import { StorageService } from './auth/_services';
import { WebStorageModule } from "ngx-store";
import { Base64 } from "./_services/base64.service";

import { environment } from '../environments/environment';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup

import { MomentModule } from 'angular2-moment';
import { NgxSpinnerModule } from "ngx-spinner"; // optional, provides moment-style pipes for date formatting


export function RestangularConfigFactory(RestangularProvider, storageService) {
    RestangularProvider.setBaseUrl(environment.apiURL);
    console.log('app.module 03');
    RestangularProvider.setDefaultHeaders({ 'Content-Type': ['application/json;', 'multipart/form-data;'] });

    RestangularProvider.addErrorInterceptor((response, subject, responseHandler, router) => {
        if (response.status == 0) {
            console.log('app.module 01', response);
            //     window.location.href = '/login'; // bu kisim acilabilir ama suan timeout sorunnundan dolayi kapatiyorum
        }
        console.log('app.module 02', response);
        if (response.status == 409) {
            if (response.data.error == 'Access is denied') {
                response.data.error = 'Erişim yetkiniz yok';
                response.data.error_description = 'Erişim yetkiniz yok';
            }
        }
        return response;
    });


    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {
        let token = storageService.getAuthToken();
        let account = storageService.getAccount();
        console.log('app.module 04', data);
        if (token && token.access_token && token.access_token != '') {
            RestangularProvider.setDefaultHeaders({
                'Content-Type': ['application/json;', 'multipart/form-data;'],
                'Authorization': 'Authorization ' + token.access_token
            });
        }
        return data;
    });


    RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {
        let token = storageService.getAuthToken();
        let account = storageService.getAccount();
        console.log('app.module 05', token);

        let extraHeaders = {};
        if (token && token.access_token && token.access_token != "") {
            extraHeaders = {
                'Authorization': 'Authorization ' + token.access_token
            };
        }

        return {
            headers: Object.assign({}, headers, extraHeaders, { 'Content-Type': ['application/json;', 'multipart/form-data;'], })
        };
    });

}


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
    declarations: [
        ThemeComponent,
        AppComponent,
    ],
    imports: [
        HttpClientModule,
        LayoutModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ThemeRoutingModule,
        AuthModule,
        WebStorageModule,
        MomentModule,
        NgxSpinnerModule,
        NgIdleKeepaliveModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        RestangularModule.forRoot([StorageService], RestangularConfigFactory)
    ],
    providers: [ScriptLoaderService, Base64],
    bootstrap: [AppComponent]
})
export class AppModule { }