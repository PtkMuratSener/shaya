import { ApiModel } from "../../api.model";
import { ArgumentOutOfRangeError } from "rxjs";
import { KeyValue } from "../key-value.model";

export class FormulaTemplate extends ApiModel {
    public language: string;
    public code: string;
    public parametres: KeyValue[] = []
    constructor() {
        super();
        //this.parametre = new Array();
    }
}