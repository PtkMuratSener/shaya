import { ApiModel } from "./../api.model";

export class Position extends ApiModel {
    public name: string;
    public hasBonus: boolean;
    constructor() {
        super();
    }
}