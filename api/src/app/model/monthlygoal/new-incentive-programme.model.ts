
import { ApiModel } from "../api.model";
import { Store } from "../brand/store.model";
import { Position } from "../position/position.model";


export class NewIncentiveProgramme extends ApiModel {
    public store: Store;
    public position: Position;
    public positionName: string;
    public positionId: number;

    public numStaff: number;
    public responsibility: string;
    public saleSplit: number;
    public salesTeamAchievement: string;//  string[]=['100 - 104.9%','105 - 109.9%','110 >%'];
    public staPercentage: number[] = [0, 0, 0];
    public staIncTLPerPos: number[] = [0, 0, 0];
    public staTotalMonIncTL: number[] = [0, 0, 0];
    public sta100: number;
    public sta105: number;
    public sta110: number;
    public order: string;
    constructor() {
        super();
        this.store = new Store();
        this.position = new Position();

    }
}
