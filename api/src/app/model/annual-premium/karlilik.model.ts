import { ApiModel } from "../api.model";

export class Karlilik extends ApiModel {
    public a: string;
    public b: string;
    public c: string;
    public d: string;
    public e: string;
    public f: string;
    public g: string;
    public h: string;
    public i: string;
    public j: string;
    public k: string;
    public l: string;
    public m: string;
    public n: string;
    public o: string;
    public p: string;
    public q: string;
    public r: string;
    public s: string;
    public final: boolean;
    public id: number;
}
