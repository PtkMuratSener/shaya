export enum DataStatus {
    ACTIVE = 'ACTIVE', PASSIVE = 'PASSIVE', DELETED = 'DELETED'
}

export enum CareerTermStatus {
    CLOSED = 'CLOSED', OPEN = 'OPEN'
}

