
import { ApiModel } from "../api.model";
import { Personel } from "../personel.model";
import { CareerTerm } from "../career-term.model";

export class ApprovementStatus extends ApiModel {
    public name: string;
    public order: number;
    public url = 'approvementStatuses';
}