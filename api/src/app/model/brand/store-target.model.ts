import { ApiModel } from "../api.model";
import { Store } from "./store.model";


export class StoreTarget extends ApiModel {
    public year: number;
    public month: number;
    public val: number;
    public store: Store;
    public isactual: boolean;
    constructor() {
        super();
        this.store = new Store();
    }
}
