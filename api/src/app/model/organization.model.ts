import { ApiModel } from "./api.model";
import { CareerTermStatus } from "./types.model";

export class Organization extends ApiModel {
    public id: number;
    public name: number;
    public url = 'organizations';
}
