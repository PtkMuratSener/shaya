import { ApiModel } from "../api.model";
import { BrandModel } from "../brand/brand.model";
import { Screen } from "./role/screen.model";
import { AuthorizationType } from "./role/authorization-type.model";

export class RoleComboRequest extends ApiModel {
    public role: number;
    public brands: BrandModel[] = [];//Markalar ,
    public screens: Screen[] = []; // Ekranlar ,
}