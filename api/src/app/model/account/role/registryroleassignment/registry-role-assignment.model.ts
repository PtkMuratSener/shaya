import { ApiModel } from "../../../api.model";
import { Role } from "../role.model";


export class RegistryRoleAssignment extends ApiModel {
    public role: Role;
    public sicil: number;
    public createdOn: any;
    constructor() {
        super();
        this.role = new Role();
    }
}