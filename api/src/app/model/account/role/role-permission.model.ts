import { ApiModel } from "../../api.model";
import { Role } from "./role.model";
import { AuthorizationType } from "./authorization-type.model";
import { Screen } from "./screen.model";
import { StoreType } from "./store-type.model";
import { WorkingArea } from "./working-area.model";

export class RolePermission extends ApiModel {
    public role: Role;
    public authorizationType: AuthorizationType;
    public screen: Screen;
    public storeType: StoreType;
    public workingArea: WorkingArea;
    public createdOn: any;
    constructor() {
        super();
        this.role = new Role();
        this.authorizationType = new AuthorizationType();
        this.screen = new Screen();
        this.storeType = new StoreType();
        this.workingArea = new WorkingArea();
    }
}