import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { CredentialsModel } from '../_models';
import { environment } from '../../../environments/environment';
import { StorageService } from './storage.service';
import { Subject } from "rxjs";

@Injectable()
export class AuthService {
    // public onCredentialUpdated$: Subject<CredentialsModel>;
    //  private authenticationState = new Subject<any>();

    constructor(private http: Http, private router: Router, private storage: StorageService) {
        //  this.onCredentialUpdated$ = new Subject();
    }


    login(credentials: CredentialsModel): Observable<any> {
        const data = 'username=' + credentials.username + '&password=' + credentials.password;

        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Accept', 'application/json');

        return this.http.post(`${environment.apiURL}/login`, data, { headers: headers })
            .map(res => res.json())
            .map(result => {
                if (result.success) {
                    this.storage.setAuth(result.data);
                    //this.onCredentialUpdated$.next(credentials);
                }

                return result.data;
            });
    }

    logOut() {
        localStorage.removeItem('currentUser');
        //localStorage.clear();
        //sessionStorage.clear();
        // this.authenticationState.next(null);

        //localStorage.$reset();
    }

}
