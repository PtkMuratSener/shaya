import { Directive, ElementRef, Renderer2, Input, OnInit } from '@angular/core';
import { Principal } from '../_services/principal.service';

/**
 * ## Use
 * <li [hasRight]" RightName "></li>
 * ```
 */
@Directive({
    selector: '[hasRight]'
})
export class HasRight implements OnInit {
    @Input('hasRight') _right: any;
    constructor(private el: ElementRef, private ren: Renderer2, private principal: Principal) { }

    ngOnInit() {
        if (this._right.length > 0)
            this.defineVisibility(true);
    }
    setVisible() {
        //this.ren.setElementClass(this.el.nativeElement, 'hidden', false);
        this.ren.removeClass(this.el.nativeElement, 'hidden');
    }
    setHidden() {
        //this.ren.setElementClass(this.el.nativeElement, 'hidden', true);
        this.ren.addClass(this.el.nativeElement, 'hidden');

    }
    defineVisibility(reset) {
        //debugger;
        var result;
        if (reset)
            this.setVisible();

        //result = this.principal.isInRight(this._right);
        result = this.principal.isInPermission(this._right[0], this._right[1], this._right[2], 0, 0);
        if (result)
            this.setVisible();
        else
            this.setHidden();
    }
}

/**
 * ## Use
 * <li [hasRight]" [RightNames] "></li>
 * ```
 */
@Directive({
    selector: '[hasAnyRight]'
})
export class HasAnyRight implements OnInit {
    constructor(private el: ElementRef, private ren: Renderer2, private principal: Principal) { }
    @Input('hasAnyRight') roles: any;
    ngOnInit() {
        //console.log(this.roles);
        //console.log("ngOnInit");

        if (this.roles.length > 0)
            this.defineVisibility(true);
    }
    setVisible() {
        //this.ren.setElementClass(this.el.nativeElement, 'hidden', false);
        this.ren.removeClass(this.el.nativeElement, 'hidden');
    }
    setHidden() {
        ///this.ren.setElementClass(this.el.nativeElement, 'hidden', true);
        this.ren.addClass(this.el.nativeElement, 'hidden');
    }
    defineVisibility(reset) {
        //console.log(reset);
        // console.log("defineVisibility");
        var result;
        if (reset)
            this.setVisible();

        result = this.principal.isInAnyRole(this.roles);
        if (result)
            this.setVisible();
        else
            this.setHidden();
    }
}
