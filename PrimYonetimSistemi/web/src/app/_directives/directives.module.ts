import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { OnlyNumber } from "./only-number.directive";
import { OnlyLetter } from "./only-letter.directive";

@NgModule({
    declarations: [

    ],
    imports: [
    ],
    providers: [

    ],
    exports: [

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: []
})

export class DirectivesModule {
}