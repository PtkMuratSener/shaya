import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { LazyLoadEvent, Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { RealizedMonthlyService } from './realized-monthly.service';
import { environment } from '../../../../../environments/environment';

import { SelectInput, Months, Years } from '../settings/data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { Pageable } from "../../../../model/pageable";
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { Rights } from "../../../../model/rights";
import { RealizedMonthly } from "../../../../model/realized-monthly/realized-monthly.model";

@Component({
    selector: 'premium-monthly',
    templateUrl: './realized-monthly.component.html'
})
export class RealizedMonthlyComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public list: RealizedMonthly[];
    cols: any[];
    public pageable: Pageable = new Pageable();
    public totalRecords: number = 0;
    public selectedItems: RealizedMonthly[] = [];

    years: SelectInput[];
    months: SelectInput[] = [
        { label: 'Tümü', value: '0', checked: false },
        { label: 'Ocak', value: '1', checked: false },
        { label: 'Şubat', value: '2', checked: false },
        { label: 'Mart', value: '3', checked: false },
        { label: 'Nisan', value: '4', checked: false },
        { label: 'Mayıs', value: '5', checked: false },
        { label: 'Haziran', value: '6', checked: false },
        { label: 'Temmuz', value: '7', checked: false },
        { label: 'Ağustos', value: '8', checked: false },
        { label: 'Eylül', value: '9', checked: false },
        { label: 'Ekim', value: '10', checked: false },
        { label: 'Kasım', value: '11', checked: false },
        { label: 'Aralık', value: '12', checked: false }
    ];
    brands: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    stores: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    employees: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }]; 

    model: any = {
        brandId: 0,
        storeId: 0,
        positionId: 0,
        employeeId: 0,
        year: 0,
        month: 0
    };

    constructor(private premiumMonthlyService: RealizedMonthlyService,
        private messageService: MessageService,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {
        this.years = Years;
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth();
        this.model.year = year;
        this.model.month = month;

    }

    ngOnInit() {

        this.cols = [
            { field: 'brand', header: 'Marka' },
            { field: 'store', header: 'Mağaza' },
            { field: 'namesurname', header: 'Ad Soyad' },
            { field: 'position', header: 'Pozisyon' },
            { field: 'year', header: 'Yıl' },
            { field: 'month', header: 'Periyot' },
            { field: 'target', header: 'Hedef' },
            { field: 'actual', header: 'Gerçekleşen' },
            { field: 'rate', header: 'Gerçekleşme(%)' }
        ];

        this.premiumMonthlyService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Marka listesi alınamadı!' });
        });
        this.premiumMonthlyService.getPositions().subscribe(result => {
            if (result.total) {
                for (const position of result.data) {
                    this.positions.push({
                        label: position.name,
                        value: position.id,
                        checked: false
                    })
                }
            }
        }, error => {
            //this.messageService.add({severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi'});
        });
        this.cdRef.detectChanges();
    }

    setStores(brandId) {
        this.stores = [{
            label: 'Tümü', value: '0', checked: false
        }];

        if (brandId) {
            this.spinner.show();
            this.premiumMonthlyService.getStores(brandId).subscribe(result => {

                if (result.total.all) {
                    for (const store of result.data) {
                        this.stores.push({
                            label: store.name,
                            value: store.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağazalar listesi alınamadı!' });
            });
        }
    }
    getEmployees(storeId) {
        this.employees = [{
            label: 'Tümü', value: '0', checked: false
        }];

        if (storeId) {
            this.spinner.show();
            this.premiumMonthlyService.getStoreEmployees(storeId).subscribe(result => {
                if (result.total.all) {
                    for (const emp of result.data) {
                        this.employees.push({
                            label: emp.name + " " + emp.surname + " (" + emp.sicil + ")",
                            value: emp.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({
                    severity: 'info', summary: 'Uyarı',
                    detail: 'Mağaza çalışanları listesi alınamadı! Mesaj: ' + error.message
                });
            });
        }
    }

    findAll(event: LazyLoadEvent) {

        this.spinner.show();
        this.premiumMonthlyService.getHesaplamaSonuc(event, this.pageable, this.model).subscribe(
            data => {
                // debugger;
                this.list = data.content;
                for (let item of this.list) {
                    item.actual = Math.round(item.actual);
                }
                this.totalRecords = data.totalElements;
                this.pageable.page = data.number;
                this.spinner.hide();
                this.cdRef.detectChanges();
            },
            response => {
                this.spinner.hide();
                this.list = [];
                this.totalRecords = 0;
                this.spinner.hide();
                this.cdRef.detectChanges();
                if (response.code == "CALCULATION_EMPTY_SET") {
                    // this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: response.message });
                }
                else {
                    this.messageService.add({ severity: 'error', summary: 'Hata', detail: response.message });
                }
            }
        );
    }
    exportCSV() {
        let csv = [];
        let self = this;
        var options = {
            fieldSeparator: ';',
            quoteStrings: '"',
            decimalseparator: ',',
            showLabels: true,
            showTitle: false,
            useBom: true,
            noDownload: false,
            headers: ["MARKA", "MAĞAZA", "AD SOYAD", "POZİSYON", "HEDEF", "GERÇEKLEŞEN", "GERÇEKLEŞME (%)"]
        };

        this.list.forEach(function(item) {
            csv.push({
                "brand": item.brand,
                "store": item.store,
                "namesurname": item.namesurname,
                "position": item.position,
                "target": item.target,
                "actual": item.actual,
                "rate": item.rate
            });
        });


        let fileName = 'gerçekleşen aylık';


        new Angular5Csv(csv, fileName, options);
    }
}
