import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { FormsModule } from '@angular/forms';
import { DataTableModule, DropdownModule, DialogModule, AutoCompleteModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RoleComponent } from './role.component';
import { HttpClientModule } from '@angular/common/http';
import { RoleService } from './role.service';
import { RoleDetailComponent } from "./role.detail.component";
import { RolePermissionComponent } from "./role-permission/role-permission.component";
import { RegistryRoleAssignmentComponent } from "./registry-role-assignment/registry-role-assignment.component";
import { TitleRoleAssignmentComponent } from "./title-role-assignment/title-role-assignment.component";

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            { 'path': '', 'component': RoleComponent },
            { "path": ':id', component: RoleDetailComponent },
            { "path": 'add', component: RoleDetailComponent }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DataTableModule,
        DropdownModule,
        DialogModule,
        AutoCompleteModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild()
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        RoleComponent,
        RoleDetailComponent,
        RolePermissionComponent,
        RegistryRoleAssignmentComponent,
        TitleRoleAssignmentComponent
    ],
    providers: [RoleService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class RoleModule {


}