import {
    Component,
    OnInit,
    Inject,
    ViewEncapsulation,
    AfterViewInit,
    ClassProvider,
    Input,
    ChangeDetectorRef
} from '@angular/core';
import { Restangular } from "ngx-restangular";

import { LazyLoadEvent, MenuItem } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';

import { ActivatedRoute } from "@angular/router";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { Pageable } from "../../../../../../model/pageable";

import { TranslateService } from "@ngx-translate/core";
import { RoleService } from "../role.service";
import { RolePermission } from "../../../../../../model/account/role/role-permission.model";
import { AuthorizationType } from "../../../../../../model/account/role/authorization-type.model";
import { Screen } from "../../../../../../model/account/role/screen.model";
import { StoreType } from "../../../../../../model/account/role/store-type.model";
import { WorkingArea } from "../../../../../../model/account/role/working-area.model";
import { RegistryRoleAssignment } from "../../../../../../model/account/role/registryroleassignment/registry-role-assignment.model";
import { Rights } from "../../../../../../model/rights";

@Component({
    selector: "registry-role-assignment",
    templateUrl: "./registry-role-assignment.component.html",
    providers: [
        MessageService
    ]
})

export class RegistryRoleAssignmentComponent implements OnInit, AfterViewInit {
    public rights = Rights;
    items: MenuItem[];
    @Input() roleId: number;
    cols: any[];
    colsRole: any[];
    roles: any[];
    public selectedItems: RegistryRoleAssignment[] = [];
    public pageable: Pageable = new Pageable();
    public totalRecords: number = 0;
    public list: RegistryRoleAssignment[] = [];

    message: string;
    title: string;
    public registryRoleAssignment = new RegistryRoleAssignment();
    public registryRoleAssignmentNew = new RegistryRoleAssignment();

    getId() {
        return this.roleId;
    }

    constructor(
        private roleService: RoleService,
        private cdRef: ChangeDetectorRef,
        public translateService: TranslateService,
        private _script: ScriptLoaderService,
        private messageService: MessageService,
        private activatedRoute: ActivatedRoute) {

    }





    getRegistryRoleAssignmentList() {
        this.roleService.getRegistryRoleAssignmentList(this.getId()).subscribe(
            data => {
                this.list = data.data;
                this.cdRef.detectChanges();
            },
            err => console.error(err)
        );
    }


    ngOnInit() {
        this.cols = [
            { field: 'sicil', header: 'Sicil' }
        ];
        this.getRegistryRoleAssignmentList();

    }
    ngAfterViewInit() {
        this.activatedRoute.params.subscribe((params: any) => {
            // alert(JSON.stringify(params));
        });
    }


    saveOrCreate(valid) {
        if (valid) {
            this.roleService.registryRoleAssignmentSaveOrUpdate(this.registryRoleAssignmentNew).subscribe((data) => {

                if (this.registryRoleAssignmentNew.id > 0) {
                    this.translateService.get('message.update.title').subscribe(msg => {
                        this.title = msg;
                    });

                    this.translateService.get('message.update.message').subscribe(msg => {
                        this.message = msg;
                    });
                    this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });
                } else {
                    this.registryRoleAssignmentNew = data.data;
                    //this.isNew = false;
                    this.translateService.get('message.save.title').subscribe(msg => {
                        this.title = msg;
                    });

                    this.translateService.get('message.save.message').subscribe(msg => {
                        this.message = msg;
                    });

                    this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });
                }
                this.cdRef.detectChanges();

            }, (response) => {
                this.translateService.get('message.error.title').subscribe(msg => {
                    this.title = msg;
                });

                this.messageService.add({ severity: 'error', summary: this.title, detail: response.data.error_description });
                this.cdRef.detectChanges();
            });
        }
    }
    delete(item) {
        this.roleService.registryRoleAssignmentDelete(item.id).subscribe(() => {
            this.translateService.get('message.delete.title').subscribe(msg => {
                this.title = msg;
            });

            this.translateService.get('message.delete.message').subscribe(msg => {
                this.message = msg;
            });
            this.getRegistryRoleAssignmentList();
            this.cdRef.detectChanges();
            this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });

        }, (response) => {
            this.translateService.get('message.error.title').subscribe(msg => {
                this.title = msg;
            });

            this.messageService.add({ severity: 'error', summary: this.title, detail: response.data.error_description });
        });

    }

    remove(item) {
        this.selectedItems = [];
        this.selectedItems.push(item);
    }

    removes() {

        if (this.selectedItems.length == 0) {
            return;
        }

        this.selectedItems.forEach(item => {
            this.delete(item);
        });
    }


    add() {
        this.registryRoleAssignmentNew.role.id = this.getId();
        //this.fullRoleList = [];
        //this.findAllRole(null);
    }


    registryRoleAssignmentAdd() {
        this.registryRoleAssignmentNew.role.id = this.getId();

        if (this.registryRoleAssignmentNew == null || this.registryRoleAssignmentNew.sicil == null || this.registryRoleAssignmentNew.sicil < 1) {
            this.messageService.add({ severity: 'error', summary: 'Sicil No', detail: "Lütfen geçerli bir sicil numarası giriniz!" });
            return;
        }


        this.roleService.registryRoleAssignmentSaveOrUpdate(this.registryRoleAssignmentNew).subscribe((parameters) => {
            this.getRegistryRoleAssignmentList();
            this.cdRef.detectChanges();
            this.messageService.add({ severity: 'success', summary: 'İşlem başarılı', detail: "Kayıt eklendi!" });
        }, error => {
            console.log('error', error);
            this.messageService.add({ severity: 'error', summary: 'Hata oluştu', detail: error });
        });
    }

}
