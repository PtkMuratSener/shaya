import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../../../environments/environment';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { StorageService } from './../../../../auth/_services';
import { Pageable } from "../../../../model/pageable";
import { LazyLoadEvent } from "primeng/api";

@Injectable()
export class PremiumMonthlyService {
    API_URL = environment.apiURL;
    headers: HttpHeaders = new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*'
    });

    constructor(private http: HttpClient, private storage: StorageService) {
        this.headers = this.headers.append('Authorization', this.storage.getToken());
    }



    //////////////////////////////////////////////////////////
    calcloglist(sicil: number) {
        return this.http.get(this.API_URL + '/svc/calcloglist/' + sicil, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getCalclog(sicil: number, calcid: number) {
        return this.http.get(this.API_URL + '/svc/calclog/' + sicil + '/' + calcid, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    //////////////////////////////////////////////////////////
    getBrands() {
        return this.http.get(this.API_URL + '/svc/brands', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getStores(brandId) {
        return this.http.get(this.API_URL + '/svc/brandStores/' + brandId, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    getPositions() {
        return this.http.get(this.API_URL + '/svc/positionList', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    getStoreActualsInfo(storeId: number, year: number, periyot: number) {
        return this.http.get(this.API_URL + '/svc/storeActualsInfo?storeId=' + storeId + '&year=' + year + '&periyot=' + periyot, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    getStoreEmployees(storeId) {
        return this.http.get(this.API_URL + '/svc/storeEmployees/' + storeId, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    getHesaplamaSonuc(event: LazyLoadEvent, pageable: Pageable, model: any) {
        const sort = this.setPagingParameters(pageable, event);
        const query = { page: pageable.page, size: pageable.size, sort: sort };
        return this.http.post(this.API_URL + '/svc/hesaplamaSonuc', Object.assign(query, model), {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    getExcelFile(model: any) {
        const query = { page: 0, size: 0, sort: '' };
        let q = this.http.post(this.API_URL + '/svc/hesaplamaSonucFile', Object.assign(query, model), {
            headers: this.headers,
            responseType: 'arraybuffer'
        }).subscribe((response: any) => {
            let dataType = response.type;
            let binaryData = [];
            binaryData.push(response);
            let downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
            downloadLink.setAttribute('download', "prim.xlsx");
            document.body.appendChild(downloadLink);
            downloadLink.click();
        });
    }

    getStoreActualInfo(storeId: number, year: number, month: number) {
        return this.http.get(this.API_URL + '/svc/storeActualsInfo' +
            '?storeId=' + storeId + '&year=' + year + '&month=' + month, {
                headers: this.headers
            }).pipe(catchError(this.handleError));
    }

    draftToFinal(year: number, month: number) {
        return this.http.post(this.API_URL + '/svc/draftToFinal/' + year + '/' + month, null, {
            headers: this.headers
        }).pipe(catchError(this.handleError));

    }

    draftToFinalArray(arrayIds) {
        return this.http.post(this.API_URL + '/svc/draftToFinal', JSON.stringify(arrayIds), {
            headers: this.headers
        }).pipe(catchError(this.handleError));

    }
    private setPagingParameters(pageable: Pageable, event: LazyLoadEvent) {
        pageable.page = Math.floor(event == null ? 0 : event.first / event.rows);
        pageable.page = pageable.page > -1 ? pageable.page : 0;

        pageable.size = event == null ? (pageable == null ? 50 : pageable.size) : event.rows;

        const key = null != event ? null == event.sortField ? "id" : event.sortField : "id";
        const value = null == event ? "desc" : 1 == event.sortOrder ? "asc" : "desc";
        const sort = key + ',' + value;
        return sort;
    }
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        if (error.error.code == "UNAUTHORIZED") {
            localStorage.removeItem('currentUser');
            window.location.reload();
        }
        // return an observable with a user-facing error message
        return ErrorObservable.create(error.error);
    };

}