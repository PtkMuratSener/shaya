import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { LazyLoadEvent, Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { PremiumMonthlyService } from './premium-monthly.service';
import { environment } from '../../../../../environments/environment';

import { SelectInput, Months, Years } from '../settings/data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { PremiumMontly } from "../../../../model/premium-monthly/premium.montly.model";
import { Pageable } from "../../../../model/pageable";
import { Role } from "../../../../model/account/role/role.model";
import { DatePipe } from "@angular/common";
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { Rights } from "../../../../model/rights";
import { StoreActual } from "../../../../model/brand/store-actual.model";

@Component({
    selector: 'premium-monthly',
    templateUrl: './premium-monthly.component.html'
})
export class PremiumMonthlyComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public list: PremiumMontly[];
    cols: any[];
    public pageable: Pageable = new Pageable();
    public totalRecords: number = 0;
    public selectedItems: PremiumMontly[] = [];
    public storeActual: StoreActual = null;
    draftOrFinal: SelectInput[] = [
        { label: 'Tümü', value: '0', checked: false },
        { label: 'Taslak', value: '1', checked: false },
        { label: 'Final', value: '2', checked: false },

    ];

    primTypeSelect: SelectInput[] = [
        { label: 'Tümü', value: '0', checked: false },
        { label: 'Victoria Secret', value: "1", checked: false },
        { label: 'Shaya', value: "2", checked: false },

    ];
    
    years: SelectInput[];
    months: SelectInput[] = [
        { label: 'Tümü', value: '0', checked: false },
        { label: 'Ocak', value: '1', checked: false },
        { label: 'Şubat', value: '2', checked: false },
        { label: 'Mart', value: '3', checked: false },
        { label: 'Nisan', value: '4', checked: false },
        { label: 'Mayıs', value: '5', checked: false },
        { label: 'Haziran', value: '6', checked: false },
        { label: 'Temmuz', value: '7', checked: false },
        { label: 'Ağustos', value: '8', checked: false },
        { label: 'Eylül', value: '9', checked: false },
        { label: 'Ekim', value: '10', checked: false },
        { label: 'Kasım', value: '11', checked: false },
        { label: 'Aralık', value: '12', checked: false }
    ];
    brands: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    stores: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    employees: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];

    model: any = {
        brandId: 0,
        storeId: 0,
        positionId: 0,
        employeeId: 0,
        year: 0,
        month: 0,
        final: 0,
    };

    constructor(private premiumMonthlyService: PremiumMonthlyService,
        private messageService: MessageService,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {
        this.years = Years;

    }

    ngOnInit() {
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth();
        this.model.year = year;
        this.model.month = month;
        /*
        this.cols = [
            { field: 'brand', header: 'Marka' },
            { field: 'store', header: 'Mağaza' },
            { field: 'sicil', header: 'Sicil' },
            { field: 'name', header: 'Ad' },
            { field: 'surname', header: 'Soyad' },
            { field: 'position', header: 'Pozisyon' },
            { field: 'target', header: 'Hedef' },
            { field: 'actual', header: 'Gerçekleşen ' },
            { field: 'actualBrut', header: 'Gçklşn Brüt ' },
            { field: 'month', header: 'Periyot' },
            { field: 'bonus', header: 'Prim Tutarı' }
        ];
        */

        this.premiumMonthlyService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Marka listesi alınamadı!' });
        });
        this.premiumMonthlyService.getPositions().subscribe(result => {
            if (result.total) {
                for (const position of result.data) {
                    this.positions.push({
                        label: position.name,
                        value: position.id,
                        checked: false
                    })
                }
            }
        }, error => {
            //this.messageService.add({severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi'});
        });
        this.cdRef.detectChanges();
    }

    setStores(brandId) {
        this.stores = [{
            label: 'Tümü', value: '0', checked: false
        }];

        if (brandId) {
            this.spinner.show();
            this.premiumMonthlyService.getStores(brandId).subscribe(result => {

                if (result.total.all) {
                    for (const store of result.data) {
                        this.stores.push({
                            label: store.name,
                            value: store.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağazalar listesi alınamadı!' });
            });
        }
    }
    getEmployees(storeId) {
        this.employees = [{
            label: 'Tümü', value: '0', checked: false
        }];

        if (storeId) {
            this.spinner.show();
            this.premiumMonthlyService.getStoreEmployees(storeId).subscribe(result => {
                if (result.total.all) {
                    for (const emp of result.data) {
                        this.employees.push({
                            label: emp.name + " " + emp.surname + " (" + emp.sicil + ")",
                            value: emp.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağaza çalışanları listesi alınamadı!' });
            });
        }
    }

    draftToFinal() {
        if (this.model.year < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Yıl seçiniz!' });
            return;
        }
        if (this.model.month < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Periyot seçiniz!' });
            return;
        }
        console.log(this.selectedItems);
        var array = [];
        for (let item of this.selectedItems) {
            if (item.final != true) {
                array.push(item.id);
            }
        }
        if (array.length < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Seçilen kişiler taslak durumunda değildir!' });
            return;
        }
        // console.log(array);
        this.spinner.show();
        this.premiumMonthlyService.draftToFinalArray(array).subscribe(
            data => {

                this.messageService.add({ severity: 'info', summary: 'Bilgi', detail: data.message });
                this.findAll(null);
                this.spinner.hide();
            },
            response => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: response.message });

            }
        );


    }
    findAll(event: LazyLoadEvent) {
        if (this.model.year < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Yıl seçiniz!' });
            return;
        }
        /*   if (this.model.month < 1) {
               this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Periyot seçiniz!' });
               return;
           }
           */
        this.premiumMonthlyService.getStoreActualInfo(this.model.storeId, this.model.year, this.model.month).
            subscribe((response) => {
                console.log('getStoreActualInfo', response);
                debugger;
                this.storeActual = response.data;

            });
        this.spinner.show();
        this.selectedItems = [];
        this.premiumMonthlyService.getHesaplamaSonuc(event, this.pageable, this.model).subscribe(
            data => {
                // debugger;
                this.list = data.content;
                for (let item of this.list) {
                    item.bonusNatural = parseFloat(item.bonus.toFixed(2));
                    item.bonus = parseFloat(item.bonus.toFixed(2));
                }
                this.totalRecords = data.totalElements;
                this.pageable.page = data.number;
                this.spinner.hide();
                this.cdRef.detectChanges();
            },
            response => {
                this.spinner.hide();
                this.list = [];
                this.totalRecords = 0;
                this.spinner.hide();
                this.cdRef.detectChanges();
                if (response.code == "CALCULATION_EMPTY_SET") {
                    // this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: response.message });
                }
                else {
                    this.messageService.add({ severity: 'error', summary: 'Hata', detail: response.message });
                }
            }
        );
    }

    downloadExcel() {
        this.premiumMonthlyService.getExcelFile(this.model);
    }

    exportCSV() {
        let csv = [];
        let self = this;
        var options = {
            fieldSeparator: ';',
            quoteStrings: '"',
            decimalseparator: ',',
            showLabels: true,
            showTitle: false,
            useBom: true,
            noDownload: false,
            headers: ["MARKA", "MAĞAZA", "SİCİL", "AD", "SOYAD",
                "POZİSYON", "HEDEF", "GERÇEKLEŞEN", "PERİYOT", "PRİM TUTARI",

                "PRİM ALMA DURUMU",
                "PRİM ORANI",
                "AYLIK SATIŞ PRİM TUTARI",
                "PRİM GÜN SAYISI",
                "KESİLECEK RAPOR/ÜCRETSİZ İZİN GÜN SAYISI",
                "PRİM ÖDENECEK GÜN SAYISI",
                "DİSİPLİN CEZASI",
                "NOTLAR"
            ]
        };

        this.list.forEach(function(item) {
            csv.push({
                "brand": item.brand,
                "store": item.store,
                "sicil": item.sicil,
                "name": item.name,
                "surname": item.surname,
                "position": item.position,
                "target": item.target,
                "actual": item.actual,
                "month": item.month,
                "bonus": item.bonusNatural,

                "bonusState": item.bonusState,
                "bonusRate": item.bonusRate,
                "monthlyTotal": item.monthlyTotal,
                "bonusTotalDays": item.bonusTotalDays,
                "bonusFreeDays": item.bonusFreeDays,
                "bonusPayDays": item.bonusPayDays,
                "discipline": item.discipline,
                "notes": item.notes,
            });
        });


        let fileName = 'primler aylık';


        new Angular5Csv(csv, fileName, options);
    }
}
