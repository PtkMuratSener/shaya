import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';

import { SelectInput } from '../data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { PremiumRateService } from "./premium-rate.service";
import { PremiumRate } from "../../../../../model/premium-rate/premium-rate.model";
import { Rights } from "../../../../../model/rights";

@Component({
    selector: 'premium-rate',
    templateUrl: './premium-rate.component.html'
})
export class PremiumRateComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public premiumRate = new PremiumRate();
    brands: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];

    constructor(private premiumRateService: PremiumRateService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {


        this.brands.push({
            label: 'Victorias Secret (VS)',
            value: '39',
            checked: false
        });


        this.premiumRateService.getPositions().subscribe(result => {
            if (result.total) {
                for (const position of result.data) {
                    if (position.hasBonus == true) {
                        this.positions.push({
                            label: position.name,
                            value: position.id,
                            checked: false
                        })
                    }
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Ünvan listesi alınamadı!' + error.message });
        });
    }

    getPremiumRateByBrandIdAndPositionId() {
        if (this.premiumRate.brand.id == null || this.premiumRate.brand.id < 1) {
            // this.messageService.add({severity: 'error', summary: 'Hata', detail: 'Marka bilgisi seçiniz!'});
            return;
        }
        if (this.premiumRate.position.id == null || this.premiumRate.position.id < 1) {
            // this.messageService.add({severity: 'error', summary: 'Hata', detail: 'Posizyon bilgisi seçiniz!'});
            return;
        }
        this.spinner.show();
        var brandId = this.premiumRate.brand.id;
        var positionId = this.premiumRate.position.id;
        this.premiumRateService.getPremiumRateByBrandIdAndPositionId(brandId, positionId).subscribe(result => {
            this.spinner.hide();
            this.premiumRate = result.data;
        }, error => {
            this.premiumRate = new PremiumRate();
            this.premiumRate.brand.id = brandId;
            this.premiumRate.position.id = positionId;
            this.spinner.hide();
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Prim Oranı bulunamadı!' });
        });

    }

    submit() {
        if (this.premiumRate.brand.id == null || this.premiumRate.brand.id < 1) {
            this.messageService.add({ severity: 'info', summary: 'Hata', detail: 'Marka bilgisi seçiniz!' });
            return;
        }
        if (this.premiumRate.position.id == null || this.premiumRate.position.id < 1) {
            this.messageService.add({ severity: 'info', summary: 'Hata', detail: 'Posizyon bilgisi seçiniz!' });
            return;
        }
        this.spinner.show();
        this.premiumRateService.save(this.premiumRate).subscribe(result => {
            this.spinner.hide();
            if (result.success) {
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: result.message });
            } else {
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı' + error.message });
        });

    }
}
