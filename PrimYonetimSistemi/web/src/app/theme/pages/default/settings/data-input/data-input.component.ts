import { Component, OnInit, OnChanges } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { DataInputService } from './data-input.service';
import { environment } from '../../../../../../environments/environment';
import { SelectInput, Months, Years } from './calendar';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataInputHistory } from "../../../../../model/setting/datainput/data-input-history.model";
import { saveAs } from 'file-saver/FileSaver';
import { Rights } from "../../../../../model/rights";
import { Principal } from "../../../../../auth/_services/principal.service";


@Component({
    selector: 'm-data',
    templateUrl: './data-input.component.html'
})
export class DataInputComponent implements OnInit, OnChanges {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    years: SelectInput[];
    filesXLSaleTargets: DataInputHistory[];
    filesXLAlShayaCalendar: DataInputHistory[];
    form: FormGroup;
    public API_URL = environment.apiURL;
    constructor(private fb: FormBuilder,
        private dataInputService: DataInputService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService,
        private principal: Principal) {
        this.form = this.fb.group({
            'target-year': this.fb.control(''),
            'target-month': this.fb.control(''),
            'calendar-year': this.fb.control('')
        });
    }

    ngOnInit() {
        this.years = Years;
        this.principal.identity(false).then(data => { });
        this.getFilesXLSaleTargets();
        this.getFilesXLAlShayaCalendar();
    }
    getFilesXLSaleTargets() {
        this.spinner.show();
        this.dataInputService.getFilesXLSaleTargets().subscribe(result => {
            this.spinner.hide();
            this.filesXLSaleTargets = result;
            console.log(this.filesXLSaleTargets);
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Cognos Satış Hedefi!', detail: error.message });
        });
    }
    getFilesXLAlShayaCalendar() {
        this.spinner.show();
        this.dataInputService.getFilesXLAlShayaCalendar().subscribe(result => {
            this.spinner.hide();
            this.filesXLAlShayaCalendar = result;

        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'AI Shaya Takvimi', detail: error.message });
        });
    }
    downloadFile(item) {
        console.log(item);
        this.spinner.show();
        this.dataInputService.downloadFile(item.id).subscribe((result: Response) => {
            this.spinner.hide();
            console.log(result.headers);
            this.saveToFileSystem(result);
            //console.log(result);
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Dosya İndirme', detail: error.message });
        });
    }
    saveToFileSystem(response) {
        const contentDispositionHeader: string = response.headers.get('Content-Disposition');
        const parts: string[] = contentDispositionHeader.split(';');
        const filename = parts[1].split('=')[1];
        const blob = new Blob([response._body], { type: 'text/plain' });
        saveAs(blob, filename);
    }
    ngOnChanges() { }

    uploadExcel(event, form, target) {
        const file = event.files[0];

        const year = this.form.controls[target + '-year'].value;

        if (!file) {
            this.messageService.add({ severity: 'warning', summary: 'Eksik Bilgi', detail: 'Dosya seçmediniz!' });
        } else if (!year) {
            this.messageService.add({ severity: 'warning', summary: 'Eksik Bilgi', detail: 'Yıl seçimi yapmadınız!' });
        } else {
            this.spinner.show();
            // debugger;
            this.dataInputService.upload(target, file, year).subscribe(result => {
                form.clear();
                this.spinner.hide();
                if (target === 'calendar') {
                    this.getFilesXLAlShayaCalendar();
                } else {
                    this.getFilesXLSaleTargets();
                }
                this.messageService.add({ severity: 'success', summary: 'Dosya yüklendi', detail: result.message });
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Dosya yüklenemedi!', detail: error.message });
            });
        }
    }
}
