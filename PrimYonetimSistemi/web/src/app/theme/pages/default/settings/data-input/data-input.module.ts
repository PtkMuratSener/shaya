import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule, DropdownModule, GrowlModule, FileUploadModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DataInputComponent } from './data-input.component';
import { HttpClientModule } from '@angular/common/http';
import { DataInputService } from './data-input.service';
import { DataViewShayaComponent } from "./data-view-shaya/data-view-shaya.component";
import { DataViewComponent } from "./data-view/data-view.component";

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            { 'path': '', 'component': DataInputComponent },
            { 'path': ':year', 'component': DataViewComponent },
            { 'path': 'shaya/:year', 'component': DataViewShayaComponent }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        DropdownModule,
        FileUploadModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild(),
        TableModule,
        DataTableModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        DataInputComponent,
        DataViewComponent,
        DataViewShayaComponent
    ],
    providers: [DataInputService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class DataInputModule {


}