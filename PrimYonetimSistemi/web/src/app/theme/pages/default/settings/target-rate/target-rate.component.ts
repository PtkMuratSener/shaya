import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { TargetRateService } from './target-rate.service';
import { environment } from '../../../../../../environments/environment';

import { SelectInput } from '../data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { catchError } from "rxjs/operators";
import { BrandModel } from "../../../../../model/brand/brand.model";
import { Rights } from "../../../../../model/rights";
import { TargetRate } from "../../../../../model/setting/targetrate/target-rate.model";

@Component({
    selector: 'target-rate',
    templateUrl: './target-rate.component.html'
})
export class TargetRateComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public targetRates: TargetRate[] = [];
    brands: SelectInput[] = [];
    public brandId: number;

    constructor(private targetRateService: TargetRateService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.brands = [{
            label: 'Seçiniz', value: '0', checked: false
        }];
        this.targetRateService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Marka listesi alınamadı! Mesaj: ' + error.message });
        });
    }
    getBrandSaleSplitByBrandId(brandId: number) {
        if (brandId < 1) {
            return;
        }
        this.targetRateService.getBrandSaleSplitByBrandId(brandId).subscribe(result => {
            this.targetRates = result;

        }, error => {
            this.targetRates = [];
            this.messageService.add({ severity: 'info', summary: 'Hedef Oranları', detail: 'Hedef Oranları bulunamadı! Mesaj:' + error.message });
        });
    }
    submit() {
        this.spinner.show();
        for (const item of this.targetRates) {
            this.targetRateService.save(item).subscribe(result => {
                this.spinner.hide();
                if (result.success) {
                    this.messageService.add({ severity: 'success', summary: item.position.name, detail: result.message });
                } else {
                    this.messageService.add({ severity: 'error', summary: 'Hata:' + item.position.name, detail: result.message });
                }
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: error.message });
            });
        }

    }
}
