import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { BonusCalculateMonthlyService } from './bonus-calculate-monthly.service';
import { environment } from '../../../../../../environments/environment';
declare var jQuery: any;

import { SelectInput, Months, Years } from '../data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { catchError } from "rxjs/operators";
import { BonusCalculateMonthly } from "../../../../../model/setting/bonuscalculatemonthly/bonus-calculate-monthly.model";
import { Rights } from "../../../../../model/rights";

@Component({
    selector: 'm-bonus',
    templateUrl: './bonus-calculate-monthly.component.html'
})
export class BonusCalculateMonthlyComponent implements OnInit, OnDestroy {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public progressValue: number = 0;
    intervalId: number;
    process: number;
    total: number;
    bonusCalculateMonthly: BonusCalculateMonthly;
    years: SelectInput[];
    months: SelectInput[];
    brands: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    stores: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    employees: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];

    model: any = {
        brandId: 0,
        storeId: 0,
        positionId: 0,
        employeeId: 0,
        year: 0,
        month: 0
    };

    constructor(private bonusCalculateService: BonusCalculateMonthlyService,
        private messageService: MessageService,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {
        this.years = Years;
        this.months = Months
    }

    ngOnInit() {
        this.bonusCalculateService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Marka listesi alınamadı!' });
        });
    }
    ngOnDestroy() {
        clearInterval(this.intervalId);
    }
    setStores(brandId) {
        this.stores = [{
            label: 'Tümü', value: '0', checked: false
        }];
        this.positions = [{
            label: 'Tümü', value: '0', checked: false
        }];
        if (brandId) {
            this.spinner.show();
            this.bonusCalculateService.getStores(brandId).subscribe(result => {

                if (result.total.all) {
                    for (const store of result.data) {
                        this.stores.push({
                            label: store.name,
                            value: store.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağazalar listesi alınamadı!' });
            });
        }
    }
    getStores(storeId: number) {
        this.positions = [{
            label: 'Tümü', value: '0', checked: false
        }];
        this.bonusCalculateService.getPositionsByStoreId(storeId).subscribe(result => {
            if (result.total) {
                for (const position of result.data) {
                    this.positions.push({
                        label: position.name,
                        value: position.id,
                        checked: false
                    })
                }
            }
        }, error => {
            //this.messageService.add({severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi'});
        });
    }
    getEmployees(storeId) {
        this.employees = [{
            label: 'Tümü', value: '0', checked: false
        }];

        if (storeId) {
            this.spinner.show();
            this.bonusCalculateService.getStoreEmployees(storeId).subscribe(result => {
                if (result.total.all) {
                    for (const emp of result.data) {
                        this.employees.push({
                            label: emp.name + " " + emp.surname + " (" + emp.sicil + ")",
                            value: emp.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağaza çalışanları listesi alınamadı!' });
            });
        }
    }

    submit() {
        if (this.model.year == null || this.model.year < 1) {
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Yıl bilgisi seçiniz!' });
            return;
        }
        if (this.model.month == null || this.model.month < 1) {
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Periyot bilgisi seçiniz!' });
            return;
        }
        this.spinner.show();
        this.bonusCalculateService.save(this.model).subscribe(result => {
            this.spinner.hide();
            //document.getElementById("process_bar_modal").click();
            if (result.success) {
                this.bonusCalculateMonthly = result.data;
                jQuery("#process_bar_modal").modal("show");
                this.progressValue = 0;
                this.process = 0;
                this.total = 0;
                clearInterval(this.intervalId);
                this.intervalId = setInterval(() => {
                    this.bonusCalculateService.getHesaplamaDurum(this.bonusCalculateMonthly.id).subscribe(response => {
                        if (response.data.total < 1) {
                            console.log("dönen değer:", response);
                            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Toplam hesaplanacak personel sayısı 0 olduğundan işlem durduruldu!' });
                            clearInterval(this.intervalId);
                            this.intervalId = null;
                            this.spinner.hide();
                            return;
                        }
                        this.process = response.data.processed;
                        this.total = response.data.total;
                        this.progressValue = (response.data.processed / response.data.total) * 100;

                        this.progressValue = this.progressValue == null ? 0 : this.progressValue;
                        if (response.data.processed == response.data.total) {
                            clearInterval(this.intervalId);
                            this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'Hesaplama tamamlandı!' });

                        }
                    }, error => {
                        this.spinner.hide();
                        this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Hesaplama durumu servisten alınamadı.' });
                        clearInterval(this.intervalId);
                    });

                }, 1000);
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'İşleniyor' });
            } else {
                if (result.code == "CALCULATION_CONTINUIES") {//CALCULATION_INFO
                    if (result != null && result.data != null) {
                        if (this.bonusCalculateMonthly == null) {
                            this.bonusCalculateMonthly = new BonusCalculateMonthly();
                        }
                        this.bonusCalculateMonthly.id = result.data.id;

                    }
                    if (this.intervalId == null || this.intervalId < 1) {
                        console.log(this.intervalId);
                        this.intervalId = setInterval(() => {
                            this.bonusCalculateService.getHesaplamaDurum(this.bonusCalculateMonthly.id).subscribe(response => {
                                if (response.data.total < 1) {
                                    console.log("dönen değer:", response);
                                    this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Toplam hesaplanacak personel sayısı 0 olduğundan işlem durduruldu!' });
                                    clearInterval(this.intervalId);
                                    this.intervalId = null;
                                    this.spinner.hide();
                                    return;
                                }
                                this.progressValue = (response.data.processed / response.data.total) * 100;

                                this.progressValue = this.progressValue == null ? 0 : this.progressValue;
                                this.process = response.data.processed;
                                this.total = response.data.total;

                                if (response.data.processed == response.data.total) {
                                    clearInterval(this.intervalId);
                                    this.intervalId = null;
                                    this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: 'Hesaplama tamamlandı!' });
                                }
                            }, error => {
                                this.spinner.hide();
                                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Hesaplama durumu servisten alınamadı.' });
                                clearInterval(this.intervalId);
                            });

                        }, 1000);
                    }
                    this.cdRef.detectChanges();
                    jQuery("#process_bar_modal").modal("show");
                }
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı! \n' + error.message });
        });
    }
}
