import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { FormsModule } from '@angular/forms';
import { DropdownModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BonusCalculateMonthlyComponent } from './bonus-calculate-monthly.component';
import { HttpClientModule } from '@angular/common/http';
import { BonusCalculateMonthlyService } from './bonus-calculate-monthly.service';
import { ProgressBarModule } from "angular-progress-bar"

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': BonusCalculateMonthlyComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DropdownModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild(),
        ProgressBarModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        BonusCalculateMonthlyComponent
    ],
    providers: [BonusCalculateMonthlyService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class BonusCalculateMonthlyModule {


}