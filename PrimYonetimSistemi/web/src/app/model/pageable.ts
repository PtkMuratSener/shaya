import { LazyLoadEvent } from 'primeng/primeng';

export class Pageable {
    public page: number = 1;
    public size: number = 50;
    public sort: string = 'id,desc';

}