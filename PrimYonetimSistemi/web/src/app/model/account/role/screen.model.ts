import { ApiModel } from "../../api.model";

export class Screen extends ApiModel {
    public name: string;
    public createdOn: any;
    public checked: boolean;
    public view: boolean;
    public edit: boolean;
    constructor() {
        super();
    }
}