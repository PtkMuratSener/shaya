import { ApiModel } from "../../../api.model";
import { Role } from "../role.model";
import { Position } from "../../../position/position.model";


export class TitleRoleAssignment extends ApiModel {
    public role: Role;
    public position: Position;
    constructor() {
        super();
        this.role = new Role();
        this.position = new Position();
    }
}