
import { ApiModel } from "../api.model";
import { Store } from "../brand/store.model";
import { Position } from "../position/position.model";


export class MonthlyGoal extends ApiModel {
    public store: Store;
    public position: Position;
    public year: number;
    public month: number;
    public numStaff: number;
    public saleSplit: number;
    public pmst: number;
    public sta100: number;
    public sta105: number;
    public sta110: number;
    public final: boolean;
    public formulaType: string;
    public brandId: number;
    public revision: number;
    constructor() {
        super();
        this.store = new Store();
        this.position = new Position();
        this.store.id = 0;
        this.year = 0;
        this.month = 0;
    }
}
