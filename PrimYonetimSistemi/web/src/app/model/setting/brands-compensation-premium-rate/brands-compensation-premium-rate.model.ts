import { ApiModel } from "../../api.model";

export class BrandsCompensationPremiumRate extends ApiModel {
    public positionName: number;
    public slab1: number;
    public slab2: number;
    public slab3: number;
}