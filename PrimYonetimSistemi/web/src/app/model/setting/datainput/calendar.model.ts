import { ApiModel } from "../../api.model";
export class Calendar extends ApiModel {
    public year: number;
    public date: any;
    public month: number;
    public quarter: number;
    public season: number;
    public week: number;
}
