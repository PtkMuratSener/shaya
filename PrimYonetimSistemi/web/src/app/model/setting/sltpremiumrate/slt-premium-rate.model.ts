import { ApiModel } from "../../api.model";
import { BrandModel } from "../../brand/brand.model";
import { Position } from "../../position/position.model";

export class SltPremiumRate extends ApiModel {
    //public brand: BrandModel = new BrandModel();
    //public position: Position = new Position();
    public brandId: number;
    public positionId: number;
    public positionName: number;
    public rate: number;
}
