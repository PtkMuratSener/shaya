import { ApiModel } from "../api.model";

export class KeyValue extends ApiModel {
    public key: string;
    public value: string;
    public result: number;
    constructor() {
        super();
        this.result = 0;
    }
}