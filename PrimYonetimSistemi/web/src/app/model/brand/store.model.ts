import { ApiModel } from "../api.model";
import { BrandModel } from "./brand.model";


export class Store extends ApiModel {
    public name: string;
    public brand: BrandModel;
    public aktif: boolean;
    constructor() {
        super();
        this.brand = new BrandModel();
    }
}