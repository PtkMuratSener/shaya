import {
    Component,
    ComponentFactoryResolver,
    OnInit,
    ViewChild,
    ViewContainerRef,
    ViewEncapsulation
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { AlertService } from './_services';
import { AlertComponent } from './_directives';
import { LoginCustom } from './_helpers/login-custom';
import { Helpers } from '../helpers';

import { AuthService } from './_services';
import { CredentialsModel } from './_models';
import { Principal } from "./_services/principal.service";

@Component({
    selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
    templateUrl: './login/login.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AuthComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    private credentials: CredentialsModel = new CredentialsModel;

    @ViewChild('alertSignin', { read: ViewContainerRef }) alertSignin: ViewContainerRef;
    @ViewChild('alertOtp', { read: ViewContainerRef }) alertOtp: ViewContainerRef;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private principal: Principal,
        private _script: ScriptLoaderService,
        private _authService: AuthService,
        private _alertService: AlertService,
        private cfr: ComponentFactoryResolver) {
    }

    ngOnInit() {
        //this._authService.logOut();

        this.model.remember = true;

        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
        this._router.navigate([this.returnUrl]);

        this._script.loadScripts('body', [
            'assets/vendors/base/vendors.bundle.js',
            'assets/demo/default/base/scripts.bundle.js'], true).then(() => {
                Helpers.setLoading(false);
                LoginCustom.init();
            });
    }

    login() {
        this.loading = true;
        this.credentials.username = this.model.username;
        this.credentials.password = this.model.password;
        this._authService.login(this.credentials).subscribe(
            data => {
                //console.log(data);
                this.principal.identity(false).then(data => {
                    this.loading = false;
                    this._router.navigate([this.returnUrl]);
                    // CHECK USER TYPE
                });
            },
            error => {
                this.showAlert('alertSignin');
                this._alertService.error('Kullanıcı adı ve parolası uyuşmadı!');
                this.loading = false;
            });

    }

    showAlert(target) {
        this[target].clear();
        let factory = this.cfr.resolveComponentFactory(AlertComponent);
        let ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    }
}