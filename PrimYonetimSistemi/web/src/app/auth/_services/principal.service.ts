import { Injectable } from '@angular/core';
//import { Account } from './account.service';
import { StorageService } from './storage.service';

@Injectable()
export class Principal {
    private _identity: any;
    private _authenticated: Boolean = false;

    constructor(//private account: Account,
        private storageService: StorageService) {

    }
    isDefined(value) {
        return typeof value !== 'undefined';
    }
    isIdentityResolved() {
        return this.isDefined(this._identity);
    }
    isAuthenticated() {
        return this._authenticated;
    }
    isInRight(role) {
        if (!this._authenticated || !this._identity || !this._identity.role) {
            return false;
        }
        return this._identity.role.indexOf(role) !== -1;
    }
    isInPermission(authorizationTypeIdG, authorizationTypeIdD, screenId, storeTypeId, workingAreaId) {
        if (!this._authenticated || !this._identity || !this._identity.role) {
            return false;
        }
        for (let i = 0; i < this._identity.role.length; i++) {
            if ((this._identity.role[i].authorizationType.id == authorizationTypeIdG ||
                this._identity.role[i].authorizationType.id == authorizationTypeIdD) &&
                this._identity.role[i].screen.id == screenId
            ) {
                return true;
            }
        }
        return false;
    }
    isInAnyRole(roles) {
        //console.log("--",roles);
        // debugger;
        if (!this._authenticated || !this._identity.role) {
            return false;
        }

        //for (var i = 0; i < roles.length; i++) {
        //if (this.isInRight(roles[i])) {
        if (this.isInPermission(roles[0], roles[1], roles[2], 0, 0)) {
            return true;
        }
        //}

        return false;
    }
    authenticate(identity) {
        this._identity = identity;
        this._authenticated = identity !== null;
    }
    identity(force): any {

        let promise = new Promise((resolve, reject) => {

            if (force === true) {
                this._identity = undefined;
            }

            if (this.isDefined(this._identity)) {
                return resolve(this._identity);
            }

            var account = this.storageService.getAccountFull();
            if (account != null) {
                this._identity = account;
                // this.storageService.setAccount(account);
                this._authenticated = true;
                return resolve(this._identity);
            }
            else {
                this._identity = undefined;
                this._authenticated = false;
                this.storageService.setAccount(null);
                return reject("Giriş bilgileri alınamadı!");
            }

            /* this.account.get().subscribe(
                 account => {
                     this._identity = account;
                     this.storageService.setAccount(account);
                     this._authenticated = true;
                     return resolve(this._identity);
                 },
                 error => {
                     this._identity = undefined;
                     this._authenticated = false;
                     this.storageService.setAccount(null);
                     return reject(error);
                 }
             );*/
        });

        return promise;
    }


}
