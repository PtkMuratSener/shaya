import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

    setAuth(data) {
        localStorage.setItem('currentUser', JSON.stringify(data));
    }

    removeAuth() {
        localStorage.removeItem('currentUser');
    }

    getToken() {
        const data = localStorage.getItem('currentUser');

        if (data) {
            const item = JSON.parse(data);
            return item.token;
        }

        return null;
    }

    getAccount(): any {
        const data = localStorage.getItem('currentUser');

        if (data) {
            const item = JSON.parse(data);

            return { 'sicil': item.sicil, 'fullName': item.nameSurname };
        }

        return null;
    }
    getAccountFull(): any {
        const data = localStorage.getItem('currentUser');

        if (data) {
            const item = JSON.parse(data);
            return item
        }

        return null;
    }
    setAccount(account) {
        localStorage.setItem("currentUser", JSON.stringify(account));
    }

    removeAccount() {
        localStorage.removeItem("currentUser");
    }

}
