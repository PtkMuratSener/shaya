const path = require('path');
const express = require('express');
const app = express();

const clientPath = path.join(__dirname, 'dist');

function forceSSL () {
  return function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
      return res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    next();
  }
}

// Run the app by serving the static files
// in the dist directory
app.use(express.static(__dirname + '/dist'));

// force SSL
/*app.use(forceSSL());*/

// For all GET requests, send back index.html
// so that PathLocationStrategy can be used
app.get('*', function (req, res) {
  res.sendFile(path.join(clientPath, 'index.html'));
});

// Start the app by listening on the default
app.listen(process.env.PORT || 8080);