using Microsoft.EntityFrameworkCore;
using SalesReload.Database;

namespace Importer
{
    public class LocalDbContext : DbContext
    {
        public LocalDbContext(DbContextOptions<LocalDbContext> options) : base ( options )
        {
        }

        /// <summary>
        /// Mağaza bazlı personel satış
        /// </summary>
        public DbSet<Sale> Sales { get; set; }
    }
}