﻿IF EXISTS(SELECT 1 FROM information_schema.tables 
  WHERE table_name = '__EFMigrationsHistory' AND table_schema = DATABASE()) 
BEGIN
CREATE TABLE `__EFMigrationsHistory` (
    `MigrationId` varchar(150) NOT NULL,
    `ProductVersion` varchar(32) NOT NULL,
    PRIMARY KEY (`MigrationId`)
);

END;

CREATE TABLE `Brands` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Name` text NULL,
    `ShortName` text NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `calendar` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Date` text NULL,
    `Month` int NULL,
    `Quarter` int NULL,
    `Season` int NULL,
    `Week` int NULL,
    `Year` int NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `Cezalar` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Code` varchar(2) NULL,
    `Effective` int NOT NULL,
    `Title` varchar(128) NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `formula` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `CreateDate` datetime NOT NULL,
    `DataStatus` int NOT NULL,
    `FormulaType` int NULL,
    `Formulas` text NOT NULL,
    `Name` varchar(60) NOT NULL,
    `UpdateDate` datetime NOT NULL,
    `createBy` text NOT NULL,
    `updateBy` text NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `Parametres` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Title` varchar(255) NULL,
    `Value` varchar(255) NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE `ResponsiBeginsEnds` (
    `Begins` varchar(767) NOT NULL,
    `Ends` text NULL,
    PRIMARY KEY (`Begins`)
);

CREATE TABLE `SasMonthses` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `BrandId` int NULL,
    `M01` bit NOT NULL,
    `M02` bit NOT NULL,
    `M03` bit NOT NULL,
    `M04` bit NOT NULL,
    `M05` bit NOT NULL,
    `M06` bit NOT NULL,
    `M07` bit NOT NULL,
    `M08` bit NOT NULL,
    `M09` bit NOT NULL,
    `M10` bit NOT NULL,
    `M11` bit NOT NULL,
    `M12` bit NOT NULL,
    `Year` int NOT NULL,
    PRIMARY KEY (`Id`),
    CONSTRAINT `FK_SasMonthses_Brands_BrandId` FOREIGN KEY (`BrandId`) REFERENCES `Brands` (`Id`) ON DELETE RESTRICT
);

CREATE TABLE `Stores` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Aktif` int NULL,
    `BrandId` int NULL,
    `Name` text NULL,
    PRIMARY KEY (`Id`),
    CONSTRAINT `FK_Stores_Brands_BrandId` FOREIGN KEY (`BrandId`) REFERENCES `Brands` (`Id`) ON DELETE RESTRICT
);

CREATE TABLE `Employees` (
    `Sicil` int NOT NULL AUTO_INCREMENT,
    `Active` bit NOT NULL,
    `Ceza` int NOT NULL,
    `Gorev` int NOT NULL,
    `GorevText` text NULL,
    `Kidem` double NOT NULL,
    `LdapUsername` text NULL,
    `Name` text NULL,
    `Position` text NULL,
    `StartDate` text NULL,
    `StoreId` int NULL,
    `Surname` text NULL,
    PRIMARY KEY (`Sicil`),
    CONSTRAINT `FK_Employees_Stores_StoreId` FOREIGN KEY (`StoreId`) REFERENCES `Stores` (`Id`) ON DELETE RESTRICT
);

CREATE TABLE `StoreTargets` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Ap01` int NULL,
    `Ap02` int NULL,
    `Ap03` int NULL,
    `Ap04` int NULL,
    `Ap05` int NULL,
    `Ap06` int NULL,
    `Ap07` int NULL,
    `Ap08` int NULL,
    `Ap09` int NULL,
    `Ap10` int NULL,
    `Ap11` int NULL,
    `Ap12` int NULL,
    `Aptotal` int NULL,
    `Bp01` int NULL,
    `Bp02` int NULL,
    `Bp03` int NULL,
    `Bp04` int NULL,
    `Bp05` int NULL,
    `Bp06` int NULL,
    `Bp07` int NULL,
    `Bp08` int NULL,
    `Bp09` int NULL,
    `Bp10` int NULL,
    `Bp11` int NULL,
    `Bp12` int NULL,
    `Bptotal` int NULL,
    `Code` int NOT NULL,
    `Name` text NULL,
    `StoreId` int NULL,
    `Year` int NOT NULL,
    PRIMARY KEY (`Id`),
    CONSTRAINT `FK_StoreTargets_Stores_StoreId` FOREIGN KEY (`StoreId`) REFERENCES `Stores` (`Id`) ON DELETE RESTRICT
);

CREATE INDEX `IX_Employees_StoreId` ON Employees (`StoreId`);

CREATE INDEX `IX_SasMonthses_BrandId` ON SasMonthses (`BrandId`);

CREATE INDEX `IX_Stores_BrandId` ON Stores (`BrandId`);

CREATE INDEX `IX_StoreTargets_StoreId` ON StoreTargets (`StoreId`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20180827101120_Init', '2.0.1-rtm-125');

CREATE TABLE `StorePositions` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Code` varchar(2) NULL,
    `Pmst` int NOT NULL,
    `PositionType` int NOT NULL,
    `StoreId` int NULL,
    `Title` varchar(128) NULL,
    PRIMARY KEY (`Id`),
    CONSTRAINT `FK_StorePositions_Stores_StoreId` FOREIGN KEY (`StoreId`) REFERENCES `Stores` (`Id`) ON DELETE RESTRICT
);

CREATE INDEX `IX_StorePositions_StoreId` ON StorePositions (`StoreId`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20180827153835_StorePositions', '2.0.1-rtm-125');

ALTER TABLE StorePositions MODIFY `PositionType` varchar(10) NOT NULL;

CREATE TABLE `Sales` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `Amount` double NOT NULL,
    `EmployeePosition` text NULL,
    `EmployeeSicil` int NULL,
    `Quantity` int NOT NULL,
    `SaleDate` datetime NOT NULL,
    `StoreId` int NULL,
    PRIMARY KEY (`Id`),
    CONSTRAINT `FK_Sales_Employees_EmployeeSicil` FOREIGN KEY (`EmployeeSicil`) REFERENCES `Employees` (`Sicil`) ON DELETE RESTRICT,
    CONSTRAINT `FK_Sales_Stores_StoreId` FOREIGN KEY (`StoreId`) REFERENCES `Stores` (`Id`) ON DELETE RESTRICT
);

CREATE INDEX `IX_Sales_EmployeeSicil` ON Sales (`EmployeeSicil`);

CREATE INDEX `IX_Sales_StoreId` ON Sales (`StoreId`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20180828073243_Sales', '2.0.1-rtm-125');

CREATE TABLE `Users` (
    `Id` int NOT NULL AUTO_INCREMENT,
    `LdpaUsername` text NULL,
    `NameSurname` text NULL,
    `SessionKey` text NULL,
    `SessionValidTil` bigint NULL,
    `Sicil` int NOT NULL,
    PRIMARY KEY (`Id`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20180829201726_User', '2.0.1-rtm-125');

