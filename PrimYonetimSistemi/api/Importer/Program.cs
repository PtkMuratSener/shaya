﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Pusula.Api.Models;

namespace Importer
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("SHAYA IMPORTER");
            var part = "ALL";
            if (args.Length > 0)
            {
                if (args[0].StartsWith("-h"))
                {
                    Console.WriteLine(" - HELP");
                    Console.WriteLine("   hr : Imports only HR data");
                    Console.WriteLine("   brs : Updates only BonusRatesSlt data");
                    Console.WriteLine("   reatil : Imports only Retail data");
                    Console.WriteLine("   default action is import all");
                }

                part = args[0];
            }
            var optionsBuilder = new DbContextOptionsBuilder<LocalDbCtx>();
                //optionsBuilder.UseMySQL("server=localhost;user id=root;password=1234;persistsecurityinfo=True;port=3306;database=patika;SslMode=none;CharSet=utf8;");
                optionsBuilder.UseSqlServer("Data Source=10.81.91.130;Initial Catalog=patika_test;User ID=patika;Password=Tq16!xM13;Persist Security Info=True;");
                
            var context = new LocalDbCtx(optionsBuilder.Options);

            var runImporter = false;
            if (part == "ALL")
            {
                /*
                 * Ayın 1, 5 günleri ile Shaya periyot bitimine 5 gün kala otomatik import.
                 * Bu import CSP den önce olmalı
                 */
                // Ayın günleri
                runImporter = DateTime.Now.Day == 1;
                if (!runImporter)
                {
                    runImporter = DateTime.Now.Day == 5;
                }

                //ve dahi shaya periyodu bitimine 5 gün kala
                if (!runImporter)
                {
                    var todayS = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
                    var shDatesS = context.Calendars.FirstOrDefault(c => c.Date == todayS);
                    if (shDatesS != null)
                    {
                        var shDatesSa = context.Calendars
                            .Where(c => c.Month == shDatesS.Month)
                            .Where(y => y.Year == shDatesS.Year)
                            .Count(d => d.Date > todayS);
                        if (shDatesSa == 4) // Bugün+4 = son 5.
                        {
                            runImporter = true;
                        }
                    }
                }
            }

            if (runImporter || part == "hr")
            {
                var hrImporter =
                    new HrImport(
                        "User Id=IKYS_IT;Password=IKYS_IT;Data Source=10.81.91.28:1521/ORCL;");
                hrImporter.Import(context);
            }

            if (part == "ALL" || part == "brs")
            {
                var brsImporter =
                    new HrImport(
                        "User Id=IKYS_IT;Password=IKYS_IT;Data Source=10.81.91.28:1521/ORCL;");
                brsImporter.UpdateBonusRatesSlt(context);
            }

            if (part == "ALL" || part == "retail")
            {
                var wrImporter =
                    new WinretailImport(
                        "Data Source=10.81.91.93;Initial Catalog=RETAIL;User ID=ret;Password=IPER;Persist Security Info=True;");
                wrImporter.Import(context);
            }

        }
    }
    
}