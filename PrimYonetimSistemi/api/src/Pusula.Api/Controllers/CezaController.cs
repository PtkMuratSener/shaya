﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Models;
using Pusula.Api.Data.DataContexts;

namespace Pusula.Api.Controllers
{
    /// <summary>
    /// Ceza işlemleri
    /// </summary>
    [Route("/svc")]
    public class CezaController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <summary>
        /// SAS Ayları İşlemleri
        /// </summary>
        /// <param name="context"></param>
        public CezaController(PatikaDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ceza")]
        [ValidateModelState]
        [SwaggerOperation("CezaAdd")]
        [SwaggerResponse(statusCode: 200, type: typeof(string), description: "Success")]
        [SwaggerResponse(statusCode: 400, type: typeof(string), description: "Failed")]
        public IActionResult CezaAdd([FromBody]Ceza request)
        {
            _context.Cezalar.Add(request);
            _context.SaveChanges();
            return StatusCode(200);
        }
        
        /// <summary>
        /// Ceza listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ceza")]
        [ValidateModelState]
        [SwaggerOperation("CezaGetAll")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(List<Ceza>), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(string), description: "Not found")]
        public IActionResult CezaGetAll()
        {
            var ceza = _context.Cezalar.AsNoTracking();
            if (ceza.Any())
            {
                return StatusCode(200, ceza);
            }    
            
            return StatusCode((int)HttpStatusCode.NotFound, "Not found");
        }
        
        /// <summary>
        /// Ceza detay bilgileri
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("ceza/{id}")]
        [ValidateModelState]
        [SwaggerOperation("CezaGetById")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(Ceza), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(string), description: "Not found")]
        public IActionResult CezaGetById([FromRoute] int id)
        {
            var ceza = _context.Cezalar.Find(id);
            if (ceza != null)
            {
                return StatusCode(200, ceza);
            }    
            return StatusCode((int)HttpStatusCode.NotFound, "Not found");
            
        }
        
        /// <summary>
        /// Ceza sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("ceza/{id}")]
        [ValidateModelState]
        [SwaggerOperation("CezaDeleteById")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(string), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(string), description: "Not found")]
        public IActionResult CezaDeleteById([FromRoute] int id)
        {
            var ceza = _context.Cezalar.Find(id);
            if (ceza == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound, "Not found");
            }

            _context.Cezalar.Remove(ceza);
            _context.SaveChanges();
            return StatusCode(200, "Success");

        }
    }
}