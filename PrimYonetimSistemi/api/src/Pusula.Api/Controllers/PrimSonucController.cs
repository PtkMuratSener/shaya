using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Requests;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Hesaplama sonuçları
    /// </summary>
    [Route("/svc/reports")]
    public class PrimSonucController : Controller
    {

        /// <summary>
        /// Database bağı
        /// </summary>
        private readonly PatikaDbContext _localDbContext;


        /// <inheritdoc />
        public PrimSonucController(PatikaDbContext localDbContext)
        {
            _localDbContext = localDbContext;
        }

        /// <summary>
        /// Aylık Prim Raporu
        /// </summary>
        /// <param name="request">ReportRequest</param>
        /// <returns>PrimResponseMonthly</returns>
        [HttpPost]
        [Route("monthly")]
        [ValidateModelState]
        [SwaggerOperation("MothlyReport")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(PrimResponseMonthly), description: "Success")]
        public IActionResult MothlyReport([FromBody] ReportRequest request)
        {
            try
            {
                var calculatedBonuses = _localDbContext.VEmployeeBonuses.AsNoTracking();
                if (request.BrandId != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.BrandId == request.BrandId);
                var userArea = HttpContext.Items["UserArea"] as string;
                if (userArea == "S")
                {
                    var store = (int) HttpContext.Items["UserStore"];
                    calculatedBonuses = calculatedBonuses.Where(c => c.StoreId == store);
                }
                else
                {
                    if (request.StoreId != 0)
                        calculatedBonuses = calculatedBonuses.Where(c => c.StoreId == request.StoreId);
                }

                if (request.Month != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.Month == request.Month);
                calculatedBonuses = calculatedBonuses.Where(c => c.Year == request.Year);

                var sort = request.Sort.Length <= 0 ? "id,asc" : request.Sort;
                var sortParam = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[0];
                var sortDir = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[1];
                switch (sortParam)
                {
                    case "brand":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Brand)
                            : calculatedBonuses.OrderByDescending(o => o.Brand);
                        break;
                    case "store":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Store)
                            : calculatedBonuses.OrderByDescending(o => o.Store);
                        break;
                    case "position":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Position)
                            : calculatedBonuses.OrderByDescending(o => o.Position);
                        break;
                    case "name":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Name)
                            : calculatedBonuses.OrderByDescending(o => o.Name);
                        break;
                    case "surname":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Surname)
                            : calculatedBonuses.OrderByDescending(o => o.Surname);
                        break;
                    case "sicil":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Sicil)
                            : calculatedBonuses.OrderByDescending(o => o.Sicil);
                        break;
                    case "bonus":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Bonus)
                            : calculatedBonuses.OrderByDescending(o => o.Bonus);
                        break;

                    default:
                        calculatedBonuses = calculatedBonuses.OrderBy(o => o.Id);
                        break;
                }

                var calculatedBonusesList = calculatedBonuses
                    .Select(x => new
                    {
                        x.Id,
                        x.Brand,
                        x.Store,
                        x.Position,
                        x.Name,
                        x.Surname,
                        x.Sicil,
                        x.Actual,
                        x.ActualBrut,
                        x.Month,
                        x.Bonus,
                        x.Notes,
                        x.Target,
                        x.Final
                    });

                if (calculatedBonusesList.Any())
                {
                    var page = request.Page ?? 0;
                    var itemsPerPage = request.Size ?? 50;

                    var tmpContent = calculatedBonusesList
                        .Skip(page * itemsPerPage)
                        .Take(itemsPerPage)
                        .ToList();

                    //var response = new PagingResponse(calculatedBonusesList, tmpContent, request);
                    var sortingen = new[]
                    {
                        new
                        {
                            property = sortParam,
                            direction = sortDir,
                            ascending = sortDir == "asc",
                            descending = sortDir == "desc",
                            ignoreCase = false,
                            nullHandling = "NATIVE"
                        }
                    };

                    var resp = new
                    {
                        first = page == 0,
                        last = page == calculatedBonusesList.Count() / itemsPerPage + 1,
                        number = page,
                        numberOfElements = tmpContent.Count,
                        size = itemsPerPage,
                        sort = sortingen,
                        totalElements = calculatedBonusesList.Count(),
                        totalPages = (calculatedBonusesList.Count() / itemsPerPage) + 1,
                        content = tmpContent
                    };

                    return Ok(resp);
                }

                var calcResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = "Hesaplama sonucu yok.",
                    Code = "CALCULATION_EMPTY_SET",
                };
                return NotFound(calcResponse);
            }
            catch (Exception ex)
            {
                ActionResponseContract calculationResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "CALCULATION_RESULTS_ERROR",
                    Data = ex
                };
                return NotFound(calculationResponse);
            }
        }
        
        
        /// <summary>
        /// Overachievement
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("yHesaplamaSonuc/overachievement")]
        [ValidateModelState]
        [SwaggerOperation("YPrimHesapmalaReportOver")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [SwaggerResponse((int) HttpStatusCode.NotFound, typeof(ActionResponseContract), "Failed")]
        [SwaggerResponse((int) HttpStatusCode.OK, typeof(ListResponseContract), "Success")]
        public IActionResult YPrimHesapmalaReportOver([FromBody] YCalculationRequest request)
        {
            try
            {
                var calculatedBonuses = _localDbContext.VEmployeeBonuses.AsNoTracking();
                if (request.BrandId != 0)
                    calculatedBonuses = calculatedBonuses.Where(c => c.BrandId == request.BrandId);
                var userArea = HttpContext.Items["UserArea"] as string;
                if (userArea == "S")
                {
                    var store = (int) HttpContext.Items["UserStore"];
                    calculatedBonuses = calculatedBonuses.Where(c => c.StoreId == store);
                }
                else
                {
                    if (request.StoreId != 0)
                        calculatedBonuses = calculatedBonuses.Where(c => c.StoreId == request.StoreId);
                }

                if (request.Final)
                    calculatedBonuses = calculatedBonuses.Where(c => c.Final == request.Final);
                calculatedBonuses = calculatedBonuses.Where(c => c.Year == request.Year);

                var sort = request.Sort ?? "id,asc";
                var sortParam = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[0];
                var sortDir = sort.Split(",", StringSplitOptions.RemoveEmptyEntries)[1];
                switch (sortParam)
                {
                    case "brand":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Brand)
                            : calculatedBonuses.OrderByDescending(o => o.Brand);
                        break;
                    case "store":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Store)
                            : calculatedBonuses.OrderByDescending(o => o.Store);
                        break;
                    case "name":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Name)
                            : calculatedBonuses.OrderByDescending(o => o.Name);
                        break;
                    case "surname":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Surname)
                            : calculatedBonuses.OrderByDescending(o => o.Surname);
                        break;
                    case "sicil":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Sicil)
                            : calculatedBonuses.OrderByDescending(o => o.Sicil);
                        break;
                    case "bonus":
                        calculatedBonuses = sortDir == "asc"
                            ? calculatedBonuses.OrderBy(o => o.Bonus)
                            : calculatedBonuses.OrderByDescending(o => o.Bonus);
                        break;

                    default:
                        calculatedBonuses = calculatedBonuses.OrderBy(o => o.Id);
                        break;
                }

                var calculatedBonusesList = calculatedBonuses
                    .Select(x => new
                    {
                        x.Id,
                        x.Brand,
                        x.Store,
                        x.Name,
                        x.Surname,
                        x.Sicil,
                        x.Actual, //Type
                        x.Bonus,
                        x.Notes,
                        x.Year,
                        x.Final
                    });

                if (calculatedBonusesList.Any())
                {
                    var page = request.Page ?? 0;
                    var itemsPerPage = request.Size ?? 50;

                    var tmpContent = calculatedBonusesList
                        .Skip(page * itemsPerPage)
                        .Take(itemsPerPage)
                        .ToList();

                    var sortingen = new[]
                    {
                        new
                        {
                            property = sortParam,
                            direction = sortDir,
                            ascending = sortDir == "asc",
                            descending = sortDir == "desc",
                            ignoreCase = false,
                            nullHandling = "NATIVE"
                        }
                    };

                    var resp = new
                    {
                        first = page == 0,
                        last = page == calculatedBonusesList.Count() / itemsPerPage + 1,
                        number = page,
                        numberOfElements = tmpContent.Count,
                        size = itemsPerPage,
                        sort = sortingen,
                        totalElements = calculatedBonusesList.Count(),
                        totalPages = (calculatedBonusesList.Count() / itemsPerPage) + 1,
                        content = tmpContent
                    };

                    return Ok(resp);
                }

                ActionResponseContract calcResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = "Hesaplama sonucu yok.",
                    Code = "CALCULATION_EMPTY_SET",
                };
                return NotFound(calcResponse);
            }
            catch (Exception ex)
            {
                ActionResponseContract calculationResponse = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "CALCULATION_RESULTS_ERROR",
                    Data = ex
                };
                return NotFound(calculationResponse);
            }
        }
    }
}