﻿using System;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Models;
using Pusula.Api.Data.DataContexts;

namespace Pusula.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("/svc")]
    public class SasMonthsController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <summary>
        /// SAS Ayları İşlemleri
        /// </summary>
        /// <param name="context"></param>
        public SasMonthsController(PatikaDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("sasmonths")]
        [ValidateModelState]
        [SwaggerOperation("SasMonthsAdd")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult SasMonthsAdd([FromBody]SasMonths request)
        {
            try
            {
                using (_context)
                {
                    //Brand için önceden eklenmişse onu güncelle yoksa yensini ekle 
                    var sm = _context.SasMonthses
                        .Include(s=>s.Brand)
                        .FirstOrDefault(s => s.Brand.Id == request.Brand.Id);
                    if (sm != null)
                    {
                        sm.M01 = request.M01;
                        sm.M02 = request.M02;
                        sm.M03 = request.M03;
                        sm.M04 = request.M04;
                        sm.M05 = request.M05;
                        sm.M06 = request.M06;
                        sm.M07 = request.M07;
                        sm.M08 = request.M08;
                        sm.M09 = request.M09;
                        sm.M10 = request.M10;
                        sm.M11 = request.M11;
                        sm.M12 = request.M12;
                    }
                    else
                    {
                        _context.SasMonthses.Add(request);
                        _context.Entry(request.Brand).State = EntityState.Unchanged;
                        sm = _context.SasMonthses.Last();
                    }

                    _context.SaveChanges();

                    ActionResponseContract responseSuccessContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "İndirim ayları eklendi.",
                        Code = "SASMONTHS_ADD_SUCCESS",
                        Data = sm
                    };
                    return Ok(responseSuccessContract);
                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "SASMONTHS_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("sasmonths")]
        [ValidateModelState]
        [SwaggerOperation("SasMonthsUpdate")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult SasMonthsUpdate([FromBody]SasMonths request)
        {
            try
            {
                using (_context)
                {
                    var sm = _context.SasMonthses
                        .Include(s=>s.Brand)
                        .FirstOrDefault(s => s.Id == request.Id);
                    if (sm != null)
                    {
                        sm.M01 = request.M01;
                        sm.M02 = request.M02;
                        sm.M03 = request.M03;
                        sm.M04 = request.M04;
                        sm.M05 = request.M05;
                        sm.M06 = request.M06;
                        sm.M07 = request.M07;
                        sm.M08 = request.M08;
                        sm.M09 = request.M09;
                        sm.M10 = request.M10;
                        sm.M11 = request.M11;
                        sm.M12 = request.M12;
                        _context.SaveChanges();


                        ActionResponseContract responseSuccessContract = new ActionResponseContract
                        {
                            Success = true,
                            Message = "İndirim ayları güncellendi.",
                            Code = "SASMONTHS_UPDATE_SUCCESS",
                            Data = sm
                        };
                        return Ok(responseSuccessContract);
                    }
                    throw new Exception("İndirim ayı bulunamadı.");
                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "SASMONTHS_UPDATE_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }

        /// <summary>
        /// İndirim aylarını döndür
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("sasmonths")]
        [ValidateModelState]
        [SwaggerOperation("SasMonthsGet")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult SasMonthsGet()
        {
            var sasMonthses = _context.SasMonthses
                .Include(s=>s.Brand)
                .LastOrDefault();
            if(sasMonthses != null)
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "İndirim ayları.",
                    Code = "SASMONTHS_GET_SUCCESS",
                    Data = sasMonthses
                };
                return Ok(responseSuccessContract);
            }
            
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "İndirim ayları bulunamadı.",
                Code = "SASMONTHS_EMPTY_SET"
            };
            return NotFound(responseContract);
        }

        /// <summary>
        /// Belirtilen yılın indirim aylarını döndür.
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("sasmonths/{brandId}")]
        [ValidateModelState]
        [SwaggerOperation("SasMonthsGetByYear")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult SasMonthsGetByYear([FromRoute] int brandId)
        {
            var sasMonthses = _context.SasMonthses
                .Include(s=>s.Brand)
                .Where(m => m.Brand.Id == brandId);
            if (sasMonthses.Any())
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "İndirim ayları.",
                    Code = "SASMONTHS_GET_SUCCESS",
                    Data = sasMonthses.First()
                };
                return Ok(responseSuccessContract);
            }
            
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "İndirim ayları bulunamadı.",
                Code = "SASMONTHS_EMPTY_SET"
            };
            return NotFound(responseContract);
        }
    }
}