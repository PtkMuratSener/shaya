﻿using Microsoft.AspNetCore.Mvc;
using System;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Models;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;

namespace Pusula.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("/svc")]
    public class ParametreController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <summary>
        /// Sistem Parametre İşlemleri
        /// </summary>
        /// <param name="context"></param>
        public ParametreController(PatikaDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Yeni parametre ekler
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("parametre")]
        [ValidateModelState]
        [SwaggerOperation("ParametreAdd")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult ParametreAdd([FromBody]Parametre request)
        {
            try
            {
                using (_context)
                {
                    _context.Parametres.Add(request);
                    _context.SaveChanges();
                    var insertedParametre = _context.Parametres.Last();
                    ActionResponseContract responseContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Parametre eklendi.",
                        Code = "PARAMETER_ADD_SUCCESS",
                        Data = insertedParametre
                    };
                    return Ok(responseContract);
                }

                //return StatusCode(200);
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "PARAMETER_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
                
            }
        }

        /// <summary>
        /// Parametre güncelle
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("parametre")]
        [ValidateModelState]
        [SwaggerOperation("ParametreUpdate")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult ParametreUpdate([FromBody]Parametre request)
        {
            var param = _context.Parametres.Find(request.Id);
            if (param != null)
            {
                param.Value = request.Value;
                param.Description = request.Description;
                _context.SaveChanges();
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Parametre güncellendi.",
                    Code = "PARAMETER_UPDATE_SUCCESS",
                    Data = param
                };
                return Ok(responseSuccessContract);  
            }
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Parametre güncellenemedi.",
                Code = "PARAMETER_UPDATE_FAILED",
            };
            return NotFound(responseContract);
        }

        
        /// <summary>
        /// Parametre listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("parametre")]
        [ValidateModelState]
        [SwaggerOperation("ParametreGetAll")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ListResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Not found")]
        public IActionResult ParametreGetAll()
        {
            var parametres = _context.Parametres.AsNoTracking()
                .Where(p => p.Visible)
                .OrderBy(s => s.Title)
                .Select(x => new
                {
                    x.Id,
                    x.Title,
                    x.Value,
                    x.Description
                })
                .ToList();
            if (parametres.Any())
            {
                ListResponseContract responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = parametres.Count},
                    Data = parametres
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Parametre bulunamadı.",
                Code = "PARAMETER_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
        }
        
        /// <summary>
        /// Parametre detay bilgileri
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("parametre/{id}")]
        [ValidateModelState]
        [SwaggerOperation("ParametreGetById")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ListResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Not found")]
        public IActionResult ParametreGetById([FromRoute] int id)
        {
            var parametre = _context.Parametres.Find(id);
            if (parametre != null)
            {
                ListResponseContract responseContract = new ListResponseContract
                {
                    Total = new TotalProperty {All = 1},
                    Data = parametre
                };
                return StatusCode(200, responseContract);
            }

            //Kayıt yoksa 
            ActionResponseContract failedResponseContract = new ActionResponseContract
            {
                Success = true,
                Message = "Parametre bulunamadı.",
                Code = "PARAMETER_EMPTY_SET"
            };
            return NotFound(failedResponseContract);
            
        }
        
        /// <summary>
        /// Parametre sil
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("parametre/{id}")]
        [ValidateModelState]
        [SwaggerOperation("ParametreDeleteById")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult ParametreDeleteById([FromRoute] int id)
        {
            var parametre = _context.Parametres.Find(id);
            if (parametre == null)
            {
                //Kayıt yoksa 
                ActionResponseContract failedResponseContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Parametre bulunamadı.",
                    Code = "PARAMETER_EMPTY_SET"
                };
                return NotFound(failedResponseContract);
            }

            _context.Parametres.Remove(parametre);
            _context.SaveChanges();
            return StatusCode(200, "Success");

        }
    }
}
