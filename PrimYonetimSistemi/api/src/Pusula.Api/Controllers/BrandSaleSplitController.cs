using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Pusula.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Mağaza ağırlıklı satış hedef oranları
    /// </summary>
    [Route("/svc")]
    public class BrandSaleSplitController : Controller
    {
        private readonly PatikaDbContext _localDbContext;

        /// <inheritdoc />
        public BrandSaleSplitController(PatikaDbContext localDbContext)
        {
            _localDbContext = localDbContext;
        }


        /// <summary>
        /// Mağaza ağırlıklı satış hedef oranları listesi
        /// </summary>
        /// <param name="brandId">Marka Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("brandSaleSplits/{brandId}")]
        [ValidateModelState]
        [SwaggerOperation("BrandSaleSplitList")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Created")]
        public IActionResult BrandSaleSplitList([FromRoute]int brandId)
        {
            var resp = _localDbContext.BrandSaleSplits.AsNoTracking()
                .Include(b=> b.Brand)
                .Include(p=>p.Position)
                .Where(w=>w.Brand.Id==brandId)
                .ToList();
            return Ok(resp);
        }

        /// <summary>
        /// Mağaza ağırlıklı satış hedef oranları Güncelle
        /// </summary>
        /// <param name="request">BrandSaleSplit</param>
        /// <returns></returns>
        [HttpPut]
        [Route("brandSaleSplit")]
        [ValidateModelState]
        [SwaggerOperation("BrandSaleSplitUpdate")]
        [Produces("application/json")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Created")]
        public IActionResult BrandSaleSplitUpdate([FromBody] BrandSaleSplit request)
        {
            var resp = _localDbContext.BrandSaleSplits.Find(request.Id);
            if (resp != null)
            {
                resp.BaseRate = request.BaseRate;
                _localDbContext.SaveChanges();
                var successResponse = new ActionResponseContract
                {
                    Success = true,
                    Message = "Prim oranı güncellendi.",
                    Code = "SPLITRATE_UPDATE_SUCCESS",
                    Data = resp
                };
                return Ok(successResponse);
            }

            var failedResponse = new ActionResponseContract
            {
                Success = false,
                Message = "Kayıt bulunamadı.",
                Code = "SPLITRATE_UPDATE_FAILED",
            };
            return NotFound(failedResponse );
        }
    }
}