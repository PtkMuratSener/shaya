using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Attributes;
using Pusula.Api.Contracts.Responses;
using Pusula.Api.Models;
using Pusula.Api.Data.DataContexts;

namespace Pusula.Api.Controllers
{
    /// <summary>
    /// Bonus oranları İşlemleri
    /// </summary>
    [Route("/svc")]
    public class BonusRatesController : Controller
    {
        private readonly PatikaDbContext _context;

        /// <summary>
        /// Bonus oranları İşlemleri
        /// </summary>
        /// <param name="context"></param>
        public BonusRatesController(PatikaDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Prim oranı ekle
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("bonusRate")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesAdd")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesAdd([FromBody]BonusRate request)
        {
            try
            {
                using (_context)
                {
                    var bonusRate = _context.BonusRates
                        .Include(m => m.Brand)
                        .Include(m => m.Position)
                        .FirstOrDefault(m => m.Brand.Id == request.Brand.Id && m.Position.Id == request.Position.Id);
                    if (bonusRate != null)
                    {
                        bonusRate.P1s1 = request.P1s1;
                        bonusRate.P1s2 = request.P1s2;
                        bonusRate.P1s3 = request.P1s3;
                        bonusRate.P2s1 = request.P2s1;
                        bonusRate.P2s2 = request.P2s2;
                        bonusRate.P2s3 = request.P2s3;
                        bonusRate.P3s1 = request.P3s1;
                        bonusRate.P3s2 = request.P3s2;
                        bonusRate.P3s3 = request.P3s3;
                        
                        _context.SaveChanges();


                        ActionResponseContract responseSuccessU = new ActionResponseContract
                        {
                            Success = true,
                            Message = "Prim oranları güncellendi.",
                            Code = "BONUSRATE_ADD_SUCCESS",
                            Data = bonusRate
                        };
                        return Ok(responseSuccessU);
                    }

                    _context.BonusRates.Add(request);
                    _context.Entry(request.Brand).State = EntityState.Unchanged;
                    _context.Entry(request.Position).State = EntityState.Unchanged;
                

                    _context.SaveChanges();

                    ActionResponseContract responseSuccessA = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Prim oranları eklendi.",
                        Code = "BONUSRATE_ADD_SUCCESS",
                        Data = request
                    };
                    return Ok(responseSuccessA);
                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "BONUSRATE_ADD_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }

        /// <summary>
        /// Prim oranı kaydını güncelle
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("bonusRate")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesUpdate")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesUpdate([FromBody]BonusRate request)
        {
            try
            {
                using (_context)
                {
                    var bonusRate = _context.BonusRates
                        .Include(m => m.Brand)
                        .Include(m => m.Position)
                        .FirstOrDefault(b => b.Id == request.Id);
                    if (bonusRate != null)
                    {
                        bonusRate.P1s1 = request.P1s1;
                        bonusRate.P1s2 = request.P1s2;
                        bonusRate.P1s3 = request.P1s3;
                        bonusRate.P2s1 = request.P2s1;
                        bonusRate.P2s2 = request.P2s2;
                        bonusRate.P2s3 = request.P2s3;
                        bonusRate.P3s1 = request.P3s1;
                        bonusRate.P3s2 = request.P3s2;
                        bonusRate.P3s3 = request.P3s3;
                        
                        _context.Entry(bonusRate.Brand).State = EntityState.Unchanged;
                        _context.Entry(bonusRate.Position).State = EntityState.Unchanged;
                        _context.SaveChanges();


                        ActionResponseContract responseSuccessContract = new ActionResponseContract
                        {
                            Success = true,
                            Message = "Prim oranları güncellendi.",
                            Code = "BONUSRATE_UPDATE_SUCCESS",
                            Data = bonusRate
                        };
                        return Ok(responseSuccessContract);
                    }
                    throw new Exception("Prim oranı bulunamadı.");
                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "BONUSRATE_UPDATE_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }

        /// <summary>
        /// Mağaza pozisyonunun Prim oranını getirir.
        /// </summary>
        [HttpGet]
        [Route("bonusRate/{brandId}/{positionId}")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesGetByBrandPos")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesGetByBrandPos([FromRoute] int brandId, [FromRoute] int positionId)
        {
            var bonusRateses = _context.BonusRates
                .Include(b => b.Brand)
                .Include(b => b.Position)
                .Where(m => m.Brand.Id == brandId && m.Position.Id == positionId);
            if (bonusRateses.Any())
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Prim oranları",
                    Code = "BONUSRATE_GET_SUCCESS",
                    Data = bonusRateses.Last()
                };
                return Ok(responseSuccessContract);
            }
            
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Prim oranı bulunamadı.",
                Code = "BONUSRATE_EMPTY_SET"
            };
            return NotFound(responseContract);
        }

        /// <summary>
        /// Mağaza SLT pozisyonunun Prim oranını getirir.
        /// </summary>
        [HttpGet]
        [Route("bonusRateSlt/{brandId}")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesGetByBrandPosSlt")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesGetByBrandPosSlt([FromRoute] int brandId)
        {
            var bonusRatesSlt = _context.BonusRatesSlt
                .Include(b => b.Brand)
                .Include(b => b.Position)
                .Where(m => m.Brand.Id == brandId)
                .Select(x => new BonusRatesSltTemplate
                {
                    Id = x.Id,
                    BrandId = x.Brand.Id,
                    PositionId = x.Position.Id,
                    PositionName = x.Position.Name,
                    Rate = x.Rate
                });
            if (bonusRatesSlt.Any())
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "SLT Prim oranları",
                    Code = "BONUSRATESLT_GET_SUCCESS",
                    Data = bonusRatesSlt
                };
                return Ok(responseSuccessContract);
            }
            
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "SLT Prim oranı bulunamadı.",
                Code = "BONUSRATESLT_EMPTY_SET"
            };
            return NotFound(responseContract);
        }
        
        /// <summary>
        /// SLT Prim oranı kaydını güncelle
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("bonusRateSlt")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesUpdateSlt")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesUpdateSlt([FromBody]List<BonusRatesSltTemplate> request)
        {
            try
            {
                using (_context)
                {
                    foreach (var req in request)
                    {
                        var pId =req.Id;
                        var pRate = req.Rate;
                        var bonusRate = _context.BonusRatesSlt
                            .FirstOrDefault(b => b.Id == pId);
                        if (bonusRate != null)
                        {
                            bonusRate.Rate = pRate;
                            _context.SaveChanges();

                        }
                    }

                    ActionResponseContract responseSuccessContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "SLT Prim oranı güncellendi.",
                        Code = "BONUSRATESLT_UPDATE_SUCCESS",
                        Data = request
                    };
                    return Ok(responseSuccessContract);

                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "BONUSRATESLT_UPDATE_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }
        
        /// <summary>
        /// Yıllık prim oranları
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("bonusRateYearly/{brandId}")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesGetByBrandYearly")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesGetByBrandYearly([FromRoute] int brandId)
        {
            var bonusRatesSlt = _context.BonusRatesSlt
                .Include(b => b.Brand)
                .Include(b => b.Position)
                .Where(m => m.Brand.Id == brandId)
                .Select(x => new BonusRatesSltTemplate
                {
                    Id = x.Id,
                    BrandId = x.Brand.Id,
                    PositionId = x.Position.Id,
                    PositionName = x.Position.Name,
                    Rate = x.Rate,
                    RateKarlilik = x.RateKarlilik,
                    RateShrinkage = x.RateShrinkage,
                    RateTelafi = x.RateTelafi,
                    RateOverAchievement = x.RateOverAchievement
                });
            if (bonusRatesSlt.Any())
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Yıllık Prim oranları",
                    Code = "BONUSRATESYEARLY_GET_SUCCESS",
                    Data = bonusRatesSlt
                };
                return Ok(responseSuccessContract);
            }
            
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Yıllık Prim oranı bulunamadı.",
                Code = "BONUSRATESYEARLY_EMPTY_SET"
            };
            return NotFound(responseContract);
        }
        
        /// <summary>
        /// Yıllık prim oranları kaydet
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("bonusRateYearly")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesUpdateYearly")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesUpdateYearly([FromBody]List<BonusRatesSltTemplate> request)
        {
            try
            {
                using (_context)
                {
                    foreach (var req in request)
                    {
                        var pId =req.Id;
                        var bonusRate = _context.BonusRatesSlt
                            .FirstOrDefault(b => b.Id == pId);
                        if (bonusRate != null)
                        {
                            bonusRate.RateKarlilik = req.RateKarlilik;
                            bonusRate.RateTelafi = req.RateTelafi;
                            bonusRate.RateShrinkage = req.RateShrinkage;
                            bonusRate.RateOverAchievement = req.RateOverAchievement;
                            _context.SaveChanges();
                        }
                    }
                    //throw new Exception("Güncellenecek SLT Prim oranı bulunamadı.");

                    ActionResponseContract responseSuccessContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Yıllık Prim oranı güncellendi.",
                        Code = "BONUSRATESYEARLY_UPDATE_SUCCESS",
                        Data = request
                    };
                    return Ok(responseSuccessContract);

                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "BONUSRATESYEARLY_UPDATE_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }
        
        /// <summary>
        /// Bütçe üstü karlılık prim oranları listele
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("bonusRatesOverAchievement")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesOverAchievement")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesOverAchievement()
        {
            var bonusRates = _context.OverAchievementRates.AsNoTracking();
            if (bonusRates.Any())
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Bütçe üstü karlılık prim oranları",
                    Code = "BONUSRATESOVERACHIEVEMENT_GET_SUCCESS",
                    Data = bonusRates
                };
                return Ok(responseSuccessContract);
            }
            
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Bütçe üstü karlılık prim oranları bulunamadı.",
                Code = "BONUSRATESOVERACHIEVEMENT_EMPTY_SET"
            };
            return NotFound(responseContract);
        }
        
        /// <summary>
        /// Bütçe üstü karlılık prim oranları güncelle
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("bonusRatesOverAchievement")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesOverAchievementSave")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesOverAchievementSave([FromBody]List<OverAchievementRate> request)
        {
            try
            {
                using (_context)
                {
                    foreach (var req in request)
                    {
                        var pId =req.Id;
                        var bonusRate = _context.OverAchievementRates
                            .FirstOrDefault(b => b.Id == pId);
                        if (bonusRate != null)
                        {
                            bonusRate.Rate12Less = req.Rate12Less;
                            bonusRate.Rate12More = req.Rate12More;
                            _context.SaveChanges();

                        }
                    }
                    //throw new Exception("Güncellenecek SLT Prim oranı bulunamadı.");

                    ActionResponseContract responseSuccessContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Bütçe üstü karlılık prim oranları güncellendi.",
                        Code = "BONUSRATESOVERACHIEVEMENT_UPDATE_SUCCESS",
                        Data = request
                    };
                    return Ok(responseSuccessContract);

                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "BONUSRATESOVERACHIEVEMENT_UPDATE_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }
        
        /// <summary>
        /// Non-SLT Yıllık Telafi prim oranları listesi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("bonusRatesNonSltSlab")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesNonSltSlab")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.OK, type: typeof(ActionResponseContract), description: "Success")]
        [SwaggerResponse(statusCode: (int)HttpStatusCode.NotFound, type: typeof(ActionResponseContract), description: "Failed")]
        public IActionResult BonusRatesNonSltSlab()
        {
            var bonusRates = _context.BonusRates.AsNoTracking()
                .Select(x => new BonusRatesNonSltSlabTemplate
                {
                    Id = (int)x.Id,
                    PositionName = x.Position.Name,
                    Slab1 = x.Slab1,
                    Slab2 = x.Slab2,
                    Slab3 = x.Slab3
                });
                
            if (bonusRates.Any())
            {
                ActionResponseContract responseSuccessContract = new ActionResponseContract
                {
                    Success = true,
                    Message = "Non-SLT yıllık telafi prim oranları",
                    Code = "BONUSRATENONSLTSLAB_GET_SUCCESS",
                    Data = bonusRates
                };
                return Ok(responseSuccessContract);
            }
            
            ActionResponseContract responseContract = new ActionResponseContract
            {
                Success = false,
                Message = "Non-SLT yıllık telafi prim oranları bulunamadı.",
                Code = "BONUSRATENONSLTSLAB_EMPTY_SET"
            };
            return NotFound(responseContract);
        }

        /// <summary>
        /// Non-SLT Yıllık telafi iprim oranları güncelle
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("bonusRatesNonSltSlab")]
        [ValidateModelState]
        [SwaggerOperation("BonusRatesNonSltSlabSave")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.OK, type: typeof(ActionResponseContract),
            description: "Success")]
        [SwaggerResponse(statusCode: (int) HttpStatusCode.NotFound, type: typeof(ActionResponseContract),
            description: "Failed")]
        public IActionResult BonusRatesNonSltSlabSave([FromBody] List<BonusRatesNonSltSlabTemplate> request)
        {
            try
            {
                using (_context)
                {
                    foreach (var req in request)
                    {
                        var pId = req.Id;
                        var bonusRate = _context.BonusRates
                            .FirstOrDefault(b => b.Id == pId);
                        if (bonusRate != null)
                        {
                            bonusRate.Slab1 = req.Slab1;
                            bonusRate.Slab2 = req.Slab2;
                            bonusRate.Slab3 = req.Slab3;
                            _context.SaveChanges();

                        }
                    }

                    ActionResponseContract responseSuccessContract = new ActionResponseContract
                    {
                        Success = true,
                        Message = "Non-SLT yıllık telafi prim oranları güncellendi.",
                        Code = "BONUSRATENONSLTSLAB_UPDATE_SUCCESS",
                        Data = request
                    };
                    return Ok(responseSuccessContract);

                }
            }
            catch (Exception ex)
            {
                ActionResponseContract responseContract = new ActionResponseContract
                {
                    Success = false,
                    Message = ex.Message,
                    Code = "BONUSRATENONSLTSLAB_UPDATE_FAILED",
                    Data = ex
                };
                return NotFound(responseContract);
            }
        }
    }
}