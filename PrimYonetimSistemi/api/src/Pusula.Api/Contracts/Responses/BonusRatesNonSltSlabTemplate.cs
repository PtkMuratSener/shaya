namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// Non-SLT Yıllık Teladi Prim Oranları Şablonu
    /// </summary>
    public class BonusRatesNonSltSlabTemplate
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Pozisyon adı
        /// </summary>
        public string PositionName { get; set; }
        
        /// <summary>
        /// Oran >100%
        /// </summary>
        public double Slab1 { get; set; }
        /// <summary>
        /// Oran >105%
        /// </summary>
        public double Slab2 { get; set; }
        /// <summary>
        /// Oran >110%
        /// </summary>
        public double Slab3 { get; set; }
    }
}
