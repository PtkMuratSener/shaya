namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// SLT Prim Oranları Şablonu
    /// </summary>
    public class BonusRatesSltTemplate
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Brand Id
        /// </summary>
        public int? BrandId { get; set; }
        
        /// <summary>
        /// Position Id
        /// </summary>
        public int? PositionId { get; set; }
        
        /// <summary>
        /// Pozisyon adı
        /// </summary>
        public string PositionName { get; set; }
        
        /// <summary>
        /// Karlılık Oranı
        /// </summary>
        public double Rate { get; set; }
        
        /// <summary>
        /// Karlılık Oranı
        /// </summary>
        public double RateKarlilik { get; set; }

        /// <summary>
        /// Telafi Oranı
        /// </summary>
        public double RateTelafi { get; set; }
        
        /// <summary>
        /// Over Achievement Oran
        /// </summary>
        public double RateOverAchievement { get; set; }
        
        /// <summary>
        /// Shrinkage Oran
        /// </summary>
        public double RateShrinkage { get; set; }
    }
}