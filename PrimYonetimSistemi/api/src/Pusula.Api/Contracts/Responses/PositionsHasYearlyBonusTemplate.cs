namespace Pusula.Api.Contracts.Responses
{
    /// <summary>
    /// Yıllık prim hesaplanacak pozisyonlar şablonu
    /// </summary>
    public class PositionsHasYearlyBonusTemplate
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Brand Id
        /// </summary>
        public int? BrandId { get; set; }
        
        /// <summary>
        /// Position Id
        /// </summary>
        public int? PositionId { get; set; }
        
        /// <summary>
        /// Pozisyon adı
        /// </summary>
        public string PositionName { get; set; }
        
        /// <summary>
        /// Yıllık prim hesaplanacak mı?
        /// </summary>
        public bool HasYearlyBonus { get; set; }
    }
}
