﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Pusula.Api.Infrastructures.Core;
using Pusula.Api.Models.Entities;

namespace Pusula.Api.Contracts.Requests
{
    /// <summary>
    /// Yıllık Hesaplama parametreleri
    /// (Tümü için 0 girin)
    /// </summary>
    [DataContract]
    public class YearlyCalculationAddRequest
    {
        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="BrandId")]
        [Required]
        public int BrandId { get; set; }

        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name="StoreId")]
        [Required]
        public int StoreId { get; set; }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        [DataMember(Name="PositionId")]
        [Required]
        public int PositionId { get; set; }

        /// <summary>
        /// Tip
        /// </summary>
        [DataMember(Name="Type")]
        [Required]
        public CalcTypeEnum Type { get; set; }

        /// <summary>
        /// Marka grubu
        /// </summary>
        [DataMember(Name="BrandType")]
        [Required]
        public BrandTypeEnum BrandType { get; set; }

        /// <summary>
        /// Yıl
        /// </summary>
        [DataMember(Name="Year")]
        [Required]
        [Range(2018, 2038)]
        public int Year { get; set; }

        /// <summary>
        /// Kalıcı veri 
        /// </summary>
        [DataMember(Name="Final")]
        public bool Final { get; set; }
    }
}