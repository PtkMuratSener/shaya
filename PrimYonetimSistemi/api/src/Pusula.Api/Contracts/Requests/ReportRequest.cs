using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Pusula.Api.Models.Entities;

namespace Pusula.Api.Contracts.Requests
{
    /// <summary>
    /// Raporlama isteği
    /// </summary>
    public class ReportRequest : PaginationBase
    {
        /// <summary>
        /// Yıl
        /// </summary>
        [DataMember(Name="Year")]
        [Required]
        [Range(2018, 2038)]
        public int Year { get; set; }

        /// <summary>
        /// Ay
        /// </summary>
        [DataMember(Name="Month")]
        [Range(0,12)]
        public int Month { get; set; }
        
        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name="StoreId")]
        public int StoreId { get; set; }

        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="BrandId")]
        public int BrandId { get; set; }
    }
}