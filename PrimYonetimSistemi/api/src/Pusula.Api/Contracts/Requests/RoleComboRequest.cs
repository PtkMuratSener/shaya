using System.Collections.Generic;
using Pusula.Api.Models;

namespace Pusula.Api.Contracts.Requests
{

    /// <summary>
    /// Rol yetkileri Marka
    /// </summary>
    public class RoleComboBrand
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Marka ismi
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Marka yetkili
        /// </summary>
        public bool Checked { get; set; }
    }

    /// <summary>
    /// Rol yetkileri Ekran
    /// </summary>
    public class RoleComboScreen
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Ekran Adı
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Görme yetkisi
        /// </summary>
        public bool View { get; set; }
        
        /// <summary>
        /// Değiştirme yetkisi
        /// </summary>
        public bool Edit { get; set; }
    }
    
    /// <summary>
    /// Rol Yetkileri Prim Tipleri
    /// </summary>
    public class RoleComboPrimType {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Prim Tipi Adı
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Prim Tipi yetkili
        /// </summary>
        public bool Checked { get; set; }
    }
    
    /// <summary>
    /// Rol-Yetki kaydı
    /// </summary>
    public class RoleComboRequest
    {
        /// <summary>
        /// Rol
        /// </summary>
        public int Role { get; set; }
        
        /// <summary>
        /// Markalar
        /// </summary>
        public List<RoleComboBrand> Brands { get; set; }
        
        /// <summary>
        /// Ekranlar
        /// </summary>
        public List<RoleComboScreen> Screens { get; set; }
        
        /// <summary>
        /// Prim Tipleri
        /// </summary>
        public List<RoleComboPrimType> PrimTypes { get; set; }
    }
}
