namespace Pusula.Api.Contracts.Requests
{
    /// <summary>
    /// Store Position Update Request POCO
    /// </summary>
    public class StorePositionsUpdateRequest
    {
        /// <summary>
        /// Hedeflerin yılı
        /// </summary>
        public int Year { get; set; }
        
        /// <summary>
        /// Hedeflerin periyotu
        /// </summary>
        public int Period { get; set; }
        
        /// <summary>
        /// Mağaza Id
        /// </summary>
        public int StoreId { get; set; }
    }
}