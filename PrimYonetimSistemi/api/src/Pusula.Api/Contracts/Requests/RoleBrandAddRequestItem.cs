using System.Runtime.Serialization;

namespace Pusula.Api.Contracts.Requests
{
    /// <summary>
    /// Role-Marka yetki ataması
    /// </summary>
    [DataContract]
    public class RoleBrandAddRequestItem
    {
        /// <summary>
        /// Marka Id
        /// </summary>
        [DataMember(Name="BrandId")]
        public int BrandId { get; set; }

        /// <summary>
        /// Rol Id
        /// </summary>
        [DataMember(Name="RoleId")]
        public int RoleId { get; set; }
    }
}