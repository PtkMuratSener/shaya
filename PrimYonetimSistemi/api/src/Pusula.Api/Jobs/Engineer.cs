using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pusula.Api.Data.DataContexts;
using Pusula.Api.Models;

namespace Pusula.Api.Jobs
{
    /// <inheritdoc />
    public class Engineer: IDisposable
    {

        private readonly PatikaDbContext _localDbContext;

        /// <inheritdoc />
        public Engineer(PatikaDbContext localDbContext)
        {
            _localDbContext = localDbContext;
        }

        /// <summary>
        /// Mağaza ağırlıklı satış hedef oranları
        /// </summary>
        /// <param name="brandId">Marka</param>
        /// <param name="year">Yıl</param>
        /// <param name="month">Ay</param>
        /// <param name="final">Son hesaplama mı</param>
        /// <returns>int</returns>
        public int CalculateStorePositions(int brandId, int year, int month, bool final=false)
        {
            var retval = 0;
            var brandSaleSplists = _localDbContext.BrandSaleSplits
                .Where(b => b.Brand.Id == brandId);
            if (!brandSaleSplists.Any())
            {
                return 1;
            }

            var stores = _localDbContext.Stores.AsNoTracking()
                .Include(b => b.Brand)
                .Where(s => s.Brand.Id == brandId)
                .Where(a => a.Aktif);
            if (stores.Any())
            {
                foreach (var store in stores)
                {
                    retval += CalculateStorePosition(store.Id, year, month, final);
                }
            }

            return 0;
        }

        /// <summary>
        /// Mağaza pozisyon prim oranları hesabı
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="final"></param>
        /// <returns></returns>
        public int CalculateStorePosition(int storeId, int year, int month, bool final = false)
        {
            var store = _localDbContext.Stores
                .Include(s => s.Brand)
                .First(s => s.Id == storeId);
            var spMevcut = _localDbContext.StorePositions.AsNoTracking()
                .Where(y => y.Year == year)
                .Where(y => y.Month == month)
                .Where(s => s.Store.Id == store.Id);

            var revNum = 0;
            if (spMevcut.Any())
            {
                if (spMevcut.First().Final)
                    return 2; //Final ise tekrar hesaplama

                revNum = _localDbContext.StorePositions.AsNoTracking()
                    .Where(y => y.Year == year)
                    .Where(y => y.Month == month)
                    .Where(s => s.Store.Id == store.Id)
                    .Max(r => r.Revision);
            }

            revNum++;
            // Eski hesapları sil
            var oldSps = _localDbContext.StorePositions
                .Where(y => y.Year == year)
                .Where(y => y.Month == month)
                .Where(s => s.Store.Id == store.Id);
            _localDbContext.StorePositions.RemoveRange(oldSps);
            _localDbContext.SaveChanges();

            //Bu ay mağazada çalışan personelden bir liste yap.
            var sql =
                $@"INSERT INTO StorePositions 
                    (NumStaff, PositionId, StoreId, FormulType, Pmst, SaleSplit, Sta100, Sta105, Sta110, Year, Month, 
                    Revision, Final) 
                SELECT Count(E.Sicil) AS NumStaff, E.PositionId, E.StoreId, 'LBRANDS' AS FormulType, 0, 0, 0, 0, 0, 
                    {year}, {month}, {revNum}, 0 FROM Employees E LEFT JOIN Positions P ON(P.Id=E.PositionId) 
                WHERE P.HasBonus=1 AND E.Active=1 AND E.StoreId = {store.Id} GROUP BY E.PositionId, E.StoreId ";            
            /*
            sql =
                $@"INSERT INTO StorePositions 
                    (NumStaff, PositionId, StoreId, FormulType, Pmst, SaleSplit, Sta100, Sta105, Sta110, Year, Month, 
                    Revision, Final) 
                SELECT Count(E.sicil) AS NumStaff, P.Id , E.store, 'LBRANDS' AS FormulType, 0, 0, 0, 0, 0,
                    {year}, {month}, {revNum}, 0 FROM tpersonel E LEFT JOIN Positions P ON(P.Name =E.position)
                WHERE P.HasBonus=1 AND E.store = {store.Id} AND E.ay={month} GROUP BY P.Id, E.store
                ";
             */
            _localDbContext.Database.ExecuteSqlCommand(sql);
            _localDbContext.SaveChanges();

            //Marka satış hedef oranları
            var splitRates = _localDbContext.BrandSaleSplits.AsNoTracking()
                .Include(b => b.Brand)
                .Include(b => b.Position)
                .Where(b => b.Brand.Id == store.Brand.Id)
                .ToList();

            var srDict = new Dictionary<int, decimal>();
            foreach (var splitRate in splitRates)
            {
                srDict.Add((int) splitRate.Position.Id, splitRate.BaseRate);
            }

            //Mağaza ağırlıklı satış hedef oranları
            var newStorePositions = _localDbContext.StorePositions
                .Include(s => s.Position)
                .Where(y => y.Year == year)
                .Where(y => y.Month == month)
                .Where(s => s.Store.Id == store.Id);

            if (!newStorePositions.Any()) return 1;
            var psrDict = new Dictionary<int, decimal>();

            foreach (var storePosition in newStorePositions)
            {
                var positionId = storePosition.Position.Id;
                var rateCalculated = Math.Round(storePosition.NumStaff * srDict[positionId], 2);
                psrDict.Add(positionId, rateCalculated);
            }

            decimal sumRate = 0;
            foreach (var rate in psrDict)
            {
                sumRate += rate.Value;
            }

            //Toplam 100 olsun diye düzeltme katsayısı ile çarp
            var correctionFactor = 100 / sumRate;

            var storeTarget = _localDbContext.StoreTargetsRetail.AsNoTracking()
                .Where(y => y.Year == year)
                .FirstOrDefault(s => s.Store.Id == store.Id);
            if (storeTarget == null)
            {
                Debug.WriteLine("Mağaza hedefi bulunamadı.");
                return 2;
            }

            //Yeni veriyi yükle
            foreach (var storePosition in newStorePositions)
            {
                //Sale split rate hesapla
                var positionId = storePosition.Position.Id;
                var newRate = Math.Round(psrDict[positionId] * correctionFactor, 2);
                storePosition.SaleSplit = newRate;
                var storeTargetVal = storeTarget.GetTarget(month);
                var pmst = storeTargetVal / (decimal) 100 * newRate / storePosition.NumStaff;
                storePosition.Pmst = Convert.ToInt32(pmst);
                storePosition.Sta100 = Convert.ToInt32(pmst);
                storePosition.Sta105 = Convert.ToInt32(pmst * (decimal) 1.05);
                storePosition.Sta110 = Convert.ToInt32(pmst * (decimal) 1.10);
                storePosition.Final = final;
            }

            _localDbContext.SaveChanges();
            return 0;
        }

        /// <summary>
        /// Aylık hedef başarımları tablosunu hazırla
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public bool PrepareMonthlyAchieved(int year, int month)
        {

            var sql = $"EXEC Fill_Next_Month_Calculations @year={year}, @month={month};";
            _localDbContext.Database.ExecuteSqlCommand(sql);
            sql = $"DELETE FROM MonthlyAchieveds WHERE Year = {year} AND Month={month}";
            _localDbContext.Database.ExecuteSqlCommand(sql);
            sql = $@"
            INSERT INTO MonthlyAchieveds (Sicil, Year, Month, Target, Achieved) 
            SELECT E.EmployeeSicil, E.Year, E.Month, S.Pmst, 0
            FROM
            EmployeeBonuses E
            LEFT JOIN StorePositions S
             ON( E.PositionId=S.PositionId
               AND S.Year=E.Year
               AND S.Month=E.Month
               AND S.StoreId=E.StoreId
            )
            WHERE E.Year = {year} AND E.Month={month}";
            _localDbContext.Database.ExecuteSqlCommand(sql);
            _localDbContext.SaveChanges();
            return true;
        }


        /// <summary>
        /// Günlük çalışan işlemler
        /// </summary>
        /// <returns></returns>
        public void CronDaily()
        {

            /*
             * Mağaza aylık hedefler ve prim oranları hesabı
             * Her ayın 1. günü
             * Her ayın 5. günü
             * Her Shaya periyodunun bitimine 5 gün kala.
             */
            Console.WriteLine("Crondaily Start");
            _localDbContext.EventLogs.Add(new EventLog
            {
                EventOn = DateTime.Now,
                Payload = GetCurrentMethod() + " start",
                Type = GetCurrentMethod()
            });
            _localDbContext.SaveChanges();
            
            var brandList = new List<int>();
            brandList.Add(39);
            var year = DateTime.Now.Year;
            var month = DateTime.Now.Month;
            var runJob = false;
            var todayS = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
            var shDatesS = _localDbContext.Calendars.FirstOrDefault(c => c.Date == todayS);

            // Ayın günleri
            runJob = DateTime.Now.Day == 1;
            if (!runJob)
            {
                runJob = DateTime.Now.Day == 5;
            }

            //ve dahi shaya periyodu bitimine 5 gün kala
            if (!runJob)
            {
                if (shDatesS != null)
                {
                    var shDatesSa = _localDbContext.Calendars
                        .Where(c => c.Month == shDatesS.Month)
                        .Where(y => y.Year == shDatesS.Year)
                        .Count(d => d.Date > todayS);
                    if (shDatesSa == 3) // Bugün+4 = son 5.
                    {
                        runJob = true;
                        var today = Convert.ToInt32(DateTime.Now.AddDays(10).ToString("yyyyMMdd"));
                        var shDates = _localDbContext.Calendars.FirstOrDefault(c => c.Date == today);
                        if (shDates != null)
                        {
                            year = shDates.Year;
                            month = shDates.Month;
                        }
                    }
                }
            }

            if (runJob)
            {
                Console.WriteLine("Crondaily -5,1,5");
                var isFinal = DateTime.Now.Day == 5;
                _localDbContext.EventLogs.Add(new EventLog
                {
                    EventOn = DateTime.Now,
                    Payload = $"CalculateStorePositions, Final?{isFinal}",
                    Type = GetCurrentMethod()
                });
                _localDbContext.SaveChanges();
                foreach (var brand in brandList)
                {
                    CalculateStorePositions(brand, year, month, isFinal);
                }
                Console.WriteLine("Crondaily CalculateStorePositions");
                _localDbContext.EventLogs.Add(new EventLog
                {
                    EventOn = DateTime.Now,
                    Payload = $"PrepareMonthlyAchieved",
                    Type = GetCurrentMethod()
                });
                PrepareMonthlyAchieved(year, month);
                Console.WriteLine("Crondaily PrepareMonthlyAchieved");
                _localDbContext.EventLogs.Add(new EventLog
                {
                    EventOn = DateTime.Now,
                    Payload = "Fill_BonusSltMonthly, Fill_Next_Month_Calculations",
                    Type = GetCurrentMethod()
                });
                var sql = $"EXEC Fill_BonusSltMonthly @year={year}, @month={month}";
                _localDbContext.Database.ExecuteSqlCommand(sql);
                Console.WriteLine("Crondaily Fill_BonusSltMonthly");
            }

            //Hedef Gerçekleşen verisni güncelle
            _localDbContext.EventLogs.Add(new EventLog
            {
                EventOn = DateTime.Now,
                Payload = "Fill_Monthly_Achieveds",
                Type = GetCurrentMethod()
            });
            _localDbContext.Database.ExecuteSqlCommand("EXEC Fill_Monthly_Achieveds");
            Console.WriteLine("Crondaily Fill_Monthly_Achieveds");
            
            /*
             * İade periyot gününden sonraki günde (Retail import için bekliyoruz)
             * Mağaza hedefleri retail satış ve iadeleri kullanılarak hesaplanacak
             */
            var iadePeriyotGunu = 13;
            var paramIadePeriyotGunu = _localDbContext.Parametres.AsNoTracking()
                .FirstOrDefault(x => x.Title == "RefundPeriodStartDay");
            if (paramIadePeriyotGunu != null)
            {
                iadePeriyotGunu = int.Parse(paramIadePeriyotGunu.Value);
            }
            if (shDatesS != null)
            {
                var periyotGunu = _localDbContext.Calendars
                    .Where(c => c.Month == shDatesS.Month)
                    .Where(y => y.Year == shDatesS.Year)
                    .Count(d => d.Date <= todayS);
                if (periyotGunu == (iadePeriyotGunu + 1))
                {
                    _localDbContext.EventLogs.Add(new EventLog
                    {
                        EventOn = DateTime.Now,
                        Payload = $"CalculateStoreTargetRetail.Iade",
                        Type = GetCurrentMethod()
                    });
                    _localDbContext.SaveChanges();
                    CalculateStoreTargetRetail(shDatesS.Year, shDatesS.Month);
                    Console.WriteLine("Crondaily CalculateStoreTargetRetail");
                }

                if (periyotGunu <= iadePeriyotGunu)
                {
                    var gecenAyBugun = Convert.ToInt32(DateTime.Now.AddMonths(-1).ToString("yyyyMMdd"));
                    var shDatesO = _localDbContext.Calendars.FirstOrDefault(c => c.Date == gecenAyBugun);
                    _localDbContext.Database.ExecuteSqlCommand($"EXEC Fill_Monthly_Achieveds_YYYY_MM {shDatesO.Year}, {shDatesO.Month}");
                }
            }
            _localDbContext.EventLogs.Add(new EventLog
            {
                EventOn = DateTime.Now,
                Payload = $"Crondaily Finish",
                Type = GetCurrentMethod()
            });
            _localDbContext.SaveChanges();
            Console.WriteLine("Crondaily Finish");
        }

        /// <summary>
        /// Retail bazlı mağaza satış verisi hesaplar
        /// </summary>
        /// <returns></returns>
        /// <param name="year"></param>
        /// <param name="periyot"></param>
        public bool CalculateStoreTargetRetail(int year, int periyot)
        {
            var iadePeriyotGunu = 15;
            var paramIadePeriyotGunu = _localDbContext.Parametres.AsNoTracking()
                .FirstOrDefault(x => x.Title == "RefundPeriodStartDay");
            if (paramIadePeriyotGunu != null)
            {
                iadePeriyotGunu = int.Parse(paramIadePeriyotGunu.Value);
            }
            var storeTargets = _localDbContext.StoreTargetsRetail
                .Include(x=>x.Store)
                .ThenInclude(s=>s.Brand)
                .Where(x => x.Store.Brand.Id == 39)  //Sadece Victorias için
                .Where(y => y.Year == year);
            if (storeTargets.Any())
            {
                var periyotBasS = _localDbContext.Calendars.AsNoTracking()
                    .Where(y => y.Year == year)
                    .Where(m => m.Month == periyot)
                    .Min(b => b.Date)
                    .ToString();
                var periyotSonS = _localDbContext.Calendars.AsNoTracking()
                    .Where(y => y.Year == year)
                    .Where(m => m.Month == periyot)
                    .Max(b => b.Date)
                    .ToString();
                
                var periyotBas = DateTime.ParseExact(periyotBasS, "yyyyMMdd",  CultureInfo.InvariantCulture);
                var periyotSon = DateTime.ParseExact(periyotSonS, "yyyyMMdd",  CultureInfo.InvariantCulture);

                var iadePeriyotBasS = _localDbContext.Calendars.AsNoTracking()
                    .Where(y => y.Year == year)
                    .Where(m => m.Month == periyot)
                    .OrderBy(b => b.Date)
                    .Skip(iadePeriyotGunu-1)
                    .First();
                var iadePeriyotSonS = _localDbContext.Calendars.AsNoTracking()
                    .Where(y => y.Date > int.Parse(periyotSonS))
                    .OrderBy(b => b.Date)
                    .Skip(iadePeriyotGunu-1)
                    .First();
                
                var iadePeriyotBas = DateTime.ParseExact(iadePeriyotBasS.Date.ToString(), "yyyyMMdd",  CultureInfo.InvariantCulture);
                var iadePeriyotSon = DateTime.ParseExact(iadePeriyotSonS.Date.ToString(), "yyyyMMdd",  CultureInfo.InvariantCulture);
                
                foreach (var storeTarget in storeTargets)
                {
                    var wrStoreRefund = _localDbContext.Sales
                        .AsNoTracking()
                        .Where(e => e.Store.Id == storeTarget.Store.Id)
                        .Where(e => e.SaleDate >= iadePeriyotBas)
                        .Where(e => e.SaleDate <  iadePeriyotSon)
                        .Where(e => e.Amount < 0)
                        .Sum(e => e.Amount);
                    wrStoreRefund = Math.Abs(wrStoreRefund);
                    var storeId = storeTarget.Store.Id;
                    storeTarget.SetIade(periyot, (int)wrStoreRefund);
                }
            }

            _localDbContext.SaveChanges();
            return true;
        }

        /// <inheritdoc />
        public void Dispose()
        {
        }
        
        /// <summary>
        /// Returns current method.
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public string GetCurrentMethod()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }
    }
}