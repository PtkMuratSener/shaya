using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class BonusRate
    { 
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [DataMember(Name="Id")]
        public int? Id { get; set; }

        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="Brand")]
        public Brand Brand { get; set; }
        
        /// <summary>
        /// Pozisyon  
        /// </summary>
        [DataMember(Name="Position")]
        public Position Position { get; set; }
        
        /// <summary>
        /// Pozisyon 100%, Mağaza 100%
        /// </summary>
        [DataMember(Name="P1s1")]
        public double P1s1 { get; set; }
        
        /// <summary>
        /// Pozisyon 100%, Mağaza 105%
        /// </summary>
        [DataMember(Name="P1s2")]
        public double P1s2 { get; set; }
        
        /// <summary>
        /// Pozisyon 100%, Mağaza 110%
        /// </summary>
        [DataMember(Name="P1s3")]
        public double P1s3 { get; set; }
        
        /// <summary>
        /// Pozisyon 105%, Mağaza 100%
        /// </summary>
        [DataMember(Name="P2s1")]
        public double P2s1 { get; set; }
        
        /// <summary>
        /// Pozisyon 105%, Mağaza 105%
        /// </summary>
        [DataMember(Name="P2s2")]
        public double P2s2 { get; set; }
        
        /// <summary>
        /// Pozisyon 105%, Mağaza 110%
        /// </summary>
        [DataMember(Name="P2s3")]
        public double P2s3 { get; set; }
        
        /// <summary>
        /// Pozisyon 110%, Mağaza 100%
        /// </summary>
        [DataMember(Name="P3s1")]
        public double P3s1 { get; set; }
        
        /// <summary>
        /// Pozisyon 110%, Mağaza 105%
        /// </summary>
        [DataMember(Name="P3s2")]
        public double P3s2 { get; set; }
        
        /// <summary>
        /// Pozisyon 110%, Mağaza 110%
        /// </summary>
        [DataMember(Name="P3s3")]
        public double P3s3 { get; set; }
        
        /// <summary>
        /// Telafi %100-%105
        /// </summary>
        [DataMember(Name="Slab1")]
        public double Slab1 { get; set; }
        
        /// <summary>
        /// Telafi %105-%110
        /// </summary>
        [DataMember(Name="Slab2")]
        public double Slab2 { get; set; }
        
        /// <summary>
        /// Telafi %110-
        /// </summary>
        [DataMember(Name="Slab3")]
        public double Slab3 { get; set; }
        
        
    }
}
