/*
 * Shaya Services
 * 
 */

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// Marka
    /// </summary>
    [DataContract]
    public class Brand
    { 
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataMember(Name="id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name="Name")]
        public string Name { get; set; }

        /// <summary>
        /// Marka kısaltması
        /// </summary>
        [DataMember(Name = "ShortName")]
        public string ShortName { get; set; }
        
        /// <summary>
        /// Birim (L BRAND, SHAYA, OTHER)
        /// </summary>
        [DataMember(Name = "Birim")]
        [StringLength(255)]
        public string Birim { get; set; }
        
        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{this.Name} [{this.Id}]";
        }
    }
}
