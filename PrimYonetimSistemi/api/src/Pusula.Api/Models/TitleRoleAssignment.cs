using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class TitleRoleAssignment
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Gets or Sets RoleId
        /// </summary>
        [DataMember(Name = "Role")]
        public virtual Role Role { get; set; }
        
        /// <summary>
        /// Ünvan
        /// </summary>
        /// <value>Position</value>
        [DataMember(Name="Position")]
        public Position Position { get; set; }
    }
}