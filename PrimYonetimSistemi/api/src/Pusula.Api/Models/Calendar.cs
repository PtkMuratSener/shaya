using System.Runtime.Serialization;
using Pusula.Api.Models.Entities;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// Al Shaya calendar all days of year
    /// </summary>
    [DataContract]
    public class Calendar : BaseEntity
    { 
        /// <summary>
        /// Gets or Sets Year
        /// </summary>
        [DataMember(Name="year")]
        public int Year { get; set; }

        /// <summary>
        /// Gets or Sets Date
        /// </summary>
        [DataMember(Name="date")]
        public int Date { get; set; }

        /// <summary>
        /// Gets or Sets Week
        /// </summary>
        [DataMember(Name="week")]
        public int Week { get; set; }

        /// <summary>
        /// Gets or Sets Month
        /// </summary>
        [DataMember(Name="month")]
        public int Month { get; set; }

        /// <summary>
        /// Gets or Sets Quarter
        /// </summary>
        [DataMember(Name="quarter")]
        public int Quarter { get; set; }

        /// <summary>
        /// Gets or Sets Season
        /// </summary>
        [DataMember(Name="season")]
        public int Season { get; set; }
    }
}