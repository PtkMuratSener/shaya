using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class RolePermission
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or Sets RoleId
        /// </summary>
        [DataMember(Name = "Role")]
        public Role Role { get; set; }


        /// <summary>
        /// Çalisma Alani
        /// </summary>
        [DataMember(Name = "WorkingArea")]
        public WorkingArea WorkingArea { get; set; }

        /// <summary>
        /// Magaza
        /// </summary>
        [DataMember(Name = "StoreType")]
        public StoreType StoreType { get; set; }

        /// <summary>
        /// Ekran
        /// </summary>
        [DataMember(Name = "Screen")]
        public Screen Screen { get; set; }

        /// <summary>
        /// Yetki
        /// </summary>
        [DataMember(Name = "AuthorizationType")]
        public AuthorizationType AuthorizationType { get; set; }

        /// <summary>
        /// Eklenme Tarihi
        /// </summary>
        [DataMember(Name = "CreatedOn")]
        public DateTime CreatedOn { get; set; }
    }
}
