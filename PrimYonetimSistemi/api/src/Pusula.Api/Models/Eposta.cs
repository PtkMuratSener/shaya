using Postal;

namespace Pusula.Api.Models
{
    /// <summary>
    /// EPosta
    /// </summary>
    public class Eposta : Email
    {
        /// <summary>
        /// Alıcı adresi
        /// </summary>
        public string To { get; set; }
        
        /// <summary>
        /// Alıcı isim
        /// </summary>
        public string ToName { get; set; }
        
        /// <summary>
        /// İçerik
        /// </summary>
        public string Detail { get; set; }        
    }
}