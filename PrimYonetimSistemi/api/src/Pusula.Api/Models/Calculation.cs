﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Calculation
    {

        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")] 
        public int? Id { get; set; }

        /// <summary>
        /// Filtre
        /// </summary>
        [DataMember(Name = "Filter")]
        [StringLength(128)]
        public string Filter { get; set; }

        /// <summary>
        /// Filtre Marka (hepsi=0)
        /// </summary>
        [DataMember(Name = "Fbrand")]
        public int Fbrand { get; set; }
        
        /// <summary>
        /// Filtre Mağaza (hepsi=0)
        /// </summary>
        [DataMember(Name = "Fstore")]
        public int Fstore { get; set; }

        /// <summary>
        /// Filtre Pozisyon (hepsi=0)
        /// </summary>
        [DataMember(Name = "Fposition")]
        public int Fposition { get; set; }

        /// <summary>
        /// Filtre personel (hepsi=0)
        /// </summary>
        [DataMember(Name = "Femployee")]
        public int Femployee { get; set; }

        /// <summary>
        /// Filtre Yıl
        /// </summary>
        [DataMember(Name = "Fyear")]
        public int Fyear { get; set; }

        /// <summary>
        /// Filtre ay (hepsi=0)
        /// </summary>
        [DataMember(Name = "Fmonth")]
        public int Fmonth { get; set; }

        /// <summary>
        /// Etiket
        /// </summary>
        [DataMember(Name = "Title")]
        [StringLength(128)]
        public string Title { get; set; }

        /// <summary>
        /// Total entries
        /// </summary>
        [DataMember(Name = "Total")]
        public int Total { get; set; }
        
        /// <summary>
        /// Processed entries
        /// </summary>
        [DataMember(Name = "Processed")]
        public int Processed { get; set; }

        /// <summary>
        /// Aktif mi?
        /// </summary>
        [DataMember(Name = "Active")]
        public bool Active { get; set; }

        /// <summary>
        /// Son çalışma zamanı.
        /// Bu zaman 55sn den önce ise tekrar çalışacak.
        /// </summary>
        [DataMember(Name = "LastRun")]
        public long LastRun { get; set; }
        
        /// <summary>
        /// Related calculation Id
        /// </summary>
        [DataMember(Name = "Related")]
        public int Related { get; set; }
        
        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{this.Filter} [{this.Id}]";
        }
    }
}
