using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Marka bazında satış hedef oranı
    /// </summary>
    [DataContract]
    public class BrandSaleSplit
    {
        
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [Key]
        [DataMember(Name="Id")]
        public int? Id { get; set; }
        
        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="Brand")]
        public Brand Brand { get; set; }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        [DataMember(Name="Position")]
        public Position Position { get; set; }
        
        /// <summary>
        /// Taban oran
        /// </summary>
        [DataMember(Name="BaseRate")]
        public decimal BaseRate { get; set; }
        
        /// <summary>
        /// Revizyon numarası
        /// </summary>
        [DataMember(Name="Revision")]
        public int Revision { get; set; }
    }
}