/*
 * CR:PY-99 
 */

using System;
using System.ComponentModel.DataAnnotations;
using System.Net.NetworkInformation;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// Mağaza hedefleri (WinRetail Bazlı)
    /// </summary>
    [DataContract]
    public class StoreTargetRetail
    { 
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [Required]
        [DataMember(Name="Id")]
        public int Id { get; set; }

        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name="Store")]
        public Store Store { get; set; }

        /// <summary>
        /// Veri Yılı
        /// </summary>
        [Required]
        [DataMember(Name="Year")]
        public int Year { get; set; }

        /// <summary>
        /// 1. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A01")]
        public int A01 { get; set; }

        /// <summary>
        /// 1. Periyot hedef
        /// </summary>
        [DataMember(Name="T01")]
        public int T01 { get; set; }

        /// <summary>
        /// 2. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A02")]
        public int A02 { get; set; }

        /// <summary>
        /// 2. Periyot hedef
        /// </summary>
        [DataMember(Name="T02")]
        public int T02 { get; set; }

        /// <summary>
        /// 3. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A03")]
        public int A03 { get; set; }

        /// <summary>
        /// 3. Periyot hedef
        /// </summary>
        [DataMember(Name="T03")]
        public int T03 { get; set; }

        /// <summary>
        /// 4. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A04")]
        public int A04 { get; set; }

        /// <summary>
        /// 4. Periyot hedef
        /// </summary>
        [DataMember(Name="T04")]
        public int T04 { get; set; }

        /// <summary>
        /// 5. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A05")]
        public int A05 { get; set; }

        /// <summary>
        /// 5. Periyot hedef
        /// </summary>
        [DataMember(Name="T05")]
        public int T05 { get; set; }

        /// <summary>
        /// 6. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A06")]
        public int A06 { get; set; }

        /// <summary>
        /// 6. Periyot hedef
        /// </summary>
        [DataMember(Name="T06")]
        public int T06 { get; set; }

        /// <summary>
        /// 7. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A07")]
        public int A07 { get; set; }

        /// <summary>
        /// 7. Periyot hedef
        /// </summary>
        [DataMember(Name="T07")]
        public int T07 { get; set; }

        /// <summary>
        /// 8. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A08")]
        public int A08 { get; set; }

        /// <summary>
        /// 8. Periyot hedef
        /// </summary>
        [DataMember(Name="T08")]
        public int T08 { get; set; }

        /// <summary>
        /// 9. Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A09")]
        public int A09 { get; set; }

        /// <summary>
        /// 9. Periyot hedef
        /// </summary>
        [DataMember(Name="T09")]
        public int T09 { get; set; }

        /// <summary>
        /// 10 Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A10")]
        public int A10 { get; set; }

        /// <summary>
        /// 10 Periyot hedef
        /// </summary>
        [DataMember(Name="T10")]
        public int T10 { get; set; }

        /// <summary>
        /// 11 Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A11")]
        public int A11 { get; set; }

        /// <summary>
        /// 11 Periyot hedef
        /// </summary>
        [DataMember(Name="T11")]
        public int T11 { get; set; }

        /// <summary>
        /// 12 Periyot gerçekleşen
        /// </summary>
        [DataMember(Name="A12")]
        public int A12 { get; set; }

        /// <summary>
        /// 12 Periyot hedef
        /// </summary>
        [DataMember(Name="T12")]
        public int T12 { get; set; }

        /// <summary>
        /// Yıllık gerçekleşen
        /// </summary>
        [DataMember(Name="A13")]
        public int A13 { get; set; }

        /// <summary>
        /// Yılllık hedef
        /// </summary>
        [DataMember(Name="T13")]
        public int T13 { get; set; }

        /// <summary>
        /// İadeler
        /// </summary>
        [DataMember(Name="Iadeler")]
        public string Iadeler { get; set; }
        
        /// <summary>
        /// Mağaza gerçekleşeni döndürür 
        /// </summary>
        /// <param name="periyot"></param>
        /// <returns>int</returns>
        public int GetActual(int periyot)
        {
            try
            {
                var propertyName = $"A{periyot:D2}";
                return GetType().GetProperty(propertyName).GetValue(this, null) is int
                    ? (int) GetType().GetProperty(propertyName).GetValue(this, null)
                    : 0;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Mağaza gerçekleşeni ayarlar
        /// </summary>
        /// <param name="periyot"></param>
        /// <param name="val"></param>
        public void SetActual(int periyot, int val)
        {
            var propertyName = $"A{periyot:D2}";
            GetType().GetProperty(propertyName).SetValue(this, val);
        }

        /// <summary>
        /// Mağaza hedefini döndürür 
        /// </summary>
        /// <param name="periyot"></param>
        /// <returns>int</returns>
        public int GetTarget(int periyot)
        {
            try
            {
                var propertyName = $"T{periyot:D2}";
                return GetType().GetProperty(propertyName).GetValue(this, null) is int 
                    ? (int) GetType().GetProperty(propertyName).GetValue(this) 
                    : 0;
            }
            catch { return 0; }
        }
        
        /// <summary>
        /// Mağaza hedefi ayarlar
        /// </summary>
        /// <param name="periyot"></param>
        /// <param name="val"></param>
        public void SetTarget(int periyot, int val)
        {
            var propertyName = $"T{periyot:D2}";
            GetType().GetProperty(propertyName).SetValue(this, val);
        }
        
        /// <summary>
        /// Priyota ait iadeyi döndürür
        /// </summary>
        /// <param name="periyot"></param>
        /// <returns></returns>
        public int GetIade(int periyot)
        {
            try
            {
                return int.Parse(Iadeler.Split(",")[periyot]);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Periyota ait iadeyi ayarlar
        /// </summary>
        /// <param name="periyot"></param>
        /// <param name="val"></param>
        public void SetIade(int periyot, int val)
        {
            var iadeArray = new string[13];
            if (Iadeler != null)
            {
                if (Iadeler.Length > 0)
                {
                    iadeArray = Iadeler.Split(",");
                }
            }

            iadeArray.SetValue(val.ToString(), periyot);
            Iadeler = String.Join(",", iadeArray);
        }
    }
}
