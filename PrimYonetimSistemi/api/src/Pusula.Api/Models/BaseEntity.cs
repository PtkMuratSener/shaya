﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models.Entities
{
    /// <summary>
    /// Veritabanı nesneleri için temel
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name="id")]
        public int? Id { get; set; }
        
    }
}