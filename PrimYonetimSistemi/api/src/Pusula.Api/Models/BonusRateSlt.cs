using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// SLT Pozisyon Prim Oranı
    /// </summary>
    [DataContract]
    public class BonusRateSlt
    { 
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [DataMember(Name="Id")]
        public int Id { get; set; }

        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="Brand")]
        public Brand Brand { get; set; }
        
        /// <summary>
        /// Pozisyon  
        /// </summary>
        [DataMember(Name="Position")]
        public Position Position { get; set; }
        
        /// <summary>
        /// Prim oranı aylık
        /// </summary>
        [DataMember(Name="Rate")]
        public double Rate { get; set; }

        /// <summary>
        /// Prim oranı yıllık - karlılık
        /// </summary>
        [DataMember(Name="RateKarlilik")]
        public double RateKarlilik { get; set; }
        
        /// <summary>
        /// Prim oranı yıllık - telafi
        /// </summary>
        [DataMember(Name="RateTelafi")]
        public double RateTelafi { get; set; }
        
        /// <summary>
        /// Prim oranı yıllık - over achievement
        /// </summary>
        [DataMember(Name="RateOverAchievement")]
        public double RateOverAchievement { get; set; }
        
        /// <summary>
        /// Prim oranı yıllık - shrinkage
        /// </summary>
        [DataMember(Name="RateShrinkage")]
        public double RateShrinkage { get; set; }
        
        /// <summary>
        /// Yıllık prim hesaplanacak mı?
        /// </summary>
        [DataMember(Name="HasYearlyBonus")]
        public bool HasYearlyBonus { get; set; }
    }
}
