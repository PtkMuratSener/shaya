﻿using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Personel bonus hesapları
    /// </summary>
    [DataContract]
    public class EmployeeBonus
    {
     
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [DataMember(Name="Id")]
        public int? Id { get; set; }
        
        /// <summary>
        /// Kaynak hesaplama
        /// </summary>
        [DataMember(Name="Calculation")]
        public Calculation Calculation { get; set; }
        
        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="Brand")]
        public Brand Brand { get; set; }
        
        /// <summary>
        /// Mağaza
        /// </summary>
        [DataMember(Name="Store")]
        public Store Store { get; set; }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        [DataMember(Name="Position")]
        public Position Position { get; set; }
        
        /// <summary>
        /// Personel
        /// </summary>
        [DataMember(Name="Employee")]
        public Employee Employee { get; set; }
        
        /// <summary>
        /// Draft mı Final mi?
        /// </summary>
        [DataMember(Name="Final")]
        public bool Final { get; set; }
        
        /// <summary>
        /// Hesaplandı mı?
        /// </summary>
        [DataMember(Name="Calculated")]
        public bool Calculated { get; set; }
        
        /// <summary>
        /// Hesaplanmış bonus miktarı
        /// </summary>
        [DataMember(Name="Amount")]
        public double Amount { get; set; }
        
        /// <summary>
        /// Hesaplamanın yılı
        /// </summary>
        [DataMember(Name="Year")]
        public int Year { get; set; }
        
        /// <summary>
        /// Hesaplama Ayı
        /// </summary>
        [DataMember(Name="Month")]
        public int Month { get; set; }
        
        /// <summary>
        /// Çalışanın ay için pozisyonundaki satış hedefi
        /// </summary>
        [DataMember(Name="Pmst")]
        public double Pmst { get; set; }
        
        /// <summary>
        /// İşlem notları
        /// </summary>
        [DataMember(Name="Notes")]
        public string Notes { get; set; }

        /// <summary>
        /// Mağazada başlangıç günü (1)
        /// </summary>
        [DataMember(Name="StartDay")]
        public int StartDay { get; set; }
        
        /// <summary>
        /// Mağazada bitiş günü (30)
        /// </summary>
        [DataMember(Name="EndDay")]
        public int EndDay { get; set; }
        
        /// <summary>
        /// Hesaplamada kullanılan normalize satış
        /// </summary>
        [DataMember(Name = "Actual")]
        public int Actual { get; set; }
        
        /// <summary>
        /// Bulunan Prim Oranı
        /// </summary>
        [DataMember(Name="Rate")]
        public double Rate { get; set; }
    }
}