using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Store
    { 
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataMember(Name="id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Brand
        /// </summary>
        [DataMember(Name="brand")]
        public Brand Brand { get; set; }

        /// <summary>
        /// Gets or Sets Aktif
        /// </summary>
        [DataMember(Name="aktif")]
        public bool Aktif { get; set; }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{this.Name} [{this.Id}]";
        }
    }
}
