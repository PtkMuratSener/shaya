/*
 * Shaya Services
 */

using System;
using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Pusula.Api.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class EmployeeWorkInfo : IEquatable<EmployeeWorkInfo>
    { 
        /// <summary>
        /// Gets or Sets SalePerMonth
        /// </summary>
        [DataMember(Name="SalePerMonth")]
        public int? SalePerMonth { get; set; }

        /// <summary>
        /// Gets or Sets Store
        /// </summary>
        [DataMember(Name="Store")]
        public int? Store { get; set; }

        /// <summary>
        /// Gets or Sets WorkerWorkDays
        /// </summary>
        [DataMember(Name="WorkerWorkDays")]
        public int? WorkerWorkDays { get; set; }

        /// <summary>
        /// Gets or Sets StoreWorkDays
        /// </summary>
        [DataMember(Name="StoreWorkDays")]
        public int? StoreWorkDays { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class EmployeeWorkInfo {\n");
            sb.Append("  SalePerMonth: ").Append(SalePerMonth).Append("\n");
            sb.Append("  Store: ").Append(Store).Append("\n");
            sb.Append("  WorkerWorkDays: ").Append(WorkerWorkDays).Append("\n");
            sb.Append("  StoreWorkDays: ").Append(StoreWorkDays).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((EmployeeWorkInfo)obj);
        }

        /// <summary>
        /// Returns true if EmployeeWorkInfo instances are equal
        /// </summary>
        /// <param name="other">Instance of EmployeeWorkInfo to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(EmployeeWorkInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    SalePerMonth == other.SalePerMonth ||
                    SalePerMonth != null &&
                    SalePerMonth.Equals(other.SalePerMonth)
                ) && 
                (
                    Store == other.Store ||
                    Store != null &&
                    Store.Equals(other.Store)
                ) && 
                (
                    WorkerWorkDays == other.WorkerWorkDays ||
                    WorkerWorkDays != null &&
                    WorkerWorkDays.Equals(other.WorkerWorkDays)
                ) && 
                (
                    StoreWorkDays == other.StoreWorkDays ||
                    StoreWorkDays != null &&
                    StoreWorkDays.Equals(other.StoreWorkDays)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (SalePerMonth != null)
                    hashCode = hashCode * 59 + SalePerMonth.GetHashCode();
                    if (Store != null)
                    hashCode = hashCode * 59 + Store.GetHashCode();
                    if (WorkerWorkDays != null)
                    hashCode = hashCode * 59 + WorkerWorkDays.GetHashCode();
                    if (StoreWorkDays != null)
                    hashCode = hashCode * 59 + StoreWorkDays.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(EmployeeWorkInfo left, EmployeeWorkInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(EmployeeWorkInfo left, EmployeeWorkInfo right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
