﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Mağaza bazlı pozisyon verisi (Aylık Hedefler)
    /// </summary>
    [DataContract]
    public class StorePosition
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name="Id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Pozisyonun ait olduğu mağaza
        /// </summary>
        [DataMember(Name="Store")]
        public Store Store { get; set; }
        
        /// <summary>
        /// Yıl
        /// </summary>
        [DataMember(Name="Year")]
        public int Year { get; set; }
        
        /// <summary>
        /// Ay
        /// </summary>
        [DataMember(Name="Month")]
        public int Month { get; set; }
        
        /// <summary>
        /// Pozisyon
        /// </summary>
        [DataMember(Name="Position")]
        public Position Position{ get; set; }

        /// <summary>
        /// Pozisyondaki personel adedi
        /// </summary>
        [DataMember(Name="NumStaff")]
        public int NumStaff  { get; set; }
        
        /// <summary>
        /// Satış dağılımı oranı (#/100)
        /// </summary>
        [DataMember(Name="SaleSplit")]
        public decimal SaleSplit { get; set; }
        
        /// <summary>
        /// Position Monthly Sale Target,
        /// Aylık satış hedefi
        /// </summary>
        [DataMember(Name="Pmst")]
        public int Pmst { get; set; }
        
        /// <summary>
        /// Sale Team Achivement %100 için
        /// </summary>
        [DataMember(Name="Sta100")]
        public int? Sta100 { get; set; }

        /// <summary>
        /// Sale Team Achivement %105 için
        /// </summary>
        [DataMember(Name="Sta105")]
        public int? Sta105 { get; set; }

        /// <summary>
        /// Sale Team Achivement %110 için
        /// </summary>
        [DataMember(Name="Sta110")]
        public int? Sta110 { get; set; }
        
        /// <summary>
        /// Hesaplamada kullanılacak son hali mi?  
        /// </summary>
        [DataMember(Name="Final")]
        public bool Final { get; set; }
        
        /// <summary>
        /// Hesaplamada kullanılacak formül tipi
        /// LBRAND (Faz 1.de tek)
        /// </summary>
        [DataMember(Name="FormulaType")]
        [MaxLength(60)]
        public string FormulType { get; set; }

        /// <summary>
        /// Reziyon numarası
        /// </summary>
        [DataMember(Name = "Revision")]
        [Range(0, 99)]
        public int Revision { get; set; }
    }
}