using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// Objelerin revizyon kaydı
    /// </summary>
    [DataContract]
    public class Revision
    {
        /// <summary>
        /// Otomatik Id
        /// </summary>
        [Key]
        [DataMember(Name="Id")]
        public int Id { get; set; }

        /// <summary>
        /// Revizyon numarası
        /// </summary>
        [DataMember(Name="RevId")]
        public int RevId { get; set; }

        /// <summary>
        /// Revizyon yapılan
        /// </summary>
        [DataMember(Name="Owner")]
        [StringLength(255)]
        public string Owner { get; set; }
        
        /// <summary>
        /// Revizyon yapan
        /// </summary>
        [DataMember(Name="Sicil")]
        public int Sicil { get; set; }
        
        /// <summary>
        /// REvizyon zamanı
        /// </summary>
        [DataMember(Name="Zaman")]
        public DateTime Zaman { get; set; }
    }
}