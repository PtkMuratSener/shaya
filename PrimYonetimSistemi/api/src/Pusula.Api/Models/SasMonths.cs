﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SasMonths

    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [DataMember(Name="Id")]
        public int? Id { get; set; }

        /// <summary>
        /// Marka
        /// </summary>
        [DataMember(Name="Brand")]
        public Brand Brand { get; set; }
        
        /// <summary>
        /// SAS Ocak
        /// </summary>
        [DataMember(Name="M01")]
        public bool M01 { get; set; }
        
        /// <summary>
        /// SAS Şubat
        /// </summary>
        [DataMember(Name="M02")]
        public bool M02 { get; set; }
        
        /// <summary>
        /// SAS Mart
        /// </summary>
        [DataMember(Name="M03")]
        public bool M03 { get; set; }
        
        /// <summary>
        /// SAS Nisan
        /// </summary>
        [DataMember(Name="M04")]
        public bool M04 { get; set; }
        
        /// <summary>
        /// SAS Mayıs
        /// </summary>
        [DataMember(Name="M05")]
        public bool M05 { get; set; }
        
        /// <summary>
        /// SAS Harizan
        /// </summary>
        [DataMember(Name="M06")]
        public bool M06 { get; set; }
        
        /// <summary>
        /// SAS Temmuz
        /// </summary>
        [DataMember(Name="M07")]
        public bool M07 { get; set; }
        
        /// <summary>
        /// SAS Ağustos
        /// </summary>
        [DataMember(Name="M08")]
        public bool M08 { get; set; }
        
        /// <summary>
        /// SAS Eylül
        /// </summary>
        [DataMember(Name="M09")]
        public bool M09 { get; set; }
        
        /// <summary>
        /// SAS Ekim
        /// </summary>
        [DataMember(Name="M10")]
        public bool M10 { get; set; }
        
        /// <summary>
        /// SAS Kasım
        /// </summary>
        [DataMember(Name="M11")]
        public bool M11 { get; set; }
        
        /// <summary>
        /// SAS Aralık
        /// </summary>
        [DataMember(Name="M12")]
        public bool M12 { get; set; }
        
        /// <summary>
        /// Periyot indirim periyotu mu?
        /// </summary>
        /// <param name="periyot"></param>
        /// <returns></returns>
        public bool IsSasMonth(int periyot)
        {
            try
            {
                var propertyName = $"M{periyot:D2}";
                return GetType().GetProperty(propertyName).GetValue(this, null) is bool && (bool) GetType().GetProperty(propertyName).GetValue(this);
            }
            catch { return false; }
        }
    }
}