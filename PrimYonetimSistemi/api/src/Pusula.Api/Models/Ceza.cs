﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Pusula.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Ceza
    {
        /// <summary>
        /// Otomatik Id özelliği
        /// </summary>
        [Key]
        [DataMember(Name = "Id")]
        public int? Id { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "Code")]
        [StringLength(2)]
        public string Code { get; set; }

        /// <summary>
        /// Marka kısaltması
        /// </summary>
        [DataMember(Name = "Title")]
        [StringLength(128)]
        public string Title { get; set; }
        
        /// <summary>
        /// Etki süresi, ay
        /// </summary>
        [DataMember(Name = "Effective")]
        [Range(1, 24)]
        public int Effective { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{this.Title} [{this.Code}]"; 
        }
    }
}