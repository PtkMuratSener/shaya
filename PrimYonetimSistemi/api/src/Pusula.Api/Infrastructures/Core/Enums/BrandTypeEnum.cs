namespace Pusula.Api.Infrastructures.Core
{
    /// <summary>
    /// Marka grubu tipleri
    /// </summary>
    public enum BrandTypeEnum
    {
        /// <summary>
        /// L Brands
        /// </summary>
        Lbrands = 1,
        
        /// <summary>
        /// Shaya
        /// </summary>
        Shaya = 2
    }
}