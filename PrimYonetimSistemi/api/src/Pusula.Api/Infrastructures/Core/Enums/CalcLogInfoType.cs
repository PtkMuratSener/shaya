namespace Pusula.Api.Infrastructures.Core
{
    /// <summary>
    /// Hesaplama logu tip sabitleri
    /// </summary>
    public static class CalcLogInfoType
    {
        /// <summary>
        /// Bilgi tipi
        /// </summary>
        public const string info = "Bilgi";
        
        /// <summary>
        /// Bilgi tipi
        /// </summary>
        public const string error = "Hata";
        
        /// <summary>
        /// Kontrol başarılı tipi
        /// </summary>
        public const string check_pass = "Kontrol-Geçti";
        
        /// <summary>
        /// Kontrol başarısız tipi
        /// </summary>
        public const string check_fail = "Kontrol-Kaldı";
        
        /// <summary>
        /// İşlem tamamlandı
        /// </summary>
        public const string finished = "Tamamlandı";
        
    }
}