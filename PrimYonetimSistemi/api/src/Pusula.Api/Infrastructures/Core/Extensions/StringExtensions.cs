﻿namespace Pusula.Api.Infrastructures.Core.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class StringExtensions
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToCultureIndependent(this string source)
        {
            source = source.Replace("ı", "i");
            source = source.Replace("ş", "s");
            source = source.Replace("ç", "c");
            source = source.Replace("ğ", "g");
            source = source.Replace("ö", "o");
            source = source.Replace("ü", "u");
            return source;
        }
    }
}
