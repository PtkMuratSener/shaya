﻿using System;


namespace Pusula.Api.Infrastructures.Core.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static int ToInt(this Enum e)
        {
            if (e == null)
            {
                throw new ArgumentNullException(nameof(e));
            }

            int value = Convert.ToInt32(e);
            return value;
        }
    }
}
