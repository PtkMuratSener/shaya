using System.IO;

namespace Pusula.Api.Infrastructures.Core.Extensions
{
    /// <summary>
    /// NPOI için MemoryStream modifiye
    /// </summary>
    public class NPOIMemoryStream: MemoryStream
    {
        /// <summary>
        /// Otomatik kapatma izni
        /// </summary>
        public bool AllowClose { get; set; }

        /// <inheritdoc />
        public NPOIMemoryStream()
        {
            AllowClose = true;
        }

        /// <summary>
        /// Otomatik kapatmayı kontrol için
        /// </summary>
        public override void Close()
        {
            if(AllowClose)
                base.Close();
        }
    }
}