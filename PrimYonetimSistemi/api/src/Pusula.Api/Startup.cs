/*
 * Shaya Services
 */

using System;
using System.Data;
using System.IO;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Pusula.Api.Data.DataContexts;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Pusula.Api.Filters;
using Pusula.Api.Infrastructures.Middlewares;
using Hangfire.SqlServer;

namespace Pusula.Api
{
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup
    {
        private readonly IHostingEnvironment _hostingEnv;

        private IConfiguration Configuration { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        /// <param name="configuration"></param>
        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            _hostingEnv = env;
            Configuration = configuration;
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services
                .AddMvc()
                .AddJsonOptions(opts =>
                {
                    opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    opts.SerializerSettings.Converters.Add(new StringEnumConverter {
                        CamelCaseText = true
                    });
                });

            services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("1.0.0", new Info
                    {
                        Version = "2.1.2",
                        Title = "Shaya Bonus Services",
                        Description = "Shaya Bonus Services",
                        Contact = new Contact()
                        {
#if DEBUG
                           Name = "Debug",
#else
                           Name = "Release",
#endif 
                           Url = "http://www.shaya.com.tr",
                           Email = "info-tr@alshaya.com"
                        },
                        TermsOfService = "Internal use only, no public access allowed."
                    });
                    c.CustomSchemaIds(type => type.FriendlyId(true));
                    c.DescribeAllEnumsAsStrings();
                    c.IncludeXmlComments($"{AppContext.BaseDirectory}{Path.DirectorySeparatorChar}{_hostingEnv.ApplicationName}.xml");

                    c.OperationFilter<GeneratePathParamsValidationFilter>();
                    
                    c.OperationFilter<FileUploadOperation>(); //Register File Upload Operation Filter

                    c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                    {
                        Description = "Authorization header using the Bearer scheme",
                        Name = "Authorization",
                        In = "header"
                    });

                    c.DocumentFilter<SwaggerSecurityRequirementsDocumentFilter>();     
                });
            
            //add localdb context
            services.AddDbContext<PatikaDbContext>(options => 
                options.UseSqlServer(
                    Configuration.GetConnectionString("LocalMssql")
                )
            );
            
            services.AddCors();
            //OracleConfiguration.OracleDataSources.Add("IKSY_IT", "()");
            OracleContext.ConnectionString = Configuration.GetConnectionString("ShayaOracleConnectionString");
            
            services.AddHangfire(config =>
            {
                config.UseSqlServerStorage(Configuration.GetConnectionString("LocalMssql"), 
                    new SqlServerStorageOptions
                    {
                        QueuePollInterval = TimeSpan.FromSeconds(1),
                        PrepareSchemaIfNecessary = true,
                        SchemaName = "hangfire"
                    }
                );
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors(opt => opt.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseMiddleware<AuthenticationMiddleware>();
            app
                .UseMvc()
                .UseDefaultFiles()
                .UseStaticFiles()
                .UseSwagger();
        
                //app.UseDeveloperExceptionPage();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/1.0.0/swagger.json", "Shaya Services");
                    c.DocExpansion("none"); //list
                    c.ShowJsonEditor();
                });
                
                app.UseHangfireDashboard();
                app.UseHangfireServer(new BackgroundJobServerOptions {
                    WorkerCount = 10
                });                
            // app.UseExceptionHandler("/Home/Error");
        }
    }
}
