using System.Runtime.Serialization;

namespace Pusula.Api.Models.Entities
{
    /// <summary>
    /// Sayfalama şablonu
    /// </summary>
    [DataContract]
    public class PaginationBase
    {
        /// <summary>
        /// Sayfa numarası
        /// </summary>
        [DataMember(Name="page")]
        public int? Page { get; set; }
        
        /// <summary>
        /// Sayfa boyutu
        /// </summary>
        [DataMember(Name="size")]
        public int? Size { get; set; }
        
        /// <summary>
        /// Sıralama ölçütü
        /// </summary>
        [DataMember(Name="sort")]
        public string Sort { get; set; }
    }
}