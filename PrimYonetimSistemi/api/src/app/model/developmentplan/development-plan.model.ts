
import { ApiModel } from "../api.model";
import { Personel } from "../personel.model";
import { CareerTerm } from "../career-term.model";
import { ApprovementStatus } from "./approvement-status.model";

export class DevelopmentPlan extends ApiModel {
    public careerTerm: CareerTerm;
    public careerTermId: number = null;
    public personelId: number;
    public personel: Personel = new Personel();
    public approvementStatus: ApprovementStatus;
    public approvementStatusId: number;
    public formOwnerId: string;
    public waitingPersonelId: string;
    public careerExpectation: string;
    public finishDate: any;
    public url = 'developmentPlans';
}