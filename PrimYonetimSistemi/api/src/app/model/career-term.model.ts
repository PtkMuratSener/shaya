import { ApiModel } from "./api.model";
import { CareerTermStatus } from "./types.model";

export class CareerTerm extends ApiModel {
    public id: number;
    public name: number;
    public startDate: any;
    public finishDate: any;
    public careerTermStatus: CareerTermStatus;
    public url = 'careerTerms';
}
