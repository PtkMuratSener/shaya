import { ApiModel } from "../api.model";

export class YCalculation extends ApiModel {
    public brandId: number; // : Marka ,
    public storeId: number; // : Mağaza ,
    public type: string; // (string): Tip = ['overAchievement', 'telafi', 'karlilik', 'shrinkage', 'total'],
    public year: number; // Yıl ,
    public final: boolean; // Durum ,
    public page: number; // Sayfa numarası ,
    public size: number; // Sayfa boyutu ,
    public sort: string; // Sıralama ölçütü
}
