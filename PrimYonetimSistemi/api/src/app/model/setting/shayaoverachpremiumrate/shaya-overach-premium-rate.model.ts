import { ApiModel } from "../../api.model";

export class ShayaOverachPremiumRate extends ApiModel {
    public label: string;
    public rate12Less: number;
    public rate12More: number;
}