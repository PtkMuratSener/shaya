import { ApiModel } from "../../api.model";
import { BrandModel } from "../../brand/brand.model";
import { Position } from "../../position/position.model";

export class TargetRate extends ApiModel {
    public baseRate: number;
    public brand: BrandModel;
    public position: Position;
    constructor() {
        super();
        this.brand = new BrandModel();
        this.position = new Position();
    }
}