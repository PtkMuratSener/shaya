import { ApiModel } from "../../api.model";

export class Formula extends ApiModel {
    public name: string;
    public formulaType: string;
    public content: string
    constructor() {
        super();
    }
}