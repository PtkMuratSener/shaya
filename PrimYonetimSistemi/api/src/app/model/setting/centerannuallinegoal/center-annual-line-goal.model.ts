import { ApiModel } from "../../api.model";

export class CenterAnnualLineGoalModel extends ApiModel {
    public unit: number;
    public unitStr: string;
    public title: number;
    public titleStr: string;
    public ln1: string;
    public weight1: number;
    public ln2: string;
    public weight2: number;
    public ln3: string;
    public weight3: number;
    public ln4: string;
    public ln5: string;
}