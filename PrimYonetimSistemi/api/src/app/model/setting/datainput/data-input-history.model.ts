import { ApiModel } from "../../api.model";

export class DataInputHistory extends ApiModel {
    public fileName: string;
    public owner: string;
    public date: any;
    constructor() {
        super();
    }
}