import { ApiModel } from "./api.model";
export class Warehouse extends ApiModel {
    public id: number;
    public name: string;
    public url = 'warehouses';
}
