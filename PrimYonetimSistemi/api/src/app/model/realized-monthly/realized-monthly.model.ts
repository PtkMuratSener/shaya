import { ApiModel } from "./../api.model";

export class RealizedMonthly extends ApiModel {
    public brand: string;
    public brandId: number;
    public store: string;
    public storeId: number;
    public namesurname: string;
    public position: string;
    public positionId: number;
    public target: number;
    public actual: number;
    public rate: number;
    public year: number;
    public month: number;
    public sicil: number;
    constructor() {
        super();
    }
}