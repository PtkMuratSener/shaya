export class PremiumCalculator {
    public brandId: number;
    public storeId: number;
    public positionId: number;
    public employeeId: number;
    public year: number;
    public final: boolean = false;
    public type: string;
    public brandType: string;
    constructor(type: string, brandType: string, ) {
        this.type = type;
        this.brandType = brandType;
    }
};