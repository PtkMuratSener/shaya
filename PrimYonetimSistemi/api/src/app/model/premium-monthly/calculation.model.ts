import { ApiModel } from "../api.model";

export class Calculation extends ApiModel {
    public filter: string; //Filtre
    public time: any;
    public fbrand: number; // Filtre Marka (hepsi=0) ,
    public fstore: number; // Filtre Mağaza (hepsi=0) ,
    public fposition: number;// Filtre Pozisyon (hepsi=0) ,
    public femployee: number; // Filtre personel (hepsi=0) ,
    public fyear: number; //Filtre Yıl ,
    public fmonth: number; // Filtre ay (hepsi=0) ,
    public title: string; // Etiket ,
    public total: number; // Total entries ,
    public processed: number; // Processed entries ,
    public active: boolean; // Aktif mi? ,
    public lastRun: number; // Son çalışma zamanı. Bu zaman 55sn den önce ise tekrar çalışacak.
}
