import { ApiModel } from "./../api.model";

export class PremiumMontly extends ApiModel {
    public brand: string;
    public store: string;
    public position: string;
    public name: string;
    public surname: string;
    public sicil: number;
    public year: number;
    public month: number;
    public bonus: number; // Prim Tutarı
    public bonusNatural: number;
    public notes: string;  // Notlar (Transfer Bilgisi)
    public type: string;
    public final: boolean;
    public target: number;
    public actual: number;

    //
    public bonusState: string; // Prim Alma Durumu ,
    public bonusRate: number; // Prim Oranı ,
    public monthlyTotal: number; // Aylık Satış Prim Tutarı ,
    public bonusTotalDays: number; // Prim Gün Sayısı ,
    public bonusFreeDays: number; // Kesilecek Rapor/Ücretsiz İzin Gün Sayısı ,
    public bonusPayDays: number; // Prim Ödenecek Gün Sayısı ,
    public discipline: string; // Disiplin Ceza Bilgisi ,


    constructor() {
        super();
    }
}
