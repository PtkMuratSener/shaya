import { ApiModel } from "../api.model";

export class Calclog extends ApiModel {
    public sicil: number;
    public action: string;
    public calcid: number;
    public tip: string;
    public sira: number;
}