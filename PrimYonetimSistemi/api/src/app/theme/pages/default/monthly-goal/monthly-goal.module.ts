import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from './../default.component';
import { FormsModule } from '@angular/forms';
import { DataTableModule, DropdownModule, DialogModule, AutoCompleteModule, GrowlModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MonthlyGoalComponent } from './monthly-goal.component';
import { HttpClientModule } from '@angular/common/http';
import { MonthlyGoalService } from './monthly-goal.service';
import { PremiumRateService } from "../settings/premium-rate/premium-rate.service";
import { OrderModule } from 'ngx-order-pipe';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': MonthlyGoalComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DataTableModule,
        DropdownModule,
        DialogModule,
        AutoCompleteModule,
        GrowlModule,
        NgxSpinnerModule,
        TableModule,
        TranslateModule.forChild(),
        OrderModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        MonthlyGoalComponent
    ],
    providers: [MonthlyGoalService, PremiumRateService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class MonthlyGoalModule {


}