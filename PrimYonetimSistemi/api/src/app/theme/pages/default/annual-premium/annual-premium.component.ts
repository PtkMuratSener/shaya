import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { LazyLoadEvent, Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { AnnualPremiumService } from './annual-premium.service';
import { environment } from '../../../../../environments/environment';

import { SelectInput, Months, Years } from '../settings/data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { PremiumMontly } from "../../../../model/premium-monthly/premium.montly.model";
import { Pageable } from "../../../../model/pageable";
import { Role } from "../../../../model/account/role/role.model";
import { DatePipe } from "@angular/common";
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { Rights } from "../../../../model/rights";

@Component({
    selector: 'premium-monthly',
    templateUrl: './annual-premium.component.html'
})
export class AnnualPremiumComponent implements OnInit {
    spinnerConfig: any = environment.spinner;
    msgs: Message[] = [];
    public rights = Rights;
    constructor(private premiumMonthlyService: AnnualPremiumService,
        private messageService: MessageService,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
    }
}
