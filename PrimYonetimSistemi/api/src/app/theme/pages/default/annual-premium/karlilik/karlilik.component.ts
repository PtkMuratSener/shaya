import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Rights } from "../../../../../model/rights";
import { LazyLoadEvent, Message } from "primeng/api";
import { environment } from "../../../../../../environments/environment";
import { PremiumMontly } from "../../../../../model/premium-monthly/premium.montly.model";
import { Pageable } from "../../../../../model/pageable";
import { SelectInput, Years } from "../../settings/data-input/calendar";
import { AnnualPremiumService } from "../annual-premium.service";
import { MessageService } from "primeng/components/common/messageservice";
import { NgxSpinnerService } from "ngx-spinner";
import { Angular5Csv } from "angular5-csv/Angular5-csv";
import { YCalculation } from "../../../../../model/annual-premium/y-calculation.model";
import { Karlilik } from "../../../../../model/annual-premium/karlilik.model";

@Component({
    selector: 'app-karlilik',
    templateUrl: 'karlilik.component.html',
    styles: []
})
export class KarlilikComponent implements OnInit {

    public rights = Rights;
    msgs: Message[] = [];
    public model: YCalculation = new YCalculation();
    public listKarlilik: Karlilik[];
    cols: any[];
    public pageable: Pageable = new Pageable();
    public totalRecords: number = 0;
    public selectedItems: PremiumMontly[] = [];

    draftOrFinal: SelectInput[] = [
        { label: 'Tümü', value: '0', checked: false },
        { label: 'Taslak', value: "1", checked: false },
        { label: 'Final', value: "2", checked: false },

    ];
    years: SelectInput[];

    brands: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    stores: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];
    employees: SelectInput[] = [{
        label: 'Tümü', value: '0', checked: false
    }];

    constructor(private annualPremiumService: AnnualPremiumService,
        private messageService: MessageService,
        private cdRef: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {
        this.years = Years;

    }

    ngOnInit() {
        let date = new Date();
        let year = date.getFullYear();
        this.model.year = year;

        this.cols = [
            { field: 'brand', header: 'Marka' },
            { field: 'store', header: 'Mağaza' },
            { field: 'name', header: 'Ad' },
            { field: 'surname', header: 'Soyad' },
            { field: 'position', header: 'Pozisyon' },
            { field: 'target', header: 'Hedef' },
            { field: 'actual', header: 'Gerçekleşen ' },
            { field: 'month', header: 'Periyot' },
            { field: 'bonus', header: 'Prim Tutarı' }
        ];

        this.annualPremiumService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Marka listesi alınamadı!' });
        });
        this.annualPremiumService.getPositions().subscribe(result => {
            if (result.total) {
                for (const position of result.data) {
                    this.positions.push({
                        label: position.name,
                        value: position.id,
                        checked: false
                    })
                }
            }
        }, error => {
            //this.messageService.add({severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi'});
        });
        this.cdRef.detectChanges();
    }

    setStores(brandId) {
        this.stores = [{
            label: 'Tümü', value: '0', checked: false
        }];

        if (brandId) {
            this.spinner.show();
            this.annualPremiumService.getStores(brandId).subscribe(result => {

                if (result.total.all) {
                    for (const store of result.data) {
                        this.stores.push({
                            label: store.name,
                            value: store.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağazalar listesi alınamadı!' });
            });
        }
    }
    getEmployees(storeId) {
        this.employees = [{
            label: 'Tümü', value: '0', checked: false
        }];

        if (storeId) {
            this.spinner.show();
            this.annualPremiumService.getStoreEmployees(storeId).subscribe(result => {
                if (result.total.all) {
                    for (const emp of result.data) {
                        this.employees.push({
                            label: emp.name + " " + emp.surname + " (" + emp.sicil + ")",
                            value: emp.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağaza çalışanları listesi alınamadı!' });
            });
        }
    }

    draftToFinal() {
        if (this.model.year < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Yıl seçiniz!' });
            return;
        }

        console.log(this.selectedItems);
        let array = [];
        for (let item of this.selectedItems) {
            if (item.final != true) {
                array.push(item.id);
            }
        }
        if (array.length < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Seçilen kişiler taslak durumunda değildir!' });
            return;
        }
        this.spinner.show();
        this.annualPremiumService.draftToFinalArray(array).subscribe(
            data => {

                this.messageService.add({ severity: 'info', summary: 'Bilgi', detail: data.message });
                this.findAll(null);
                this.spinner.hide();
            },
            response => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: response.message });

            }
        );


    }
    findAll(event: LazyLoadEvent) {
        if (this.model.year < 1) {
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'Yıl seçiniz!' });
            return;
        }

        this.spinner.show();
        this.selectedItems = [];
        this.annualPremiumService.getHesaplamaSonucKarlilik(event, this.pageable, this.model).subscribe(
            data => {
                // debugger;
                this.listKarlilik = data.content;
                this.totalRecords = data.totalElements;
                this.pageable.page = data.number;
                this.spinner.hide();
                this.cdRef.detectChanges();
            },
            response => {
                this.spinner.hide();
                this.listKarlilik = [];
                this.totalRecords = 0;
                this.spinner.hide();
                this.cdRef.detectChanges();
                if (response.code == "CALCULATION_EMPTY_SET") {
                    // this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: response.message });
                }
                else {
                    this.messageService.add({ severity: 'error', summary: 'Hata', detail: response.message });
                }
            }
        );
    }
    exportCSV() {
        let csv = [];
        let self = this;
        var options = {
            fieldSeparator: ';',
            quoteStrings: '"',
            decimalseparator: ',',
            showLabels: true,
            showTitle: false,
            useBom: true,
            noDownload: false,
            headers: [
                "PRİM HESAPLAMASI YAPILAN AY",
                "İŞYERİ ADI",
                "SİCİL NO",
                "AD SOYAD",
                "ÜCRET",
                "ÜNVAN",
                "İŞE GİRİŞ TARİHİ",
                "TRANSFER TARİHİ",
                "YILLIK PRİM ALMA DURUMU",
                "PRİM ORANI",
                "YILLIK KARLILIK PRİM TUTARI",
                "PRİM GÜN SAYISI",
                "KESİLECEK RAPOR/ÜCRETSİZ İZİN GÜN SAYISI",
                "ÖDENECEK PRİM GÜN SAYISI",
                "ÖDENECEK NET PRİM TUTARI",
                "DİSİPLİN CEZASI",
                "AÇIKLAMA"
            ]
        };


        this.listKarlilik.forEach(function(item) {
            csv.push({
                "a": item.a,
                "b": item.b,
                "c": item.c,
                "d": item.d,
                "e": item.e,
                "f": item.f,
                "g": item.g,
                "h": item.h,
                "i": item.i,
                "j": item.j,
                "k": item.k,
                "l": item.l,
                "m": item.m,
                "n": item.n,
                "o": item.o,
                "p": item.p,
                "q": item.q
            });

        });


        let fileName = 'karlilik';


        new Angular5Csv(csv, fileName, options);
    }

    draftToFinalKarlilik() {

    }
}
