import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
declare var jQuery: any;

import { SelectInput } from '../../settings/data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { catchError } from "rxjs/operators";
import { BonusCalculateMonthly } from "../../../../../model/setting/bonuscalculatemonthly/bonus-calculate-monthly.model";
import { Rights } from "../../../../../model/rights";
import { environment } from "../../../../../../environments/environment";
import { AnnualDraftPremiumCalculatorService } from "../../annual-draft-premium-calculator/annual-draft-premium-calculator.service";
import { PremiumCalculatorService } from "./premium-calculator.service";
import { PremiumCalculator } from "../../../../../model/component/premiumcalculator/premium-calculator.model";
import { BonusCalculateMonthlyService } from "../../settings/bonus-calculate-monthly/bonus-calculate-monthly.service";

@Component({
    selector: 'premium-calculator',
    templateUrl: './premium-calculator.component.html'
})

export class PremiumCalculatorComponent implements OnInit, OnDestroy {

    @Input() years: SelectInput[];
    @Input() brands: SelectInput[];
    @Input() stores: SelectInput[];
    @Input() positions: SelectInput[];
    @Input() type: string;
    @Input() brandType: string;
    @Input() screen: number;
    model: PremiumCalculator = new PremiumCalculator(this.type, this.brandType);

    @Output() valueChange = new EventEmitter();

    public rights = Rights;

    public progressValue: number = 0;
    intervalId: number;
    process: number;
    total: number;


    constructor(private messageService: MessageService,
        private premiumCalculatorService: PremiumCalculatorService,
        private bonusCalculateService: BonusCalculateMonthlyService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.model = new PremiumCalculator(this.type, this.brandType)
    }
    ngOnDestroy() {

    }
    submit() {
        this.valueChange.emit(this.model);
    }
    setStores(brandId) {
        this.stores = [{
            label: 'Tümü', value: '0', checked: false
        }];

        this.positions = [{
            label: 'Tümü', value: '0', checked: false
        }];
        if (brandId) {
            this.spinner.show();
            this.premiumCalculatorService.getStores(brandId).subscribe(result => {

                if (result.total.all) {
                    for (const store of result.data) {
                        this.stores.push({
                            label: store.name,
                            value: store.id,
                            checked: false
                        });
                    }
                }

                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Mağazalar listesi alınamadı!' });
            });
        }
    }

    getStores(storeId: number) {
        this.positions = [{
            label: 'Tümü', value: '0', checked: false
        }];
        this.bonusCalculateService.getPositionsByStoreId(storeId).subscribe(result => {
            if (result.total) {
                for (const position of result.data) {
                    this.positions.push({
                        label: position.name,
                        value: position.id,
                        checked: false
                    })
                }
            }
        }, error => {
            //this.messageService.add({severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi'});
        });
    }
}
