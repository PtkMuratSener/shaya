import {
    Component,
    OnInit,
    Inject,
    ViewEncapsulation,
    AfterViewInit,
    ClassProvider,
    Input,
    ChangeDetectorRef
} from '@angular/core';
import { Restangular } from "ngx-restangular";

import { LazyLoadEvent, MenuItem } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';

import { ActivatedRoute } from "@angular/router";
import { ScriptLoaderService } from "../../../../../../_services/script-loader.service";
import { Pageable } from "../../../../../../model/pageable";

import { TranslateService } from "@ngx-translate/core";
import { RoleService } from "../role.service";
import { TitleRoleAssignment } from "../../../../../../model/account/role/titleroleassignment/title-role-assignment.model";
import { Position } from "../../../../../../model/position/position.model";
import { Rights } from "../../../../../../model/rights";

@Component({
    selector: "title-role-assignment",
    templateUrl: "./title-role-assignment.component.html",
    providers: [
        MessageService
    ]
})

export class TitleRoleAssignmentComponent implements OnInit, AfterViewInit {
    public rights = Rights;
    items: MenuItem[];
    @Input() roleId: number;
    cols: any[];
    positions: Position[];
    public selectedItems: TitleRoleAssignment[] = [];
    public pageable: Pageable = new Pageable();
    public totalRecords: number = 0;
    public list: TitleRoleAssignment[] = [];

    message: string;
    title: string;
    public titleRoleAssignment = new TitleRoleAssignment();
    public titleRoleAssignmentNew = new TitleRoleAssignment();

    getId() {
        return this.roleId;
    }

    constructor(
        private roleService: RoleService,
        private cdRef: ChangeDetectorRef,
        public translateService: TranslateService,
        private _script: ScriptLoaderService,
        private messageService: MessageService,
        private activatedRoute: ActivatedRoute) {

    }





    getTitleRoleAssignmentList() {
        this.roleService.getTitleRoleAssignmentList(this.getId()).subscribe(
            data => {
                this.list = data.data;
                this.cdRef.detectChanges();
            },
            err => console.error(err)
        );
    }


    ngOnInit() {
        this.cols = [
            { field: 'position.name', header: 'Ünvan' }
        ];
        this.getTitleRoleAssignmentList();

    }
    ngAfterViewInit() {
        this.activatedRoute.params.subscribe((params: any) => {
            // alert(JSON.stringify(params));
        });
    }


    saveOrCreate(valid) {
        if (valid) {
            this.roleService.titleRoleAssignmentSaveOrUpdate(this.titleRoleAssignmentNew).subscribe((data) => {

                if (this.titleRoleAssignmentNew.id > 0) {
                    this.translateService.get('message.update.title').subscribe(msg => {
                        this.title = msg;
                    });

                    this.translateService.get('message.update.message').subscribe(msg => {
                        this.message = msg;
                    });
                    this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });
                } else {
                    this.titleRoleAssignmentNew = data.data;
                    //this.isNew = false;
                    this.translateService.get('message.save.title').subscribe(msg => {
                        this.title = msg;
                    });

                    this.translateService.get('message.save.message').subscribe(msg => {
                        this.message = msg;
                    });

                    this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });
                }
                this.cdRef.detectChanges();

            }, (response) => {
                this.translateService.get('message.error.title').subscribe(msg => {
                    this.title = msg;
                });

                this.messageService.add({ severity: 'error', summary: this.title, detail: response.data.error_description });
                this.cdRef.detectChanges();
            });
        }
    }
    delete(item) {
        this.roleService.titleRoleAssignmentDelete(item.id).subscribe(() => {
            this.translateService.get('message.delete.title').subscribe(msg => {
                this.title = msg;
            });

            this.translateService.get('message.delete.message').subscribe(msg => {
                this.message = msg;
            });
            this.getTitleRoleAssignmentList();
            this.cdRef.detectChanges();
            this.messageService.add({ severity: 'success', summary: this.title, detail: this.message });

        }, (response) => {
            this.translateService.get('message.error.title').subscribe(msg => {
                this.title = msg;
            });

            this.messageService.add({ severity: 'error', summary: this.title, detail: response.data.error_description });
        });

    }

    remove(item) {
        this.selectedItems = [];
        this.selectedItems.push(item);
    }

    removes() {

        if (this.selectedItems.length == 0) {
            return;
        }

        this.selectedItems.forEach(item => {
            this.delete(item);
        });
    }


    add() {
        this.titleRoleAssignmentNew.role.id = this.getId();
        this.getPositionList();

        //this.fullRoleList = [];
        //this.findAllRole(null);
    }
    getPositionList() {
        this.roleService.getPositionList().subscribe(result => {
            if (result != null) {
                this.positions = result.data;
                this.cdRef.detectChanges();
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'İşlem gerçekleştirilemedi' });
        })
    }

    titleRoleAssignmentAdd() {
        this.titleRoleAssignmentNew.role.id = this.getId();

        if (this.titleRoleAssignmentNew == null || this.titleRoleAssignmentNew.position == null || this.titleRoleAssignmentNew.position.id < 1) {
            this.messageService.add({ severity: 'error', summary: 'Ünvan', detail: "Lütfen geçerli bir ünvan seçiniz!" });
            return;
        }


        this.roleService.titleRoleAssignmentSaveOrUpdate(this.titleRoleAssignmentNew).subscribe((parameters) => {
            this.getTitleRoleAssignmentList();
            this.cdRef.detectChanges();
            this.messageService.add({ severity: 'success', summary: 'İşlem başarılı', detail: "Kayıt eklendi!" });
        }, error => {
            console.log('error', error);
            this.messageService.add({ severity: 'error', summary: 'Hata oluştu', detail: error });
        });
    }

}
