import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../../environments/environment';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { StorageService } from '../../../../../auth/_services';
import { Role } from "../../../../../model/account/role/role.model";
import { RolePermission } from "../../../../../model/account/role/role-permission.model";
import { RegistryRoleAssignment } from "../../../../../model/account/role/registryroleassignment/registry-role-assignment.model";
import { TitleRoleAssignment } from "../../../../../model/account/role/titleroleassignment/title-role-assignment.model";
import { RoleComboRequest } from "../../../../../model/account/role-combo-request.model";

@Injectable()
export class RoleService {
    API_URL = environment.apiURL;
    headers: HttpHeaders = new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Origin': '*'
    });

    constructor(private http: HttpClient, private storage: StorageService) {
        this.headers = this.headers.append('Authorization', this.storage.getToken());
    }

    getBrands() {
        return this.http.get(this.API_URL + '/svc/brands', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    getRoleList() {
        return this.http.get(this.API_URL + '/svc/role', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getRoleById(id) {
        return this.http.get(this.API_URL + '/svc/role/' + id, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getRolePermissionsList(id) {
        return this.http.get(this.API_URL + '/svc/rolePermission/' + id, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getAuthorizationTypesList() {
        return this.http.get(this.API_URL + '/svc/role/authorizationTypes', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getScreensList() {
        return this.http.get(this.API_URL + '/svc/role/screens', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getStoreTypesList() {
        return this.http.get(this.API_URL + '/svc/role/storeTypes', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getWorkingAreasList() {
        return this.http.get(this.API_URL + '/svc/role/workingAreas', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    saveOrUpdate(role: Role) {
        return this.http.post(this.API_URL + '/svc/role', role, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    delete(id: number) {
        return this.http.delete(this.API_URL + '/svc/role/' + id, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    rolePermissionSave(rolePermission: RolePermission) {
        return this.http.post(this.API_URL + '/svc/rolePermission', rolePermission, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    roleComboSave(roleComboRequest: RoleComboRequest) {
        return this.http.post(this.API_URL + '/svc/roleCombo', roleComboRequest, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    rolePermissionDelete(id: number) {
        return this.http.delete(this.API_URL + '/svc/rolePermission/' + id, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    registryRoleAssignmentSaveOrUpdate(registryRoleAssignment: RegistryRoleAssignment) {
        return this.http.post(this.API_URL + '/svc/registryRoleAssignment', registryRoleAssignment, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getRegistryRoleAssignmentList(id) {
        return this.http.get(this.API_URL + '/svc/registryRoleAssignment/' + id, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    registryRoleAssignmentDelete(id: number) {
        return this.http.delete(this.API_URL + '/svc/registryRoleAssignment/' + id, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }


    titleRoleAssignmentSaveOrUpdate(titleRoleAssignment: TitleRoleAssignment) {
        return this.http.post(this.API_URL + '/svc/titleRoleAssignment', titleRoleAssignment, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getTitleRoleAssignmentList(id) {
        return this.http.get(this.API_URL + '/svc/titleRoleAssignment/' + id, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    titleRoleAssignmentDelete(id: number) {
        return this.http.delete(this.API_URL + '/svc/titleRoleAssignment/' + id, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }

    getPositionList() {
        return this.http.get(this.API_URL + '/svc/positionListAll', {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    getRoleCombo(roleId: number) {
        return this.http.get(this.API_URL + '/svc/roleCombo/' + roleId, {
            headers: this.headers
        }).pipe(catchError(this.handleError));
    }
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        if (error.error.code == "UNAUTHORIZED") {
            localStorage.removeItem('currentUser');
            window.location.reload();
        }
        // return an observable with a user-facing error message
        return ErrorObservable.create(error.error);
    };

}