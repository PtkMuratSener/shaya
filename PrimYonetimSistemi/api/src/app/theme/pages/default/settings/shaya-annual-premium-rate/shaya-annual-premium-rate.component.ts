import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';

import { SelectInput } from '../data-input/calendar';
import { NgxSpinnerService } from 'ngx-spinner';
import { ShayaAnnualPremiumRateService } from "./shaya-annual-premium-rate.service";
import { PremiumRate } from "../../../../../model/premium-rate/premium-rate.model";
import { Rights } from "../../../../../model/rights";
import { SltPremiumRate } from "../../../../../model/setting/sltpremiumrate/slt-premium-rate.model";

@Component({
    selector: 'shaya-annual-premium-rate',
    templateUrl: './shaya-annual-premium-rate.component.html'
})
export class ShayaAnnualPremiumRateComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public shayaAnnualPremiumRate = new SltPremiumRate();
    list: SltPremiumRate[] = [];
    brands: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];
    positions: SelectInput[] = [{
        label: 'Seçiniz', value: '0', checked: false
    }];

    constructor(private shayaAnnualPremiumRateService: ShayaAnnualPremiumRateService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {

        this.shayaAnnualPremiumRateService.getBrands().subscribe(result => {
            if (result.total) {
                for (const brand of result.data) {
                    this.brands.push({
                        label: brand.name + " (" + brand.shortName + ")",
                        value: brand.id,
                        checked: false
                    })
                }
            }
        }, error => {
            this.messageService.add({ severity: 'info', summary: 'Hata oluştu', detail: 'Marka listesi alınamadı!' + error.message });
        });

    }

    getShayaAnnualPremiumRateByBrandId() {
        if (this.shayaAnnualPremiumRate.brandId == null || this.shayaAnnualPremiumRate.brandId < 1) {
            // this.messageService.add({severity: 'error', summary: 'Hata', detail: 'Marka bilgisi seçiniz!'});
            return;
        }

        this.spinner.show();
        this.shayaAnnualPremiumRateService.getPremiumRateByBrandId(this.shayaAnnualPremiumRate.brandId).subscribe(result => {
            this.spinner.hide();
            this.list = result.data;
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'info', summary: 'Uyarı', detail: 'SLT Prim Oranı bulunamadı! ' + error.message });
        });

    }

    submit() {
        if (this.shayaAnnualPremiumRate.brandId == null || this.shayaAnnualPremiumRate.brandId < 1) {
            this.messageService.add({ severity: 'info', summary: 'Hata', detail: 'Marka bilgisi seçiniz!' });
            return;
        }

        this.spinner.show();
        this.shayaAnnualPremiumRateService.save(this.list).subscribe(result => {
            this.spinner.hide();
            if (result.success) {
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: result.message });
            } else {
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı' + error.message });
        });

    }
}
