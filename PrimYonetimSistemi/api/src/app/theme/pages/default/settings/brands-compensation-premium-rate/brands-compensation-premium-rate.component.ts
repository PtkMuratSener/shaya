import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';

import { NgxSpinnerService } from 'ngx-spinner';
import { BrandsCompensationPremiumRateService } from "./brands-compensation-premium-rate.service";
import { Rights } from "../../../../../model/rights";
import { ShayaOverachPremiumRate } from "../../../../../model/setting/shayaoverachpremiumrate/shaya-overach-premium-rate.model";
import { CenterAnnualLineGoalModel } from "../../../../../model/setting/centerannuallinegoal/center-annual-line-goal.model";
import { BrandsCompensationPremiumRate } from "../../../../../model/setting/brands-compensation-premium-rate/brands-compensation-premium-rate.model";

@Component({
    selector: 'center-annual-line-goal',
    templateUrl: './brands-compensation-premium-rate.component.html'
})
export class BrandsCompensationPremiumRateComponent implements OnInit {
    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    public brandsCompensationPremiumRate = new BrandsCompensationPremiumRate();
    list: BrandsCompensationPremiumRate[] = [];


    constructor(private brandsCompensationPremiumRateService: BrandsCompensationPremiumRateService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService) {

    }

    ngOnInit() {
        this.brandsCompensationPremiumRateService.getBonusRatesNonSltSlab().subscribe(result => {
            this.list = result.data;
        }, error => {
            this.list = [];
            this.messageService.add({
                severity: 'info', summary: 'Hata oluştu',
                detail: 'Merkez yıllık line hedefler alınamadı!' + error.message
            });
        });

    }


    submit() {
        this.spinner.show();
        this.brandsCompensationPremiumRateService.save(this.list).subscribe(result => {
            this.spinner.hide();
            if (result.success) {
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: result.message });
            } else {
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: result.message });
            }
        }, error => {
            this.spinner.hide();
            this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'İşlem başlatılamadı' + error.message });
        });

    }
}
