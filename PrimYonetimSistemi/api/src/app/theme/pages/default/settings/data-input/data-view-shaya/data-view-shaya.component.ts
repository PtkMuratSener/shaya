import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { Message } from "primeng/api";

import { Rights } from "../../../../../../model/rights";
import { environment } from "../../../../../../../environments/environment";
import { DataInputService } from "../data-input.service";
import { MessageService } from "primeng/components/common/messageservice";
import { NgxSpinnerService } from "ngx-spinner";
import { Calendar } from "../../../../../../model/setting/datainput/calendar.model";
import { StoreTarget } from "../../../../../../model/setting/datainput/store-target.model";
import { Angular5Csv } from "angular5-csv/Angular5-csv";

@Component({
    selector: 'm-dataview-shaya',
    templateUrl: './data-view-shaya.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class DataViewShayaComponent implements OnInit {

    public rights = Rights;
    msgs: Message[] = [];
    spinnerConfig: any = environment.spinner;
    year: number;
    calendars: Calendar[] = [];
    public API_URL = environment.apiURL;

    constructor(private activatedRoute: ActivatedRoute,
        private dataInputService: DataInputService,
        private messageService: MessageService,
        private spinner: NgxSpinnerService, ) {
        this.activatedRoute.params.subscribe((params) => {
            this.year = params.year;
            // console.log('year', this.year);
        })
    }

    ngOnInit() {
        this.getCalendars();
    }
    getCalendars() {
        this.dataInputService.getCalendars(this.year).subscribe((response) => {
            console.log('getCalendars', response);
            this.calendars = response.data;
        })
    }

    exportCSV() {
        let csv = [];
        let self = this;
        var options = {
            fieldSeparator: ';',
            quoteStrings: '"',
            decimalseparator: ',',
            showLabels: true,
            showTitle: false,
            useBom: true,
            noDownload: false,
            headers: [
                "YIL",
                "GÜN",
                "HAFTA",
                "AY",
                "ÇEYREK",
                "SEZON"
            ]
        };

        this.calendars.forEach(function(item) {
            csv.push({
                "year": item.year,
                "date": item.date,
                "week": item.week,
                "month": item.month,
                "quarter": item.quarter,
                "season": item.season
            });
        });


        let fileName = 'AI_Shaya_Takvimi_Verileri';


        new Angular5Csv(csv, fileName, options);
    }
}