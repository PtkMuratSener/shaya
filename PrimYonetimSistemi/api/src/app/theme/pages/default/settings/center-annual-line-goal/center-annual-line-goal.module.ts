import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { FormsModule } from '@angular/forms';
import { DropdownModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';
import { CenterAnnualLineGoalComponent } from "./center-annual-line-goal.component";
import { CenterAnnualLineGoalService } from "./center-annual-line-goal.service";

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': CenterAnnualLineGoalComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DropdownModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild()
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        CenterAnnualLineGoalComponent
    ],
    providers: [CenterAnnualLineGoalService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class CenterAnnualLineGoalModule {


}