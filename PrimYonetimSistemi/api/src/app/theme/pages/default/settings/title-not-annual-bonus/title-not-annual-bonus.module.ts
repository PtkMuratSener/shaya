import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { FormsModule } from '@angular/forms';
import { DropdownModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';
import { TitleNotAnnualBonusComponent } from "./title-not-annual-bonus.component";
import { TitleNotAnnualBonusService } from "./title-not-annual-bonus.service";
import { OnlyNumber } from "../../../../../_directives/only-number.directive";
import { OnlyLetter } from "../../../../../_directives/only-letter.directive";
import { RoleService } from "../../account/role/role.service";

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': TitleNotAnnualBonusComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DropdownModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild()
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        TitleNotAnnualBonusComponent
    ],
    providers: [TitleNotAnnualBonusService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class TitleNotAnnualBonusModule {


}