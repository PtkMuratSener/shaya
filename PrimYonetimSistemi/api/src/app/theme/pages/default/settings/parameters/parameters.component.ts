import { Component, OnInit, OnChanges } from '@angular/core';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../../../../environments/environment';
import { ParametersService } from './parameters.service';
import { Parameter } from './parameter';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Rights } from "../../../../../model/rights";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
    selector: 'm-parameters',
    templateUrl: './parameters.component.html'
})
export class ParametersComponent implements OnInit, OnChanges {
    public rights = Rights;
    spinnerConfig: any = environment.spinner;
    msgs: Message[] = [];
    form: FormGroup;
    items: FormArray;

    defaultParameters: Parameter[] = [
        // new Parameter(0, 'parameter_nonslt_personal_target_rate', 0, "Açıklama")
    ];

    constructor(private fb: FormBuilder,
        private parametersService: ParametersService,
        private spinner: NgxSpinnerService,
        private messageService: MessageService) {
        this.createForm();
    }

    ngOnInit() {
        this.parametersService.getList().subscribe(result => {
            for (const item of result.data) {
                this.addParameter(item);
            }

            for (const parameter of this.defaultParameters) {
                let exists: boolean = false;

                for (const item of result.data) {
                    if (item.title === parameter.title) {
                        exists = true;
                    }
                }

                if (!exists) {
                    this.addParameter(parameter);
                }
            }
        }, error => {
            this.messageService.add({ severity: 'error', summary: 'Hata oluştu', detail: 'Liste alınamadı' });
        });
    }

    ngOnChanges() {
        this.form.setControl('parameters', this.items);
    }

    createForm() {
        this.form = this.fb.group({
            'parameters': this.fb.array([])
        });
    }

    addParameter(parameter: Parameter) {
        this.items = this.form.get('parameters') as FormArray;
        this.items.push(this.fb.group({
            id: parameter.id,
            title: parameter.title,
            value: parameter.value,
            description: parameter.description
        }));
    }

    get parameters() {
        return (this.form.get('parameters') as FormArray).controls;
    }

    submit() {
        const formModel = this.form.value;
        const parameters = formModel.parameters;

        for (const parameter of parameters) {
            this.spinner.show();
            this.parametersService.save(parameter).subscribe(result => {
                this.spinner.hide();
                this.messageService.add({ severity: 'success', summary: 'Başarılı', detail: parameter.description + ': ' + result.message });
            }, error => {
                this.spinner.hide();
                this.messageService.add({ severity: 'error', summary: 'Hata', detail: 'Kayıt yapılamadı!' + error.message });
            });
        }
    }
}
