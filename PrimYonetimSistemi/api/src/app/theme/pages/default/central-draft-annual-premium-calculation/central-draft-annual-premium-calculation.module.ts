import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from './../../../layouts/layout.module';
import { DefaultComponent } from './../default.component';
import { FormsModule } from '@angular/forms';
import { DropdownModule, GrowlModule } from 'primeng/primeng';

import { TranslateModule } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CentralDraftAnnualPremiumCalculationComponent } from './central-draft-annual-premium-calculation.component';
import { HttpClientModule } from '@angular/common/http';
import { CentralDraftAnnualPremiumCalculationService } from './central-draft-annual-premium-calculation.service';
import { ProgressBarModule } from "angular-progress-bar"

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': CentralDraftAnnualPremiumCalculationComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        DropdownModule,
        GrowlModule,
        NgxSpinnerModule,
        TranslateModule.forChild(),
        ProgressBarModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [
        CentralDraftAnnualPremiumCalculationComponent
    ],
    providers: [CentralDraftAnnualPremiumCalculationService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class CentralDraftAnnualPremiumCalculationModule {


}