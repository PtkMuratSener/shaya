import { AuthService } from "../auth/_services/auth.service";

declare var jQuery: any;
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Helpers } from '../helpers';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { DEFAULT_INTERRUPTSOURCES, Idle } from "@ng-idle/core";
import { SessionStorageService } from "ngx-store";
import { Keepalive } from "@ng-idle/keepalive";
import { TranslateService } from "@ngx-translate/core";

declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;
@Component({
    selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
    templateUrl: "./theme.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ThemeComponent implements OnInit {

    idleState = 'Not started.';
    timedOut = false;
    timedOutCountdown = 0;
    lastPing?: Date = null;
    constructor(private _router: Router,
        private translate: TranslateService,
        private sessionStorageService: SessionStorageService,
        private _authService: AuthService,
        private idle: Idle,
        private keepalive: Keepalive,
        private _script: ScriptLoaderService
    ) {
        // sets an idle timeout of 5 seconds, for testing purposes.
        idle.setIdle(1720);
        // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
        idle.setTimeout(20);
        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

        idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
        idle.onTimeout.subscribe(() => {
            this.idleState = 'Timed out!';
            this.timedOut = true;
            //go logout
            this.logout();
        });

        idle.onIdleStart.subscribe(() => {
            jQuery("#session-timeout-dialog").modal("show");
            this.idleState = 'You\'ve gone idle!'
        });

        idle.onTimeoutWarning.subscribe((countdown) => {
            this.idleState = 'You will time out in ' + countdown + ' seconds!'
            this.timedOutCountdown = countdown;
        });

        // sets the ping interval to 15 seconds
        keepalive.interval(15);

        keepalive.onPing.subscribe(() => this.lastPing = new Date());
        this.resetSession();
    }

    resetSession() {
        this.idle.watch();
        this.idleState = 'Started.';
        this.timedOut = false;
    }

    logout() {
        jQuery("#session-timeout-dialog").modal("hide");
        Helpers.setLoading(true);
        // reset login status
        this._authService.logOut();
        this._router.navigate(['/login?returnUrl=%2F']);
        window.location.reload(true);
    }


    ngOnInit() {
        this._script.loadScripts('body', ['assets/vendors/base/vendors.bundle.js', 'assets/demo/default/base/scripts.bundle.js', 'assets/vendors/custom/scannerdetection/jquery.scannerdetection.js'], true)
            .then(result => {
                Helpers.setLoading(false);
                // optional js to be loaded once
                this._script.loadScripts('head', ['assets/vendors/custom/fullcalendar/fullcalendar.bundle.js']);
            });
        this._router.events.subscribe((route) => {
            if (route instanceof NavigationStart) {
                (<any>mLayout).closeMobileAsideMenuOffcanvas();
                (<any>mLayout).closeMobileHorMenuOffcanvas();
                (<any>mApp).scrollTop();
                Helpers.setLoading(true);
                // hide visible popover
                (<any>$('[data-toggle="m-popover"]')).popover('hide');
            }
            if (route instanceof NavigationEnd) {
                // init required js
                (<any>mApp).init();
                (<any>mUtil).init();
                Helpers.setLoading(false);
                // content m-wrapper animation
                let animation = 'm-animate-fade-in-up';
                $('.m-wrapper').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e) {
                    $('.m-wrapper').removeClass(animation);
                }).removeClass(animation).addClass(animation);
                this.resetSession();
            }
        });
    }

}