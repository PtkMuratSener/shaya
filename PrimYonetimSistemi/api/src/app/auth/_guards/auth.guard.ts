import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Principal } from "../_services/principal.service";
import { AuthService, StorageService } from "../_services";
import { Observable } from "rxjs";

@Injectable()
export class AuthGuard implements CanActivate {


    constructor(private _router: Router,
        private principal: Principal,
        private authService: AuthService,
        private storageService: StorageService) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        let dataRolesModel: any = route.data;
        let _self = this;

        return this.principal.identity(false).then(
            data => {
                if (data !== null) {
                    // logged in so return true
                    // check aouthorization here.

                    // this.authService.authorize(dataRolesModel, false, state.url);
                    if (localStorage.getItem('currentUser'))
                        return true;

                }
                // error when verify so redirect to login page with the return url
                _self._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                return false;
            },
            error => {

                // error when verify so redirect to login page with the return url
                _self._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                return false;
            });
    }



    /*
    constructor(private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url and return false
        this.router.navigate(['login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
    */
}