import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.routing';
import { AuthComponent } from './auth.component';
import { AlertComponent } from './_directives';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from './_guards';
import { AlertService } from './_services';
import { StorageService, AuthService } from './_services';
import { TextMaskModule } from 'angular2-text-mask';

import {
    AutoCompleteModule, CalendarModule, CheckboxModule, MessageModule, SelectButtonModule,
    TabViewModule
} from 'primeng/primeng';
import { TranslateModule } from '@ngx-translate/core';
import { MessageService } from 'primeng/components/common/messageservice';
import { HttpModule } from '@angular/http';
import { Principal } from "./_services/principal.service";

@NgModule({
    declarations: [
        AuthComponent,
        AlertComponent,
        LogoutComponent
    ],
    imports: [
        TranslateModule.forChild(),
        CommonModule,
        FormsModule,
        AuthRoutingModule,
        AutoCompleteModule,
        CalendarModule,
        CheckboxModule,
        SelectButtonModule,
        TabViewModule,
        MessageModule,
        TextMaskModule,
        HttpModule
    ],
    providers: [
        AuthGuard,
        AlertService,
        MessageService,
        AuthService, StorageService, Principal
        // api backend simulation
    ],
    exports: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    entryComponents: [AlertComponent]
})

export class AuthModule {
}