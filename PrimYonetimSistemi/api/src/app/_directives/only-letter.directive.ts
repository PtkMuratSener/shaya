
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: '[OnlyLetter]'
})
export class OnlyLetter {

    constructor(private el: ElementRef) { }

    @Input() OnlyLetter: boolean;

    @HostListener('keydown', ['$event']) onKeyDown(event) {
        let e = <KeyboardEvent>event;

        var regex = /^[0-9]*$/;
        if (this.OnlyLetter) {
            if ([46, 8, 9, 27, 13, 110, 190, 32].indexOf(e.keyCode) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }

            // Ensure that it is a number and stop the keypress
            /*  if ((e.shiftKey || (e.keyCode > 32 && (e.keyCode < 48 || e.keyCode > 58) && e.keyCode !== 127))) {
                  e.preventDefault();
              }*/


            if (!regex.test(e.key)) {
                return;
            } else {
                e.preventDefault();
            }

        }
    }
}